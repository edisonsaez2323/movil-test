package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class cl_autentia_realmObjects_Evidence_RealmProxy extends cl.autentia.realmObjects.Evidence_
    implements RealmObjectProxy, cl_autentia_realmObjects_Evidence_RealmProxyInterface {

    static final class Evidence_ColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long idVerificationIndex;
        long jsonDataIndex;
        long auditIndex;
        long typeIndex;
        long fingerprintIndex;
        long attachmentsIndex;
        long fingerprintBmpIndex;
        long createDateIndex;
        long firmaIndex;
        long retratoIndex;

        Evidence_ColumnInfo(OsSchemaInfo schemaInfo) {
            super(10);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Evidence_");
            this.idVerificationIndex = addColumnDetails("idVerification", "idVerification", objectSchemaInfo);
            this.jsonDataIndex = addColumnDetails("jsonData", "jsonData", objectSchemaInfo);
            this.auditIndex = addColumnDetails("audit", "audit", objectSchemaInfo);
            this.typeIndex = addColumnDetails("type", "type", objectSchemaInfo);
            this.fingerprintIndex = addColumnDetails("fingerprint", "fingerprint", objectSchemaInfo);
            this.attachmentsIndex = addColumnDetails("attachments", "attachments", objectSchemaInfo);
            this.fingerprintBmpIndex = addColumnDetails("fingerprintBmp", "fingerprintBmp", objectSchemaInfo);
            this.createDateIndex = addColumnDetails("createDate", "createDate", objectSchemaInfo);
            this.firmaIndex = addColumnDetails("firma", "firma", objectSchemaInfo);
            this.retratoIndex = addColumnDetails("retrato", "retrato", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        Evidence_ColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new Evidence_ColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final Evidence_ColumnInfo src = (Evidence_ColumnInfo) rawSrc;
            final Evidence_ColumnInfo dst = (Evidence_ColumnInfo) rawDst;
            dst.idVerificationIndex = src.idVerificationIndex;
            dst.jsonDataIndex = src.jsonDataIndex;
            dst.auditIndex = src.auditIndex;
            dst.typeIndex = src.typeIndex;
            dst.fingerprintIndex = src.fingerprintIndex;
            dst.attachmentsIndex = src.attachmentsIndex;
            dst.fingerprintBmpIndex = src.fingerprintBmpIndex;
            dst.createDateIndex = src.createDateIndex;
            dst.firmaIndex = src.firmaIndex;
            dst.retratoIndex = src.retratoIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private Evidence_ColumnInfo columnInfo;
    private ProxyState<cl.autentia.realmObjects.Evidence_> proxyState;

    cl_autentia_realmObjects_Evidence_RealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (Evidence_ColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<cl.autentia.realmObjects.Evidence_>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$idVerification() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idVerificationIndex);
    }

    @Override
    public void realmSet$idVerification(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'idVerification' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$jsonData() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.jsonDataIndex);
    }

    @Override
    public void realmSet$jsonData(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.jsonDataIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.jsonDataIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.jsonDataIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.jsonDataIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$audit() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.auditIndex);
    }

    @Override
    public void realmSet$audit(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.auditIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.auditIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.auditIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.auditIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$type() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.typeIndex);
    }

    @Override
    public void realmSet$type(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.typeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.typeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.typeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.typeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public byte[] realmGet$fingerprint() {
        proxyState.getRealm$realm().checkIfValid();
        return (byte[]) proxyState.getRow$realm().getBinaryByteArray(columnInfo.fingerprintIndex);
    }

    @Override
    public void realmSet$fingerprint(byte[] value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fingerprintIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setBinaryByteArray(columnInfo.fingerprintIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fingerprintIndex);
            return;
        }
        proxyState.getRow$realm().setBinaryByteArray(columnInfo.fingerprintIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public byte[] realmGet$attachments() {
        proxyState.getRealm$realm().checkIfValid();
        return (byte[]) proxyState.getRow$realm().getBinaryByteArray(columnInfo.attachmentsIndex);
    }

    @Override
    public void realmSet$attachments(byte[] value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.attachmentsIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setBinaryByteArray(columnInfo.attachmentsIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.attachmentsIndex);
            return;
        }
        proxyState.getRow$realm().setBinaryByteArray(columnInfo.attachmentsIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public byte[] realmGet$fingerprintBmp() {
        proxyState.getRealm$realm().checkIfValid();
        return (byte[]) proxyState.getRow$realm().getBinaryByteArray(columnInfo.fingerprintBmpIndex);
    }

    @Override
    public void realmSet$fingerprintBmp(byte[] value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fingerprintBmpIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setBinaryByteArray(columnInfo.fingerprintBmpIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fingerprintBmpIndex);
            return;
        }
        proxyState.getRow$realm().setBinaryByteArray(columnInfo.fingerprintBmpIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$createDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.createDateIndex);
    }

    @Override
    public void realmSet$createDate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.createDateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.createDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.createDateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.createDateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$firma() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.firmaIndex);
    }

    @Override
    public void realmSet$firma(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.firmaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.firmaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.firmaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.firmaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$retrato() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.retratoIndex);
    }

    @Override
    public void realmSet$retrato(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.retratoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.retratoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.retratoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.retratoIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Evidence_", 10, 0);
        builder.addPersistedProperty("idVerification", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("jsonData", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("audit", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("type", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fingerprint", RealmFieldType.BINARY, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("attachments", RealmFieldType.BINARY, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fingerprintBmp", RealmFieldType.BINARY, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("createDate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("firma", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("retrato", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static Evidence_ColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new Evidence_ColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Evidence_";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Evidence_";
    }

    @SuppressWarnings("cast")
    public static cl.autentia.realmObjects.Evidence_ createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        cl.autentia.realmObjects.Evidence_ obj = null;
        if (update) {
            Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
            Evidence_ColumnInfo columnInfo = (Evidence_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class);
            long pkColumnIndex = columnInfo.idVerificationIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("idVerification")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("idVerification"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class), false, Collections.<String> emptyList());
                    obj = new io.realm.cl_autentia_realmObjects_Evidence_RealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("idVerification")) {
                if (json.isNull("idVerification")) {
                    obj = (io.realm.cl_autentia_realmObjects_Evidence_RealmProxy) realm.createObjectInternal(cl.autentia.realmObjects.Evidence_.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.cl_autentia_realmObjects_Evidence_RealmProxy) realm.createObjectInternal(cl.autentia.realmObjects.Evidence_.class, json.getInt("idVerification"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'idVerification'.");
            }
        }

        final cl_autentia_realmObjects_Evidence_RealmProxyInterface objProxy = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) obj;
        if (json.has("jsonData")) {
            if (json.isNull("jsonData")) {
                objProxy.realmSet$jsonData(null);
            } else {
                objProxy.realmSet$jsonData((String) json.getString("jsonData"));
            }
        }
        if (json.has("audit")) {
            if (json.isNull("audit")) {
                objProxy.realmSet$audit(null);
            } else {
                objProxy.realmSet$audit((String) json.getString("audit"));
            }
        }
        if (json.has("type")) {
            if (json.isNull("type")) {
                objProxy.realmSet$type(null);
            } else {
                objProxy.realmSet$type((String) json.getString("type"));
            }
        }
        if (json.has("fingerprint")) {
            if (json.isNull("fingerprint")) {
                objProxy.realmSet$fingerprint(null);
            } else {
                objProxy.realmSet$fingerprint(JsonUtils.stringToBytes(json.getString("fingerprint")));
            }
        }
        if (json.has("attachments")) {
            if (json.isNull("attachments")) {
                objProxy.realmSet$attachments(null);
            } else {
                objProxy.realmSet$attachments(JsonUtils.stringToBytes(json.getString("attachments")));
            }
        }
        if (json.has("fingerprintBmp")) {
            if (json.isNull("fingerprintBmp")) {
                objProxy.realmSet$fingerprintBmp(null);
            } else {
                objProxy.realmSet$fingerprintBmp(JsonUtils.stringToBytes(json.getString("fingerprintBmp")));
            }
        }
        if (json.has("createDate")) {
            if (json.isNull("createDate")) {
                objProxy.realmSet$createDate(null);
            } else {
                objProxy.realmSet$createDate((String) json.getString("createDate"));
            }
        }
        if (json.has("firma")) {
            if (json.isNull("firma")) {
                objProxy.realmSet$firma(null);
            } else {
                objProxy.realmSet$firma((String) json.getString("firma"));
            }
        }
        if (json.has("retrato")) {
            if (json.isNull("retrato")) {
                objProxy.realmSet$retrato(null);
            } else {
                objProxy.realmSet$retrato((String) json.getString("retrato"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static cl.autentia.realmObjects.Evidence_ createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final cl.autentia.realmObjects.Evidence_ obj = new cl.autentia.realmObjects.Evidence_();
        final cl_autentia_realmObjects_Evidence_RealmProxyInterface objProxy = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("idVerification")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idVerification((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'idVerification' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("jsonData")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$jsonData((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$jsonData(null);
                }
            } else if (name.equals("audit")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$audit((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$audit(null);
                }
            } else if (name.equals("type")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$type((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$type(null);
                }
            } else if (name.equals("fingerprint")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fingerprint(JsonUtils.stringToBytes(reader.nextString()));
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fingerprint(null);
                }
            } else if (name.equals("attachments")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$attachments(JsonUtils.stringToBytes(reader.nextString()));
                } else {
                    reader.skipValue();
                    objProxy.realmSet$attachments(null);
                }
            } else if (name.equals("fingerprintBmp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fingerprintBmp(JsonUtils.stringToBytes(reader.nextString()));
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fingerprintBmp(null);
                }
            } else if (name.equals("createDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$createDate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$createDate(null);
                }
            } else if (name.equals("firma")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$firma((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$firma(null);
                }
            } else if (name.equals("retrato")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$retrato((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$retrato(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'idVerification'.");
        }
        return realm.copyToRealm(obj);
    }

    private static cl_autentia_realmObjects_Evidence_RealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating uexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class), false, Collections.<String>emptyList());
        io.realm.cl_autentia_realmObjects_Evidence_RealmProxy obj = new io.realm.cl_autentia_realmObjects_Evidence_RealmProxy();
        objectContext.clear();
        return obj;
    }

    public static cl.autentia.realmObjects.Evidence_ copyOrUpdate(Realm realm, Evidence_ColumnInfo columnInfo, cl.autentia.realmObjects.Evidence_ object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (cl.autentia.realmObjects.Evidence_) cachedRealmObject;
        }

        cl.autentia.realmObjects.Evidence_ realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
            long pkColumnIndex = columnInfo.idVerificationIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.cl_autentia_realmObjects_Evidence_RealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static cl.autentia.realmObjects.Evidence_ copy(Realm realm, Evidence_ColumnInfo columnInfo, cl.autentia.realmObjects.Evidence_ newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (cl.autentia.realmObjects.Evidence_) cachedRealmObject;
        }

        cl_autentia_realmObjects_Evidence_RealmProxyInterface realmObjectSource = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) newObject;

        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idVerificationIndex, realmObjectSource.realmGet$idVerification());
        builder.addString(columnInfo.jsonDataIndex, realmObjectSource.realmGet$jsonData());
        builder.addString(columnInfo.auditIndex, realmObjectSource.realmGet$audit());
        builder.addString(columnInfo.typeIndex, realmObjectSource.realmGet$type());
        builder.addByteArray(columnInfo.fingerprintIndex, realmObjectSource.realmGet$fingerprint());
        builder.addByteArray(columnInfo.attachmentsIndex, realmObjectSource.realmGet$attachments());
        builder.addByteArray(columnInfo.fingerprintBmpIndex, realmObjectSource.realmGet$fingerprintBmp());
        builder.addString(columnInfo.createDateIndex, realmObjectSource.realmGet$createDate());
        builder.addString(columnInfo.firmaIndex, realmObjectSource.realmGet$firma());
        builder.addString(columnInfo.retratoIndex, realmObjectSource.realmGet$retrato());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.cl_autentia_realmObjects_Evidence_RealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, cl.autentia.realmObjects.Evidence_ object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        long tableNativePtr = table.getNativePtr();
        Evidence_ColumnInfo columnInfo = (Evidence_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class);
        long pkColumnIndex = columnInfo.idVerificationIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$jsonData = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$jsonData();
        if (realmGet$jsonData != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, realmGet$jsonData, false);
        }
        String realmGet$audit = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$audit();
        if (realmGet$audit != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.auditIndex, rowIndex, realmGet$audit, false);
        }
        String realmGet$type = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
        }
        byte[] realmGet$fingerprint = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprint();
        if (realmGet$fingerprint != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, realmGet$fingerprint, false);
        }
        byte[] realmGet$attachments = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$attachments();
        if (realmGet$attachments != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, realmGet$attachments, false);
        }
        byte[] realmGet$fingerprintBmp = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprintBmp();
        if (realmGet$fingerprintBmp != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, realmGet$fingerprintBmp, false);
        }
        String realmGet$createDate = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$createDate();
        if (realmGet$createDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.createDateIndex, rowIndex, realmGet$createDate, false);
        }
        String realmGet$firma = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$firma();
        if (realmGet$firma != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.firmaIndex, rowIndex, realmGet$firma, false);
        }
        String realmGet$retrato = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$retrato();
        if (realmGet$retrato != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.retratoIndex, rowIndex, realmGet$retrato, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        long tableNativePtr = table.getNativePtr();
        Evidence_ColumnInfo columnInfo = (Evidence_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class);
        long pkColumnIndex = columnInfo.idVerificationIndex;
        cl.autentia.realmObjects.Evidence_ object = null;
        while (objects.hasNext()) {
            object = (cl.autentia.realmObjects.Evidence_) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$jsonData = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$jsonData();
            if (realmGet$jsonData != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, realmGet$jsonData, false);
            }
            String realmGet$audit = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$audit();
            if (realmGet$audit != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.auditIndex, rowIndex, realmGet$audit, false);
            }
            String realmGet$type = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$type();
            if (realmGet$type != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
            }
            byte[] realmGet$fingerprint = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprint();
            if (realmGet$fingerprint != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, realmGet$fingerprint, false);
            }
            byte[] realmGet$attachments = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$attachments();
            if (realmGet$attachments != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, realmGet$attachments, false);
            }
            byte[] realmGet$fingerprintBmp = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprintBmp();
            if (realmGet$fingerprintBmp != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, realmGet$fingerprintBmp, false);
            }
            String realmGet$createDate = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$createDate();
            if (realmGet$createDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.createDateIndex, rowIndex, realmGet$createDate, false);
            }
            String realmGet$firma = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$firma();
            if (realmGet$firma != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.firmaIndex, rowIndex, realmGet$firma, false);
            }
            String realmGet$retrato = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$retrato();
            if (realmGet$retrato != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.retratoIndex, rowIndex, realmGet$retrato, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, cl.autentia.realmObjects.Evidence_ object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        long tableNativePtr = table.getNativePtr();
        Evidence_ColumnInfo columnInfo = (Evidence_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class);
        long pkColumnIndex = columnInfo.idVerificationIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
        }
        cache.put(object, rowIndex);
        String realmGet$jsonData = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$jsonData();
        if (realmGet$jsonData != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, realmGet$jsonData, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, false);
        }
        String realmGet$audit = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$audit();
        if (realmGet$audit != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.auditIndex, rowIndex, realmGet$audit, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.auditIndex, rowIndex, false);
        }
        String realmGet$type = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.typeIndex, rowIndex, false);
        }
        byte[] realmGet$fingerprint = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprint();
        if (realmGet$fingerprint != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, realmGet$fingerprint, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, false);
        }
        byte[] realmGet$attachments = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$attachments();
        if (realmGet$attachments != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, realmGet$attachments, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, false);
        }
        byte[] realmGet$fingerprintBmp = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprintBmp();
        if (realmGet$fingerprintBmp != null) {
            Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, realmGet$fingerprintBmp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, false);
        }
        String realmGet$createDate = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$createDate();
        if (realmGet$createDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.createDateIndex, rowIndex, realmGet$createDate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.createDateIndex, rowIndex, false);
        }
        String realmGet$firma = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$firma();
        if (realmGet$firma != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.firmaIndex, rowIndex, realmGet$firma, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.firmaIndex, rowIndex, false);
        }
        String realmGet$retrato = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$retrato();
        if (realmGet$retrato != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.retratoIndex, rowIndex, realmGet$retrato, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.retratoIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        long tableNativePtr = table.getNativePtr();
        Evidence_ColumnInfo columnInfo = (Evidence_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Evidence_.class);
        long pkColumnIndex = columnInfo.idVerificationIndex;
        cl.autentia.realmObjects.Evidence_ object = null;
        while (objects.hasNext()) {
            object = (cl.autentia.realmObjects.Evidence_) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$idVerification());
            }
            cache.put(object, rowIndex);
            String realmGet$jsonData = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$jsonData();
            if (realmGet$jsonData != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, realmGet$jsonData, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.jsonDataIndex, rowIndex, false);
            }
            String realmGet$audit = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$audit();
            if (realmGet$audit != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.auditIndex, rowIndex, realmGet$audit, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.auditIndex, rowIndex, false);
            }
            String realmGet$type = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$type();
            if (realmGet$type != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.typeIndex, rowIndex, false);
            }
            byte[] realmGet$fingerprint = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprint();
            if (realmGet$fingerprint != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, realmGet$fingerprint, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fingerprintIndex, rowIndex, false);
            }
            byte[] realmGet$attachments = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$attachments();
            if (realmGet$attachments != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, realmGet$attachments, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.attachmentsIndex, rowIndex, false);
            }
            byte[] realmGet$fingerprintBmp = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$fingerprintBmp();
            if (realmGet$fingerprintBmp != null) {
                Table.nativeSetByteArray(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, realmGet$fingerprintBmp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fingerprintBmpIndex, rowIndex, false);
            }
            String realmGet$createDate = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$createDate();
            if (realmGet$createDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.createDateIndex, rowIndex, realmGet$createDate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.createDateIndex, rowIndex, false);
            }
            String realmGet$firma = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$firma();
            if (realmGet$firma != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.firmaIndex, rowIndex, realmGet$firma, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.firmaIndex, rowIndex, false);
            }
            String realmGet$retrato = ((cl_autentia_realmObjects_Evidence_RealmProxyInterface) object).realmGet$retrato();
            if (realmGet$retrato != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.retratoIndex, rowIndex, realmGet$retrato, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.retratoIndex, rowIndex, false);
            }
        }
    }

    public static cl.autentia.realmObjects.Evidence_ createDetachedCopy(cl.autentia.realmObjects.Evidence_ realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        cl.autentia.realmObjects.Evidence_ unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new cl.autentia.realmObjects.Evidence_();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (cl.autentia.realmObjects.Evidence_) cachedObject.object;
            }
            unmanagedObject = (cl.autentia.realmObjects.Evidence_) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        cl_autentia_realmObjects_Evidence_RealmProxyInterface unmanagedCopy = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) unmanagedObject;
        cl_autentia_realmObjects_Evidence_RealmProxyInterface realmSource = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$idVerification(realmSource.realmGet$idVerification());
        unmanagedCopy.realmSet$jsonData(realmSource.realmGet$jsonData());
        unmanagedCopy.realmSet$audit(realmSource.realmGet$audit());
        unmanagedCopy.realmSet$type(realmSource.realmGet$type());
        unmanagedCopy.realmSet$fingerprint(realmSource.realmGet$fingerprint());
        unmanagedCopy.realmSet$attachments(realmSource.realmGet$attachments());
        unmanagedCopy.realmSet$fingerprintBmp(realmSource.realmGet$fingerprintBmp());
        unmanagedCopy.realmSet$createDate(realmSource.realmGet$createDate());
        unmanagedCopy.realmSet$firma(realmSource.realmGet$firma());
        unmanagedCopy.realmSet$retrato(realmSource.realmGet$retrato());

        return unmanagedObject;
    }

    static cl.autentia.realmObjects.Evidence_ update(Realm realm, Evidence_ColumnInfo columnInfo, cl.autentia.realmObjects.Evidence_ realmObject, cl.autentia.realmObjects.Evidence_ newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        cl_autentia_realmObjects_Evidence_RealmProxyInterface realmObjectTarget = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) realmObject;
        cl_autentia_realmObjects_Evidence_RealmProxyInterface realmObjectSource = (cl_autentia_realmObjects_Evidence_RealmProxyInterface) newObject;
        Table table = realm.getTable(cl.autentia.realmObjects.Evidence_.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addInteger(columnInfo.idVerificationIndex, realmObjectSource.realmGet$idVerification());
        builder.addString(columnInfo.jsonDataIndex, realmObjectSource.realmGet$jsonData());
        builder.addString(columnInfo.auditIndex, realmObjectSource.realmGet$audit());
        builder.addString(columnInfo.typeIndex, realmObjectSource.realmGet$type());
        builder.addByteArray(columnInfo.fingerprintIndex, realmObjectSource.realmGet$fingerprint());
        builder.addByteArray(columnInfo.attachmentsIndex, realmObjectSource.realmGet$attachments());
        builder.addByteArray(columnInfo.fingerprintBmpIndex, realmObjectSource.realmGet$fingerprintBmp());
        builder.addString(columnInfo.createDateIndex, realmObjectSource.realmGet$createDate());
        builder.addString(columnInfo.firmaIndex, realmObjectSource.realmGet$firma());
        builder.addString(columnInfo.retratoIndex, realmObjectSource.realmGet$retrato());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Evidence_ = proxy[");
        stringBuilder.append("{idVerification:");
        stringBuilder.append(realmGet$idVerification());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{jsonData:");
        stringBuilder.append(realmGet$jsonData() != null ? realmGet$jsonData() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{audit:");
        stringBuilder.append(realmGet$audit() != null ? realmGet$audit() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{type:");
        stringBuilder.append(realmGet$type() != null ? realmGet$type() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fingerprint:");
        stringBuilder.append(realmGet$fingerprint() != null ? realmGet$fingerprint() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{attachments:");
        stringBuilder.append(realmGet$attachments() != null ? realmGet$attachments() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fingerprintBmp:");
        stringBuilder.append(realmGet$fingerprintBmp() != null ? realmGet$fingerprintBmp() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{createDate:");
        stringBuilder.append(realmGet$createDate() != null ? realmGet$createDate() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{firma:");
        stringBuilder.append(realmGet$firma() != null ? realmGet$firma() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{retrato:");
        stringBuilder.append(realmGet$retrato() != null ? realmGet$retrato() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        cl_autentia_realmObjects_Evidence_RealmProxy aEvidence_ = (cl_autentia_realmObjects_Evidence_RealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aEvidence_.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aEvidence_.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aEvidence_.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
