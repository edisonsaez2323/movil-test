package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class cl_autentia_realmObjects_Auditoria_RealmProxy extends cl.autentia.realmObjects.Auditoria_
    implements RealmObjectProxy, cl_autentia_realmObjects_Auditoria_RealmProxyInterface {

    static final class Auditoria_ColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long idAuditoriaIndex;
        long numeroAuditoriaIndex;
        long isUsedIndex;

        Auditoria_ColumnInfo(OsSchemaInfo schemaInfo) {
            super(3);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Auditoria_");
            this.idAuditoriaIndex = addColumnDetails("idAuditoria", "idAuditoria", objectSchemaInfo);
            this.numeroAuditoriaIndex = addColumnDetails("numeroAuditoria", "numeroAuditoria", objectSchemaInfo);
            this.isUsedIndex = addColumnDetails("isUsed", "isUsed", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        Auditoria_ColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new Auditoria_ColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final Auditoria_ColumnInfo src = (Auditoria_ColumnInfo) rawSrc;
            final Auditoria_ColumnInfo dst = (Auditoria_ColumnInfo) rawDst;
            dst.idAuditoriaIndex = src.idAuditoriaIndex;
            dst.numeroAuditoriaIndex = src.numeroAuditoriaIndex;
            dst.isUsedIndex = src.isUsedIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private Auditoria_ColumnInfo columnInfo;
    private ProxyState<cl.autentia.realmObjects.Auditoria_> proxyState;

    cl_autentia_realmObjects_Auditoria_RealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (Auditoria_ColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<cl.autentia.realmObjects.Auditoria_>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$idAuditoria() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idAuditoriaIndex);
    }

    @Override
    public void realmSet$idAuditoria(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'idAuditoria' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$numeroAuditoria() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.numeroAuditoriaIndex);
    }

    @Override
    public void realmSet$numeroAuditoria(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.numeroAuditoriaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.numeroAuditoriaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.numeroAuditoriaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.numeroAuditoriaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$isUsed() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.isUsedIndex);
    }

    @Override
    public void realmSet$isUsed(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.isUsedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.isUsedIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Auditoria_", 3, 0);
        builder.addPersistedProperty("idAuditoria", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("numeroAuditoria", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("isUsed", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static Auditoria_ColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new Auditoria_ColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Auditoria_";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Auditoria_";
    }

    @SuppressWarnings("cast")
    public static cl.autentia.realmObjects.Auditoria_ createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        cl.autentia.realmObjects.Auditoria_ obj = null;
        if (update) {
            Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
            Auditoria_ColumnInfo columnInfo = (Auditoria_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class);
            long pkColumnIndex = columnInfo.idAuditoriaIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("idAuditoria")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("idAuditoria"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class), false, Collections.<String> emptyList());
                    obj = new io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("idAuditoria")) {
                if (json.isNull("idAuditoria")) {
                    obj = (io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy) realm.createObjectInternal(cl.autentia.realmObjects.Auditoria_.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy) realm.createObjectInternal(cl.autentia.realmObjects.Auditoria_.class, json.getInt("idAuditoria"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'idAuditoria'.");
            }
        }

        final cl_autentia_realmObjects_Auditoria_RealmProxyInterface objProxy = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) obj;
        if (json.has("numeroAuditoria")) {
            if (json.isNull("numeroAuditoria")) {
                objProxy.realmSet$numeroAuditoria(null);
            } else {
                objProxy.realmSet$numeroAuditoria((String) json.getString("numeroAuditoria"));
            }
        }
        if (json.has("isUsed")) {
            if (json.isNull("isUsed")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isUsed' to null.");
            } else {
                objProxy.realmSet$isUsed((boolean) json.getBoolean("isUsed"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static cl.autentia.realmObjects.Auditoria_ createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final cl.autentia.realmObjects.Auditoria_ obj = new cl.autentia.realmObjects.Auditoria_();
        final cl_autentia_realmObjects_Auditoria_RealmProxyInterface objProxy = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("idAuditoria")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idAuditoria((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'idAuditoria' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("numeroAuditoria")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$numeroAuditoria((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$numeroAuditoria(null);
                }
            } else if (name.equals("isUsed")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isUsed((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isUsed' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'idAuditoria'.");
        }
        return realm.copyToRealm(obj);
    }

    private static cl_autentia_realmObjects_Auditoria_RealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating uexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class), false, Collections.<String>emptyList());
        io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy obj = new io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy();
        objectContext.clear();
        return obj;
    }

    public static cl.autentia.realmObjects.Auditoria_ copyOrUpdate(Realm realm, Auditoria_ColumnInfo columnInfo, cl.autentia.realmObjects.Auditoria_ object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (cl.autentia.realmObjects.Auditoria_) cachedRealmObject;
        }

        cl.autentia.realmObjects.Auditoria_ realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
            long pkColumnIndex = columnInfo.idAuditoriaIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static cl.autentia.realmObjects.Auditoria_ copy(Realm realm, Auditoria_ColumnInfo columnInfo, cl.autentia.realmObjects.Auditoria_ newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (cl.autentia.realmObjects.Auditoria_) cachedRealmObject;
        }

        cl_autentia_realmObjects_Auditoria_RealmProxyInterface realmObjectSource = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) newObject;

        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idAuditoriaIndex, realmObjectSource.realmGet$idAuditoria());
        builder.addString(columnInfo.numeroAuditoriaIndex, realmObjectSource.realmGet$numeroAuditoria());
        builder.addBoolean(columnInfo.isUsedIndex, realmObjectSource.realmGet$isUsed());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.cl_autentia_realmObjects_Auditoria_RealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, cl.autentia.realmObjects.Auditoria_ object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        long tableNativePtr = table.getNativePtr();
        Auditoria_ColumnInfo columnInfo = (Auditoria_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class);
        long pkColumnIndex = columnInfo.idAuditoriaIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$numeroAuditoria = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$numeroAuditoria();
        if (realmGet$numeroAuditoria != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, realmGet$numeroAuditoria, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isUsedIndex, rowIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$isUsed(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        long tableNativePtr = table.getNativePtr();
        Auditoria_ColumnInfo columnInfo = (Auditoria_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class);
        long pkColumnIndex = columnInfo.idAuditoriaIndex;
        cl.autentia.realmObjects.Auditoria_ object = null;
        while (objects.hasNext()) {
            object = (cl.autentia.realmObjects.Auditoria_) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$numeroAuditoria = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$numeroAuditoria();
            if (realmGet$numeroAuditoria != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, realmGet$numeroAuditoria, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isUsedIndex, rowIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$isUsed(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, cl.autentia.realmObjects.Auditoria_ object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        long tableNativePtr = table.getNativePtr();
        Auditoria_ColumnInfo columnInfo = (Auditoria_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class);
        long pkColumnIndex = columnInfo.idAuditoriaIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
        }
        cache.put(object, rowIndex);
        String realmGet$numeroAuditoria = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$numeroAuditoria();
        if (realmGet$numeroAuditoria != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, realmGet$numeroAuditoria, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isUsedIndex, rowIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$isUsed(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        long tableNativePtr = table.getNativePtr();
        Auditoria_ColumnInfo columnInfo = (Auditoria_ColumnInfo) realm.getSchema().getColumnInfo(cl.autentia.realmObjects.Auditoria_.class);
        long pkColumnIndex = columnInfo.idAuditoriaIndex;
        cl.autentia.realmObjects.Auditoria_ object = null;
        while (objects.hasNext()) {
            object = (cl.autentia.realmObjects.Auditoria_) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$idAuditoria());
            }
            cache.put(object, rowIndex);
            String realmGet$numeroAuditoria = ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$numeroAuditoria();
            if (realmGet$numeroAuditoria != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, realmGet$numeroAuditoria, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.numeroAuditoriaIndex, rowIndex, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isUsedIndex, rowIndex, ((cl_autentia_realmObjects_Auditoria_RealmProxyInterface) object).realmGet$isUsed(), false);
        }
    }

    public static cl.autentia.realmObjects.Auditoria_ createDetachedCopy(cl.autentia.realmObjects.Auditoria_ realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        cl.autentia.realmObjects.Auditoria_ unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new cl.autentia.realmObjects.Auditoria_();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (cl.autentia.realmObjects.Auditoria_) cachedObject.object;
            }
            unmanagedObject = (cl.autentia.realmObjects.Auditoria_) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        cl_autentia_realmObjects_Auditoria_RealmProxyInterface unmanagedCopy = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) unmanagedObject;
        cl_autentia_realmObjects_Auditoria_RealmProxyInterface realmSource = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$idAuditoria(realmSource.realmGet$idAuditoria());
        unmanagedCopy.realmSet$numeroAuditoria(realmSource.realmGet$numeroAuditoria());
        unmanagedCopy.realmSet$isUsed(realmSource.realmGet$isUsed());

        return unmanagedObject;
    }

    static cl.autentia.realmObjects.Auditoria_ update(Realm realm, Auditoria_ColumnInfo columnInfo, cl.autentia.realmObjects.Auditoria_ realmObject, cl.autentia.realmObjects.Auditoria_ newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        cl_autentia_realmObjects_Auditoria_RealmProxyInterface realmObjectTarget = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) realmObject;
        cl_autentia_realmObjects_Auditoria_RealmProxyInterface realmObjectSource = (cl_autentia_realmObjects_Auditoria_RealmProxyInterface) newObject;
        Table table = realm.getTable(cl.autentia.realmObjects.Auditoria_.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addInteger(columnInfo.idAuditoriaIndex, realmObjectSource.realmGet$idAuditoria());
        builder.addString(columnInfo.numeroAuditoriaIndex, realmObjectSource.realmGet$numeroAuditoria());
        builder.addBoolean(columnInfo.isUsedIndex, realmObjectSource.realmGet$isUsed());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Auditoria_ = proxy[");
        stringBuilder.append("{idAuditoria:");
        stringBuilder.append(realmGet$idAuditoria());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numeroAuditoria:");
        stringBuilder.append(realmGet$numeroAuditoria() != null ? realmGet$numeroAuditoria() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isUsed:");
        stringBuilder.append(realmGet$isUsed());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        cl_autentia_realmObjects_Auditoria_RealmProxy aAuditoria_ = (cl_autentia_realmObjects_Auditoria_RealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aAuditoria_.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAuditoria_.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aAuditoria_.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
