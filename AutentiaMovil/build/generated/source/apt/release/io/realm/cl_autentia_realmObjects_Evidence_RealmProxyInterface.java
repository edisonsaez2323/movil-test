package io.realm;


public interface cl_autentia_realmObjects_Evidence_RealmProxyInterface {
    public int realmGet$idVerification();
    public void realmSet$idVerification(int value);
    public String realmGet$jsonData();
    public void realmSet$jsonData(String value);
    public String realmGet$audit();
    public void realmSet$audit(String value);
    public String realmGet$type();
    public void realmSet$type(String value);
    public byte[] realmGet$fingerprint();
    public void realmSet$fingerprint(byte[] value);
    public byte[] realmGet$attachments();
    public void realmSet$attachments(byte[] value);
    public byte[] realmGet$fingerprintBmp();
    public void realmSet$fingerprintBmp(byte[] value);
    public String realmGet$createDate();
    public void realmSet$createDate(String value);
    public String realmGet$firma();
    public void realmSet$firma(String value);
    public String realmGet$retrato();
    public void realmSet$retrato(String value);
}
