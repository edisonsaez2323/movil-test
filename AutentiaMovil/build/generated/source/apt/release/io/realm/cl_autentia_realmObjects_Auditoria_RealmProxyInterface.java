package io.realm;


public interface cl_autentia_realmObjects_Auditoria_RealmProxyInterface {
    public int realmGet$idAuditoria();
    public void realmSet$idAuditoria(int value);
    public String realmGet$numeroAuditoria();
    public void realmSet$numeroAuditoria(String value);
    public boolean realmGet$isUsed();
    public void realmSet$isUsed(boolean value);
}
