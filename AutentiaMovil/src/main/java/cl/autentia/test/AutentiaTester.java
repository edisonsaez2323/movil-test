package cl.autentia.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acepta.android.fetdroid.R;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosmart.sdk.MorphoDevice;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import cl.autentia.activity.InfoNFCActivity_;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;

//import cl.autentia.activity.InfoNFCActivity_;

/**
 * Created by Edison Saez on 21-03-2017.
 */
@EActivity(R.layout.activity_tester)
public class AutentiaTester extends Activity implements FingerprintReader.ScanCallback, FingerprintReader.InfoCallback, FingerprintGrantReceiver {

    private static final String TAG = "AutentiaMovilTest";

    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    private static final int REQUEST_INTENT_BARCODE = 0x00001;
    private static final int REQUEST_INTENT_BARCODE_CEDULA = 0x00002;
    private static final int REQUEST_INTENT_INFO_CEDULA = 0x00003;
    private String mBarcode = "", mName = "";
    private AutentiaPreferences mAutentiaPreferences;
    private FingerprintManager mFingerprintManager;
    private FingerprintReader mFingerprintReader;
    private ProgressDialog mProgressDialog;
    public UsbManager usbManager;
    private MorphoDevice morphoDevice = new MorphoDevice();



    @ViewById(R.id.bt_fingerprint)
    Button finger_button;

    @InstanceState
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    private FingerprintInfo mFingerprintInfo;

    @Override
    public void onFingerprintAccessGranted() {
        Log.e("onFingerprintAccessGd","HERE");
        mFingerprintReader = mFingerprintManager.getReader(this);
        startCaptureFingerprint();
    }

    @Override
    public void onFingerprintAccessDenied() {
        showMessage("Acceso al huellero denegado");
    }

    @Override
    public void onFingerprintAccessError() {
        showMessage("Error de permisos del huellero");
    }

    @Override
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void scanResult(FingerprintImage image) {
//        dismissProgress();
        drawFingerPrintImage(image);
    }

    @Override
    public void infoResult(FingerprintInfo info) {


        try {

            mFingerprintInfo = info;
            mFingerprintReader.capture(this);
        } catch (Exception e) {
            showMessage(e.getMessage());
        }

    }

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {
        interface In {
            String REVERSE = "REVERSE";
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFingerprintManager = FingerprintManager.getInstance(this);
        mAutentiaPreferences = new AutentiaPreferences(this);
        if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager")) {
            usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            USBManager.getInstance().initialize(this,"com.morpho.morphosample.USB_ACTION");
            countDevices();
//            setPowerFP2(1);
        }
    }

    void setPowerFP2(int onOff) {
        //1 ON 2 OFF
//        SledManager sm = (SledManager) this.getSystemService("sled");

//        UsbManager m_usb_manager;
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (onOff == 1) {
            Log.i(TAG, "POWER ON");
            usbManager.setFingerPrinterPower(true);
            //started = true;
        } else if (onOff == 0) {
            Log.i(TAG, "POWER OFF");
            usbManager.setFingerPrinterPower(false);
            //started = false;
        }
    }

    @OnActivityResult(value = REQUEST_FINGERPRINT_ACCESS)
    protected void onRequestFingerprintAccessResult(int resultCode, Intent data) {
        mFingerprintManager.processAccessGrant(resultCode, data, this);
    }

    public void cedulaNuevaTest(View view) {
        Intent intent = new Intent();
        intent.setAction("cl.autentia.operacion.BARCODE");
        intent.putExtra(Extras.In.REVERSE, false);
        startActivityForResult(intent, REQUEST_INTENT_BARCODE_CEDULA);
    }

    public void barcodeTest(View view) {
        Intent intent = new Intent();
        intent.setAction("cl.autentia.operacion.BARCODE");
        intent.putExtra(Extras.In.REVERSE, false);
        startActivityForResult(intent, REQUEST_INTENT_BARCODE);
    }

    public void conexionTest(View view) {
        mProgressDialog = new ProgressDialog(AutentiaTester.this);
        mProgressDialog.setTitle("Test de conexión");
        mProgressDialog.setMessage("Por favor, espere...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        getConnectionStatus();
    }

    public void huellerosTest(View view) {
        finger_button.setClickable(false);
        testHuella();
    }

    private void testHuella() {
        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    finger_button.setClickable(true);
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    startCaptureFingerprint();

                } else {
                    finger_button.setClickable(true);
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                finger_button.setClickable(true);
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                startCaptureFingerprint();
            }
        } catch (AutentiaMovilException e) {
            finger_button.setClickable(true);
            showMessage(e.returnCode.getDescription());

        } catch (Exception e) {
            finger_button.setClickable(true);
            showMessage(e.toString());
        }
    }

    private void startCaptureFingerprint() {
        mProgressDialog = new ProgressDialog(AutentiaTester.this);
        mProgressDialog.setTitle(R.string.captura_huella_test);
        mProgressDialog.setMessage(getText(R.string.espera_huella));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mFingerprintReader.cancelCapture();
                mFingerprintReader.close();
                finish();
            }
        });
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        getInfo();
    }

    @Background
    void getInfo() {
        try {
            mFingerprintReader.getInfo(this);
            finger_button.setClickable(true);
        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }

    private void drawFingerPrintImage(FingerprintImage pfingerprintImage) {

        try {
            Log.i("wsq", Base64.encodeToString(pfingerprintImage.getImageWSQ(), Base64.DEFAULT));
            Log.i("height", String.valueOf(pfingerprintImage.getHeight()));
            Log.i("width", String.valueOf(pfingerprintImage.getWidth()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (pfingerprintImage != null) {
            View view = getLayoutInflater().inflate(R.layout.test_huella_layout, null);
            ImageView ivFinger = view.findViewById(R.id.iv_finger);
            ivFinger.setImageBitmap(pfingerprintImage.getBitmap());
            TextView tvCopiar = view.findViewById(R.id.tv_copiar);
            tvCopiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", mFingerprintInfo.serialNumber);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(AutentiaTester.this, String.format("Nro. de serie copiado en portapapeles"), Toast.LENGTH_SHORT).show();
                }
            });
            new AlertDialog.Builder(this)
                    .setTitle("Resultado")
                    .setMessage(String.format("Nro. Serie:\n\n%s", mFingerprintInfo.serialNumber))
                    .setCancelable(false)
                    .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(mProgressDialog.isShowing()) mProgressDialog.dismiss();
                            dialog.dismiss();
                            dialog.cancel();
                            mFingerprintReader = null;
                            testHuella();
                        }
                    })
                    .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if(mProgressDialog.isShowing()) mProgressDialog.dismiss();
                            if (mFingerprintReader.isOpen) {
                                Log.e("READER","CLOSING-------");
                                mFingerprintReader.close();
                            }
                        }
                    })
                    .setView(view)
                    .create()
                    .show();
        }
    }

//    private void drawFingerPrintImage(FingerprintImage pfingerprintImage) {
//        if (pfingerprintImage != null) {
//
//            LinearLayout linear = (LinearLayout) findViewById(R.id.linear_botones);
//            linear.setVisibility(View.GONE);
//
//            ((ImageView) findViewById(R.id.img_finger)).setImageBitmap(pfingerprintImage.getBitmap());
//            RelativeLayout rv = (RelativeLayout) findViewById(R.id.rv_fingerprint);
//            rv.setVisibility(View.VISIBLE);
//            rv.bringToFront();
//
//            new AlertDialog.Builder(this)
//                    .setTitle("Resultado")
//                    .setMessage(String.format("Nro. Serie:\n\n%s", mFingerprintInfo.serialNumber))
//                    .setCancelable(false)
//                    .setPositiveButton("Copiar", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//                            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", mFingerprintInfo.serialNumber);
//                            clipboard.setPrimaryClip(clip);
//                            Toast.makeText(AutentiaTester.this, String.format("Nro. Serie %s\n\ncopiado en portapapeles", mFingerprintInfo.serialNumber), Toast.LENGTH_SHORT).show();
//                        }
//                    })
//                    .setNegativeButton("Aceptar", null)
//                    .create()
//                    .show();
//        }
//    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void dismissProgress() {
        mProgressDialog.dismiss();
    }

    @Background
    void getConnectionStatus() {

        String hostname = mAutentiaPreferences.getString(KeyPreferences.URL_BASE);
        String error = "";
        boolean isConnected = false;

        try {

            try {
                isConnected = isConnectedToServer(hostname);

            } catch (SocketTimeoutException e) {
                error = "Dispositivo con Internet:\n Error de puerto de conexion";
            } catch (UnknownHostException e) {
                error = "Url desconocida";
            } catch (MalformedURLException e) {
                error = "Url incorrecta";
            }

            if (!isConnected) {
                showMessage("Prueba No Exitosa:\n\n" + (!TextUtils.isEmpty(error) ? error : "Dispositivo sin internet"));
            } else {
                showMessage("Prueba Exitosa:\n\nConexion establecida\n" + hostname);
            }
        } finally {
            dismissProgress();
        }
    }

    public void onFinish(View view) {
//        if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager"))setPowerFP2(0);
        if(mFingerprintReader != null && mFingerprintReader.isOpen){
//            Log.e("IM OPEN","CLOSING----------");
            mFingerprintReader.close();
        }
        finish();
    }

    public static boolean isConnectedToServer(String host)
            throws SocketTimeoutException, UnknownHostException, MalformedURLException {
        try {
            URL myUrl = new URL(host);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(5000);
            connection.connect();
            return true;
        } catch (SocketTimeoutException | UnknownHostException | MalformedURLException e) {
            throw e;
        } catch (Exception e) {
            return false;
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle("Resultado:")
                .setCancelable(false)
                .setPositiveButton("Aceptar", null)
                .create().show();
    }

    @OnActivityResult(REQUEST_INTENT_BARCODE)
    void onBarcodeResult(int resultCode, Intent data) {
        try {

            if (resultCode == RESULT_OK) Log.d(TAG, "RESULT_OK");
            else if (resultCode == RESULT_CANCELED) Log.d(TAG, "RESULT_CANCELED");

            if (data == null) {
                Log.d(TAG, "Intent result null");
                throw new Exception("Intent result null");
            }

            Bundle result = data.getExtras();

            if (result == null) {
                Log.d(TAG, "Bundle result null");
                throw new Exception("Bundle result null");
            }

            Iterator iterator = data.getExtras().keySet().iterator();
            String msg = "";

            while (iterator.hasNext()) {

                String key = iterator.next().toString();
                Object value = result.get(key);

                if (key.equalsIgnoreCase("barcode")) {
                    mBarcode = value.toString();
                }

                msg += String.format("%s : %s\n\n", key, value);
                Log.d(TAG, String.format("%s : %s", key, value));
            }
            showMessage(msg);

        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }

    @OnActivityResult(REQUEST_INTENT_BARCODE_CEDULA)
    void onBarcodeCedulaResult(int resultCode, Intent data) {
        Bundle result = data.getExtras();

        if (result == null) {
            return;
        }

        Iterator iterator = data.getExtras().keySet().iterator();
        while (iterator.hasNext()) {

            String key = iterator.next().toString();
            Object value = result.get(key);

            if (key.equalsIgnoreCase("barcode")) {
                mBarcode = value.toString();
            }
        }

        Intent intent = new Intent(AutentiaTester.this, InfoNFCActivity_.class);
        intent.putExtra("BARCODE", mBarcode);
        intent.putExtra("READ_EXTENDED_INFO", true);
        intent.putExtra("TIMEOUT", 30);
        startActivityForResult(intent, REQUEST_INTENT_INFO_CEDULA);
    }

    @OnActivityResult(REQUEST_INTENT_INFO_CEDULA)
    void onInfoCedulaResult(int resultCode, Intent data) {
        try {

            if (resultCode == RESULT_OK) Log.d(TAG, "RESULT_OK");
            else if (resultCode == RESULT_CANCELED) Log.d(TAG, "RESULT_CANCELED");

            if (data == null) {
                Log.d(TAG, "Intent result null");
                throw new Exception("Intent result null");
            }

            Bundle result = data.getExtras();

            if (result == null) {
                Log.d(TAG, "Bundle result null");
                throw new Exception("Bundle result null");
            }

            if (result.containsKey("retrato")) {

                byte[] img_c = result.getByteArray("retrato");
                mName = String.format("%s %s", result.get("nombres"), result.get("apellidos"));
                showResult(img_c);
//                store(img_c,"Imagen.jpg");
            } else {
                showMessage("Retrato no se pudo obtener");
            }

        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }


    public static void store(byte[] bitmapdata, String fileName) throws IOException {
        Bitmap bm = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        String dirPath;
        dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Test/Cedula/" + new SimpleDateFormat("yyyyMM").format(new Date());
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);

        FileOutputStream fOut = new FileOutputStream(file);
        bm.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        fOut.flush();
        fOut.close();

    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void showResult(byte[] img_celuda) {
        Bitmap bmp = BitmapFactory.decodeByteArray(img_celuda, 0, img_celuda.length);
        Drawable drawable = new BitmapDrawable(getResources(), bmp);

        LinearLayout linear = (LinearLayout) findViewById(R.id.linear_botones);
        linear.setVisibility(View.GONE);

        ((ImageView) findViewById(R.id.img_cedula)).setBackgroundDrawable(drawable);
        ((TextView) findViewById(R.id.txt_nombre)).setText(mName);
        RelativeLayout rv = (RelativeLayout) findViewById(R.id.rv_picture);
        rv.setVisibility(View.VISIBLE);
        rv.bringToFront();

    }

    private void countDevices() {
        int count = 0;
//        UsbManager usbManager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> usbDeviceList = usbManager.getDeviceList();

        Iterator<UsbDevice> usbDeviceIterator = usbDeviceList.values().iterator();
        while (usbDeviceIterator.hasNext())
        {
            UsbDevice usbDevice = usbDeviceIterator.next();
//            if (MorphoTools.isSupported(usbDevice.getVendorId(), usbDevice.getProductId()))
//            {
                boolean hasPermission = usbManager.hasPermission(usbDevice);
                if (!hasPermission)
                {
                    // Request permission for using the device
                    usbManager.requestPermission(usbDevice, PendingIntent.getBroadcast(this, 0, new Intent("com.morpho.android.usb.USB_PERMISSION"), 0));
                }
                else {
                    count++;
                }
//            }
        }
//        return count;
    }
}
