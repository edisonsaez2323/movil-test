package cl.autentia.preferences;

public interface KeyPreferences {

    /**
     * Properties
     */

    String URL_BASE = "URL_BASE";
    String INSTITUTION = "INSTITUTION";
    String ENVIROMENT = "ENVIROMENT_CODE";
    String FINGERPRINT_READER_CONTROLLER_CLASS = "FINGERPRINT_READER_CONTROLLER_CLASS";
    String NFC_CONTROLLER_CLASS = "NFC_CONTROLLER_CLASS";
    String HASH_CONFIGURATION = "HASH_CONFIGURATION";
    String RUN_INSTITUTION = "RUN_INSTITUTION";
    String DATA_CONFIGURATION = "DATA_CONFIGURATION";
    String AUDIT_VALUE = "AUDIT_VALUE";
    String NFC_CONFIGURATION = "NFC_CONFIGURATION";
    String FIRST_START = "FIRST_START";
    String QUANTITY_AUDIT = "QUANTITY_AUDIT";
    String LOGGED_IN = "LOGGED_IN";
    String PENDING_LOGIN = "PENDING_LOGIN";
    String FIRST_TRY = "FIRST_TRY";
    String TIPO_LOGON = "TIPO_LOGON";
    String TERMS_AND_CONDITIONS = "TERMS_AND_CONDITIONS";
    //Nuevo properties desde el servidor
    String AUTENTIA_WS_USER = "AUTENTIA_WS_USER";
    String AUTENTIA_WS_PASSWD = "AUTENTIA_WS_PASSWD";
    String VERSION_CONFIG = "VERSION_CONFIG";
    String FINGERPRINT_SERIAL_NUMBER = "FINGERPRINT_SERIAL_NUMBER";
    String FIREBASE_CREATED_AT_VALUE = "FIREBASE_CREATED_AT_VALUE";

    /**
     * HTTP
     */

    String LAST_CHECK = "LAST_CHECK";
    String LAST_VERSION_REPORTED = "LAST_VERSION_REPORTED";
}
