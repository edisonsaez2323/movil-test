package cl.autentia.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

public class AutentiaPreferences {

//    private final String PREFERENCES_FILE = "Autentia.config";

    private Context mContext;

    public AutentiaPreferences(Context mContext) {
        this.mContext = mContext;
    }

    private SharedPreferences getSettings() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public String getString(String key) {
        return getSettings().getString(key, null);
    }

    public String getString(String key, String sDefaultValue) {
        return getSettings().getString(key, sDefaultValue);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        return getSettings().getInt(key, 0);
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return getSettings().getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public long getLong(String key) {
        return getSettings().getLong(key, 0);
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public Map<String, ?> getAll() {
        return getSettings().getAll();
    }

    public void deleteAll() {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.clear();
        editor.commit();
    }

    public void deleteKey(String key) {

        SharedPreferences.Editor editor = getSettings().edit();
        editor.remove(key);
        editor.commit();
    }

    public boolean containsKey(String key){
        return getSettings().contains(key);
    }

}
