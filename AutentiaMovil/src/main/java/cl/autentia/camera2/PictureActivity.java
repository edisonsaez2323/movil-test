package cl.autentia.camera2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import cl.autentia.activity.PhotoActivity;
import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Edison Saez on 13-04-2018.
 */

public class PictureActivity extends AppCompatActivity{

    private static String image;
    private static Activity activity;

    public static void setImage(String image, Activity activity) {
        PictureActivity.image = image;
        PictureActivity.activity = activity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finishProcess(image);
    }

    private void finishProcess(String base64){
        Intent intent = new Intent();
        if (base64 != null)
            intent.putExtra(Extras.Out.BASE64_IMAGEN, base64);
        intent.putExtra(Extras.Out.ESTADO, Status.OK);
        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
        intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
        setResult(RESULT_OK, intent);
//        cameraView.lis
//        mProgressDialog.dismiss();
        activity.finish();
        PictureActivity.this.finish();
    }

    interface Extras {
        interface Out extends CommonOutExtras {
            String BASE64_IMAGEN = "BASE64_IMAGEN";
        }

        interface In extends CommonInExtras {
            String ORIENTACION = "ORIENTACION";
            String HEIGHT = "HEIGHT";
            String WIDTH = "HEIGHT";
            String TYPE = "TYPE";
        }
    }


}
