package cl.autentia.transunion.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cl.autentia.http.parameters.PreguntaParam;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    List<PreguntaParam> questions;

    public ScreenSlidePagerAdapter(FragmentManager fm, List<PreguntaParam> questions) {
        super(fm);
        this.questions = questions;
    }

    @Override
    public Fragment getItem(int position) {

        ScreenSlidePageFragment screenSlidePageFragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putString(ScreenSlidePageFragment.ARG_QUESTION_TITLE,questions.get(position).getPregunta());
        args.putString(ScreenSlidePageFragment.ARG_QUESTION_ID,questions.get(position).getId());
        args.putStringArrayList(ScreenSlidePageFragment.ARG_QUESTION_ANSWER, (ArrayList<String>) questions.get(position).getRespuestas());
        screenSlidePageFragment.setArguments(args);
        updateFragment(screenSlidePageFragment);
        return screenSlidePageFragment;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }

    public void updateFragment(ScreenSlidePageFragment fgm){
        fgm.onResume();
    }

    @Override
    public int getCount() {
        return questions.size();
    }

}