package cl.autentia.transunion;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GetCuestionarioResp;
import cl.autentia.http.parameters.PreguntaParam;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.transunion.adapter.ScreenSlidePagerAdapter;

import static com.acepta.Utils.getTrackID;


public class TransunionActivity extends AppCompatActivity implements FingerprintReader.InfoCallback {

    AlertDialog optionDialog;
    private AutentiaWSClient mAutentiaWSClient;
    private List<PreguntaParam> questionClassList;
    private FingerprintReader mFingerprintReader;
    private FingerprintManager mFingerprintManager;
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;

    private Toolbar toolbar;

    String mSerialNumber;
    private AutentiaPreferences mPreferences;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private LinearLayout mLinearLayout;

    Button sendButton;
    String mRut;
    String mDv;
    String mNs;
    String mId_c;
    ArrayList respuestas;

    String TAG = "TransunionActivity";

    private String trackID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transunion);
        mAutentiaWSClient = new AutentiaWSClient(this);
        mFingerprintManager = FingerprintManager.getInstance(this);
        mPreferences = new AutentiaPreferences(this);
        sendButton = (Button) findViewById(R.id.send_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Autentia Movil - TransUnion");
        sendButton.setText("siguiente");
        mLinearLayout = (LinearLayout) findViewById(R.id.main_linear_layout);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            mRut = String.valueOf(getIntent().getIntExtra("RUT", 0)) + String.valueOf(getIntent().getCharExtra("DV", 'k'));
            mDv = String.valueOf(getIntent().getCharExtra("DV", 'k'));
            if (!Utils.validaRutDV(getIntent().getIntExtra("RUT", 0), getIntent().getCharExtra("DV", 'k'))) {
                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.RUT_INVALIDO));
            }
            if (getIntent().getStringExtra("NS") == null || getIntent().getStringExtra("NS").equals("")) {
                errorHandlerMethod("", new AutentiaMovilException("el numero de serie esta vacío", ReturnCode.ERROR_GENERICO_TRANSUNION));
            }
            if (getIntent().getStringExtra("ID") == null || getIntent().getStringExtra("ID").equals("")) {
                mId_c = "0";
            } else {
                mId_c = getIntent().getStringExtra("ID");
            }
            mNs = getIntent().getStringExtra("NS");

            try {
                if (getIntent().getExtras().containsKey("trackId")) {
                    trackID = getIntent().getStringExtra(Extras.In.TRACK_ID);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!trackID.isEmpty()) {
                prepareFingerprintReader();
            } else {
                initControlsStart();
            }

        } else
            errorHandlerMethod("", new AutentiaMovilException("No hay extras en el intent de llamada.", ReturnCode.ERROR_GENERICO_TRANSUNION));
//            finish();
    }


    @Override
    protected void onDestroy() {

        dismissProgressMessage();
        if (mFingerprintReader != null) mFingerprintReader.close();
        if (mFingerprintManager != null) mFingerprintManager = null;
        super.onDestroy();

    }

    /**
     * METHODS
     */

    public void getQuestions() {

        showDialog("Obteniendo cuestionario...");

        try {
            mAutentiaWSClient.getQuestions(mRut, mNs, mPreferences.getString(KeyPreferences.INSTITUTION), mSerialNumber, mId_c, new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    dismissProgressMessage();
                    try {
                        GetCuestionarioResp gqr = new Gson().fromJson(result.getString("result"), GetCuestionarioResp.class);
                        questionClassList = gqr.getPreguntas();
                        showQuestions();
                    } catch (Exception e) {
                        errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
                    }
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 3001 || result.returnCode.getCode() == 201) {
                        errorHandlerMethod("", result);
                    }
                    errorHandlerMethod(Status.ERROR, result);
                }
            });
        } catch (Exception e) {
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
        }
    }

    public void showQuestions() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(null);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), questionClassList);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == questionClassList.size() - 1) sendButton.setText("Finalizar");
                else sendButton.setText("siguiente");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mPager, true);

    }

    public void buttonOnClick(View v) {


        if (mPager.getCurrentItem() == questionClassList.size() - 2) {
            sendButton.setText("Finalizar");
            mPager.setCurrentItem(mPager.getCurrentItem() + 1);
        } else {
            if (sendButton.getText().equals("Finalizar")) {
                dismissProgressMessage();
                showDialog("Validando respuestas");
                sendResponse();
            } else {
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        }
    }

    public void sendResponse() {
        respuestas = new ArrayList();
        HashMap<String, String> hashMap = new HashMap<>();
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        for (int i = 0; i < questionClassList.size(); i++) {
            RadioGroup rg = (RadioGroup) viewPager.findViewWithTag(questionClassList.get(i).getId());
            for (int j = 0; j < rg.getChildCount(); j++) {
                RadioButton rb = (RadioButton) rg.getChildAt(j);
                if (rb.isChecked()) {
                    respuestas.add(rb.getId());
                    hashMap.put(questionClassList.get(i).getId(), String.valueOf(rb.getId()));
                }
            }
        }
        if (respuestas.size() < questionClassList.size()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissProgressMessage();
                    Snackbar snackbar = Snackbar
                            .make(mLinearLayout, "Debes contestar todas las preguntas antes de continuar", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    mPager.setCurrentItem(0);
                }
            });
        } else {
            try {
                mAutentiaWSClient.sendResponse(hashMap, mRut, mPreferences.getString(KeyPreferences.INSTITUTION), mSerialNumber, mNs, mId_c, trackID, new ResponseCallback() {
                    @Override
                    public void onResponseSuccess(Bundle result) {
                        dismissProgressMessage();
                        try {
                            //determinamos el comportamiento de acuerdo al tipo de respuesta que nos llegue.
                            org.json.JSONObject jsonResp = new org.json.JSONObject(result.getString("result"));
                            if (jsonResp.getBoolean("newQ")) { //si es un nuevo cuestionario
                                questionClassList.clear();
                                mPager.setCurrentItem(0);
                                GetCuestionarioResp gqr = new Gson().fromJson(result.getString("result"), GetCuestionarioResp.class);
                                questionClassList = gqr.getPreguntas();
                                showQuestions();
                            } else { //si es una respuesta

                                String genAuditResult = result.getString("result");
                                final Bundle bundle = new Bundle();
                                bundle.putBoolean("match", true);
                                bundle.putString("auditoria", genAuditResult);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showValidationResponse(bundle);
                                    }
                                });
                            }


                        } catch (Exception e) {
                            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
                        }
                    }

                    @Override
                    public void onResponseError(AutentiaMovilException result) {
                        if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 3001 || result.returnCode.getCode() == 201) {
                            errorHandlerMethod("", result);
                        }
                        errorHandlerMethod(Status.ERROR, result);
                    }
                });
            } catch (Exception e) {
                errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
            }
        }


    }

    /**
     * UTILS
     */

    void showValidationResponse(Bundle result) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressMessage();
            }
        });
        int status;
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(result.get("auditoria")));
            status = jsonObject.getInt("status");
            if (status == 0 || status == 1) { //si falla el servicio
                dismissProgressMessage();
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Extras.Out.RUT, mRut.substring(0, mRut.length() - 1));
                returnIntent.putExtra(Extras.Out.DV, mDv);
                returnIntent.putExtra(Extras.Out.ESTADO, jsonObject.getString("glosa"));
                returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, status);
                returnIntent.putExtra(Extras.Out.DESCRIPCION, jsonObject.getString("message"));
                returnIntent.putExtra(Extras.Out.TRACK_ID, trackID);
                if (status == 0) {
                    returnIntent.putExtra(Extras.Out.CODIGO_AUDITORIA, jsonObject.getString("nroAuditoria"));
                    returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, true);
                    returnIntent.putExtra(Extras.Out.ENROLADO, true);
                } else {
                    returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, false);
                    returnIntent.putExtra(Extras.Out.ENROLADO, false);
                }
                returnIntent.putExtra(Extras.Out.NUMERO_SERIE_HUELLERO, mSerialNumber);
                setResult(RESULT_OK, returnIntent);
                finish();
            } else {
                dismissProgressMessage();
                errorHandlerMethod("", new AutentiaMovilException(jsonObject.getString("glosa"), ReturnCode.ERROR_GENERICO_TRANSUNION));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void dismissProgressMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (optionDialog != null && optionDialog.isShowing())
                    optionDialog.dismiss();
                Log.v(TAG, "dialog dismissed");
            }
        });

    }

    private void prepareFingerprintReader() {

        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    getInfo();
                } else {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                getInfo();
            }
        } catch (AutentiaMovilException e) {
            errorHandlerMethod(e.status, new AutentiaMovilException(e.returnCode.getDescription(), e.returnCode));
        } catch (Exception e) {
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
        }
    }

    //    @Background
    void getInfo() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    showDialog("Cargando...");
                    mFingerprintReader.getInfo(TransunionActivity.this);
                } catch (Exception e) {
                    errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
                }
            }
        });
    }

    public void showDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (optionDialog != null)
                    if (optionDialog.isShowing())
                        optionDialog.dismiss();
                optionDialog = new AlertDialog.Builder(TransunionActivity.this).create();

                View v = getLayoutInflater().inflate(R.layout.progress_layout, null);
                LottieAnimationView lottie = (LottieAnimationView) v.findViewById(R.id.lottie);
                TextView tv = v.findViewById(R.id.tv_message);
                tv.setText(message);
                try {
                    lottie.setAnimation(new JSONObject(getResources().getString(R.string.json_animation)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lottie.playAnimation();
                lottie.setScale(10);
                lottie.loop(true);

                optionDialog.setView(v);
                optionDialog.setCanceledOnTouchOutside(false);
                optionDialog.setCancelable(false);
                optionDialog.show();
            }
        });

    }


    void finishActivityWithError(String status, int resultCode, String descripcion) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.RUT, mRut);
        returnIntent.putExtra(Extras.Out.DV, mDv);
        returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, false);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        if (!trackID.isEmpty()) {
            returnIntent.putExtra(Extras.Out.TRACK_ID, trackID);
        }
        returnIntent.putExtra(Extras.Out.DESCRIPCION, descripcion);
        setResult(RESULT_OK, returnIntent);
        finish();

    }


    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        Log.e("ameRC", "" + ame.returnCode.getCode());
        Log.e("INVALID_TOKENRC", "" + ReturnCode.INVALID_TOKEN.getCode());


        if (ame.returnCode.getCode() == ReturnCode.INVALID_TOKEN.getCode()) {
            Log.d(TAG, "borrando número de serie...");
            mPreferences.deleteKey(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
        }

        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            default:
                if (ame.returnCode.getCode() == 9000) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status);
                } else if (ame.returnCode.getCode() == 3001) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO_TRANSUNION.getCode(),
                            ame.status);
                } else {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status);
                }
                break;
        }
    }

    @Override
    public void infoResult(FingerprintInfo info) {
        mSerialNumber = info.serialNumber;
        //save data fingerprint reader
        mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, mSerialNumber);

        if (trackID.isEmpty()) {
            trackID = getTrackID();
        }

//        boolean pendingLogin = false;
//        boolean loggedIn = false;
//
//        Log.e("pending","enter");
//        try {
//            mPreferences = new AutentiaPreferences(this);
//            if (mPreferences.containsKey(KeyPreferences.PENDING_LOGIN)){
//                pendingLogin = mPreferences.getBoolean(KeyPreferences.PENDING_LOGIN);
//                loggedIn = mPreferences.getBoolean(KeyPreferences.LOGGED_IN);
//                Log.e("pendig ver",String.valueOf(pendingLogin));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        if (pendingLogin && !loggedIn){
//            Log.e("pedir","rut");
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    AlertDialog alertDialog = new AlertDialog.Builder(TransunionActivity.this).create();
//                    alertDialog.setIcon(R.drawable.autentia_logo_blue);
//                    alertDialog.setTitle("Verificacion Operador");
//                    alertDialog.setCancelable(false);
//                    alertDialog.setMessage("Debe verificarse como operador antes de operar");
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent intent = new Intent(TransunionActivity.this,NavegationMenuActivity_.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                    alertDialog.show();
//                }
//            });
//
//        }else {
        getQuestions();
//        }
    }

    /**
     * INTERFACES.
     */

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {

        interface In extends CommonInExtras {

            String BARCODE = "BARCODE";
            String TIMEOUT = "TIMEOUT";
            String TRACK_ID = "trackId";
        }

        interface Out extends CommonOutExtras {

            String IDENTIDAD_VERIFICADA = "identidadVerificada";
            String RUT = "rut";
            String DV = "dv";
            String NUMERO_SERIE_HUELLERO = "serialNumber";
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String ENROLADO = "enrolado";

            String TRACK_ID = "trackId";
        }
    }

    /**
     * Terminos y condiciones
     */

    private void initControlsStart() {

        if(!mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS).isEmpty()) {
            TerminosParam tp = new Gson().fromJson(mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS), TerminosParam.class);
            final String texto = tp.getTerminos();
            if (texto.isEmpty()) errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
            final AlertDialog.Builder builder = new AlertDialog.Builder(TransunionActivity.this);
            @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.layout_terms_in, null);
            builder.setView(v);
            TextView tvTermsText = (TextView) v.findViewById(R.id.tv_text_terms);
            Button btAceptar = (Button) v.findViewById(R.id.btn_acept_terms);
            Button btCancel = (Button) v.findViewById(R.id.btn_cancel_terms);
            tvTermsText.setText(texto);
            builder.setCancelable(false);
            builder.create();
            final AlertDialog show = builder.show();
            btAceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    show.dismiss();
                    prepareFingerprintReader();
                }
            });
            btCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCancelDisclamer(view);
                }
            });
        }else{
            errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
        }
    }

    public void onCancelDisclamer(View v) {
        createAudit(new byte[0],
                ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES.getDescription(),
                ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES.getCode());
        errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES_TRANSUNION));

        HttpUtil.sendReportUncaughtException(new Exception(ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES_TRANSUNION.getDescription()), this);
    }

    private static final String AUDIT_TYPE_ERROR = "RECHAZO";
    private static final String AUDIT_SOURCE = "ANDROID";
    private String mReaderType;

    void createAudit(byte[] baseImagen, String error, int codigoError) {

        String versionApp = null;
        JSONObject jsonObject = new JSONObject();
        try {
            versionApp = AppHelper.getCurrentVersion(this);

            jsonObject.put("type", error);
            jsonObject.put("rut", String.valueOf(mRut));
            jsonObject.put("dv", String.valueOf(mDv));
            jsonObject.put("versionApp", versionApp);
            jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
            jsonObject.put("ip", DeviceHelper.getIPAddress(true));
            jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
            jsonObject.put("imei", DeviceHelper.getIMEI(this));
            jsonObject.put("operacion", AUDIT_TYPE_ERROR);
            jsonObject.put("origen", AUDIT_SOURCE);
            if (!trackID.isEmpty()) {
                jsonObject.put("trackId", trackID);
            }

            if (mSerialNumber == null) {
                try {
                    mSerialNumber = mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
                    mReaderType = mPreferences.getString("mReaderType");
                } catch (Exception e) {
                    HttpUtil.sendReportUncaughtException(e, this);
                }
            }

            //Obtención número de auditoria
            mAutentiaWSClient.genAudit(baseImagen,
                    "wsq",
                    String.format("%s-%s", mRut, mDv),
                    "10",
                    mReaderType,
                    mSerialNumber,
                    baseImagen,
                    String.valueOf(codigoError),
                    mPreferences.getString(KeyPreferences.INSTITUTION),
                    jsonObject.toString(),
                    mPreferences.getString(KeyPreferences.RUN_INSTITUTION),
                    versionApp,
                    "sin datos",
                    "0",
                    "",
                    "",
                    mPreferences.getString(KeyPreferences.INSTITUTION), false, new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
                            Log.d(TAG, "Create Audit succeed");
                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            Log.e(TAG, "Create Audit HERROR");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
