package cl.autentia.usb;

/**
 * Created by marcin on 3/14/17.
 */

public class CheckUtils {
    public static ConditionChecker isTrue(boolean condition) {
        return new ConditionChecker(condition);
    }

    public static ConditionChecker isFalse(boolean condition) {
        return new ConditionChecker(! condition);
    }

    public static class ConditionChecker {

        private final boolean mCondition;

        ConditionChecker(boolean condition) {
            this.mCondition = condition;
        }

        public void orThrow(String message) {
            if (! mCondition) {
                throw new RuntimeException(message);
            }
        }
    }
}
