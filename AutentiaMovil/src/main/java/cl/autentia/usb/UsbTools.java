package cl.autentia.usb;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

public class UsbTools {

	static public long deviceId(UsbDevice device) {
		return device.getVendorId() << 16 | device.getProductId();
	}

	public static String getDeviceSerial(UsbManager m_manager, UsbDevice device) {
		UsbDeviceConnection connection = m_manager.openDevice(device);
		String result = connection.getSerial();
		connection.close();
		return result;
	}
}
