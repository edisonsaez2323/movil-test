package cl.autentia.usb;

import android.hardware.usb.UsbDevice;

import java.util.HashMap;
import java.util.Map;

public class UsbPermissionManager {

	public enum Permission {
		GRANTED, PENDING, DENIED, UNKNOWN
	};
	
	Map<Long, Permission> m_permissions = new HashMap<Long, Permission>();

	private static UsbPermissionManager m_instance;  
	
	private UsbPermissionManager() {
	}

	synchronized
	public static UsbPermissionManager getInstance() {
		if (m_instance == null) {
			m_instance = new UsbPermissionManager();
		}
		return m_instance;
	}
	
	synchronized
	public boolean permissionRequestPending() {
		for(Permission value : m_permissions.values()) {
			if (value.equals(Permission.PENDING))
				return true;
		}
		return false;
	}
	
	synchronized
	public Permission status(UsbDevice device) {
		long key = UsbTools.deviceId(device);
		if (m_permissions.containsKey(key))
			return m_permissions.get(key);
		return Permission.UNKNOWN;
	}

	synchronized
	public Permission updateStatus(UsbDevice device, Permission newStatus) {
		long key = UsbTools.deviceId(device);
		Permission currentStatus = status(device);
		if (currentStatus != newStatus) {
			m_permissions.put(key, newStatus);
		}
		return currentStatus;
	}
}
