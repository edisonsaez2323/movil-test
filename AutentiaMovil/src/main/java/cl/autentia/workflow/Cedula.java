
package cl.autentia.workflow;


public class Cedula {

    private int rut;
    private String dv;
    private int prioridad = 1;
    private String icon;
    private String title;
    private String colorprimary;
    private String colorprimarydark;
    private Boolean hiderut;
    private int timeout;
    private String orientacion;
    private String proposito;
    private int intentos;
    private String ok;
    private String nok;

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getNok() {
        return nok;
    }

    public void setNok(String nok) {
        this.nok = nok;
    }

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColorprimary() {
        return colorprimary;
    }

    public void setColorprimary(String colorprimary) {
        this.colorprimary = colorprimary;
    }

    public String getColorprimarydark() {
        return colorprimarydark;
    }

    public void setColorprimarydark(String colorprimarydark) {
        this.colorprimarydark = colorprimarydark;
    }

    public Boolean getHiderut() {
        return hiderut;
    }

    public void setHiderut(Boolean hiderut) {
        this.hiderut = hiderut;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }

    public String getProposito() {
        return proposito;
    }

    public void setProposito(String proposito) {
        this.proposito = proposito;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

}
