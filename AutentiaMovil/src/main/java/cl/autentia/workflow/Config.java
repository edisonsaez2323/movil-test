package cl.autentia.workflow;

public class Config {

    private boolean bmp;
    private String color;
    private String icono;
    private String titulo;

    public boolean isBmp() {
        return bmp;
    }

    public void setBmp(boolean bmp) {
        this.bmp = bmp;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
