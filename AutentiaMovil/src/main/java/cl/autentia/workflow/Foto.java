
package cl.autentia.workflow;

public class Foto {

    private int rut;
    private String dv;
    private int prioridad = 1;
    private boolean front;
    private String ok;
    private String nok;

    public boolean getFront() {
        return front;
    }

    public void setFront(boolean front) {
        this.front = front;
    }

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getNok() {
        return nok;
    }

    public void setNok(String nok) {
        this.nok = nok;
    }


}
