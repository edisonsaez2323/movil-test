
package cl.autentia.workflow;


public class Data {

    private Base base;
    private Cedula cedula;
    private Transunion transunion;
    private Evidencia evidencia;
    private Firma firma;
    private Infocedula infocedula;
    private Estadocedula estadocedula;
    private Foto foto;
    private Config config;

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Infocedula getInfocedula() {
        return infocedula;
    }

    public void setInfocedula(Infocedula infocedula) {
        this.infocedula = infocedula;
    }

    public Estadocedula getEstadocedula() {
        return estadocedula;
    }

    public void setEstadocedula(Estadocedula estadocedula) {
        this.estadocedula = estadocedula;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public Firma getFirma() {return firma;}

    public void setFirma(Firma firma){
        this.firma = firma;
    }

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public Cedula getCedula() {
        return cedula;
    }

    public void setCedula(Cedula cedula) {
        this.cedula = cedula;
    }

    public Transunion getTransunion() {
        return transunion;
    }

    public void setTransunion(Transunion transunion) {
        this.transunion = transunion;
    }

    public Evidencia getEvidencia() {
        return evidencia;
    }

    public void setEvidencia(Evidencia evidencia) {
        this.evidencia = evidencia;
    }

}
