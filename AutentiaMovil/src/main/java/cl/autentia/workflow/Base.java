
package cl.autentia.workflow;


public class Base {

    private int rut;
    private String dv;
    private int prioridad = 1;
    private String icon;
    private Boolean traspaso;
    private Boolean oti;
    private String title;
    private String urldocument;
    private Boolean hiderut;
    private String orientacion;
    private String proposito;
    private int intentos;
    private boolean previred;
    private String ok;
    private String nok;

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getNok() {
        return nok;
    }

    public void setNok(String nok) {
        this.nok = nok;
    }

    public boolean getPrevired() {
        return previred;
    }

    public void setPrevired(boolean previred) {
        this.previred = previred;
    }

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getTraspaso() {
        return traspaso;
    }

    public void setTraspaso(Boolean traspaso) {
        this.traspaso = traspaso;
    }

    public Boolean getOti() {
        return oti;
    }

    public void setOti(Boolean oti) {
        this.oti = oti;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getUrldocument() {
        return urldocument;
    }

    public void setUrldocument(String urldocument) {
        this.urldocument = urldocument;
    }

    public Boolean getHiderut() {
        return hiderut;
    }

    public void setHiderut(Boolean hiderut) {
        this.hiderut = hiderut;
    }

    public String getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }

    public String getProposito() {
        return proposito;
    }

    public void setProposito(String proposito) {
        this.proposito = proposito;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

}
