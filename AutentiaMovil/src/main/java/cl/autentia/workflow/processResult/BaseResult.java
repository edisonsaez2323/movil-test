package cl.autentia.workflow.processResult;

public class BaseResult {

    private int rut;
    private char dv;
    private boolean identidadVerificada;
    private String serialNumber;
    private String codigoAuditoria;
    private String nombre;
    private String fechaNac;
    private String tipoLector;
    private boolean enrolado;
    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String urlTraspaso;
    private String URL_OTI;
    private String identificacion_proveedor;
    private String fecha_verificacion;
    private String tipo_verificacion;

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public boolean isIdentidadVerificada() {
        return identidadVerificada;
    }

    public void setIdentidadVerificada(boolean identidadVerificada) {
        this.identidadVerificada = identidadVerificada;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCodigoAuditoria() {
        return codigoAuditoria;
    }

    public void setCodigoAuditoria(String codigoAuditoria) {
        this.codigoAuditoria = codigoAuditoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public boolean isEnrolado() {
        return enrolado;
    }

    public void setEnrolado(boolean enrolado) {
        this.enrolado = enrolado;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getUrlTraspaso() {
        return urlTraspaso;
    }

    public void setUrlTraspaso(String urlTraspaso) {
        this.urlTraspaso = urlTraspaso;
    }

    public String getURL_OTI() {
        return URL_OTI;
    }

    public void setURL_OTI(String URL_OTI) {
        this.URL_OTI = URL_OTI;
    }

    public String getIdentificacion_proveedor() {
        return identificacion_proveedor;
    }

    public void setIdentificacion_proveedor(String identificacion_proveedor) {
        this.identificacion_proveedor = identificacion_proveedor;
    }

    public String getFecha_verificacion() {
        return fecha_verificacion;
    }

    public void setFecha_verificacion(String fecha_verificacion) {
        this.fecha_verificacion = fecha_verificacion;
    }

    public String getTipo_verificacion() {
        return tipo_verificacion;
    }

    public void setTipo_verificacion(String tipo_verificacion) {
        this.tipo_verificacion = tipo_verificacion;
    }
}
