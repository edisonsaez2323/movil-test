package cl.autentia.workflow.processResult;

import android.os.Bundle;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class Pdf417Data {

    /*
     * RUN 9 ASCII Application Number 10 ASCII Apellido Paterno 30 ASCII Código
     * de País 3 ASCII Fecha de Vencimiento 6 ASCII Número de Serie 10 ASCII
     * Registro de Discapacidad 1 ASCII Tipo de Documento 1 ASCII Dedo
     * Codificado 4 ASCII Equivalente Tamao del Archivo PC1 4 ASCII Equivalente
     * Archivo PC1 342 Byte
     */
    public String appNumber, apPaterno, codigoPais, fechaVencimiento,
            numeroSerie, registroDiscapacidad, tipoDocumento;
    public byte[] PC1;
    public char dv;
    public int rut, dedoCodificado, sizePC1;

    public Pdf417Data(byte[] data) throws UnsupportedEncodingException {

        String dataString = new String(data, "ISO-8859-1");

        int largoMinimo = 9 + 10 + 30 + 3 + 6 + 10 + 1 + 1 + 4 + 4;

        if (dataString.length() >= largoMinimo) {
            int p = 0;
            String fullRut = dataString.substring(p, p + 9).trim();
            p = p + 9;
            rut = Integer.parseInt(fullRut.substring(0, fullRut.length() - 1));
            dv = fullRut.charAt(fullRut.length() - 1);
            appNumber = dataString.substring(p, p + 10).trim();
            p = p + 10;
            apPaterno = dataString.substring(p, p + 30).trim();
            p = p + 30;
            codigoPais = dataString.substring(p, p + 3).trim();
            p = p + 3;
            fechaVencimiento = dataString.substring(p, p + 6).trim();
            p = p + 6;
            numeroSerie = dataString.substring(p, p + 10).trim();
            p = p + 10;
            registroDiscapacidad = dataString.substring(p, p + 1);
            p = p + 1;
            tipoDocumento = dataString.substring(p, p + 1);
            p = p + 1;
            dedoCodificado = byteToInt(data, p, p + 4);
            p = p + 4;
            sizePC1 = byteToInt(data, p, p + 4);

            PC1 = new byte[sizePC1];

            int j = 0;
            for (int i = largoMinimo; i < data.length && j < sizePC1; i++) {
                PC1[j] = data[i];
                j++;
            }
        }
    }

    private static int byteToInt(byte[] arr, int start, int end) {
        // create a byte buffer and wrap the array

        ByteBuffer bb = ByteBuffer.wrap(Arrays.copyOfRange(arr, start, end));

        // if the file uses little endian as apposed to network
        // (big endian, Java's native) format,
        // then set the byte order of the ByteBuffer
        bb.order(ByteOrder.LITTLE_ENDIAN);

        // read your integers using ByteBuffer's getInt().
        // four bytes converted into an integer!
        return bb.getInt();

    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString("appNumber", appNumber);
        bundle.putString("apPaterno", apPaterno);
        bundle.putString("codigoPais", codigoPais);
        bundle.putString("fechaVencimiento", fechaVencimiento);
        bundle.putString("numeroSerie", numeroSerie);
        bundle.putString("registroDiscapacidad", registroDiscapacidad);
        bundle.putString("tipoDocumento", tipoDocumento);
        bundle.putChar("dv", dv);
        bundle.putInt("rut", rut);
        bundle.putInt("dedoCodificado", dedoCodificado);
        bundle.putInt("sizePC1", sizePC1);
        bundle.putString("PC1", Base64.encodeToString(PC1, Base64.DEFAULT));
        return bundle;
    }

}
