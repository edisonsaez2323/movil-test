package cl.autentia.workflow.processResult;

public class InfoCedulaResult {

    private int rut;
    private char dv;
    private String nombres;
    private String apellidos;
    private String fechaNac;
    private String tipoLector;
    private String fecha_vencimiento;
    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String tipo;
    private String mrz;
    private String tipo_doc;
    private String serie;
    private String pais_emi;
    private String sexo;
    private String nacionalidad;
    private String nacio_en;
    private int bit1;
    private int bit2;
    private byte[] firma;
    private byte[] retrato;

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMrz() {
        return mrz;
    }

    public void setMrz(String mrz) {
        this.mrz = mrz;
    }

    public String getTipo_doc() {
        return tipo_doc;
    }

    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getPais_emi() {
        return pais_emi;
    }

    public void setPais_emi(String pais_emi) {
        this.pais_emi = pais_emi;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getNacio_en() {
        return nacio_en;
    }

    public void setNacio_en(String nacio_en) {
        this.nacio_en = nacio_en;
    }

    public int getBit1() {
        return bit1;
    }

    public void setBit1(int bit1) {
        this.bit1 = bit1;
    }

    public int getBit2() {
        return bit2;
    }

    public void setBit2(int bit2) {
        this.bit2 = bit2;
    }

    public byte[] getFirma() {
        return firma;
    }

    public void setFirma(byte[] firma) {
        this.firma = firma;
    }

    public byte[] getRetrato() {
        return retrato;
    }

    public void setRetrato(byte[] retrato) {
        this.retrato = retrato;
    }
}
