package cl.autentia.workflow.processResult;

public class FirmaResult {

    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String SIGNATURE_TIF;
    private String SIGNATURE_PNG;
    private String numeroAuditoria;

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getSIGNATURE_TIF() {
        return SIGNATURE_TIF;
    }

    public void setSIGNATURE_TIF(String SIGNATURE_TIF) {
        this.SIGNATURE_TIF = SIGNATURE_TIF;
    }

    public String getSIGNATURE_PNG() {
        return SIGNATURE_PNG;
    }

    public void setSIGNATURE_PNG(String SIGNATURE_PNG) {
        this.SIGNATURE_PNG = SIGNATURE_PNG;
    }

    public String getNumeroAuditoria() {
        return numeroAuditoria;
    }

    public void setNumeroAuditoria(String numeroAuditoria) {
        this.numeroAuditoria = numeroAuditoria;
    }
}
