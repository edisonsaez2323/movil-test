package cl.autentia.workflow.processResult;

public class TransunionResult {

    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String identidadVerificada;
    private String serialNumber;
    private String codigoAuditoria;
    private int rut;
    private char dv;

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getIdentidadVerificada() {
        return identidadVerificada;
    }

    public void setIdentidadVerificada(String identidadVerificada) {
        this.identidadVerificada = identidadVerificada;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCodigoAuditoria() {
        return codigoAuditoria;
    }

    public void setCodigoAuditoria(String codigoAuditoria) {
        this.codigoAuditoria = codigoAuditoria;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }
}
