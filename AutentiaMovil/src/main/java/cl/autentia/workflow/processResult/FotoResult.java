package cl.autentia.workflow.processResult;

public class FotoResult {

    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String numeroAuditoria;
    private String BASE64_IMAGEN;

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getNumeroAuditoria() {
        return numeroAuditoria;
    }

    public void setNumeroAuditoria(String numeroAuditoria) {
        this.numeroAuditoria = numeroAuditoria;
    }

    public String getBASE64_IMAGEN() {
        return BASE64_IMAGEN;
    }

    public void setBASE64_IMAGEN(String BASE64_IMAGEN) {
        this.BASE64_IMAGEN = BASE64_IMAGEN;
    }
}
