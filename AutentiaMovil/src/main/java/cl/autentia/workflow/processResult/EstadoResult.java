package cl.autentia.workflow.processResult;

public class EstadoResult {

    private String ESTADO;
    private int CODIGO_RESPUESTA;
    private String DESCRIPCION;
    private String estado_cedula;

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public int getCODIGO_RESPUESTA() {
        return CODIGO_RESPUESTA;
    }

    public void setCODIGO_RESPUESTA(int CODIGO_RESPUESTA) {
        this.CODIGO_RESPUESTA = CODIGO_RESPUESTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getEstado_cedula() {
        return estado_cedula;
    }

    public void setEstado_cedula(String estado_cedula) {
        this.estado_cedula = estado_cedula;
    }
}
