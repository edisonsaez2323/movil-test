package cl.autentia.workflow.processResult;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import java.io.UnsupportedEncodingException;
import java.util.Set;

public class QrData {

    private String run;
    private String type;
    private String serial;
    private String mrz;
    public String getDedoCodificado() {
        return dedoCodificado;
    }

    public void setDedoCodificado(String dedoCodificado) {
        this.dedoCodificado = dedoCodificado;
    }

    private String dedoCodificado;

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMrz() {
        return mrz;
    }

    public void setMrz(String mrz) {
        this.mrz = mrz;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public QrData(String data) throws UnsupportedEncodingException {

        Uri uri = Uri.parse(data);
        Set<String> params = uri.getQueryParameterNames();

        for (String param : params) {
            if ("RUN".equalsIgnoreCase(param))
                run = uri.getQueryParameter(param);
            else if ("type".equalsIgnoreCase(param))
                type = uri.getQueryParameter(param);
            else if ("serial".equalsIgnoreCase(param))
                serial = uri.getQueryParameter(param);
            else if ("mrz".equalsIgnoreCase(param))
                mrz = uri.getQueryParameter(param);

        }
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        // run, type, serial, mrz
        String rut[] = run.split("-");
        bundle.putInt("rut", Integer.parseInt(rut[0]));
        bundle.putChar("dv", rut[1].charAt(0));
        bundle.putString("tipoDocumento", type);
        bundle.putString("numeroSerie", serial);
        bundle.putString("mrz", mrz);
        return bundle;
    }
}
