package cl.autentia.workflow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.acepta.Utils;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.json.JSONObject;

import java.util.HashMap;

import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GenAuditResp;
import cl.autentia.http.parameters.GetInfoAuditResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.workflow.processResult.BarcodeResult;
import cl.autentia.workflow.processResult.BaseResult;
import cl.autentia.workflow.processResult.CedulaResult;
import cl.autentia.workflow.processResult.EstadoResult;
import cl.autentia.workflow.processResult.EvidenciaResult;
import cl.autentia.workflow.processResult.FirmaResult;
import cl.autentia.workflow.processResult.FotoResult;
import cl.autentia.workflow.processResult.InfoCedulaResult;
import cl.autentia.workflow.processResult.TransunionResult;

//import cl.autentia.common.ReturnCode;
//import cl.autentia.common.Status;
//import cl.autentia.helper.AutentiaMovilException;

@EActivity
public class Workflow extends AppCompatActivity {

    private static final int REQUEST_BASE = 0x0001;
    private static final int REQUEST_CEDULA = 0x0002;
    private static final int REQUEST_EVIDENCIA = 0x0003;
    private static final int REQUEST_TRANSUNION = 0x0004;
    private static final int REQUEST_BARCODE_CEDULA = 0x0005;
    private static final int REQUEST_FIRMA = 0x0006;
    private static final int REQUEST_INFO_CEDULA = 0x0007;
    private static final int REQUEST_ESTADO_CEDULA = 0x0008;
    private static final int REQUEST_FOTO = 0x0009;
    private static final int REQUEST_BARCODE_ESTADO = 0x0010;
    private static final int REQUEST_BARCODE_INFO = 0x00011;

    @Extra("data")
    String data = "";

    Data dataValue;
    HashMap<Integer, String> orden = new HashMap<>();
    String lastAudit = "";
    String mIcon = "";
    String mColor = "";
    String mTitulo = "";
    boolean mBmp = false;
    String mBMPAudit = "";

    //result
    BaseResult baseResult;
    CedulaResult CResult;
    EvidenciaResult evidenciaResult;
    EstadoResult estadoResult;
    FirmaResult firmaResult;
    FotoResult fotoResult;
    InfoCedulaResult infoCedulaResult;
    TransunionResult tuResult;
    BarcodeResult barcodeResult;

    Bundle blGlobal = new Bundle();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterExtras
    void afterExtras() {


        try {

            dataValue = new Gson().fromJson(data, Data.class);
            Log.e("data", data);
            String first = "";

            Base base = dataValue.getBase();
            if (base != null && base.getPrioridad() != null && base.getPrioridad() == 0) {
                first = "base";

            }
            Cedula cedula = dataValue.getCedula();
            if (cedula != null && cedula.getPrioridad() != null && cedula.getPrioridad() == 0) {
                first = "cedula";
            }
            Evidencia evidencia = dataValue.getEvidencia();
            if (evidencia != null && evidencia.getPrioridad() != null && cedula.getPrioridad() == 0) {
                first = "evidencia";
            }
            Transunion transunion = dataValue.getTransunion();
            if (transunion != null && transunion.getPrioridad() != null && transunion.getPrioridad() == 0) {
                first = "transunion";
            }
            Firma firma = dataValue.getFirma();
            if (firma != null && firma.getPrioridad() != null && firma.getPrioridad() == 0) {
                first = "firma";
            }
            Estadocedula estadocedula = dataValue.getEstadocedula();
            if (estadocedula != null && estadocedula.getPrioridad() != null && estadocedula.getPrioridad() == 0) {
                first = "estadocedula";
            }
            Foto foto = dataValue.getFoto();
            if (foto != null && foto.getPrioridad() != null && foto.getPrioridad() == 0) {
                first = "foto";
            }
            Infocedula infocedula = dataValue.getInfocedula();
            if (infocedula != null && infocedula.getPrioridad() != null && infocedula.getPrioridad() == 0) {
                first = "infocedula";
            }

            Config config = dataValue.getConfig();
            if (config != null) {
                try {
                    mBmp = config.isBmp();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mColor = TextUtils.isEmpty(config.getColor()) ? "" : config.getColor();
                mIcon = TextUtils.isEmpty(config.getIcono()) ? "" : config.getIcono();
                mTitulo = TextUtils.isEmpty(config.getTitulo()) ? "" : config.getTitulo();
            }

            validarRut(base, evidencia, firma, transunion, cedula, first);


        } catch (Exception e) {
            finishActivityWithError(Status.ERROR, ReturnCode.INVALID_JSON.getCode(), "Error en json de entrada");
            e.printStackTrace();
        }

    }

    private void validarRut(Base base, Evidencia evidencia, Firma firma, Transunion transunion, Cedula cedula, String first) {

        try {
            if (base != null) {
                if (base.getRut() != null && !base.getRut().equals(0) && base.getDv() != null && !base.getDv().equals("0")) {
                    if (!Utils.validaRutDV(base.getRut(), base.getDv().charAt(0))) {
                        finishActivityWithError("NO_OK", 201, "Rut invalido");
                    }
                }
            }
            if (evidencia != null) {
                if (evidencia.getRut() != null && !evidencia.getRut().equals(0) && evidencia.getDv() != null && !evidencia.getDv().equals("0")) {
                    if (!Utils.validaRutDV(evidencia.getRut(), evidencia.getDv().charAt(0))) {
                        finishActivityWithError("NO_OK", 201, "Rut invalido");
                    }
                }
            }
            if (firma != null) {
                if (firma.getRut() != null && !firma.getRut().equals(0) && firma.getDv() != null && !firma.getDv().equals("0")) {
                    if (!Utils.validaRutDV(firma.getRut(), firma.getDv().charAt(0))) {
                        finishActivityWithError("NO_OK", 201, "Rut invalido");
                    }
                }
            }
            if (transunion != null) {
                if (transunion.getRut() != null && !transunion.getRut().equals(0) && transunion.getDv() != null && !transunion.getDv().equals("0")) {
                    if (!Utils.validaRutDV(transunion.getRut(), transunion.getDv().charAt(0))) {
                        finishActivityWithError("NO_OK", 201, "Rut invalido");
                    }
                }
            }
            if (cedula != null) {
                if (cedula.getRut() != null && !cedula.getRut().equals(0) && cedula.getDv() != null && !cedula.getDv().equals("0")) {
                    if (!Utils.validaRutDV(cedula.getRut(), cedula.getDv().charAt(0))) {
                        finishActivityWithError("NO_OK", 201, "Rut invalido");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        processWorkflow(first);

    }

    private void processWorkflow(String next) throws NullPointerException {

//        Log.e("CURRENT", String.valueOf(orden.entrySet().iterator().next().getValue()));

//        switch (orden.entrySet().iterator().next().getValue()) {
        switch (next) {

            case "base":
                Base base = dataValue.getBase();
                if (base != null) {

                    Intent intent = new Intent();
                    intent.setAction("cl.autentia.operacion.VERIFICAR_IDENTIDAD");

                    intent.putExtra("RUT", base.getRut());
                    intent.putExtra("DV", base.getDv().charAt(0));
                    if (base.getIntentos() != null) {
                        intent.putExtra("INTENTOS", base.getIntentos());
                    }
                    if (!mColor.equals("")) {
                        intent.putExtra("COLOR_PRIMARY", mColor);
                    }
                    if (base.getUrldocument() != null) {
                        intent.putExtra("URL_DOCUMENT", base.getUrldocument());
                    }
                    if (!mTitulo.equals("")) {
                        intent.putExtra("TITLE", mTitulo);
                    }
                    if (!mIcon.equals("")) {
                        intent.putExtra("ICON", Base64.decode(mIcon, Base64.DEFAULT));
                    }
                    if (base.getPrevired()) {
                        intent.putExtra("PREVIRED", base.getPrevired());
                    }
                    try {
                        if (base.getTraspaso()) {
                            intent.putExtra("TRASPASO", base.getTraspaso());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (base.getOti() != null) {
                            intent.putExtra("OTI", base.getOti());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (base.getOrientacion() != null) {
                        String orientacion = base.getOrientacion().toUpperCase();
                        intent.putExtra("ORIENTACION", orientacion);
                    }
                    if (base.getProposito() != null && !base.getProposito().equals("")) {
                        intent.putExtra("PROPOSITO", base.getProposito());
                    }
                    startActivityForResult(intent, REQUEST_BASE);
                }

                break;

            case "cedula":

                Intent intentB = new Intent();
                intentB.setAction("cl.autentia.operacion.BARCODE");
                startActivityForResult(intentB, REQUEST_BARCODE_CEDULA);

                break;

            case "evidencia":
                Evidencia evidencia = dataValue.getEvidencia();
                Intent intentE = new Intent();
                intentE.setAction("cl.autentia.operacion.CAPTURAR_MULTIPLES_HUELLAS");
                intentE.putExtra("RUT", evidencia.getRut());
                intentE.putExtra("DV", evidencia.getDv().charAt(0));
                startActivityForResult(intentE, REQUEST_EVIDENCIA);

                break;

            case "firma":
                Firma firma = dataValue.getFirma();

                Intent intent = new Intent();
                intent.setAction("cl.autentia.operacion.FIRMA_HOLOGRAFICA");
                intent.putExtra("RUT", firma.getRut());
                intent.putExtra("DV", firma.getDv().charAt(0));
                if (!mColor.equals("")) {
                    intent.putExtra("COLOR_PRIMARY", mColor);
                }
                if (!mIcon.equals("")) {
                    intent.putExtra("ICON", Base64.decode(mIcon, Base64.DEFAULT));
                }
                if (firma.getOrientacion() != null && !TextUtils.isEmpty(firma.getOrientacion())) {
                    String orientacion = firma.getOrientacion().toUpperCase();
                    intent.putExtra("ORIENTATION", orientacion);
                }
                if (firma.getTerminos() != null && !TextUtils.isEmpty(firma.getTerminos())) {
                    intent.putExtra("TERMS", firma.getTerminos());
                }
                startActivityForResult(intent, REQUEST_FIRMA);
                break;
            case "foto":
                Foto foto = dataValue.getFoto();
                Intent intentFoto = new Intent();
                intentFoto.setAction("cl.autentia.operacion.CAPTURAR_FOTOGRAFIA");
                intentFoto.putExtra("RUT", foto.getRut());
                intentFoto.putExtra("DV", foto.getDv().charAt(0));
                try {
                    if (foto.getFront()) {
                        intentFoto.putExtra("FRONT_CAMERA", true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                startActivityForResult(intentFoto, REQUEST_FOTO);
                break;
            case "infocedula":
                Intent intentInfo = new Intent();
                intentInfo.setAction("cl.autentia.operacion.BARCODE");
                startActivityForResult(intentInfo, REQUEST_BARCODE_INFO);
                break;
            case "estadocedula":
                Intent intentEstado = new Intent();
                intentEstado.setAction("cl.autentia.operacion.BARCODE");
                startActivityForResult(intentEstado, REQUEST_BARCODE_ESTADO);
                break;

            case "transunion":
                Transunion transunion = dataValue.getTransunion();
                Intent intentT = new Intent();
                intentT.setAction("cl.autentia.operacion.TRANSUNION");
                intentT.putExtra("RUT", transunion.getRut());
                intentT.putExtra("DV", transunion.getDv().charAt(0));

                if (transunion.getNs() != null && !transunion.getNs().equals("")) {
                    intentT.putExtra("NS", transunion.getNs());
                }
                if (!transunion.getId().equals("")) {
                    intentT.putExtra("ID", transunion.getId());
                }
                startActivityForResult(intentT, REQUEST_TRANSUNION);
                break;

        }


    }


    void finishActivityWithError(String status, int resultCode, String descripcion) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("CODIGO_RESPUESTA", resultCode);
        returnIntent.putExtra("ESTADO", status);
        returnIntent.putExtra("DESCRIPCION", descripcion);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @OnActivityResult(value = REQUEST_BASE)
    void onBaseResult(int resultCode, Intent data) {
        Base base = dataValue.getBase();

        Log.e("result", data.getExtras().toString());

        if (resultCode == RESULT_OK) {

            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
            if (base.getOk().equalsIgnoreCase("FIN")) {
                    baseResult = new Gson().fromJson(Utils.bundleToJsonString(data), BaseResult.class);
                    lastAudit = baseResult.getCodigoAuditoria();
                    try {
                        blGlobal.putAll(data.getExtras());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    createWorkflowAudit();
                } else {
                    baseResult = new Gson().fromJson(Utils.bundleToJsonString(data), BaseResult.class);
                    processWorkflow(base.getOk());
                }

            } else if (data.getExtras().get("CODIGO_RESPUESTA").equals(903)) {
                if (base.getNok().equalsIgnoreCase("FIN")) {
                    Intent intent = new Intent();
                    intent.putExtras(data.getExtras());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    processWorkflow(base.getNok());
                }
            } else {
                Intent intent = new Intent();
                intent.putExtras(data.getExtras());
                setResult(RESULT_OK, intent);
                finish();
            }
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    void finishWorkFlow(String auditoria) {

        Bundle clBundle = getCleanBundle(blGlobal);

        Intent intent = new Intent();
        if (clBundle != null) {
            intent.putExtras(clBundle);
        }
        intent.putExtra("CODIGO_RESPUESTA", 200);
        intent.putExtra("DESCRIPCION", "Proceso finalizado correctamente");
        intent.putExtra("ESTADO", Status.OK);
        intent.putExtra("auditoriaFlujo", auditoria);
        setResult(RESULT_OK, intent);
        finish();

    }

    private Bundle getCleanBundle(Bundle blGlobal) {

        blGlobal.remove("codigoAuditoria");
        return blGlobal;
    }

    @OnActivityResult(value = REQUEST_BARCODE_CEDULA)
    void onBarcodeResult(int resultCode, Intent data) {
        Cedula cedula = dataValue.getCedula();
        if (resultCode == RESULT_OK) {

            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                Bundle bl = data.getExtras();

                String barcode = bl.getString("barcode");
                barcodeResult = new Gson().fromJson(String.format("{\"barcode\":\"%s\"}", barcode), BarcodeResult.class);

                Intent intentC = new Intent();
                intentC.setAction("cl.autentia.operacion.VERIFICAR_IDENTIDAD");
                if (cedula.getRut() != null) {
                    intentC.putExtra("RUT", cedula.getRut());
                } else {
                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(Status.NO_OK,
                            ReturnCode.RUT_INVALIDO));
                }
                if (cedula.getDv() != null) {
                    intentC.putExtra("DV", cedula.getDv().charAt(0));
                } else {
                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(Status.NO_OK,
                            ReturnCode.RUT_INVALIDO));
                }
                if (!Utils.validaRutDV(cedula.getRut(), cedula.getDv().charAt(0))) {

                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(Status.NO_OK,
                            ReturnCode.RUT_INVALIDO));

                }
                if (!mTitulo.equals("")) {
                    intentC.putExtra("TITLE", mTitulo);
                }
                if (!mIcon.equals("")) {
                    intentC.putExtra("ICON", Base64.decode(mIcon, Base64.DEFAULT));
                }
                intentC.putExtra("BARCODE", barcode);
//                Log.e("order-current", String.valueOf(orden.entrySet().iterator().next().getValue()));
                startActivityForResult(intentC, REQUEST_CEDULA);

            } else {
                processWorkflow(cedula.getNok());
            }
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    @OnActivityResult(value = REQUEST_CEDULA)
    void onCedulaResult(int resultCode, Intent data) {
        Cedula cedula = dataValue.getCedula();
//
//        Log.e("response_codigo", String.valueOf(data.getExtras().getInt("CODIGO_RESPUESTA")));
//        Log.e("response_descripcion", String.valueOf(data.getExtras().getInt("DESCRIPCION")));

        if (resultCode == RESULT_OK) {

            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (cedula.getOk().equalsIgnoreCase("FIN")) {
                    try {
                        CResult = new Gson().fromJson(Utils.bundleToJsonString(data), CedulaResult.class);
                        lastAudit = CResult.getCodigoAuditoria();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        blGlobal.putAll(data.getExtras());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    createWorkflowAudit();
                } else {
                    CResult = new Gson().fromJson(Utils.bundleToJsonString(data), CedulaResult.class);
                    processWorkflow(cedula.getOk());
                }
            } else {
                if (cedula.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
//                    CResult = new Gson().fromJson(Utils.bundleToJsonString(data), CedulaResult.class);
                    processWorkflow(cedula.getNok());
                }
            }
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    @OnActivityResult(value = REQUEST_EVIDENCIA)
    void onEvidenciaResult(int resultCode, Intent data) {
        Evidencia evidencia = dataValue.getEvidencia();

        if (resultCode == RESULT_OK) {

            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (evidencia.getOk().equalsIgnoreCase("FIN")) {
                    try {
                        EvidenciaResult evidenciaResult = new Gson().fromJson(Utils.bundleToJsonString(data), EvidenciaResult.class);
                        lastAudit = evidenciaResult.getCodigoAuditoria();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        blGlobal.putAll(data.getExtras());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    createWorkflowAudit();
                } else {
                    evidenciaResult = new Gson().fromJson(Utils.bundleToJsonString(data), EvidenciaResult.class);
                    processWorkflow(evidencia.getOk());
                }
            } else {
                if (evidencia.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
                    evidenciaResult = new Gson().fromJson(Utils.bundleToJsonString(data), EvidenciaResult.class);
                    processWorkflow(evidencia.getNok());
                }

            }
        } else {
            if (evidencia.getNok().equalsIgnoreCase("FIN")) {
                Intent intent = new Intent();
                intent.putExtras(data.getExtras());
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }

        }

    }

    @OnActivityResult(value = REQUEST_TRANSUNION)
    void onTransunionResult(int resultCode, Intent data) {
        Transunion transunion = dataValue.getTransunion();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (transunion.getOk().equalsIgnoreCase("FIN")) {
                    Intent intent = new Intent();
                    intent.putExtras(data.getExtras());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    tuResult = new Gson().fromJson(Utils.bundleToJsonString(data), TransunionResult.class);
                    processWorkflow(transunion.getNok());
                }

            } else {
                if (transunion.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        try {
                            blGlobal.putAll(data.getExtras());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
                    tuResult = new Gson().fromJson(Utils.bundleToJsonString(data), TransunionResult.class);
                    processWorkflow(transunion.getNok());
                }
            }
        } else {
            if (transunion.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
//                intent.putExtra("auditoriaEvidencia", lastAudit);
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @OnActivityResult(value = REQUEST_FIRMA)
    void onFirmaResult(int resultCode, Intent data) {
        Firma firma = dataValue.getFirma();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (firma.getOk().equalsIgnoreCase("FIN")) {
                    Intent intent = new Intent();
                    intent.putExtras(data.getExtras());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    firmaResult = new Gson().fromJson(Utils.bundleToJsonString(data), FirmaResult.class);
                    processWorkflow(firma.getNok());
                }

            } else {
                if (firma.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        try {
                            blGlobal.putAll(data.getExtras());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
                    firmaResult = new Gson().fromJson(Utils.bundleToJsonString(data), FirmaResult.class);
                    processWorkflow(firma.getNok());
                }
            }
        } else {
            if (firma.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }


    @OnActivityResult(value = REQUEST_BARCODE_INFO)
    void onBarcodeInfoResult(int resultCode, Intent data) {
        Infocedula infocedula = dataValue.getInfocedula();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {

                Intent intent = new Intent();
                intent.setAction("cl.autentia.operacion.INFO_NFC");
                intent.putExtra("BARCODE", data.getExtras().getString("barcode"));
                intent.putExtra("READ_EXTENDED_INFO", true);
                intent.putExtra("TIMEOUT", 20);
                startActivityForResult(intent, REQUEST_INFO_CEDULA);

            } else {
                if (infocedula.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
                    firmaResult = new Gson().fromJson(Utils.bundleToJsonString(data), FirmaResult.class);
                    processWorkflow(infocedula.getNok());
                }
            }
        } else {
            if (infocedula.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @OnActivityResult(value = REQUEST_INFO_CEDULA)
    void onInfoResult(int resultCode, Intent data) {
        Infocedula infocedula = dataValue.getInfocedula();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (infocedula.getOk().equalsIgnoreCase("FIN")) {
                    createWorkflowAudit();
                } else {
                    infoCedulaResult = new Gson().fromJson(Utils.bundleToJsonString(data), InfoCedulaResult.class);
                    processWorkflow(infocedula.getNok());
                }

            } else {
                if (infocedula.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        try {
                            blGlobal.putAll(data.getExtras());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                } else {
                    infoCedulaResult = new Gson().fromJson(Utils.bundleToJsonString(data), InfoCedulaResult.class);
                    processWorkflow(infocedula.getNok());
                }
            }
        } else {
            if (infocedula.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @OnActivityResult(value = REQUEST_FOTO)
    void onFotoResult(int resultCode, Intent data) {
        Foto foto = dataValue.getFoto();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (foto.getOk().equalsIgnoreCase("FIN")) {
                    createWorkflowAudit();
                } else {
                    fotoResult = new Gson().fromJson(Utils.bundleToJsonString(data), FotoResult.class);
                    processWorkflow(foto.getOk());
                }

            } else {
                if (foto.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        try {
                            blGlobal.putAll(data.getExtras());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }

                } else {
                    fotoResult = new Gson().fromJson(Utils.bundleToJsonString(data), FotoResult.class);
                    processWorkflow(foto.getNok());
                }
            }
        } else {
            if (foto.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @OnActivityResult(value = REQUEST_BARCODE_ESTADO)
    void onBarcodeEstadoResult(int resultCode, Intent data) {
        Estadocedula estadocedula = dataValue.getEstadocedula();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {

                Intent intent = new Intent();
                intent.setAction("cl.autentia.operacion.ESTADO_CEDULA");
                intent.putExtra("BARCODE", data.getExtras().getString("barcode"));
                startActivityForResult(intent, REQUEST_ESTADO_CEDULA);

            } else {
                if (estadocedula.getNok().equalsIgnoreCase("FIN")) {
                    Intent intent = new Intent();
                    intent.putExtras(data.getExtras());
                    setResult(RESULT_CANCELED, intent);
                    finish();
                } else {
                    firmaResult = new Gson().fromJson(Utils.bundleToJsonString(data), FirmaResult.class);
                    processWorkflow(estadocedula.getNok());
                }
            }
        } else {
            if (estadocedula.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @OnActivityResult(value = REQUEST_ESTADO_CEDULA)
    void onEstadoResult(int resultCode, Intent data) {
        Estadocedula estadocedula = dataValue.getEstadocedula();

        if (resultCode == RESULT_OK) {
            if (data.getExtras().get("CODIGO_RESPUESTA").equals(200)) {
                if (estadocedula.getOk().equalsIgnoreCase("FIN")) {
//                    Intent intent = new Intent();
//                    intent.putExtras(data.getExtras());
//                    setResult(RESULT_OK, intent);
//                    finish();
                    try {
                        blGlobal.putAll(data.getExtras());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    createWorkflowAudit();
                } else {
                    estadoResult = new Gson().fromJson(Utils.bundleToJsonString(data), EstadoResult.class);
                    processWorkflow(estadocedula.getOk());
                }

            } else {
                if (estadocedula.getNok().equalsIgnoreCase("FIN")) {
                    if (!lastAudit.equals("")) {
                        createWorkflowAudit();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtras(data.getExtras());
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
//
                } else {
                    estadoResult = new Gson().fromJson(Utils.bundleToJsonString(data), EstadoResult.class);
                    processWorkflow(estadocedula.getNok());
                }
            }
        } else {
            if (estadocedula.getNok().equalsIgnoreCase("FIN")) {
//                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }

    }

    @Background
    void getBmpFromAudit() {
        AutentiaWSClient mAutentiaWSClient = new AutentiaWSClient(this);
        AutentiaPreferences mPreferences = new AutentiaPreferences(this);

        try {

            mAutentiaWSClient.getStatusAudit("AUTE-QAEV-6WGD-P2FB", mPreferences.getString(KeyPreferences.INSTITUTION), mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {

                    GetInfoAuditResp gir = new Gson().fromJson(result.getString("result"), GetInfoAuditResp.class);
                    mBMPAudit = gir.getBmp();

                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201) {
                        errorHandlerMethod("", result);
                    } else errorHandlerMethod(Status.ERROR, result);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Background
    void createWorkflowAudit() {
        AutentiaWSClient mAutentiaWSClient = new AutentiaWSClient(this);
        AutentiaPreferences mPreferences = new AutentiaPreferences(this);
        String mBitmap = "";

        String versionApp = null;
        JSONObject jsonObject = new JSONObject();
        try {
            versionApp = AppHelper.getCurrentVersion(this);
            String[] rut = getRut();

            jsonObject.put("type", "FLUJO");
            jsonObject.put("rut", String.valueOf(rut[0]));
            jsonObject.put("dv", String.valueOf(rut[1]));
            jsonObject.put("versionApp", versionApp);
            jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
            jsonObject.put("ip", DeviceHelper.getIPAddress(true));
            jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
            jsonObject.put("imei", DeviceHelper.getIMEI(this));
            jsonObject.put("proposito", "1");
            jsonObject.put("operacion", "FLUJO");

            String mSerialNumber = null;
            String mReaderType = null;

            try {
                mSerialNumber = mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
                mReaderType = mPreferences.getString("mReaderType");
            } catch (Exception e) {
                HttpUtil.sendReportUncaughtException(e, mPreferences.getString(KeyPreferences.INSTITUTION), this);
            }

//            if (mBmp && !lastAudit.equals("")){
//                getBmpFromAudit();
//                JSONObject jBmp = new JSONObject();
//                jBmp.put("bitmap",mBMPAudit);
//                mBitmap = jBmp.toString();
//            }

            //Obtención número de auditoria
            mAutentiaWSClient.genAudit(new byte[0],
                    "wsq",
                    String.format("%s-%s", rut[0], rut[1]),
                    "10",
                    mReaderType,
                    mSerialNumber,
                    new byte[0],
                    "0000",
                    "",
                    jsonObject.toString(),
                    mPreferences.getString(KeyPreferences.RUN_INSTITUTION),
                    versionApp,
                    "sin datos",
                    "0",
                    data,
                    mBitmap,
                    mPreferences.getString(KeyPreferences.INSTITUTION), false,new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
//                            Log.d(TAG, "Create Audit succeed");
                            GenAuditResp gar = new Gson().fromJson(result.getString("result"), GenAuditResp.class);
                            finishWorkFlow(gar.getNroAuditoria());
                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            Log.e("FLUJO", "Create Audit HERROR");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String[] getRut() {
        Base base = dataValue.getBase();
        Cedula cedula = dataValue.getCedula();
        Evidencia evidencia = dataValue.getEvidencia();
        Transunion transunion = dataValue.getTransunion();
        Firma firma = dataValue.getFirma();

        if (base != null) {
            String[] mRut = new String[2];
            mRut[0] = base.getRut().toString();
            mRut[1] = base.getDv();

            return mRut;
        } else if (cedula != null) {
            String[] mRut = new String[2];
            mRut[0] = cedula.getRut().toString();
            mRut[1] = cedula.getDv();
            return mRut;
        } else if (evidencia != null) {
            String[] mRut = new String[2];
            mRut[0] = evidencia.getRut().toString();
            mRut[1] = evidencia.getDv();
            return mRut;
        } else if (transunion != null) {
            String[] mRut = new String[2];
            mRut[0] = transunion.getRut().toString();
            mRut[1] = transunion.getDv();
            return mRut;
        } else if (firma != null) {
            String[] mRut = new String[2];
            mRut[0] = firma.getRut().toString();
            mRut[1] = firma.getDv();
            return mRut;
        }

        return null;
    }

    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        Log.e("ameRC", "" + ame.returnCode.getCode());
        Log.e("INVALID_TOKENRC", "" + ReturnCode.INVALID_TOKEN.getCode());

        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            default:
                if (ame.returnCode.getCode() == 9000) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status);
                }
                if (ame.returnCode.getCode() == 5007) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status);
                } else {
                    Log.d("status.ERROR", Status.ERROR);
                    Log.d("getCode()", "" + ReturnCode.ERROR_GENERICO.getCode());
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status);
                }
                break;
        }
    }
}
