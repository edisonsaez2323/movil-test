package cl.autentia.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.acepta.android.fetdroid.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cl.autentia.activity.VerificationActivity;

public class ViewPagerAdapter extends PagerAdapter {

    private List<Bitmap> bmpList;
    private Context mContext;
    private ViewPager viewPager;

    public ViewPagerAdapter(Context context, List<Bitmap> bmpList, ViewPager viewPager) {
        this.bmpList = bmpList;
        this.mContext = context;
        this.viewPager= viewPager;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image_slide_layout, null);
        ImageView imageView = view.findViewById(R.id.hand_image);
        imageView.setImageBitmap(bmpList.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return bmpList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    public void moveHands() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                viewPager.setCurrentItem(0, true);
                ((VerificationActivity) mContext).hideLeftArrow();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        viewPager.setCurrentItem(1, true);
                        ((VerificationActivity) mContext).hideRightArrow();
                    }
                };
                timer.schedule(task, 1000);
            }
        };
        timer.schedule(task, 1500);
    }

}