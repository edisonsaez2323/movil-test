package cl.autentia.nfc;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;

import com.acepta.smartcardio.NativeSmartCardConnection;

/**
 * Created by marcin on 3/15/17.
 */

class NativeNFCReader extends NFCReader {

    private final NfcAdapter mAdapter;

    private NfcAdapter.ReaderCallback mNativeReaderCallback = new NfcAdapter.ReaderCallback() {
        @Override
        public void onTagDiscovered(Tag tag) {
            mReaderCallback.onNewConnection(new NativeSmartCardConnection(IsoDep.get(tag)));
        }
    };

    private ReaderCallback mReaderCallback;

    public NativeNFCReader(Activity activity, NfcAdapter adapter) {
        super(activity);
        mAdapter = adapter;
    }

    @Override
    public void open(ReaderCallback readerCallback) {
        mReaderCallback = readerCallback;
        mAdapter.enableReaderMode(mActivity, mNativeReaderCallback,
                NfcAdapter.FLAG_READER_NFC_B | NfcAdapter.FLAG_READER_NFC_A |
                        NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK |
                        NfcAdapter.FLAG_READER_NO_PLATFORM_SOUNDS, null);
    }

    @Override
    public void close() {
        mAdapter.disableReaderMode(mActivity);
    }
}
