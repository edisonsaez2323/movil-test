package cl.autentia.nfc;

import android.app.Activity;
import android.nfc.NfcAdapter;

import com.acepta.smartcardio.SmartCardConnection;

import java.io.IOException;

/**
 * Created by marcin on 3/15/17.
 */

public abstract class NFCReader {

    Activity mActivity;

    NFCReader(Activity activity) {
        mActivity = activity;
    }

    public abstract void open(ReaderCallback mReaderCallback) throws IOException;

    public abstract void close();

    public interface ReaderCallback {
        void onNewConnection(SmartCardConnection connection);
    }
}
