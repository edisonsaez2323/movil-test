package cl.autentia.nfc;

import android.content.Intent;

/**
 * Created by marcin on 3/14/17.
 */

public interface NFCAccessGrantReceiver {

    void onNFCAccessGranted();

    void onNFCAccessDenied();

    void onNFCAccessError();
}
