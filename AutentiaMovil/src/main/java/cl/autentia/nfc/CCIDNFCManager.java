package cl.autentia.nfc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Parcelable;
import android.util.Log;

import com.acepta.android.fetdroid.R;
import com.acepta.smartcardio.SmartCardConnection;
import com.acepta.smartcardio.ccid.CCIDCallback;
import com.acepta.smartcardio.ccid.CCIDReader;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.List;

import cl.autentia.usb.DeviceFilter;
import cl.autentia.usb.UsbDeviceAccessRequestActivity;

/**
 * Created by marcin on 3/14/17.
 */

public class CCIDNFCManager extends NFCManager {

    private final static String TAG = "CCIDNFCManager";

    @Override
    public boolean isAdapterPresent() {
        UsbDevice aDevice = findSuitableDevice();
        return aDevice != null;
    }

    @Override
    public boolean isAdapterAccessible() {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        UsbDevice aDevice = findSuitableDevice();
        return manager.hasPermission(aDevice);
    }

    @Override
    public void requestAccess(int requestCode) {
        Intent intent = new Intent(mActivity, UsbDeviceAccessRequestActivity.class);
        UsbDevice aDevice = findSuitableDevice();
        intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, aDevice);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void processAccessGrant(int resultCode, Intent result, NFCAccessGrantReceiver receiver) {
        if (resultCode == Activity.RESULT_OK) {

            if (result == null) {
                receiver.onNFCAccessDenied();
                return;
            }

            Parcelable authorizedDevice = result.getParcelableExtra(
                    UsbDeviceAccessRequestActivity.Extras.USB_DEVICE);
            if (authorizedDevice != null) {
                receiver.onNFCAccessGranted();
            } else {
                receiver.onNFCAccessDenied();
            }
        } else {
            receiver.onNFCAccessError();
        }
    }

    @Override
    public NFCReader getReader(Activity activity) {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        CCIDNFCReader theReader = new CCIDNFCReader(activity);
        UsbDevice suitableDevice = findSuitableDevice();
        if (suitableDevice != null) {
            theReader.mCcidReader = new SoftReference<>(new CCIDReader(manager, suitableDevice, theReader));
            return theReader;
        }
        return null;
    }

    private UsbDevice findSuitableDevice() {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        Collection<UsbDevice> deviceList = manager.getDeviceList().values();
        for (UsbDevice aDevice : deviceList) {

            List<DeviceFilter> deviceFilters = DeviceFilter.getDeviceFilters(mActivity, R.xml.device_filter_ccid);
            for (DeviceFilter filter : deviceFilters) {

                if (filter.matches(aDevice))
                    return aDevice;
            }
        }
        return null;
    }

    private class CCIDNFCReader extends NFCReader implements CCIDCallback {
        private SoftReference<CCIDReader> mCcidReader;
        private ReaderCallback mReaderCallback;

        public CCIDNFCReader(Activity activity) {
            super(activity);
        }

        @Override
        public void open(ReaderCallback readerCallback) throws IOException {
            mReaderCallback = readerCallback;
            CCIDReader reader = mCcidReader.get();
            if (reader != null) {
                reader.open();
            } else {
                Log.w(TAG, "reader was null when opening");
                throw new IOException("reader was null when opening");
            }
        }

        @Override
        public void close() {
            try {
                CCIDReader reader = mCcidReader.get();
                if (reader != null) {
                    reader.close();
                } else {
                    Log.w(TAG, "reader was null when closing");
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void inserted(SmartCardConnection conn) {
            Log.i(TAG, "new connection");
            mReaderCallback.onNewConnection(conn);
        }

        @Override
        public void removed() {
            Log.w(TAG, "card removed");
        }
    }
}