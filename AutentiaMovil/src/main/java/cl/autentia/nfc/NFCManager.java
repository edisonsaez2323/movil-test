package cl.autentia.nfc;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

/**
 * Created by marcin on 3/9/17.
 */

public abstract class NFCManager {

    private static final String TAG = "NFCManager";

    private static Class<?> mNfcControllerClass;
    protected Activity mActivity = null;

    public static NFCManager getInstance(Activity activity) {
        try {
            if (mNfcControllerClass == null) {
                synchronized (NFCManager.class) {
                    AutentiaPreferences preferences = new AutentiaPreferences(activity);
                    if (mNfcControllerClass == null) {
                        String nfcControllerClassName = preferences.getString(KeyPreferences.NFC_CONTROLLER_CLASS);
                        mNfcControllerClass = Class.forName(nfcControllerClassName);
                        Log.i(TAG, "NFC_CONTROLLER_CLASS = " + nfcControllerClassName);
                    }
                }
            }
            NFCManager nfcManager = (NFCManager) mNfcControllerClass.newInstance();
            nfcManager.setActivity(activity);
            return nfcManager;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract boolean isAdapterPresent();

    public abstract boolean isAdapterAccessible();

    public abstract void requestAccess(int requestCode);

    public abstract void processAccessGrant(int resultCode, Intent result, NFCAccessGrantReceiver receiver);

    private void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    public abstract NFCReader getReader(Activity activity);
}
