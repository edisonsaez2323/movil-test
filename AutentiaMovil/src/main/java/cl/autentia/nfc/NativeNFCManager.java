package cl.autentia.nfc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.provider.Settings;
import android.widget.Toast;

/**
 * Created by marcin on 3/14/17.
 */

@SuppressWarnings("unused")
public class NativeNFCManager extends NFCManager {

    @Override
    public boolean isAdapterPresent() {
        NfcManager manager = (NfcManager) mActivity.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        return adapter != null;
    }

    @Override
    public boolean isAdapterAccessible() {
        NfcManager manager = (NfcManager) mActivity.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        return adapter.isEnabled();
    }

    @Override
    public void requestAccess(int requestCode) {
        Toast.makeText(mActivity, "Favor activar la interfaz NFC", Toast.LENGTH_LONG).show();
        mActivity.startActivityForResult(new Intent(Settings.ACTION_NFC_SETTINGS), requestCode);
    }

    @Override
    public void processAccessGrant(int resultCode, Intent result, NFCAccessGrantReceiver receiver) {
        if (isAdapterAccessible()) {
            receiver.onNFCAccessGranted();
        }
        else {
            receiver.onNFCAccessDenied();
        }
    }

    @Override
    public NFCReader getReader(Activity activity) {
        return new NativeNFCReader(activity, NfcAdapter.getDefaultAdapter(activity));
    }
}
