package cl.autentia.helper;

import cl.autentia.common.ReturnCode;

public class AutentiaMovilException extends Exception {

    public String status;
    public ReturnCode returnCode;

    public AutentiaMovilException(ReturnCode returnCode) {
        super(returnCode.getDescription());
        this.returnCode = returnCode;
        this.status = null;
    }

    public AutentiaMovilException(String status, ReturnCode returnCode) {
        super(returnCode.getDescription());
        this.returnCode = returnCode;
        this.status = status;
    }
}
