package cl.autentia.helper;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.acepta.android.fetdroid.R;

import cl.autentia.FirebaseService.DialogActivity;
import cl.autentia.configuration.parameters.autoUpdateResp;


public class NotificationHelper {

    private static final String CHANNEL_ID = "cl.autentia";

    public synchronized static void showNotification(Context mContext, String contentText, String title, int id) {

        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "my_channel";
        String Description = "This is my channel";

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(true);

            if (notificationManager != null) {

                notificationManager.createNotificationChannel(mChannel);
            }

        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(contentText)
                .setSmallIcon(R.mipmap.ic_autentia)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setColor(mContext.getResources().getColor(R.color.colorPrimary));


        if (notificationManager != null) {

            notificationManager.notify(id, builder.build());
        }
    }

    public synchronized static void showPushNotification(Context mContext, autoUpdateResp aur, String contentText, String title, int id) {

        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "my_channel";
        String Description = "This is my channel";

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(true);

            if (notificationManager != null) {

                notificationManager.createNotificationChannel(mChannel);
            }

        }

        Bundle bundle = new Bundle();
        bundle.putString("apkUrl",aur.getUrl());
        bundle.putInt("priority",aur.getPriority());
        Intent dialogIntent = new Intent(mContext, DialogActivity.class);
        dialogIntent.putExtra("data",bundle);
//                    mContext.startActivity(dialogIntent);
        dialogIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
                dialogIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(contentText)
                .setSmallIcon(R.mipmap.ic_autentia)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setColor(mContext.getResources().getColor(R.color.colorPrimary));


        if (notificationManager != null) {

            notificationManager.notify(id, builder.build());
        }
    }



    public synchronized static void showProgressNotification(Context mContext, String contentText, String title, int totalLength, int currentLength, int id) {

        String CHANNEL_ID = "my_channel_02";
        CharSequence name = "my_channel_02";
        String Description = "This is my second channel";

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.setVibrationPattern(new long[]{ 0 });
            mChannel.enableVibration(true);
            mChannel.setLightColor(Color.RED);
            mChannel.setShowBadge(true);

            if (notificationManager != null) {

                notificationManager.createNotificationChannel(mChannel);
            }

        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_autentia)
                .setProgress(totalLength,currentLength,false)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setColor(mContext.getResources().getColor(R.color.colorPrimary));

        if (totalLength == 0 && currentLength == 0)
            builder.setProgress(0, 0, false)
                    .setContentText("Descarga completada!");

        if (notificationManager != null) {

            notificationManager.notify(id, builder.build());
        }

    }

    public synchronized static void showIndeterminateNotification(Context mContext, String contentText, String title, int totalLength, int id) {

        String CHANNEL_ID = "my_channel_02";
        CharSequence name = "my_channel_02";
        String Description = "This is my second channel";

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.setVibrationPattern(new long[]{ 0 });
            mChannel.enableVibration(true);
            mChannel.setLightColor(Color.RED);
            mChannel.setShowBadge(true);

            if (notificationManager != null) {

                notificationManager.createNotificationChannel(mChannel);
            }

        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_autentia)
                .setProgress(totalLength,totalLength, true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setColor(mContext.getResources().getColor(R.color.colorPrimary));

        if (notificationManager != null) {

            notificationManager.notify(id, builder.build());
        }

    }

    public static void dismissNotification(Context mContext, int id) {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }

}
