package cl.autentia.helper;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


public class LocationHelper extends Service implements LocationListener {

    private static final int PERMISSION_GPS = 0x0452;

    private LocationManager locationManager;
    private Location location;
    private Activity activity;

    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;

    public LocationHelper(Activity activity) {
        locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        this.activity = activity;
    }

    public double[] getCoordinates() {
        double[] coordinates = new double[2];

        double latitud = 0.0;
        double longitud = 0.0;

        try {

            Location mGPSLocation = getLocation(LocationManager.GPS_PROVIDER);
            if (mGPSLocation != null) {
                latitud = mGPSLocation.getLatitude();
                longitud = mGPSLocation.getLongitude();

            } else {
                Location mNETLocation = getLocation(LocationManager.NETWORK_PROVIDER);
                if (mNETLocation != null) {
                    latitud = mNETLocation.getLatitude();
                    longitud = mNETLocation.getLongitude();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        coordinates[0] = latitud;
        coordinates[1] = longitud;
        return coordinates;
    }

    public double getAccuracy() {
        if (location != null) {
            return location.getAccuracy();
        }
        return new Double(0);
    }

    private Location getLocation(String provider) throws Exception {

        if (locationManager.isProviderEnabled(provider)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_GPS);

                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_GPS);
                    }
                }
            }
            locationManager.requestLocationUpdates(provider, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                return location;
            }
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}