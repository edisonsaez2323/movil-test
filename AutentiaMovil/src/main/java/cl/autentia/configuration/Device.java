package cl.autentia.configuration;

import com.acepta.android.fetdroid.R;

import java.util.ArrayList;
import java.util.List;

import cl.autentia.nfc.CCIDNFCManager;
import cl.autentia.nfc.NativeNFCManager;
import cl.autentia.reader.usb.secugen.SecugenManager;
import cl.autentia.reader.usb.suprema.SupremaManager;
import cl.autentia.reader.usb.uareu.UareUManager;
import cl.autentia.reader.usb.eikon.EikonManager;

public class Device {

    public int image;
    public String desc;
    public String className;
    public boolean selected;

    private Device(int image, String desc, String className, boolean selected) {
        this.image = image;
        this.desc = desc;
        this.className = className;
        this.selected = selected;
    }

    public static List<Device> getDevicesList() {

        List<Device> devicesList = new ArrayList<Device>();
        devicesList.add(new Device(R.drawable.ic_device_eikon, "Eikon Touch", EikonManager.class.getName(), false));
        devicesList.add(new Device(R.drawable.ic_device_uareu, "UareU", UareUManager.class.getName(), false));
        devicesList.add(new Device(R.drawable.ic_device_suprema, "Suprema", SupremaManager.class.getName(), false));
        devicesList.add(new Device(R.drawable.ic_device_secugen, "Sucugen", SecugenManager.class.getName(), false));
        devicesList.add(new Device(R.drawable.ic_device_morpho, "Morpho", cl.autentia.reader.usb.morpho.MorphoManager.class.getName(), false));
        return devicesList;
    }

    public static List<Device> getNFCDevicesList() {

        List<Device> devicesList = new ArrayList<Device>();
        devicesList.add(new Device(R.drawable.ic_device_omnikey_5022, "NFC CCID", CCIDNFCManager.class.getName(), false));
        devicesList.add(new Device(R.drawable.logo_native, "NFC nativo", NativeNFCManager.class.getName(), false));

        return devicesList;
    }

    public static Device getDeviceFromPreferences(String device_controller_selected) {

        List<Device> devices = getDevicesList();

        if (device_controller_selected != null && !device_controller_selected.equals("")) {
            for (int i = 0; i < devices.size(); i++) {
                Device device = devices.get(i);

                if (device.className.equalsIgnoreCase(device_controller_selected)) {
                    return device;
                }
            }
        }
        return null;
    }

    public static Device getNFCDeviceFromPreferences(String device_controller_selected) {

        List<Device> devices = getNFCDevicesList();

        if (device_controller_selected != null && !device_controller_selected.equals("")) {
            for (int i = 0; i < devices.size(); i++) {
                Device device = devices.get(i);

                if (device.className.equalsIgnoreCase(device_controller_selected)) {
                    return device;
                }
            }
        }
        return null;
    }
}
