package cl.autentia.configuration.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;

import java.util.List;

import cl.autentia.configuration.Device;
import cl.autentia.http.HttpUtil;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

/**
 * Created by android on 21-11-16.
 */

public class NFCAdapter extends RecyclerView.Adapter<NFCAdapter.ViewHolder> {


    private List<Device> mDeviceItems;
    private Context mContext;
    private AutentiaPreferences mPreferences;
    private NFCItemActionListener mNfcItemActionListener;

    public void setActionListener(NFCItemActionListener nfcItemActionListener) {
        this.mNfcItemActionListener = nfcItemActionListener;
    }

    public NFCAdapter(List<Device> items) {
        this.mDeviceItems = items;
    }

    @Override
    public int getItemCount() {
        return mDeviceItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        this.mContext = parent.getContext();
        View v = LayoutInflater.from(this.mContext)
                .inflate(R.layout.card_view_nfc_devices, parent, false);
        mPreferences = new AutentiaPreferences(this.mContext);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Device item = mDeviceItems.get(position);

        holder.desc.setText(item.desc);
        holder.image.setImageResource(item.image);
        holder.check.setVisibility(item.selected ? View.VISIBLE : View.INVISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPreferences.setString(KeyPreferences.NFC_CONTROLLER_CLASS, item.className);

                try {
                    new AlertDialog.Builder(mContext)
                            .setMessage(String.format("Ha seleccionado la configuración para el NFC \"%s\".\n\n" +
                                    "Para un correcto uso, conecte el nuevo nfc y reinicie las apps que se comunican con Autentia Móvil,\n\n" +
                                    "Tras presionar \"Aceptar\" Autentia Móvil se reiniciará.", item.desc))
                            .setTitle("Cambio de configuración")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    mNfcItemActionListener.onItemTap();

                                }
                            })
                            .create().show();
                } catch (Exception e) {
                    HttpUtil.sendReportUncaughtException(e, mContext);
                }
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView desc;
        ImageView image;
        ImageView check;

        public ViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.iv_nfc);
            desc = (TextView) view.findViewById(R.id.txt_desc_nfc);
            check = (ImageView) view.findViewById(R.id.iv_selected_nfc);
        }
    }

    public interface NFCItemActionListener {
        void onItemTap();
    }
}
