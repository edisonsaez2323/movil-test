package cl.autentia.configuration.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;

import java.util.List;

import cl.autentia.configuration.Device;
import cl.autentia.http.HttpUtil;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

/**
 * Created by android on 21-11-16.
 */

public class FingerprintAdapter extends RecyclerView.Adapter<FingerprintAdapter.ViewHolder> {


    private static List<Device> mItems;
    private Context mContext;
    private AutentiaPreferences mPreferences;

    public void setFingerPrintItemActionListener(FingerPrintItemActionListener fingerPrintItemActionListener) {
        this.mFingerPrintItemActionListener = fingerPrintItemActionListener;
    }

    private FingerPrintItemActionListener mFingerPrintItemActionListener;

    public FingerprintAdapter(List<Device> items) {
        this.mItems = items;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.card_view_firgerprint_devices, parent, false);
        mPreferences = new AutentiaPreferences(mContext);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Device item = mItems.get(position);

        holder._desc.setText(item.desc);
        holder._image.setImageResource(item.image);
        holder._check.setVisibility(item.selected ? View.VISIBLE : View.INVISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPreferences.setString(KeyPreferences.FINGERPRINT_READER_CONTROLLER_CLASS, item.className);

                try {
                    new AlertDialog.Builder(mContext)
                            .setMessage(String.format("Ha seleccionado la configuración para el huellero \"%s\".\n\n" +
                                    "Para un correcto uso, conecte el nuevo huellero y reinicie las apps que se comunican con Autentia Móvil,\n\n" +
                                    "Tras presionar \"Aceptar\" Autentia Móvil se reiniciará.", item.desc))
                            .setTitle("Cambio de configuración")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    mFingerPrintItemActionListener.onItemTap();
                                }
                            })
                            .create().show();
                } catch (Exception e) {
//                    try {
//                        Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
//                                .putCustomAttribute("Error", e.getMessage())
//                                .putCustomAttribute("Institucion", mPreferences.getString(KeyPreferences.INSTITUTION))
//                                .putCustomAttribute("App version", AppHelper.getCurrentVersion(mContext))
//                                .putCustomAttribute("IMEI", DeviceHelper.getIMEI(mContext)));
//                    } catch (PackageManager.NameNotFoundException e1) {
//                        e1.printStackTrace();
//                    }
                    HttpUtil.sendReportUncaughtException(e, mContext);
                }

            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView _desc;
        ImageView _image;
        ImageView _check;

        public ViewHolder(View view) {
            super(view);
            _image = (ImageView) view.findViewById(R.id.iv_fingerprint);
            _desc = (TextView) view.findViewById(R.id.txt_desc_fingerprint);
            _check = (ImageView) view.findViewById(R.id.iv_selected);
        }
    }

    public interface FingerPrintItemActionListener {
        void onItemTap();
    }
}
