package cl.autentia.configuration.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.acepta.android.fetdroid.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cl.autentia.realmObjects.Evidence_;

/**
 * Created by android on 21-11-16.
 */
public class EvidenceOfflineDataAdapter extends RecyclerView.Adapter<EvidenceOfflineDataAdapter.ViewHolder> {

    private String TAG = "Evidence Offline";
    private static List<Evidence_> items;
    private Context mContext;

    public EvidenceOfflineDataAdapter(List<Evidence_> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        this.mContext = parent.getContext();
        View v = LayoutInflater.from(this.mContext)
                .inflate(R.layout.card_view_pending_evidence, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Evidence_ item = items.get(position);

        holder._process.setText(String.format("Proceso: %s", item.type));
        holder._creationDate.setText(String.format("Fecha creación: %s", item.createDate));
        holder._audit.setText(String.format("Código Auditoria: %s", item.audit));

        try {

            //Inicialización de fecha de alerta amarilla.
            Calendar calendarWarn = Calendar.getInstance();

            Date creationDate = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            creationDate = new Date();
            calendarWarn.setTime(creationDate);

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", item.jsonData);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(mContext, "Datos de evidencia copiados en portapapeles", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            // Mensaje con partes no enviadas, pendientes por más de 24hrs.
            if (creationDate.after(calendarWarn.getTime())) {
                holder._btnSent.setBackgroundResource(R.mipmap.ic_excl_red);

            } else // Mensaje con partes no enviadas, pendientes por menos de 24hrs.
            {
                holder._btnSent.setBackgroundResource(R.mipmap.ic_excl_yellow);
            }

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView _process;
        TextView _creationDate;
        TextView _audit;
        Button _btnSent;

        public ViewHolder(View view) {
            super(view);

            _process = (TextView) view.findViewById(R.id.txt_process);
            _creationDate = (TextView) view.findViewById(R.id.txt_creation_date);
            _audit = (TextView) view.findViewById(R.id.txt_audit);
            _btnSent = (Button) view.findViewById(R.id.btn_sent);
        }
    }
}
