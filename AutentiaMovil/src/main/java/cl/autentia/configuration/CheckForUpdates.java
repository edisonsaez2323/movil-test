package cl.autentia.configuration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.google.gson.Gson;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import cl.autentia.FirebaseService.DialogActivity;
import cl.autentia.activity.NavegationMenuActivity;
import cl.autentia.async.AsyncTaskManager;
import cl.autentia.configuration.parameters.autoUpdateResp;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.NotificationHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;


public class CheckForUpdates {

    private Activity mContext;
    AutentiaPreferences mPreferences;
    String mUrlBase;
    private AutentiaWSClient mAutentiaWSClient;
    Uri apkURI;
    String apkName;

    public CheckForUpdates(Activity context) {
        mContext = context;
        mAutentiaWSClient = new AutentiaWSClient(mContext);
        mPreferences = new AutentiaPreferences(context);
        mUrlBase = mPreferences.getString(KeyPreferences.URL_BASE, "");
    }

    @SuppressLint("StaticFieldLeak")
    public void checkCurrentVersion() {

        NavegationMenuActivity.showDialog("Buscando actualizaciones");

        new AsyncTaskManager<Void>() {

            @Override
            public Void doWork(Object... objects) throws Exception {

                mAutentiaWSClient.getCheckVersion(new ResponseCallback() {

                    @Override
                    public void onResponseSuccess(Bundle result) {
                        NavegationMenuActivity.dismissDialog();
                        autoUpdateResp aur;
                        aur = new Gson().fromJson(result.getString("result"), autoUpdateResp.class);
                        if (aur.getUrl() == null) { //sino necesito actualizar
                            NavegationMenuActivity.showProgressNoUpgrade();
                        } else { //si existen actualizaciones
                            Bundle bundle = new Bundle();
                            bundle.putString("apkUrl", aur.getUrl());
                            bundle.putInt("priority", aur.getPriority());
                            Intent dialogIntent = new Intent(mContext, DialogActivity.class);
                            dialogIntent.putExtra("data", bundle);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(dialogIntent);
                        }
                    }

                    @Override
                    public void onResponseError(AutentiaMovilException result) {
                        NavegationMenuActivity.dismissDialog();
                        if (result.returnCode.getCode() == 201) {
                            NavegationMenuActivity.showProgressAlert(result.returnCode.getDescription());
                        } else {
                            Log.e("elsegetCheckVersionERR", result.returnCode.getDescription());
                            NavegationMenuActivity.showProgressAlert(result.returnCode.getDescription());
                        }
                    }

                });

                return null;
            }

            @Override
            public void onResult(Void result) {

            }

            @Override
            public void onError(Exception exception) {
                exception.printStackTrace();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void downloadAndUpdate(String urlApk) {

        int id_Notity = new Random().nextInt();

//        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), "", "");
        NotificationHelper.showProgressNotification(mContext, "Descargando", "AutentiaMovil", 100, 0, id_Notity);
        String[] segments = new String[0];
        try {
            segments = new URL(urlApk).getPath().split("/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        apkName = segments[segments.length - 1];

        String PATH = Environment.getExternalStorageDirectory() + "/download/";
        File directory = new File(PATH);
        if (!directory.exists())
            directory.mkdirs();

        File file = new File(directory + "/" + apkName);
        if (file.exists())
            file.delete();

        apkURI = FileProvider.getUriForFile(
                            mContext
                            , mContext.getApplicationContext()
                                    .getPackageName() + ".provider", file);

        AndroidNetworking.download(urlApk, file.toString(), apkName).build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
//                        Log.e("url",urlApk);
//                        Log.e("total - downloaded", String.format("%s - %s", String.valueOf(totalBytes), String.valueOf(bytesDownloaded)));
                        NotificationHelper.showProgressNotification(mContext, "Descargando", "AutentiaMovil", (int) totalBytes, (int) bytesDownloaded, id_Notity);
                    }
                }).startDownload(new DownloadListener() {
            @Override
            public void onDownloadComplete() {
                NotificationHelper.dismissNotification(mContext, id_Notity);
                onInstall(apkURI + "/" +apkName);

            }

            @Override
            public void onError(ANError anError) {
                Log.e("error", "download");
                Log.e("error", anError.getErrorDetail());
            }
        });

    }

    void onInstall(String file){
        Intent install = new Intent(Intent.ACTION_VIEW);
        install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        install.setDataAndType(Uri.parse(file), "application/vnd.android.package-archive");
        install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        mContext.startActivity(install);
    }


}
