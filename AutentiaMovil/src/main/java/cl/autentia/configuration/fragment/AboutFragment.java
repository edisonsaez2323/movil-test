package cl.autentia.configuration.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Date;

import cl.autentia.preferences.AutentiaPreferences;

@EFragment(R.layout.fragment_about)
public class AboutFragment extends Fragment {

    private AutentiaPreferences mPreferences;

    @ViewById(R.id.txt_about)
    TextView _txtAbout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreferences = new AutentiaPreferences(getActivity());
    }

    @AfterViews
    void afterViews() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Acerca de...");
        populatePropertiesView();
    }

    private void populatePropertiesView() {

        StringBuilder sb = new StringBuilder();

        PackageManager manager = getActivity().getPackageManager();
        String version = "";
        try {
            PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), 0);
            version = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }

        sb.append(String.format("%s\n", "Autentia Móvil"));
        sb.append(String.format("Versión = %s\n", version));
        sb.append(String.format("%s\n", "Autentia S.A."));
        sb.append(String.format("%s", new SimpleDateFormat("yyyy").format(new Date())));

        _txtAbout.setText(sb.toString());
    }
}
