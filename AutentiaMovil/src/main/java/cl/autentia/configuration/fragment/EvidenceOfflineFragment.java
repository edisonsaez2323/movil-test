package cl.autentia.configuration.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import cl.autentia.configuration.adapter.EvidenceOfflineDataAdapter;
import cl.autentia.realmObjects.Evidence_;

@EFragment(R.layout.fragment_pending_evidence)
public class EvidenceOfflineFragment extends Fragment {

    private static final String TAG = "EvidenceOffline";

    public static final String BROADCAST_EVIDENCE = "cl.autentia.android.broadcast.processevidence";

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @ViewById(R.id.rv_internal_data)
    RecyclerView _rvEvidenceOffline;

    @ViewById(R.id.txtVacio)
    TextView _txtVacio;

    @ViewById(R.id.bt_menu)
    ImageView _btMenu;

    private BroadcastReceiver receiverProcessEvidence = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent data) {
            populateRecicler();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterViews
    void afterViews() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Evidencias Offline");
        populateRecicler();

        final DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

        _btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_EVIDENCE);
        getActivity().registerReceiver(receiverProcessEvidence, filter);

        populateRecicler();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiverProcessEvidence);
        super.onPause();
    }

    private void populateRecicler() {
        try {

            mLayoutManager = new LinearLayoutManager(getActivity());
            _rvEvidenceOffline.setLayoutManager(mLayoutManager);
            _rvEvidenceOffline.getRecycledViewPool().setMaxRecycledViews(0, 0);
            List<Evidence_> evidenceList = Evidence_.getAllEvicences();

            if (evidenceList.isEmpty()) {

                _txtVacio.setVisibility(View.VISIBLE);
                _rvEvidenceOffline.setVisibility(View.GONE);
            } else {
                _txtVacio.setVisibility(View.GONE);
                _rvEvidenceOffline.setVisibility(View.VISIBLE);
            }

            mAdapter = new EvidenceOfflineDataAdapter(evidenceList);
            _rvEvidenceOffline.setAdapter(mAdapter);

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }
}
