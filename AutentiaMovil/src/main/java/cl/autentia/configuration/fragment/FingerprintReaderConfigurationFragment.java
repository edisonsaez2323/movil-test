package cl.autentia.configuration.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.acepta.android.fetdroid.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import cl.autentia.activity.NavegationMenuActivity_;
import cl.autentia.configuration.Device;
import cl.autentia.configuration.adapter.FingerprintAdapter;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.sync.MyServiceSyncAdapter;

@EFragment(R.layout.fragment_fingerprintreader_configuration)
public class FingerprintReaderConfigurationFragment extends Fragment {

    private AutentiaPreferences mPreferences;
    private FingerprintAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @ViewById(R.id.rv_fingerprint_devices)
    RecyclerView _rvFingerprintDevices;

    @ViewById(R.id.bt_menu)
    ImageView _btMenu;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Selección de Huellero");
        mPreferences = new AutentiaPreferences(getActivity());
    }

    @AfterViews
    void afterViews() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        _rvFingerprintDevices.setLayoutManager(mLayoutManager);
        _rvFingerprintDevices.getRecycledViewPool().setMaxRecycledViews(0, 0);
        _rvFingerprintDevices.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        final DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        _btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        List<Device> deviceList = Device.getDevicesList();

        Device mDevice = Device.getDeviceFromPreferences(mPreferences.getString(KeyPreferences.FINGERPRINT_READER_CONTROLLER_CLASS, ""));
        if (mDevice != null) {
            for (Device device : deviceList) {
                if (device.className.equalsIgnoreCase(mDevice.className)) {
                    device.selected = true;
                }
            }
        }

        mAdapter = new FingerprintAdapter(deviceList);
        mAdapter.setFingerPrintItemActionListener(new FingerprintAdapter.FingerPrintItemActionListener() {
            @Override
            public void onItemTap() {

                int pid = android.os.Process.myPid();

                AlarmManager alm = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                alm.set(AlarmManager.RTC, System.currentTimeMillis() + 0, PendingIntent.getActivity(getActivity(), 0, new Intent(getActivity(), NavegationMenuActivity_.class), 0));
                android.os.Process.killProcess(pid);
                MyServiceSyncAdapter.syncImmediately(getActivity());

            }
        });
        _rvFingerprintDevices.setAdapter(mAdapter);
    }
}
