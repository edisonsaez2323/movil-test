package cl.autentia.configuration.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Map;

import cl.autentia.configuration.Device;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;

@EFragment(R.layout.fragment_current_configuration)
public class CurrentConfigurationFragment extends Fragment {

    private static final String TAG = "CurrentConfiguration";
    public static final String BROADCAST_UPDATE_QUANTITY_AUDIT = "cl.autentia.android.broadcast.updatequantity";

    private AutentiaPreferences mPreferences;
//    private GenericDao<Auditoria> mAditoriaDao;

    @ViewById(R.id.tv_url_base)
    TextView _etUrlBase;

    @ViewById(R.id.tv_institucion)
    TextView _etInstitution;

    @ViewById(R.id.tv_ambiente)
    TextView _etCodeEnviroment;

    @ViewById(R.id.tv_huellero)
    TextView _etFingerprint;

    @ViewById(R.id.tv_nfc)
    TextView _etNFCReader;

//    @ViewById(R.id.et_version_config)
//    TextView _etConfigVersion;

    @ViewById(R.id.tv_preauditoria)
    TextView _etQuatityAudit;

    @ViewById(R.id.bt_menu)
    ImageView _btMenu;

    @ViewById(R.id.progressBarAudit)
    static
    ProgressBar progressBarAudit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.durga.action.refreshAuditCount");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);
        mPreferences = new AutentiaPreferences(getActivity());
    }

    @AfterViews
    void afterViews() {

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Configuración actual");



        final DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

        _btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        populatePropertiesView();
    }

    private BroadcastReceiver receiverUpdateQuatityAudit = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            if(Auditoria_.getAuditCount() < Integer.parseInt(mPreferences.getString(KeyPreferences.AUDIT_VALUE)) && Auditoria_.getAuditCount() > 0){
                setOnOff(true);
            }else{
                setOnOff(false);
            }
            try {
                if (intent != null) {
                    Bundle extras = intent.getExtras();

                    if (extras.get("quantity") != null) {
                        int quantity = extras.getInt("quantity");
                        _etQuatityAudit.setText(String.format("%s", quantity));
                    }
                    if (extras.get("version") != null) {

                        int versionConfig = extras.getInt("version");
//                        _etConfigVersion.setText(String.valueOf(versionConfig));
                    }
                }else{
                    setOnOff(false);
                }
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }
    };

    @Override
    public void onResume() {

        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_UPDATE_QUANTITY_AUDIT);
        getActivity().registerReceiver(receiverUpdateQuatityAudit, filter);

    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiverUpdateQuatityAudit);
        super.onPause();
    }

    public void setOnOff(boolean state){
        if(progressBarAudit != null){
            if (state)
                progressBarAudit.setVisibility(View.VISIBLE);
            else
                progressBarAudit.setVisibility(View.GONE);
        }
    }

    private void populatePropertiesView() {


        String [] arrayUrl = mPreferences.getString(KeyPreferences.URL_BASE, "").split("/");
        for(String url: arrayUrl){
            if(url.contains(".")){
                _etUrlBase.setText(url);
                break;
            }
        }
        _etInstitution.setText(mPreferences.getString(KeyPreferences.INSTITUTION, "Ninguno"));
        _etCodeEnviroment.setText(mPreferences.getString(KeyPreferences.ENVIROMENT, "Ninguno"));

        Device huellero = Device.getDeviceFromPreferences(mPreferences.getString(KeyPreferences.FINGERPRINT_READER_CONTROLLER_CLASS, ""));
        if (huellero != null) {
            huellero.selected = true;
        }

        Device nfc = Device.getNFCDeviceFromPreferences(mPreferences.getString(KeyPreferences.NFC_CONTROLLER_CLASS, "cl.autentia.nfc.NativeNFCManager"));
        if (nfc != null) {
            nfc.selected = true;
        }

        Map<String, ?> keys = mPreferences.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.d(TAG, entry.getKey() + ": " +
                    entry.getValue().toString());
        }

        _etFingerprint.setText(huellero == null ? "Ninguno" : huellero.desc);
        _etNFCReader.setText(nfc == null ? "Ninguno" : nfc.desc);

        try {

            _etQuatityAudit.setText(String.format("%s", Auditoria_.getAuditCount()));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    LocalBroadcastManager mLocalBroadcastManager;
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("com.durga.action.refreshAuditCount")){
                IntentFilter filter = new IntentFilter();
                filter.addAction(BROADCAST_UPDATE_QUANTITY_AUDIT);
                getActivity().registerReceiver(receiverUpdateQuatityAudit, filter);
            }
        }
    };


}