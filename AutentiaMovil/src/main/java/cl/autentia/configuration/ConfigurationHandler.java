package cl.autentia.configuration;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.google.gson.Gson;
import com.jakewharton.processphoenix.ProcessPhoenix;
import com.morpho.morphosmart.sdk.MorphoDevice;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ch.boye.httpclientandroidlib.client.cache.Resource;
import cl.autentia.activity.NavegationMenuActivity;
import cl.autentia.activity.NavegationMenuActivity_;
import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.configuration.fragment.CurrentConfigurationFragment_;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GetInstitResponse;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.usb.morpho.MorphoGlobals;
import cl.autentia.uareu.Globals;
import cl.autentia.usb.DeviceFilter;

/**
 * Created by Edison on 07-01-2019.
 */

@EBean
public class ConfigurationHandler {

    private static final String UAREU_FINGERPRINT_CLASS = "cl.autentia.reader.usb.uareu.UareUManager";
    private static final String EIKON_FINGERPRINT_CLASS = "cl.autentia.reader.usb.eikon.EikonManager";
    private static final String MORPHORT080_FINGERPRINT_CLASS = "cl.autentia.reader.usb.morpho.MorphoManager";
    private static final String CCID_NFC_CONTROLLER_CLASS = "cl.autentia.nfc.CCIDNFCManager";
    private static final String URL_PROD = "https://movil.autentia.cl/autentiaws/";

    private Context mContext;
    AutentiaWSClient mAutentiaWSClient;
    private Activity mActivity;
    private Reader mReader = null;
    private UsbManager manager;
    private AutentiaPreferences preferences;
    private static String mSerialNumber;
    private String mCurrentFingerPrint;
    private boolean mNFCDeviceCCID = false;
    static ConfigurationHandler mConfigurationHandler;


    public ConfigurationHandler(Context context) {
        Log.e("ConfigurationHanlder", "im in");
        mContext = context;
        mActivity = (Activity) context;
        mAutentiaWSClient = new AutentiaWSClient(context);
        preferences = new AutentiaPreferences(context);
    }

    public void loadConfiguration(){
        loadProperties();
    }

    public boolean checkForDevices() {
        manager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        return manager.getDeviceList().isEmpty();
    }

    public String getReaderSerialNumber() {

        Collection<UsbDevice> mDevices = manager.getDeviceList().values();

        for (UsbDevice device : mDevices) {

            switch (device.getVendorId()) {

                case 8797: /* Morpho*/
                    MorphoDevice morphoDevice = MorphoGlobals.GetMorphodeviceInstance();
                    UsbManager usbManager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
                    if (morphoDevice.getUsbDeviceName(0).equals(""))
                        Log.e("getMorphoSN", "IM empty");
                    Collection<UsbDevice> deviceList = usbManager.getDeviceList().values();
                    for (UsbDevice aDevice : deviceList) {
                        List<DeviceFilter> deviceFilters = DeviceFilter.getDeviceFilters(mActivity, R.xml.device_filter_morpho);
                        for (DeviceFilter filter : deviceFilters) {
                            if (filter.matches(aDevice)) {
                                Log.e("getDeviceName", aDevice.getDeviceName());
                                Log.e("getProductName", "" + aDevice.getProductName());
                                Log.e("getSerialNumber", "" + aDevice.getSerialNumber());
                                mCurrentFingerPrint = MORPHORT080_FINGERPRINT_CLASS;
                                preferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, aDevice.getSerialNumber());
                                morphoDevice.closeDevice();
                                return aDevice.getSerialNumber();
                            }
                        }
                    }
                    break;
                case 5246:/* Eikon */
                case 1466:/* UareU */
                    Log.d("switch", "case 1466");

                    ReaderCollection readers = null;
                    try {
                        mReader = null;
                        readers = Globals.getInstance().getReaders(mContext);
                        mReader = Globals.getInstance().getReader(readers.get(0).GetDescription().name, mContext);
                        if (mReader.GetDescription().serial_number.isEmpty()) {
                            mCurrentFingerPrint = EIKON_FINGERPRINT_CLASS;
                            return getEikonSerialNumber();
                        } else {
                            mCurrentFingerPrint = UAREU_FINGERPRINT_CLASS;
                            mSerialNumber = mReader.GetDescription().serial_number;
                            Log.d("SERIALNUMBER", mSerialNumber);
                            preferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, mReader.GetDescription().serial_number);
                            return mReader.GetDescription().serial_number;
                        }
                    } catch (UareUException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
        return null;
    }

    public void checkForNFCDevice() {
        manager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        Collection<UsbDevice> mDevices = manager.getDeviceList().values();

        for (UsbDevice device : mDevices) {
            switch (device.getVendorId()) {

                case 1899:
                    preferences.setString(KeyPreferences.NFC_CONTROLLER_CLASS, CCID_NFC_CONTROLLER_CLASS);
                    preferences.setString(KeyPreferences.NFC_CONFIGURATION, "1");
                    forceFinishProcess();
//                    updateUi();
                    break;
            }
        }

    }

    private String getEikonSerialNumber() throws UareUException {

        mReader.Open(Reader.Priority.EXCLUSIVE);
        int mDpi = Globals.GetFirstDPI(mReader);

        String strPtapiGuid = new String("N/A");
        try {

            byte[] guid = mReader.GetParameter(Reader.ParamId.PARMID_PTAPI_GET_GUID);
            if (16 == guid.length) {
                final char[] hexArray = "0123456789ABCDEF".toCharArray();
                char[] hexChars = new char[guid.length * 2];
                for (int j = 0; j < guid.length; j++) {
                    int v = guid[j] & 0xFF;
                    hexChars[j * 2] = hexArray[v >>> 4];
                    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                }
                strPtapiGuid = new String(hexChars);
            }

            List<String> original = getParts(strPtapiGuid, 2);

            StringBuilder str = new StringBuilder();
            str.append("{");
            str.append(original.get(3));
            str.append(original.get(2));
            str.append(original.get(1));
            str.append(original.get(0));
            str.append("-");
            str.append(original.get(5));
            str.append(original.get(4));
            str.append("-");
            str.append(original.get(7));
            str.append(original.get(6));
            str.append("-");
            str.append(original.get(8));
            str.append(original.get(9));
            str.append("-");
            str.append(original.get(10));
            str.append(original.get(11));
            str.append(original.get(12));
            str.append(original.get(13));
            str.append(original.get(14));
            str.append(original.get(15));
            str.append("}");
            preferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, str.toString());
            return str.toString();
        } catch (UareUException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Background
    void loadProperties() {

        if (preferences.getString(KeyPreferences.DATA_CONFIGURATION).equals("00001")) {

            if (!checkForDevices()) {

                String serialNumber = getReaderSerialNumber();
                try {


                    mAutentiaWSClient.getConfigData(serialNumber, new ResponseCallback() {

                        @Override
                        public void onResponseSuccess(Bundle result) {

                            try {
//                                jsonResponse = new JSONObject(result.getString("result"));
                                GetInstitResponse institResponse = new Gson().fromJson(result.getString("result"), GetInstitResponse.class);
                                if ((institResponse.getStatus() == 0) && institResponse.getGlosa() == null) {

                                    String mInstitution = institResponse.getInstitucion();
                                    String mRutInstitution = institResponse.getRutInstitucion();

                                    if (!mRutInstitution.isEmpty()) {
                                        preferences.setString(KeyPreferences.INSTITUTION, mInstitution);
                                    }
                                    if (!mInstitution.isEmpty()) {
                                        preferences.setString(KeyPreferences.RUN_INSTITUTION, mRutInstitution);
                                    }
                                    if (!mCurrentFingerPrint.isEmpty()) {
                                        preferences.setString(KeyPreferences.FINGERPRINT_READER_CONTROLLER_CLASS, mCurrentFingerPrint);
                                    }
                                    if (!mInstitution.isEmpty()) {
                                        preferences.setString(KeyPreferences.ENVIROMENT, "PRODUCCION");
                                    }
                                    preferences.setString(KeyPreferences.URL_BASE, URL_PROD);
                                    preferences.setString(KeyPreferences.TERMS_AND_CONDITIONS, new Gson().toJson(institResponse.getTerminos()));
                                    preferences.setString(KeyPreferences.HASH_CONFIGURATION, createHash(mInstitution));
                                    preferences.setString(KeyPreferences.DATA_CONFIGURATION, "00002");
                                    preferences.setString(KeyPreferences.AUDIT_VALUE, "50");
                                    preferences.deleteKey("glosa");
//                                    updateUi();
                                    forceFinishProcess();
                                } else {
                                    NavegationMenuActivity.showProgressAlert(institResponse.getGlosa());
                                }
                            } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            NavegationMenuActivity.showProgressAlert(result.status);
                        }

                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else { //si no queremos plug in configuration el qr debe ser distinto a 00001 y a 00002
            if (!preferences.getString(KeyPreferences.DATA_CONFIGURATION, "00003").equals("00002")) {
                if (!checkForDevices()) {
                    if (preferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER) == null) {
                        preferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, getReaderSerialNumber());
                        preferences.setString(KeyPreferences.DATA_CONFIGURATION, "00002");
                        forceFinishProcess();
                    } else if (!preferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER).equals("")) {
                        preferences.setString(KeyPreferences.DATA_CONFIGURATION, "00003");
                        forceFinishProcess();
                    } else {
                        preferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, getReaderSerialNumber());
                        preferences.setString(KeyPreferences.DATA_CONFIGURATION, "00002");
                        forceFinishProcess();
                    }
                }
            }
        }
    }

    private String createHash(String institucion) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        return Utils.SHA1(String.format("%s/PRODUCCION/1760", institucion));
    }

    private static List<String> getParts(String string, int partitionSize) {
        List<String> parts = new ArrayList<String>();
        int len = string.length();
        for (int i = 0; i < len; i += partitionSize) {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }
        return parts;
    }

    void updateUi() {
        Fragment currentFragment = mActivity.getFragmentManager().findFragmentById(R.layout.fragment_current_configuration);

        FragmentTransaction fragTransaction = mActivity.getFragmentManager().beginTransaction();
        fragTransaction.detach(currentFragment);
        fragTransaction.attach(currentFragment);
        fragTransaction.commit();
    }

    void forceFinishProcess() {
        int pid = android.os.Process.myPid();

//        AlarmManager alm = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent newIntent = new Intent(mContext, NavegationMenuActivity_.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        ProcessPhoenix.triggerRebirth(mContext, newIntent);
//        alm.set(AlarmManager.RTC, System.currentTimeMillis() + 100, PendingIntent.getActivity(mContext, 0, newIntent, 0));
//        android.os.Process.killProcess(pid);
    }

/**
 * INTERFACES.
 */


protected enum InternalState {
    JUST_STARTING, WAITING_FOR_PERMISSION, READY
}

interface Extras {

    interface In extends CommonInExtras {
        String BARCODE = "BARCODE";

    }

    interface Out extends CommonOutExtras {
    }
}

}
