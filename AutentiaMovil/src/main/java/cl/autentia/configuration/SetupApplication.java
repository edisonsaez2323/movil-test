package cl.autentia.configuration;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.digitalpersona.uareu.Reader;

import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

import static cl.autentia.test.AutentiaTester.isConnectedToServer;

public class SetupApplication {

    public static final String SIN_DISPOSITIVOS_CONECTADOS = "Sin dispositivos conectados";
    private Context mContext;
    private AutentiaPreferences mPreferences;
    private UsbManager mManager;
    private Reader mReader = null;

    public SetupApplication(Context context) {
        mContext = context;
        mPreferences = new AutentiaPreferences(context);
        mManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
    }

    public String init() {
        Log.e("Init", "Started");
        if (verificarProperties()) {
            String mFingerPrintInfo = verificarHuellero();

        } else {
            Log.e("ERROR", "VERIFICARPROPERTIES");
        }
        return "";
    }

    private boolean verificarProperties() {

        Map<String, ?> mMap = mPreferences.getAll();

        for (Map.Entry<String, ?> entry : mMap.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

        return true;

    }

    private String verificarHuellero() {

        HashMap<String, UsbDevice> mDeviceList = mManager.getDeviceList();

        for (UsbDevice device : mDeviceList.values()) {

            switch (device.getVendorId()) {

                case 5246:/* Eikon */
                    return "Eikon";
                case 1466:/* UareU */
                    return "UareU";
                default:
                    return SIN_DISPOSITIVOS_CONECTADOS;

            }
        }
        return "Empty data";
    }

    private boolean verificarConexion(){

        boolean isConnected = false;

        try {
            isConnected = isConnectedToServer(mPreferences.getString(KeyPreferences.URL_BASE));

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return isConnected;
    }

}
