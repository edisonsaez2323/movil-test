package cl.autentia.reader.usb.suprema;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Parcelable;

import com.acepta.android.fetdroid.R;

import java.util.Collection;
import java.util.List;

import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.usb.DeviceFilter;
import cl.autentia.usb.UsbDeviceAccessRequestActivity;

public class SupremaManager extends FingerprintManager {


    @Override
    public boolean isUsbPresent() throws Exception {
        UsbDevice aDevice = findSuitableDevice();
        return aDevice != null;
    }

    @Override
    public boolean isUsbAccessible() {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        UsbDevice aDevice = null;
        try {
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        return manager.hasPermission(aDevice);
    }

    @Override
    public void requestAccess(int requestCode) {
        Intent intent = new Intent(mActivity, UsbDeviceAccessRequestActivity.class);
        UsbDevice aDevice = null;
        try {
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, aDevice);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void processAccessGrant(int resultCode, Intent result, FingerprintGrantReceiver receiver) {
        if (resultCode == Activity.RESULT_OK) {

            if (result == null) {
                receiver.onFingerprintAccessDenied();
                return;
            }
            Parcelable authorizedDevice = result.getParcelableExtra(
                    UsbDeviceAccessRequestActivity.Extras.USB_DEVICE);

            if (authorizedDevice != null) {
                receiver.onFingerprintAccessGranted();
            } else {
                receiver.onFingerprintAccessDenied();
            }
        } else {
            receiver.onFingerprintAccessError();
        }
    }

    @Override
    public FingerprintReader getReader(Activity activity) {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        SupremaReader theReader = new SupremaReader(activity, manager);
        UsbDevice suitableDevice = null;
        try {
            suitableDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        if (suitableDevice != null) {
            return theReader;
        }
        return null;
    }

    @Override
    public UsbDevice findSuitableDevice() throws Exception {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        Collection<UsbDevice> deviceList = manager.getDeviceList().values();
        for (UsbDevice aDevice : deviceList) {

            List<DeviceFilter> deviceFilters = DeviceFilter.getDeviceFilters(mActivity, R.xml.device_filter_suprema);
            for (DeviceFilter filter : deviceFilters) {

                if (filter.matches(aDevice))
                    return aDevice;
            }
        }
        throw new Exception("Error en configuración del huellero");
    }
}
