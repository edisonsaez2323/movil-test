package cl.autentia.reader.usb.morpho;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.ImageView;

import com.acepta.android.fetdroid.R;
//import com.bluebird.sled.SledManager;
//import com.bluebird.sled.SledManager;
import com.morpho.morphosmart.sdk.MorphoDevice;

import java.util.Collection;
import java.util.List;

import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.usb.DeviceFilter;
import cl.autentia.usb.UsbDeviceAccessRequestActivity;

public class MorphoManager extends FingerprintManager {

    private boolean started = false;
    UsbManager usbManager;

    @Override
    public boolean isUsbPresent() throws Exception {
        Log.e("isUsbPresent","1");
        UsbDevice aDevice = findSuitableDevice();
        return aDevice != null;
    }

    @Override
    public boolean isUsbAccessible() {
        if(usbManager == null) usbManager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        UsbDevice aDevice = null;
        try {
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        return usbManager.hasPermission(aDevice);
    }

    void setPowerFP2(int onOff) {
        if(usbManager == null) usbManager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        if(Build.MODEL.contains("RT080")) {
            if (onOff == 1) {
                Log.i("EF500MorphoManager", "POWER ON");
                usbManager.setFingerPrinterPower(true);
            } else if (onOff == 0) {
                Log.i("EF500MorphoManager", "POWER OFF");
                usbManager.setFingerPrinterPower(false);
            }
        }
//        else{
//
//            SledManager sm = (SledManager) mActivity.getSystemService("sled");
//            if (onOff == 1) {
//                Log.i("EF500MorphoManager", "POWER ON");
//                sm.BBSetFngrPower(1);
//            }else{
//                Log.i("EF500MorphoManager", "POWER OFF");
//                sm.BBSetFngrPower(0);
//            }
//        }
    }

    @Override
    public void requestAccess(int requestCode) {
        Intent intent = new Intent(mActivity, UsbDeviceAccessRequestActivity.class);
        UsbDevice aDevice = null;
        try {
            Log.e("requestAccess","1");
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, aDevice);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void processAccessGrant(int resultCode, Intent result, FingerprintGrantReceiver receiver) {
        if (resultCode == Activity.RESULT_OK) {

            if (result == null) {
                receiver.onFingerprintAccessDenied();
                return;
            }
            Parcelable authorizedDevice = result.getParcelableExtra(
                    UsbDeviceAccessRequestActivity.Extras.USB_DEVICE);

            if (authorizedDevice != null) {
                receiver.onFingerprintAccessGranted();
            } else {
                receiver.onFingerprintAccessDenied();
            }
        } else {
            receiver.onFingerprintAccessError();
        }

    }

    @Override
    public FingerprintReader getReader(Activity activity) {
        usbManager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
        Log.e("getReader","1");
        MorphoReader theReader = new MorphoReader(activity, usbManager);
        Log.e("getReader","2");
        UsbDevice suitableDevice = null;
        try {
            Log.e("getReader","3");
            suitableDevice = findSuitableDevice();
            Log.e("getReader","4");
        } catch (Exception e) {
        }
        if (suitableDevice != null) {
            Log.e("getReader","5");
            return theReader;
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public UsbDevice findSuitableDevice() throws Exception {

        MorphoDevice morphoDevice = MorphoGlobals.GetMorphodeviceInstance();
        if(usbManager == null) usbManager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        if (!started && morphoDevice.getUsbDeviceName(0).equals("")) setPowerFP2(1);
        Collection<UsbDevice> deviceList = usbManager.getDeviceList().values();
            for (UsbDevice aDevice : deviceList) {
                List<DeviceFilter> deviceFilters = DeviceFilter.getDeviceFilters(mActivity, R.xml.device_filter_morpho);
                for (DeviceFilter filter : deviceFilters) {
                    if (filter.matches(aDevice)) {
                        Log.e("getDeviceName",aDevice.getDeviceName());
                        Log.e("getProductName",""+aDevice.getProductName());
                        Log.e("getSerialNumber",""+aDevice.getSerialNumber());
                        return aDevice;
                    }
                }
            }

        throw new Exception("Error en configuración del huellero");

    }

}
