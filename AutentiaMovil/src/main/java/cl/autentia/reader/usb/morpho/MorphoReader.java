package cl.autentia.reader.usb.morpho;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.acepta.android.fetdroid.R;
import com.morpho.morphosmart.sdk.CallbackMask;
import com.morpho.morphosmart.sdk.CallbackMessage;
import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.DetectionMode;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.usb.DeviceFilter;

public class MorphoReader extends FingerprintReader implements Observer{


    Activity mActivity;


    int imageWidth;
    int imageHeight;
    int dpi = 500;
    UsbManager mUsbManager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MorphoReader(Activity activity, UsbManager usbManager) {

        super(activity, usbManager);
        mUsbManager = usbManager;
        mActivity = activity;
        morphoDevice = MorphoGlobals.GetMorphodeviceInstance();
        if (!isOpen) open();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void open() {

        if (!isOpen) open(mActivity);

    }

    @Override
    public void close() {
        Log.e("close()","im closing :c");
        cancelLiveAcquisition();
        release();
        isOpen = false;
    }

    @Override
    public void cancelCapture() {
        cancelLiveAcquisition();
    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {
        startCapture(scanCallback);
//        close();
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {
        if (!isOpen) open();

        Collection<UsbDevice> deviceList = mUsbManager.getDeviceList().values();
        for (UsbDevice aDevice : deviceList) {
            List<DeviceFilter> deviceFilters = DeviceFilter.getDeviceFilters(mActivity, R.xml.device_filter_morpho);
            for (DeviceFilter filter : deviceFilters) {
                if (filter.matches(aDevice)) {
                    FingerprintInfo info = new FingerprintInfo(
                            String.valueOf(aDevice.getSerialNumber()),
                            String.valueOf(morphoDevice.getSoftwareDescriptor()),
                            aDevice.getProductName() + " - " + aDevice.getManufacturerName());
                    infoCallback.infoResult(info);
                }
            }
        }

    }

    private static final String TAG="Morphoreader";
    public Bitmap bm = null;

    /**
     * The Morpho device.
     */

    private MorphoDevice morphoDevice;

    /**
     * The latent detection.
     */
    private LatentDetection latentDetection = LatentDetection.LATENT_DETECT_ENABLE;

    /**
     * The detect mode choice.
     */

    private int detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.getValue()
            | DetectionMode.MORPHO_FORCE_FINGER_ON_TOP_DETECT_MODE.getValue();


    /**
     * The callback command.
     */

    private int callbackCmd = CallbackMask.MORPHO_CALLBACK_IMAGE_CMD.getValue() ^ CallbackMask.MORPHO_CALLBACK_ENROLLMENT_CMD.getValue();

    /**
     * The last image height.
     */
    public int lastImageHeight = 0;

    /**
     * last callback msg
     */

    private String callbackMsg = "";

    //private int value;

    public  byte[] rawImage;

    /**
     * Instantiates a new MorphoTablet fingerprint sensor device.
     *
     * @since 2.0
     */

    /**
     * Sets the compression ratio.
     *
     */

//    private UsbManager usbManager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("UseValueOf")
    public int open(Activity arg0) {
        Log.e("OPEN","OPENING");
        String sensorName;
        Log.e("OPEN","1");
        if (mUsbManager == null) mUsbManager = (UsbManager) arg0.getSystemService(Context.USB_SERVICE);
        Integer nbUsbDevice = new Integer(0);
        int ret = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);
        if (ret == 0 ){
            Log.e("OPEN","2");
            if (nbUsbDevice > 0) {
                sensorName = morphoDevice.getUsbDeviceName(0);
                Log.e("OPEN",sensorName);
                isOpen = true;
                return morphoDevice.openUsbDevice(sensorName, 0);
            }
            return  -1;
        }else{
            Log.e("OPEN","3");
            Log.e("RET",""+ret);
            return 1;
        }
    }

    /**
     * Start com.morpho.capture.
     *
     */

    private static final int COMPRESSION_RATE_DEFAULT	= 0;
    private static final CompressionAlgorithm COMPRESSION_ALGORITHM = CompressionAlgorithm.MORPHO_NO_COMPRESS;

    @SuppressLint("LongLogTag")
    public void startCapture(final ScanCallback scanCallback) {
        final Observer oThis = this;
                // Capture the image
        MorphoImage morphoImage = new MorphoImage();

        /**
         * The timeout.
         */

        int timeout = 0;

        /**
         * The acquisition threshold.
         */

        int acquisitionThreshold = 55;

        final int ret2 = morphoDevice.getImage(timeout, acquisitionThreshold,COMPRESSION_ALGORITHM,COMPRESSION_RATE_DEFAULT, detectModeChoice, latentDetection, morphoImage, callbackCmd, oThis);

        try {
            if ((ret2 == ErrorCodes.MORPHO_OK)) {

                try {

                    rawImage = morphoImage.getImage();
                    imageWidth = morphoImage
                            .getMorphoImageHeader()
                            .getNbColumn();
                    imageHeight = morphoImage
                            .getMorphoImageHeader().getNbRow();

                } catch (Exception e) {
                    close();
                }

                Log.i("INFOimageHeight", String.valueOf(imageHeight));
                Log.i("INFOimageWidth", String.valueOf(imageWidth));
                Log.i("INFOdpi", String.valueOf(dpi));
                scanCallback.scanResult(new FingerprintImage(rawImage,imageWidth,imageHeight,dpi));
                close();

            } else if (ret2 == ErrorCodes.MORPHOERR_TIMEOUT) {
                Log.i("Capture Timed Out", String.valueOf(ret2));
                Log.i("Capture Timed Out", String.valueOf(morphoDevice.getInternalError()));
                close();
            }else{
                Log.i("Error in Capturing Finger", String.valueOf(ret2));
                Log.i("Error in Capturing Finger", String.valueOf(morphoDevice.getInternalError()));
                close();
            }

        } catch (Exception e) {
            close();
        }
    }

    /**
     * Cancel live acquisition.
     *
     */

    private void cancelLiveAcquisition() {
        try {

            morphoDevice.cancelLiveAcquisition();
            release();

        } catch (Exception e) {
            Log.e(TAG, "cancelLiveAcquisition", e);
        }
    }

    /**
     * Release.
     *
     */

    private void release() {

        morphoDevice.closeDevice();

    }

    private void updateLiveView(byte[] liveImage, String msg, int imageWidth,
                                int imageHeight){

        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;

        if (liveImage != null){
            byte[] Bits = new byte[liveImage.length * 4];
            int i;
            for (i = 0; i < liveImage.length; i++) {
                Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = ((byte) ~liveImage[i]);
                Bits[i * 4 + 3] = -1;
            }

            bm = Bitmap.createBitmap(imageWidth, imageHeight,
                    Bitmap.Config.ARGB_8888);
            bm.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));
        }
    }

    /**
     * Update.
     *
     * @version 2.0
     * @param observable
     *            the observable
     * @param arg
     *            the arg
     * @see Observer#update(Observable, Object)
     * @since 2.0
     */
    @Override
    public void update(Observable observable, Object arg) {
        try {
            // convert the object to a callback back message.
            CallbackMessage message = (CallbackMessage) arg;
            int type = message.getMessageType();
            /**
             * The raw header size.
             */
            int RAW_HEADER_SIZE = 12;
            switch (type) {
                // --------------------
                // MESSAGES
                // --------------------
                case 1:
                    // FingerPrintMessage fingerPrintMessage =
                    // FingerPrintMessage.UNKNOWN_MESSAGE;
                    // message is a command.
                    Integer command = (Integer) message.getMessage();

                    // Analyze the command.
                    switch (command) {
                        case 0:
                            /** < The terminal waits for the user's finger. */
                            // fingerPrintMessage =
                            // FingerPrintMessage.PLACE_FINGER_FOR_ACQUISITION;
                            callbackMsg = "Place Finger For Acquisition";
                            break;
                        case 1:
                            /** < The user must move his/her finger up. */
                            // fingerPrintMessage = FingerPrintMessage.MOVE_UP;
                            callbackMsg = "Move Up";
                            break;
                        case 2:
                            /** < The user must move his/her finger down. */
                            // fingerPrintMessage = FingerPrintMessage.MOVE_DOWN;
                            callbackMsg = "Move Down";
                            break;
                        case 3:
                            /** < The user must move his/her finger to the left. */
                            // fingerPrintMessage = FingerPrintMessage.MOVE_LEFT;
                            callbackMsg = "Move Left";
                            break;
                        case 4:
                            /** < The user must move his/her finger to the right. */
                            // fingerPrintMessage = FingerPrintMessage.MOVE_RIGHT;
                            callbackMsg = "Move Right";
                            break;
                        case 5:
                            /**
                             * < The user must press his/her finger harder for the
                             * device to acquire a larger fingerprint image.
                             */
                            // fingerPrintMessage = FingerPrintMessage.PRESS_HARDER;
                            callbackMsg = "Press Harder";
                            break;
                        case 6:
                            /**
                             * < The system has detected a latent fingerprint in the
                             * input fingerprint. Please change finger position.
                             */
                            // fingerPrintMessage =
                            // FingerPrintMessage.REMOVE_YOUR_FINGER;
                            callbackMsg = "Remove Finger";
                            break;
                        case 7:
                            /** < User must remove his finger. */
                            // fingerPrintMessage =
                            // FingerPrintMessage.REMOVE_YOUR_FINGER;
                            callbackMsg = "Remove Finger";
                            break;
                        case 8:
                            /** < The finger acquisition was correctly completed. */
                            // fingerPrintMessage =
                            // FingerPrintMessage.ACQUISITION_COMPLETE;
                            callbackMsg = "Finger Capture Complete";
                            break;
                    }

                    updateLiveView(null, callbackMsg, 0, 0);

                    break;

                // --------------------
                // IMAGES
                // --------------------
                case 2:
                    // message is a low resolution image, display it.
                    byte[] image = (byte[]) message.getMessage();
                    // quality = (Integer) message.getMessage();
                    byte[] imageRAW = new byte[image.length - RAW_HEADER_SIZE];
                    MorphoImage morphoImage = MorphoImage
                            .getMorphoImageFromLive(image);
                    if (morphoImage != null) {
                        int imageRowNumber = morphoImage.getMorphoImageHeader()
                                .getNbRow();
                        int imageColumnNumber = morphoImage.getMorphoImageHeader()
                                .getNbColumn();
                        System.arraycopy(image, RAW_HEADER_SIZE, imageRAW, 0,
                                image.length - RAW_HEADER_SIZE);

                        updateLiveView(imageRAW, callbackMsg, imageColumnNumber,
                                imageRowNumber);

                        /**
                         * The last image.
                         */
                        /**
                         * The last image width.
                         */
                        lastImageHeight = imageRowNumber;
                    }

                    break;
                // --------------------
                // QUALITY
                // --------------------
                case 3:
                    int quality = (Integer) message.getMessage();
                    Log.i("", "quality " + quality);
                    break;

            }

        } catch (Exception e) {
            Log.e(TAG, "update", e);
        }
    }

}
