package cl.autentia.reader.usb.eikon;

import android.app.Activity;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;

import java.util.ArrayList;
import java.util.List;

import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.uareu.Globals;

public class EikonReader extends FingerprintReader {

    private static final String TAG = "EikonReader";

    private Reader mReader = null;
    private int mDpi = 500;

    public EikonReader(Activity activity, UsbManager usbManager) {
        super(activity, usbManager);

        try {

            ReaderCollection readers = Globals.getInstance().getReaders(activity);
            mReader = Globals.getInstance().getReader(readers.get(0).GetDescription().name, activity);

        } catch (UareUException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void open() throws Exception {

        try {
            mReader.Open(Reader.Priority.EXCLUSIVE);
            mDpi = Globals.GetFirstDPI(mReader);

        } catch (Exception e) {
            throw new Exception("Error de inicialización del huellero");
        }

        isOpen = true;
    }

    @Override
    public void close() {
        try {
            mReader.Close();
            isOpen = false;
        } catch (UareUException e) {
            Log.d(TAG, e.toString());
        }
    }

    @Override
    public void cancelCapture() {
        try {
            mReader.CancelCapture();
        } catch (UareUException e) {
            Log.d(TAG, e.toString());
        } finally {
            close();
        }
    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {

        if (!isOpen) open();

        try {

            boolean scan = true;
            FingerprintImage image = null;

            while (scan) {

                Reader.CaptureResult captureResult = mReader.Capture(
                        Fid.Format.ANSI_381_2004,
                        Globals.DefaultImageProcessing,
                        mDpi,
                        -1);
                if (captureResult == null || captureResult.image == null) continue;

                image = createFingerprintImage(captureResult);
                break;
            }

            scanCallback.scanResult(image);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {

        if (!isOpen) open();

        Reader.Description description = mReader.GetDescription();

        String strPtapiGuid = new String("N/A");
        try {

            byte[] guid = mReader.GetParameter(Reader.ParamId.PARMID_PTAPI_GET_GUID);
            if (16 == guid.length) {
                final char[] hexArray = "0123456789ABCDEF".toCharArray();
                char[] hexChars = new char[guid.length * 2];
                for (int j = 0; j < guid.length; j++) {
                    int v = guid[j] & 0xFF;
                    hexChars[j * 2] = hexArray[v >>> 4];
                    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                }
                strPtapiGuid = new String(hexChars);
            }

            List<String> original = getParts(strPtapiGuid, 2);

            StringBuilder str = new StringBuilder();
            str.append("{");
            str.append(original.get(3));
            str.append(original.get(2));
            str.append(original.get(1));
            str.append(original.get(0));
            str.append("-");
            str.append(original.get(5));
            str.append(original.get(4));
            str.append("-");
            str.append(original.get(7));
            str.append(original.get(6));
            str.append("-");
            str.append(original.get(8));
            str.append(original.get(9));
            str.append("-");
            str.append(original.get(10));
            str.append(original.get(11));
            str.append(original.get(12));
            str.append(original.get(13));
            str.append(original.get(14));
            str.append(original.get(15));
            str.append("}");

            strPtapiGuid = str.toString();

        } catch (Exception e) {
            throw new Exception("Número de serie inválido");
        }

        FingerprintInfo info = new FingerprintInfo(
                strPtapiGuid,
                String.format("%s.%s.%s",
                        description.version.firmware_version.major,
                        description.version.firmware_version.minor,
                        description.version.firmware_version.maintenance),
                "reader.eikon");

        infoCallback.infoResult(info);
    }

    private FingerprintImage createFingerprintImage(Reader.CaptureResult captureResult) {
        final Fid.Fiv fiv = captureResult.image.getViews()[0];
        byte[] fingerImage = fiv.getImageData();
        return new FingerprintImage(fingerImage, fiv.getWidth(), fiv.getHeight(), captureResult.image.getImageResolution());
    }

    private static List<String> getParts(String string, int partitionSize) {
        List<String> parts = new ArrayList<String>();
        int len = string.length();
        for (int i = 0; i < len; i += partitionSize) {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }
        return parts;
    }
}