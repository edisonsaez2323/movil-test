package cl.autentia.reader.usb.morpho;

import com.morpho.morphosmart.sdk.MorphoDevice;

public class MorphoGlobals  {

    static MorphoDevice morphoDevice;

    //Clase para acceder a las librerias locales del modulo "morphomodule".
    public static MorphoDevice GetMorphodeviceInstance(){

        if (morphoDevice == null ) {
            morphoDevice = new MorphoDevice();
            return morphoDevice;
        }
        else return morphoDevice;

    }
}


