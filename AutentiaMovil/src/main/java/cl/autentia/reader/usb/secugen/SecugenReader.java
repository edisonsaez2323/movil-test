package cl.autentia.reader.usb.secugen;

import android.app.Activity;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Base64;
import android.util.Log;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGDeviceInfoParam;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.secugen.JSGFPLibGlobal;


public class SecugenReader extends FingerprintReader {

    private JSGFPLib mReader;
    private SGDeviceInfoParam deviceInfo;

    SecugenReader(Activity activity, UsbManager usbManager) {
        super(activity, usbManager);
        mReader = new JSGFPLibGlobal().getReader(usbManager);
    }

    @Override
    public void open() throws Exception {

        long error = mReader.Init(SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
            throw new Exception("Error en inicialización de dispositivo");
        }

        UsbDevice usbDevice = mReader.GetUsbDevice();
        if (usbDevice == null) {
            throw new Exception("Error en inicialización de dispositivo");
        }

        error = mReader.OpenDevice(0);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
            throw new Exception("Error en apertura del dispositivo");
        }

        if (deviceInfo == null)
            deviceInfo = new JSGFPLibGlobal().getDeviceInfo();
        error = mReader.GetDeviceInfo(deviceInfo);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
            throw new Exception("Error en obtención de info del dispositivo");
        }

        isOpen = true;
    }

    @Override
    public void close() {
        mReader.CloseDevice();
        mReader.Close();
        isOpen = false;
    }

    @Override
    public void cancelCapture() {

    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {

        if (!isOpen) open();

        int mImageWidth = deviceInfo.imageWidth;
        int mImageHeight = deviceInfo.imageHeight;
        int mDPI = deviceInfo.imageDPI;

        try {

            mReader.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_ANSI378);
            int[] mMaxTemplateSize = new int[1];
            mReader.GetMaxTemplateSize(mMaxTemplateSize);

            FingerprintImage image = null;
            byte[] buffer = new byte[mImageWidth * mImageHeight];

            boolean scan = true;
            while (scan) {
                long error = mReader.GetImage(buffer);

                if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
                    continue;
                }

                image = new FingerprintImage(buffer, mImageWidth, mImageHeight, mDPI);
                int[] quality = new int[1];

                mReader.GetImageQuality(
                        image.getWidth(),
                        image.getHeight(),
                        image.getRawImage(),
                        quality);

                image.setQuality(quality[0]);

                if (quality[0] < 80) {
                    // Capture again
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        Log.d("Error", e.getMessage());
                    }
                } else {
                    break;
                }
            }

            scanCallback.scanResult(image);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {

        if (!isOpen) open();

        FingerprintInfo info = new FingerprintInfo(
                Base64.encodeToString(deviceInfo.deviceSN(), Base64.DEFAULT),
                String.valueOf(deviceInfo.deviceID),
                "reader.secugen");

        infoCallback.infoResult(info);
    }
}
