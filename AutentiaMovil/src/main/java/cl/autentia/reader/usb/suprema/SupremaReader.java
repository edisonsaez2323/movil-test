package cl.autentia.reader.usb.suprema;

import android.app.Activity;
import android.hardware.usb.UsbManager;

import com.android.biomini.BioMiniAndroid;

import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintReader;


public class SupremaReader extends FingerprintReader {

    private static BioMiniAndroid mBioMiniAndroid;

    SupremaReader(Activity activity, UsbManager usbManager) {
        super(activity, usbManager);
        mBioMiniAndroid = new BioMiniAndroid(usbManager);
    }

    @Override
    public void open() throws Exception {
        int ufa_res = 0;

        ufa_res = mBioMiniAndroid.UFA_FindDevice();
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        ufa_res = mBioMiniAndroid.UFA_Init();
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        isOpen = true;
    }

    @Override
    public void close() {
        mBioMiniAndroid.UFA_Uninit();
        isOpen = false;
    }

    @Override
    public void cancelCapture() {
        mBioMiniAndroid.UFA_AbortCapturing();
    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {

        int nsensitivity = 7;
        int ntimeout = 0;
        int nsecuritylevel = 4;
        int bfastmode = 1;

        if (!isOpen) open();

        int ufa_res = mBioMiniAndroid.UFA_SetParameter(mBioMiniAndroid.UFA_PARAM_SENSITIVITY, nsensitivity);
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        ufa_res = mBioMiniAndroid.UFA_SetParameter(mBioMiniAndroid.UFA_PARAM_TIMEOUT, ntimeout * 1000);
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        ufa_res = mBioMiniAndroid.UFA_SetParameter(mBioMiniAndroid.UFA_PARAM_SECURITY_LEVEL, nsecuritylevel);
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        ufa_res = mBioMiniAndroid.UFA_SetParameter(mBioMiniAndroid.UFA_PARAM_FAST_MODE, bfastmode);
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));

        ufa_res = mBioMiniAndroid.UFA_SetTemplateType(mBioMiniAndroid.UFA_TEMPLATE_TYPE_ISO19794_2);
        if (ufa_res != mBioMiniAndroid.UFA_OK)
            throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));


        int imageWidth = mBioMiniAndroid.getImageWidth();
        int imageHeight = mBioMiniAndroid.getImageHeight();
        byte[] pImage = new byte[imageWidth * imageHeight];
        ufa_res = mBioMiniAndroid.UFA_CaptureSingle(pImage);

        try {

            byte[] template = new byte[1024];
            int[] templateSize = new int[4];
            int[] nquality = new int[4];
            ufa_res = mBioMiniAndroid.UFA_ExtractTemplate(template, templateSize, nquality, 1024);

            //if nQuality <40 error repetir
            if (ufa_res != mBioMiniAndroid.UFA_OK || nquality[0] < 40) {
                throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));
            }

            FingerprintImage img = new FingerprintImage(
                    pImage, mBioMiniAndroid.getImageWidth(),
                    mBioMiniAndroid.getImageHeight(), 500);

            if (ufa_res != mBioMiniAndroid.UFA_OK) {
                throw new Exception(mBioMiniAndroid.UFA_GetErrorString(ufa_res));
            }

            scanCallback.scanResult(img);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {

        if (!isOpen) open();

        FingerprintInfo info = new FingerprintInfo(
                mBioMiniAndroid.UFA_GetSerialNumber(),
                String.format("Suprema %s", mBioMiniAndroid.UFA_GetVersionString()),
                "reader.suprema");

        infoCallback.infoResult(info);
    }
}
