package cl.autentia.reader.usb.uareu;

import android.app.Activity;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;

import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;

import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.uareu.Globals;

public class UareUReader extends FingerprintReader {

    private static final String TAG = "UareUJava";

    private Reader mReader = null;
    private int mDpi = 500;

    public UareUReader(Activity activity, UsbManager usbManager) {
        super(activity, usbManager);

        try {

            ReaderCollection readers = Globals.getInstance().getReaders(activity);
            mReader = Globals.getInstance().getReader(readers.get(0).GetDescription().name, activity);

        } catch (UareUException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void open() throws Exception {
        try {
            mReader.Open(Reader.Priority.EXCLUSIVE);
            mDpi = Globals.GetFirstDPI(mReader);

        } catch (Exception e) {
            throw new Exception("Error de inicialización del huellero");
        }

        isOpen = true;
    }

    @Override
    public void close() {
        try {
            mReader.Close();
            isOpen = false;
        } catch (UareUException e) {
            Log.d(TAG, e.toString());
        }
    }

    @Override
    public void cancelCapture() {

        try {
            mReader.CancelCapture();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        } finally {
            close();
        }
    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {

        if (!isOpen) open();

        try {

            boolean scan = true;
            FingerprintImage image = null;

            while (scan) {

                Reader.CaptureResult captureResult = mReader.Capture(
                        Fid.Format.ANSI_381_2004,
                        Globals.DefaultImageProcessing,
                        mDpi,
                        -1);
                if (captureResult == null || captureResult.image == null) continue;

                image = createFingerprintImage(captureResult);
                break;
            }

            scanCallback.scanResult(image);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {

        if (!isOpen) open();

        Reader.Description description = mReader.GetDescription();

        FingerprintInfo info = new FingerprintInfo(
                description.serial_number,
                String.format("%s.%s.%s",
                        description.version.hardware_version.major,
                        description.version.hardware_version.minor,
                        description.version.hardware_version.maintenance),
                "reader.uareu");

        infoCallback.infoResult(info);
    }

    private FingerprintImage createFingerprintImage(Reader.CaptureResult captureResult) {
        final Fid.Fiv fiv = captureResult.image.getViews()[0];
        byte[] fingerImage = fiv.getImageData();
        return new FingerprintImage(fingerImage, fiv.getWidth(), fiv.getHeight(), captureResult.image.getImageResolution());
    }
}
