package cl.autentia.reader;

import android.app.Activity;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.util.Log;

import cl.autentia.http.HttpUtil;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

public abstract class FingerprintManager {

    private static final String TAG = "USBManager";

    private static Class<?> mUsbFingerprintClass;
    protected Activity mActivity;

    public static FingerprintManager getInstance(Activity activity) {
        try {
            if (mUsbFingerprintClass == null) {
                synchronized (FingerprintManager.class) {
                    AutentiaPreferences preferences = new AutentiaPreferences(activity);
                    if (mUsbFingerprintClass == null) {
                        String usbControllerClassName = preferences.getString(KeyPreferences.FINGERPRINT_READER_CONTROLLER_CLASS);
                        mUsbFingerprintClass = Class.forName(usbControllerClassName);
                        Log.i(TAG, "USB_CONTROLLER_CLASS = " + usbControllerClassName);
                    }
                }
            }
            FingerprintManager manager = (FingerprintManager) mUsbFingerprintClass.newInstance();
            manager.setActivity(activity);
            return manager;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Error en configuración de huellero");
        }
    }

    private void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    public abstract boolean isUsbPresent() throws Exception;

    public abstract boolean isUsbAccessible();

    public abstract void requestAccess(int requestCode);

    public abstract void processAccessGrant(int resultCode, Intent result, FingerprintGrantReceiver receiver);

    public abstract FingerprintReader getReader(Activity activity);

    public abstract UsbDevice findSuitableDevice() throws Exception;
}
