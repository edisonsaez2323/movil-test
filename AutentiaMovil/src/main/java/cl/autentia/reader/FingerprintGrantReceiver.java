package cl.autentia.reader;

public interface FingerprintGrantReceiver {

    void onFingerprintAccessGranted();

    void onFingerprintAccessDenied();

    void onFingerprintAccessError();
}
