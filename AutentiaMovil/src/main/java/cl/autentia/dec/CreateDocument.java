package cl.autentia.dec;

public class CreateDocument {

    public CreateDocument(String type_code, String name, String institution, String[] signers_roles,
                          String[] signers_institutions, String[] signers_emails,
                          String[] signers_ruts, String[] signers_type, String[] signers_order,
                          String[] signers_notify, String template_version, String[] tags,
                          String session_id) {

        this.type_code = type_code;
        this.name = name;
        this.institution = institution;
        this.signers_roles = signers_roles;
        this.signers_institutions = signers_institutions;
        this.signers_emails = signers_emails;
        this.signers_ruts = signers_ruts;
        this.signers_type = signers_type;
        this.signers_order = signers_order;
        this.signers_notify = signers_notify;
        this.template_version = template_version;
        this.tags = tags;
        this.session_id = session_id;
    }

    public String type_code;
    public String name;
    public String institution;
    public String[] signers_roles;
    public String[] signers_institutions;
    public String[] signers_emails;
    public String[] signers_ruts;
    public String[] signers_type;
    public String[] signers_order;
    public String[] signers_notify;
    public String template_version;
    public String[] tags;
    public String session_id;
}
