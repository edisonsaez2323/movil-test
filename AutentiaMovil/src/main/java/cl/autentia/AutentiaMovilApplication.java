package cl.autentia;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import cl.autentia.FirebaseService.FirebaseValueEventListenerService;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.MyServiceSyncAdapter;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AutentiaMovilApplication extends MultiDexApplication {

    public static final String AUTENTIA_MOVIL_PROPERTIES_PATH_CONTENTS = "Contents/movil.properties";
    public static final String AUTENTIA_MOVIL_PROPERTIES_PATH = "AutentiaMovil/movil.properties";
    public static final String AUTENTIA_MOVIL_PROPERTIES_PATH_ROOT = "movil.properties";

    private static final String TAG = "AutentiaAPP";

    static {
//        if (!OpenCVLoader.initDebug()) {
//
//        }
    }

    AutentiaWSClient mAutentiaWSClient;
    private AutentiaPreferences preferences;

    private static List<String> getParts(String string, int partitionSize) {
        List<String> parts = new ArrayList<String>();
        int len = string.length();
        for (int i = 0; i < len; i += partitionSize) {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }
        return parts;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(configuration);

        mAutentiaWSClient = new AutentiaWSClient(this);

        preferences = new AutentiaPreferences(this);

        AndroidNetworking.initialize(getApplicationContext());

        FirebaseValueEventListenerService.enqueueWork(this, new Intent(this, FirebaseValueEventListenerService.class));

        //TODO verificando version

        try {
            loadPreferences();

        } catch (Exception e) {

        }


        MyServiceSyncAdapter.syncImmediately(getApplicationContext());

    }

    private void loadPreferences() throws Exception {

        boolean confFromFile = false;
        Properties properties = new Properties();

        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard, AUTENTIA_MOVIL_PROPERTIES_PATH);
        File fileInRoot = new File(sdcard, AUTENTIA_MOVIL_PROPERTIES_PATH_ROOT);
        File fileInContents = new File(sdcard, AUTENTIA_MOVIL_PROPERTIES_PATH_CONTENTS);

        if (file.exists()) {
            confFromFile = setConfigFromProperties(properties, sdcard, file);

        } else if (fileInRoot.exists()) {
            confFromFile = setConfigFromProperties(properties, sdcard, fileInRoot);

        } else if (fileInContents.exists()) {
            confFromFile = setConfigFromProperties(properties, sdcard, fileInContents);

        }
        if (!confFromFile) {

            if (preferences.getString(KeyPreferences.URL_BASE) == null) {

                properties.load(getAssets().open("default.properties"));

                if (properties.size() >= 0) {

                    preferences.deleteAll();
                    Auditoria_.deleteAllAudit();
                    Evidence_.deleteAllEvidences();

                    for (Map.Entry<Object, Object> entry : properties
                            .entrySet()) {

                        preferences.setString(String.valueOf(entry.getKey()),
                                String.valueOf(entry.getValue()));
                    }
//                    Log.e("ENTER - default", "AUDRECH");
//                    if (Utils.isConnectingToInternet(this))
//                        getAudrech();

                }
            }
        }
    }

    /**
     * @param properties
     * @param sdcard
     * @param file
     * @return
     * @throws Exception
     */
    private boolean setConfigFromProperties(Properties properties, File sdcard, File file) throws Exception {

        boolean confFromFile = false;

        //Check storage permission
        if (ContextCompat.checkSelfPermission( this, Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED ) {
            return false;
        }

        properties.load(new FileReader(file));
        if (properties.size() >= 0) {

//            init();
            preferences.deleteAll();
            Auditoria_.deleteAllAudit();

            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                preferences.setString(String.valueOf(entry.getKey()),
                        String.valueOf(entry.getValue()));
            }
            confFromFile = true;

        }
        File fileRenamed = new File(sdcard, String.format("%s.%s", AUTENTIA_MOVIL_PROPERTIES_PATH,
                new SimpleDateFormat("yyyyMMdd").format(new Date())));
        file.renameTo(fileRenamed);
        return confFromFile;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
