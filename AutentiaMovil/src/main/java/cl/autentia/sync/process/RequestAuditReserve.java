package cl.autentia.sync.process;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import cl.autentia.activity.NavegationMenuActivity;
import cl.autentia.configuration.fragment.CurrentConfigurationFragment;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.PreAuditResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;


public class RequestAuditReserve {

    private static final String TAG = "RequestAuditReserve";

    private String mInstitucion;
    private String mRutInstitucion;

    private AutentiaPreferences mAutentiaPreferences;
    private AutentiaWSClient mAutentiaWSClient;
    private Context mContext;
    private Activity mActivity;
    private int audTotal;

    public RequestAuditReserve(Context context) {

        try {

            mAutentiaWSClient = new AutentiaWSClient(context);
            mAutentiaPreferences = new AutentiaPreferences(context);
            mRutInstitucion = mAutentiaPreferences.getString(KeyPreferences.RUN_INSTITUTION);
            mInstitucion = mAutentiaPreferences.getString(KeyPreferences.INSTITUTION);

            mContext = context;
            mActivity = (Activity) context;

        } catch (Exception e) {
//            HttpUtil.sendReportUncaughtException(e, mContext);
            Log.d(TAG,""+ e.getMessage());
        }
    }

    /**
     * @param newQuantity
     * @return
     */
    public synchronized void getAudit(final int newQuantity, final int currentQuantity, final int maxQuantity) {

        try {
            mAutentiaWSClient.getPreAudit(String.valueOf(newQuantity), mInstitucion, mRutInstitucion, new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {

                    int newnewQuantity;

                    PreAuditResp preAuditResp = new Gson().fromJson(result.getString("result"), PreAuditResp.class);
                    ArrayList<String> auditoriasReservadas = new ArrayList<>();
                    if (preAuditResp.getAuditorias().size() > 0 && Auditoria_.getAuditCount() < maxQuantity){
                        auditoriasReservadas.addAll(preAuditResp.getAuditorias());
                        for (String audit : auditoriasReservadas) {
                            Auditoria_ auditoria = new Auditoria_();
                            auditoria.setNumeroAuditoria(audit);
                            Auditoria_.addAuditoria(auditoria);
                        }
                        int auditCount = Auditoria_.getAuditCount();
                        newnewQuantity = maxQuantity - auditCount;
                        if(newnewQuantity > 0) {
//                            Intent intent = new Intent(
//                                    "com.durga.action.reserve");
//                            intent.putExtra("quantity",auditCount);
//                            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
//                                    .getInstance(mContext);
//                            localBroadcastManager.sendBroadcast(intent);
                            broadcastCallerAudit(auditCount);
                            getAudit(newnewQuantity < 10 ? newnewQuantity : 10, auditCount, maxQuantity);
                        }else{
                            broadcastCaller();
                        }
                    }else{
                        broadcastCaller();
                    }

                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    if(result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 9001){
                        Log.e("onResponseError if",result.returnCode.getDescription());
                        Intent intent = new Intent(
                                "com.durga.action.reserve");
                        intent.putExtra("message",result.status);
                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                                .getInstance(mContext);
                        localBroadcastManager.sendBroadcast(intent);
                    }else{
                        Log.e("onResponseError else","ANOTHERERROR");
                        Log.e("stack", result.status);
                    }

                }
            });
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mContext);
            Log.d(TAG,""+ e.getMessage());
        }
    }

    private void broadcastCaller(){
        try {
            Intent broadcast = new Intent();
            broadcast.putExtra("quantity", Auditoria_.getAuditCount());
            broadcast.setAction(CurrentConfigurationFragment.BROADCAST_UPDATE_QUANTITY_AUDIT);
            mContext.sendBroadcast(broadcast);
            // Proceso exitoso
            Log.d(TAG, "Reserva de auditoria ejecutado exitosamente, cantidad:" + Auditoria_.getAuditCount());
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mContext);
            Log.d(TAG, e.getMessage());
        }
    }
    private void broadcastCallerAudit(int auditcount){
        try {
            Intent broadcast = new Intent();
            broadcast.putExtra("quantity", auditcount);
            broadcast.setAction(CurrentConfigurationFragment.BROADCAST_UPDATE_QUANTITY_AUDIT);
            mContext.sendBroadcast(broadcast);
            // Proceso exitoso
            Log.d(TAG, "Reserva de auditoria ejecutado exitosamente, cantidad:" + Auditoria_.getAuditCount());
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mContext);
            Log.d(TAG, e.getMessage());
        }
    }
}