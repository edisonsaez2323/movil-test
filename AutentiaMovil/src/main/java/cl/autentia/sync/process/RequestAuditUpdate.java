package cl.autentia.sync.process;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

import cl.autentia.configuration.fragment.EvidenceOfflineFragment;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.NotificationHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GetInfoAuditResp;
import cl.autentia.http.parameters.UpdateAuditResp;
import cl.autentia.realmObjects.Evidence_;

public class RequestAuditUpdate {

    private static final String TAG = "RequestAuditUpdate";

    private Context mContext;
    private AutentiaWSClient mAutentiaWSClient;

    public RequestAuditUpdate(Context context) {

        try {

            mAutentiaWSClient = new AutentiaWSClient(context);
            mContext = context;

        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mContext);
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * @param tipo
     * @param idEvidencia
     * @param numAuditoria
     * @param institution
     * @param origen
     * @param operacion
     * @param numSerieLector
     * @param tipoLector
     * @param rutOperador
     * @param resultadoVerificacion
     * @param descripcionJsonData
     * @param versionApp
     * @param rutPersona
     * @param nombre
     * @param dedoId
     * @param dedoFecha
     * @param ubicacion
     * @param muestra
     * @param texto1
     * @param texto2
     * @throws Exception
     */
    public void updateAudit(
            final String tipo,
            final int idEvidencia,
            final String numAuditoria,
            final String institution,
            final String origen,
            final String operacion,
            final String numSerieLector,
            final String tipoLector,
            final String rutOperador,
            final String resultadoVerificacion,
            final String descripcionJsonData,
            final String versionApp,
            final String rutPersona,
            final String nombre,
            final String dedoId,
            final String dedoFecha,
            final String ubicacion,
            final byte[] muestra,
            final byte[] bitmap,
            final String texto1,
            final String texto2,
            final String trackId) throws Exception {

        mAutentiaWSClient.updateAudit(numAuditoria, institution, origen, operacion, numSerieLector, tipoLector,
                rutOperador, resultadoVerificacion, descripcionJsonData, versionApp, rutPersona,
                nombre, dedoId, dedoFecha, ubicacion, muestra, bitmap, texto1, texto2, trackId,new ResponseCallback() {
                    @Override
                    public void onResponseSuccess(Bundle result) {

                        final UpdateAuditResp updateAuditResp = new Gson().fromJson(result.getString("result"), UpdateAuditResp.class);

                        if (updateAuditResp.getErr() == 0) {

//                            try {
//                                Evidence_.deleteEvidence(numAuditoria);
//                            } catch (Exception e) {
//                                try {
//                                    throw new Exception(updateAuditResp.getGlosa());
//                                } catch (Exception e1) {
//                                    e1.printStackTrace();
//                                }
//                            }
//
//                            NotificationHelper.showNotification(
//                                    mContext,
//                                    String.format("%s %s", tipo, "registrada en Servidor"),
//                                    String.format("Auditoria: %s", numAuditoria),
//                                    idEvidencia);
//
//                            Intent broadcast = new Intent();
//                            broadcast.setAction(EvidenceOfflineFragment.BROADCAST_EVIDENCE);
//                            mContext.sendBroadcast(broadcast);

                            try {
                                mAutentiaWSClient.getStatusAudit(numAuditoria, institution, numSerieLector, new ResponseCallback() {
                                    @Override
                                    public void onResponseSuccess(Bundle result) {
                                        GetInfoAuditResp gir = new Gson().fromJson(result.getString("result"), GetInfoAuditResp.class);

                                        String statusAudit = gir.getResultado();
                                        if (statusAudit.equals("0000") || statusAudit.equals("0002") || statusAudit.equals("0003")) {
                                            try {
                                                Evidence_.deleteEvidence(numAuditoria);
                                            } catch (Exception e) {
                                                try {
                                                    throw new Exception(updateAuditResp.getGlosa());
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                            Intent broadcast = new Intent();
                                            broadcast.setAction(EvidenceOfflineFragment.BROADCAST_EVIDENCE);
                                            mContext.sendBroadcast(broadcast);

                                            NotificationHelper.showNotification(
                                                    mContext,
                                                    String.format("%s %s", tipo, "registrada en Servidor"),
                                                    String.format("Auditoria: %s", numAuditoria),
                                                    idEvidencia);
                                        }
                                    }

                                    @Override
                                    public void onResponseError(AutentiaMovilException result) {
                                        Log.e(TAG, "ERROR GETTING STATUSAUDIT");

                                    }
                                });

                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }

                            // Proceso exitoso
                            Log.d(TAG, "Actualización de auditoria ejecutada exitosamente");

                        } else if (updateAuditResp.getErr() == 5007) {
                            try {

                                mAutentiaWSClient.getStatusAudit(numAuditoria, institution, numSerieLector, new ResponseCallback() {
                                    @Override
                                    public void onResponseSuccess(Bundle result) {
                                        GetInfoAuditResp gir = new Gson().fromJson(result.getString("result"), GetInfoAuditResp.class);

                                        String statusAudit = gir.getResultado();
                                        if (statusAudit.equals("0000") || statusAudit.equals("0002") || statusAudit.equals("0003")) {
                                            try {
                                                Evidence_.deleteEvidence(numAuditoria);
                                            } catch (Exception e) {
                                                try {
                                                    throw new Exception(updateAuditResp.getGlosa());
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                            Intent broadcast = new Intent();
                                            broadcast.setAction(EvidenceOfflineFragment.BROADCAST_EVIDENCE);
                                            mContext.sendBroadcast(broadcast);

                                            NotificationHelper.showNotification(
                                                    mContext,
                                                    String.format("%s %s", tipo, "registrada en Servidor"),
                                                    String.format("Auditoria: %s", numAuditoria),
                                                    idEvidencia);
                                        }
                                    }

                                    @Override
                                    public void onResponseError(AutentiaMovilException result) {
                                        Log.e(TAG, "ERROR GETTING STATUSAUDIT");

                                    }
                                });
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }

                        } else {
                            try {
                                throw new Exception(updateAuditResp.getGlosa());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onResponseError(AutentiaMovilException result) {
                        Log.e(TAG, "HERRORUPDATINGAUDIT");
                    }
                });
    }
}