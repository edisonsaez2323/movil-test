package cl.autentia.sync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

public class MyServiceSync extends JobIntentService {
    private static final Object sSyncAdapterLock = new Object();
    private static MyServiceSyncAdapter myServiceSyncAdapter = null;
    public static final int JOB_ID = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyServiceSync", "onCreate");
        synchronized (sSyncAdapterLock) {
            if (myServiceSyncAdapter == null) {
                myServiceSyncAdapter = new MyServiceSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, MyServiceSync.class, JOB_ID, work);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MyServiceSync", "onBind");
        return myServiceSyncAdapter.getSyncAdapterBinder();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

    }
}