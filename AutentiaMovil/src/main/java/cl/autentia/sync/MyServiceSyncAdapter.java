package cl.autentia.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.acepta.android.fetdroid.R;

import org.json.JSONObject;

import java.util.List;

import cl.autentia.activity.VerificationActivity;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.process.RequestAuditReserve;
import cl.autentia.sync.process.RequestAuditUpdate;

public class MyServiceSyncAdapter extends AbstractThreadedSyncAdapter {
    //TODO change this constant SYNC_INTERVAL to change the sync frequency
    public static final int SYNC_INTERVAL = 10; //60 * 180;       // 60 seconds (1 minute) * 180 = 3 hours
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;
    private Context mContext;

    private static final String TAG = "SyncAdapter";
    private static final int QUANTITY_INTERVAL = 10;

    private RequestAuditReserve mRequestAuditReserve;
    private RequestAuditUpdate mRequestAuditUpdate;
    private AutentiaPreferences mAutentiaPreferences;
    private AutentiaWSClient mAutentiaWSClient;

    public MyServiceSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i("MyServiceSyncAdapter", "onPerformSync");
        //TODO get some data from the internet, api calls, etc.
        //TODO save the data to database, sqlite, couchbase, etc

        mAutentiaPreferences = new AutentiaPreferences(mContext);


        if (mAutentiaPreferences.getString(KeyPreferences.DATA_CONFIGURATION, "").equals("00002")) {
            //Obtención de properties desde el directorio del servidor base.
            Log.d(TAG, "Sincronización de Properties desde el servidor asociadas a la Institución");
            Log.d(TAG, "Sincronización de reserva de auditorias");

            mRequestAuditReserve = new RequestAuditReserve(mContext);

            try {

                int rangeAudit = Integer.parseInt(mAutentiaPreferences.getString(KeyPreferences.AUDIT_VALUE));

                int currentAudit = Auditoria_.getAuditCount();

                int quantity = rangeAudit - currentAudit;

                Log.d("RequestAuditReserve", quantity + "");

                if (quantity > 0 && mAutentiaPreferences.getString(KeyPreferences.DATA_CONFIGURATION).equals("00002")
                        && mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER) != null
                        && !mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER).equals("")) { //pedimos auditorias SOLO después que se seteo la config del huellero
                    mRequestAuditReserve.getAudit(quantity > QUANTITY_INTERVAL ? QUANTITY_INTERVAL : quantity, currentAudit, rangeAudit);
                    new AutentiaWSClient(mContext).deviceRegistration();
                } else {
                    if (mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER).equals("")) {
                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                                .getInstance(mContext);
                        localBroadcastManager.sendBroadcast(new Intent(
                                "com.durga.action.close"));
                    }
                }

            } catch (Exception e) {
//                sendReportUncaughtException(e);
                Log.d(TAG, e.getMessage());
            }

            Log.d(TAG, "Sincronización de evidencias");

            mRequestAuditUpdate = new RequestAuditUpdate(mContext);

            try {

                List<Evidence_> evidencesOffline = Evidence_.getAllEvicences();

                for (Evidence_ offline : evidencesOffline) {

                    JSONObject jsonEvidence = new JSONObject(offline.jsonData);
                    String tipoEvidencia = jsonEvidence.getString("type");
                    String numAuditoria = jsonEvidence.getString("codigoAuditoria");
                    String institution = jsonEvidence.getString("institucion");
                    String origen = jsonEvidence.getString("origen");
                    String operacion = jsonEvidence.getString("operacion");
                    String numSerieLector = jsonEvidence.getString("numeroSerieLector");
                    String rutOperador = jsonEvidence.getString("rutOperador");
                    String resultadoVerificacion = jsonEvidence.getString("resultado");
                    String descripcionJsonData = jsonEvidence.toString();
                    String versionApp = jsonEvidence.getString("versionApp");
                    String rutPersona = String.format("%s-%s", jsonEvidence.getString("rut"), jsonEvidence.getString("dv"));
                    String nombre = jsonEvidence.getString("nombre");
                    String dedoId = jsonEvidence.getString("dedoCodificado");
                    String dedoFecha = jsonEvidence.getString("dedoFecha");
                    String ubicacion = jsonEvidence.getString("ubicacion");
                    String tipoLector = jsonEvidence.getString("tipoLector");
                    String trackId = jsonEvidence.getString("trackId");
                    String texto1 = "";
                    String texto2 = "";
                    if (tipoEvidencia.equals("Evidencia")) {
                        texto1 = "indicio";
                        texto2 = "Toma de Evidencia, LA VERIFICACION ES RESPONSABILIDAD DEL CLIENTE";
                    } else if (tipoEvidencia.equals("Verificacion_contra_Cedula_Nueva_OFFLINE") && offline.getFirma() != null && offline.getRetrato() != null) {
                        texto1 = VerificationActivity.URI_STANDARD + offline.getFirma() + ";" + VerificationActivity.URI_STANDARD + offline.getRetrato();
                    }


                    mRequestAuditUpdate.updateAudit(
                            tipoEvidencia,
                            offline.idVerification,
                            numAuditoria,
                            institution,
                            origen,
                            operacion,
                            numSerieLector,
                            tipoLector,
                            rutOperador,
                            resultadoVerificacion,
                            descripcionJsonData,
                            versionApp,
                            rutPersona,
                            nombre,
                            dedoId,
                            dedoFecha,
                            ubicacion,
                            offline.fingerprint,
                            offline.fingerprintBmp,
                            texto1,
                            texto2,
                            trackId);
                }
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }

    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, new Bundle(), syncInterval);
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        Log.i("MyServiceSyncAdapter", "syncImmediately");
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context), context.getString(R.string.content_authority), bundle);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE); // Get an instance of the Android account manager
        Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.account)); // Create the account type and default account

        // If the password doesn't exist, the account doesn't exist
        if (accountManager.getPassword(newAccount) == null) {
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                Log.e("MyServiceSyncAdapter", "getSyncAccount Failed to create new account.");
                return null;
            }
            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        Log.i("MyServiceSyncAdapter", "onAccountCreated");
        MyServiceSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
        Log.d("MyServiceSyncAdapter", "initializeSyncAdapter");
        getSyncAccount(context);
    }

    public void sendReportUncaughtException(Exception exception) {
        HttpUtil.sendReportUncaughtException(exception, getContext());
    }
}