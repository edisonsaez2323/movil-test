package cl.autentia.sync.onrequest;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.json.JSONObject;

import java.util.List;

import cl.autentia.activity.VerificationActivity;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.process.RequestAuditReserve;
import cl.autentia.sync.process.RequestAuditUpdate;

@EActivity
public class SyncNow extends Activity {

    public static String TAG = "DO_NOW";
    private RequestAuditUpdate mRequestAuditUpdate;
    private Context mContext;
    private AutentiaPreferences mAutentiaPreferences;
    private RequestAuditReserve mRequestAuditReserve;
    private static final int QUANTITY_INTERVAL = 10;
    private VerificationActivity.NetworkChangeReceiver mReceiver;

    public SyncNow(){

    }

    public SyncNow(Context context, VerificationActivity.NetworkChangeReceiver receiver) {
        mContext = context;
        mReceiver = receiver;
        syncNow();
    }

    @Background
    void syncNow() {

        mAutentiaPreferences = new AutentiaPreferences(mContext);

        if(mAutentiaPreferences.getString(KeyPreferences.DATA_CONFIGURATION,"").equals("00002")){
            //Obtención de properties desde el directorio del servidor base.
            Log.d(TAG, "Sincronización de Properties desde el servidor asociadas a la Institución");
            Log.d(TAG, "Sincronización de reserva de auditorias");

            mRequestAuditReserve = new RequestAuditReserve(mContext);
            try {

                int rangeAudit = Integer.parseInt(mAutentiaPreferences.getString(KeyPreferences.AUDIT_VALUE));

                int currentAudit = Auditoria_.getAuditCount();

                int quantity = rangeAudit - currentAudit;

                Log.d("RequestAuditReserve", quantity + "");

                if (quantity > 0 && mAutentiaPreferences.getString(KeyPreferences.DATA_CONFIGURATION).equals("00002")
                        && mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER) != null
                        && !mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER).equals("")) { //pedimos auditorias SOLO después que se seteo la config del huellero
                    mRequestAuditReserve.getAudit(quantity > QUANTITY_INTERVAL ? QUANTITY_INTERVAL : quantity, currentAudit, rangeAudit);
//                    new AutentiaWSClient(mContext).deviceRegistration();
                }else{
                    if (mAutentiaPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER).equals("")){
                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                                .getInstance(mContext);
                        localBroadcastManager.sendBroadcast(new Intent(
                                "com.durga.action.close"));
                    }
                }

            } catch (Exception e) {
//                sendReportUncaughtException(e);
                Log.d(TAG, e.getMessage());
            }

            Log.d(TAG, "Sincronización de evidencias");

            mRequestAuditUpdate = new RequestAuditUpdate(mContext);

            try {

                List<Evidence_> evidencesOffline = Evidence_.getAllEvicences();

                for (Evidence_ offline : evidencesOffline) {

                    JSONObject jsonEvidence = new JSONObject(offline.jsonData);
                    String tipoEvidencia = jsonEvidence.getString("type");
                    String numAuditoria = jsonEvidence.getString("codigoAuditoria");
                    String institution = jsonEvidence.getString("institucion");
                    String origen = jsonEvidence.getString("origen");
                    String operacion = jsonEvidence.getString("operacion");
                    String numSerieLector = jsonEvidence.getString("numeroSerieLector");
                    String rutOperador = jsonEvidence.getString("rutOperador");
                    String resultadoVerificacion = jsonEvidence.getString("resultado");
                    String descripcionJsonData = jsonEvidence.toString();
                    String versionApp = jsonEvidence.getString("versionApp");
                    String rutPersona = String.format("%s-%s", jsonEvidence.getString("rut"), jsonEvidence.getString("dv"));
                    String nombre = jsonEvidence.getString("nombre");
                    String dedoId = jsonEvidence.getString("dedoCodificado");
                    String dedoFecha = jsonEvidence.getString("dedoFecha");
                    String ubicacion = jsonEvidence.getString("ubicacion");
                    String tipoLector = jsonEvidence.getString("tipoLector");
                    String trackId = jsonEvidence.getString("trackId");
                    String texto1 = "";
                    String texto2 = "";
                    if (tipoEvidencia.equals("Evidencia")){
                        texto1 = "indicio";
                        texto2 = "Toma de Evidencia, LA VERIFICACION ES RESPONSABILIDAD DEL CLIENTE";
                    }



                    mRequestAuditUpdate.updateAudit(
                            tipoEvidencia,
                            offline.idVerification,
                            numAuditoria,
                            institution,
                            origen,
                            operacion,
                            numSerieLector,
                            tipoLector,
                            rutOperador,
                            resultadoVerificacion,
                            descripcionJsonData,
                            versionApp,
                            rutPersona,
                            nombre,
                            dedoId,
                            dedoFecha,
                            ubicacion,
                            offline.fingerprint,
                            offline.fingerprintBmp,
                            texto1,
                            texto2,
                            trackId);
                }
                unregisterReceiver(mReceiver);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }

    }

    @Background
    public void getTyC(Context context){
        AutentiaPreferences mPreference = new AutentiaPreferences(context);
         new AutentiaWSClient(context).getTyCforApplication(mPreference.getString(KeyPreferences.INSTITUTION), new ResponseCallback() {
            @Override
            public void onResponseSuccess(Bundle result) {

                Log.e("Resultado", "Obtenidos TyC");
//                forceFinishProcess("none");
            }

            @Override
            public void onResponseError(AutentiaMovilException result) {
//                showProgressAlert(result.status);
            }
        });
    }

}
