package cl.autentia.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;


@EActivity
public class NewPhotoActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;


    @AfterExtras
    void afterExtras(){

        openCameraIntent();

    }

    private void openCameraIntent() {

    }

    @OnActivityResult(REQUEST_IMAGE_CAPTURE)
    void onCameraResult(int resultCode, Intent data){



    }

}
