package cl.autentia.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;
import com.acepta.eid.EID;
import com.acepta.iso19794_2.ISO19794_2;
import com.acepta.smartcardio.SmartCardConnection;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.data.QrData;
import cl.autentia.http.HttpUtil;
import cl.autentia.nfc.NFCAccessGrantReceiver;
import cl.autentia.nfc.NFCManager;
import cl.autentia.nfc.NFCReader;


@EActivity(R.layout.activity_info_nfc)
public class InfoNFCActivity extends Activity implements NFCAccessGrantReceiver {

    private final static String TAG = "INFO_NFC";

    private static final int REQUEST_NFC_ACCESS = 1000;

    interface Extras {
        interface In {
            String CODIGO_BARRAS = "BARCODE";
            String TIMEOUT = "TIMEOUT";
            String READ_EXTENDED_INFO = "READ_EXTENDED_INFO";
        }

        interface Out extends CommonOutExtras {
        }
    }

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    @InstanceState
    InternalState mCurrentState = InternalState.JUST_STARTING;

    @Extra(Extras.In.CODIGO_BARRAS)
    String mBarcode = "";

    // indica si fue pedida la lectura de info extendida:
    //      retrato y firma manuscrita del titular
    @Extra(Extras.In.READ_EXTENDED_INFO)
    boolean mReadExtendedInfo = false;

    // tiempo para postura de la cedula
    @Extra(Extras.In.TIMEOUT)
    int mTimeOut = 20;

    // data interna
    private QrData mQRData;
    private NFCReader mNfcReader;
    private Bundle mRetorno = new Bundle();
    private NFCManager mNfcManager;

    // widgets
    @ViewById(R.id.leerQr)
    Button mLeerQr;

    @ViewById(R.id.textViewCedula)
    TextView mTextViewCedula;

    private void finishActivity(String status, int resultCode, String description) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, description);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private boolean esQRCedula(String barcode) {
        return !TextUtils.isEmpty(barcode) &&
                barcode.contains("http") &&
                barcode.contains("mrz");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNfcManager = NFCManager.getInstance(this);

    }

    @AfterViews
    protected void afterViews() {
        mTextViewCedula.setVisibility(TextView.VISIBLE);
        mLeerQr.setVisibility(View.GONE);
        Log.v(TAG, "views ready");
    }

    @AfterExtras
    protected void afterExtras() {
        if (esQRCedula(mBarcode)) {
            try {
                mQRData = new QrData(mBarcode);

            } catch (UnsupportedEncodingException e) {

                finishActivity(Status.ERROR,
                        ReturnCode.CODIGO_BARRAS_INVALIDO.getCode(),
                        ReturnCode.CODIGO_BARRAS_INVALIDO.getDescription());
                return;
            }
        } else {

            finishActivity(Status.ERROR,
                    ReturnCode.CODIGO_BARRAS_INVALIDO.getCode(),
                    ReturnCode.CODIGO_BARRAS_INVALIDO.getDescription());
            return;
        }
        Log.v(TAG, "extras set");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mCurrentState.equals(InternalState.JUST_STARTING)) {
            if (!mNfcManager.isAdapterPresent()) {
                finishActivity(Status.ERROR,
                        ReturnCode.DISPOSITIVO_SIN_NFC.getCode(),
                        ReturnCode.DISPOSITIVO_SIN_NFC.getDescription());
                return;
            }
            if (mNfcManager.isAdapterAccessible()) {
                mCurrentState = InternalState.READY;

                mNfcReader = mNfcManager.getReader(this);

                iniciarLectura();
            } else {
                mCurrentState = InternalState.WAITING_FOR_PERMISSION;
                mNfcManager.requestAccess(REQUEST_NFC_ACCESS);
            }
        } else if (mCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
            // permiso llegara mediante onActivityResult
        } else if (mCurrentState.equals(InternalState.READY)) {
            // tengo impresion que nunca deberiamos llegar aqui
            mNfcReader = mNfcManager.getReader(this);

            iniciarLectura();
        }

        Log.v(TAG, "onResume done");
    }

    @OnActivityResult(value = REQUEST_NFC_ACCESS)
    protected void onRequestNfcAccessResult(int resultCode, Intent data) {
        mNfcManager.processAccessGrant(resultCode, data, this);
    }

    @Override
    public void onNFCAccessGranted() {
        mNfcReader = mNfcManager.getReader(this);
        iniciarLectura();
    }

    @Override
    public void onNFCAccessDenied() {
        finishActivity(Status.ERROR,
                ReturnCode.ACCESO_NFC_DENEGADO.getCode(),
                ReturnCode.ACCESO_NFC_DENEGADO.getDescription());
    }

    @Override
    public void onNFCAccessError() {
        finishActivity(Status.ERROR,
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_NFC.getCode(),
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_NFC.getDescription());
    }

    @UiThread
    protected void iniciarLectura() {
        showProgressMessage("Acerque la cédula al lector", mTimeOut);
        try {
            mNfcReader.open(mReaderCallback);
        } catch (IOException e) {
            HttpUtil.sendReportUncaughtException(e, this);
            finishActivity(Status.ERROR,
                    ReturnCode.FALLO_APERTURA_NFC.getCode(),
                    ReturnCode.FALLO_APERTURA_NFC.getDescription());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mNfcReader != null) mNfcReader.close();
        dismissProgressMessage();
    }

    NFCReader.ReaderCallback mReaderCallback = new NFCReader.ReaderCallback() {
        @Override
        public void onNewConnection(SmartCardConnection connection) {

            Log.d(TAG, "new card connection");

            showProgressMessage(
                    "Leyendo datos del titular...",
                    "La comunicación con la cédula está en curso...");

            EID eid = null;
            try {

                eid = new EID(connection);
                eid.selectMRTD();
                String mrz = mQRData.getMrz();
                eid.openSecureChannel(mrz);
                String info = eid.getDocumentInfo();
                String extendedInfo = null;

                eid.selectMOC();

                info = info.replace(EID.BIT1_VALUE, Integer.toString(ISO19794_2
                        .decodeMOCFingerPos(eid.getBIT(1))));
                info = info.replace(EID.BIT2_VALUE, Integer.toString(ISO19794_2
                        .decodeMOCFingerPos(eid.getBIT(2))));

                if (mReadExtendedInfo) {
                    showProgressMessage(
                            "Leyendo retrato del titular...",
                            "La comunicación con la cédula está en curso...");

                    eid.selectMRTD();
                    eid.openSecureChannel(mrz);
                    extendedInfo = eid.getExtendedDocumentInfo();
                }

                Log.d(TAG, info);

                final String finalInfo = info;
                final String finalExtendedInfo = extendedInfo;


                returnSmartCardData(finalInfo, finalExtendedInfo);

            } catch (Throwable throwable) {
                Log.d(TAG, "card error: " + throwable.getClass().getName() +
                        ": " + throwable.getMessage());
                HttpUtil.sendReportUncaughtException(throwable, getApplicationContext());
                showProgressMessage("Vuelve acercar la cédula al lector", mTimeOut);
            }
        }
    };


    public void onReadQR(View v) {
        throw new RuntimeException("codigo eliminado");
    }

    public void onReadCI(View v) {
        throw new RuntimeException("codigo eliminado");
    }

    private CountDownTimer mCountDownTimer;
    private ProgressDialog mProgressDialog = null;

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void showProgressMessage(final String message, final int timeOut) {
        // se cancela cuenta reversa y dialogo anterior por si hubo
        dismissProgressMessage();

        Log.v(TAG, String.format("starting progress: message=%s, timeOut=%d", message, timeOut));

        long timeOutInMiliseconds = TimeUnit.MILLISECONDS.convert(timeOut, TimeUnit.SECONDS);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(timeOut);
        mProgressDialog.setProgress(timeOut);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressNumberFormat("%1d seg");
        mProgressDialog.setProgressPercentFormat(null);
        mProgressDialog.setTitle(message);
        mProgressDialog.setMessage(String.format("Quedan %s seg.", timeOut));
        mProgressDialog.show();

        Log.v(TAG, "progress dialog should be visible");

        mCountDownTimer = new CountDownTimer(timeOutInMiliseconds, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                int secondsLeft = (int) ((millisUntilFinished + 500) / 1000);

                Log.v(TAG, "countdown: " + millisUntilFinished + " ms left");
                updateProgressMessage(
                        String.format("quedan %s seg.", secondsLeft), secondsLeft);
            }

            @Override
            public void onFinish() {
                finishActivity(Status.ERROR,
                        ReturnCode.NFC_TIMEOUT.getCode(),
                        ReturnCode.NFC_TIMEOUT.getDescription());
            }

        }.start();

        Log.v(TAG, "CountDownTimer started");
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void updateProgressMessage(final String message, int secondsLeft) {
        mProgressDialog.setProgress(secondsLeft);
        mProgressDialog.setMessage(message);
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void showProgressMessage(final String title, final String message) {
        // por si hubo contador lo cancelamos
        dismissProgressMessage();

        Log.v(TAG, String.format("message: %s: %s", title, message));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void dismissProgressMessage() {
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        Log.v(TAG, "dialog dismissed");
    }

    public void onFinalizarClick(View v) {

        Intent returnIntent = new Intent();
        returnIntent.putExtras(mRetorno);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    void returnSmartCardData(String info, String extendedInfo) {

        /*
         * tipo, mrz, tipo_doc,pais_emi,serie,fch_nac,sexo,fch_exp,nacionalidad
         * ,run,apellidos,nombres,nacio_en,bit1,bit2
         */

        JSONObject doc;
        try {
            doc = new JSONObject(info);

            String runtext = doc.getString("run");
            String[] runParts = runtext.split("<");
            int run = 0;
            char dv = '0';
            if (runParts.length >= 2) {
                run = Integer.parseInt(runParts[0]);
                dv = runParts[1].charAt(0);
                mRetorno.putInt("mRut", run);
                mRetorno.putChar("mDv", dv);
            } else {
                return;
            }

            int scFinger1 = Integer.parseInt(doc.getString("bit1"));
            int scFinger2 = Integer.parseInt(doc.getString("bit2"));

            mRetorno.putInt("bit1", scFinger1);
            mRetorno.putInt("bit2", scFinger2);
            mRetorno.putString("nombres", doc.getString("nombres"));
            mRetorno.putString("apellidos", doc.getString("apellidos"));
            mRetorno.putString("fechaNac", doc.getString("fch_nac"));
            mRetorno.putString("fecha_vencimiento", doc.getString("fch_exp"));
            mRetorno.putString("tipo", doc.getString("tipo"));
            mRetorno.putString("mrz", doc.getString("mrz"));
            mRetorno.putString("tipo_doc", doc.getString("tipo_doc"));
            mRetorno.putString("pais_emi", doc.getString("pais_emi"));
            mRetorno.putString("serie", doc.getString("serie"));
            mRetorno.putString("sexo", doc.getString("sexo"));
            mRetorno.putString("nacionalidad", doc.getString("nacionalidad"));
            mRetorno.putString("nacio_en", doc.getString("nacio_en"));
            mRetorno.putString(Extras.Out.ESTADO, Status.OK);
            mRetorno.putInt(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
            mRetorno.putString(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());

            if (extendedInfo != null) {
                JSONObject extendedDoc = new JSONObject(extendedInfo);
                mRetorno.putByteArray("retrato",
                        Base64.decode(extendedDoc.getString("retrato"), Base64.DEFAULT));

                mRetorno.putByteArray("firma",
                        Base64.decode(extendedDoc.getString("firma"), Base64.DEFAULT));
                Log.i(TAG, "retornando informacion extendida");
            }

            onFinalizarClick(null);

        } catch (JSONException e) {
            Log.e("returnSmartCardData", e.toString());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissProgressMessage();
        setResult(RESULT_CANCELED);
        finish();
    }
}
