package cl.autentia.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;

import cl.autentia.barcode.QRActivity;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.data.Pdf417Data;
import cl.autentia.data.QrData;
import cl.autentia.http.HttpUtil;
import cl.autentia.preferences.AutentiaPreferences;


@EActivity
public class BarcodeScannerActivity extends Activity {

    public static final String PDF_TYPE = "pdf_417";
    public static final String QR_TYPE = "qr";
    private static final int REQUEST_CAMERA_BARCODE = 0x00000011;
    @Extra(Extras.In.REVERSE)
    boolean reverse = false;
    @Extra(Extras.In.PARAMETER_TYPE)
    String parameterType = "";
    @Extra(Extras.In.PORTRAIT)
    boolean portrait = false;
    private AutentiaPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterExtras
    void afterExtras() {
        mPreferences = new AutentiaPreferences(this);

        Intent intent = new Intent();
        intent.setClass(this, QRActivity.class);
        if (!TextUtils.isEmpty(parameterType))
            intent.putExtra(Extras.In.PARAMETER_TYPE, parameterType);
        if (reverse) intent.putExtra(Extras.In.REVERSE, reverse);
        if (portrait) intent.putExtra(Extras.In.PORTRAIT, portrait);
        startActivityForResult(intent, REQUEST_CAMERA_BARCODE);
    }

    @OnActivityResult(value = REQUEST_CAMERA_BARCODE)
    void onRequestCameraResult(int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            Bundle resultData = data.getExtras();

            if (resultData.getString("error") == null) {

                if (resultData.getString("type").equals(PDF_TYPE)) {

                    try {

                        Pdf417Data pdf417Data = new Pdf417Data(resultData.getByteArray("rawdata"));
                        Bundle bundle = pdf417Data.toBundle();
                        bundle.putString("barcode", resultData.getString("barcode"));
                        sendResponse(bundle);

                    } catch (Exception ex) {
//                        try {
//                            Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
//                                    .putCustomAttribute("Error", ex.getMessage())
//                                    .putCustomAttribute("Institucion", mPreferences.getString(KeyPreferences.INSTITUTION))
//                                    .putCustomAttribute("App version", AppHelper.getCurrentVersion(this))
//                                    .putCustomAttribute("IMEI", DeviceHelper.getIMEI(this)));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                        HttpUtil.sendReportUncaughtException(ex, this);
                        sendError(resultCode);
                    }

                } else if (resultData.getString("type").equals(QR_TYPE)) {

                    try {

                        QrData qrData = new QrData(resultData.getString("barcode"));
                        Bundle bundle = qrData.toBundle();
                        bundle.putString("barcode", resultData.getString("barcode"));
                        sendResponse(bundle);

                    } catch (Exception ex) {
//                        try {
//                            Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
//                                    .putCustomAttribute("Error", ex.getMessage())
//                                    .putCustomAttribute("Institucion", mPreferences.getString(KeyPreferences.INSTITUTION))
//                                    .putCustomAttribute("App version", AppHelper.getCurrentVersion(this))
//                                    .putCustomAttribute("IMEI", DeviceHelper.getIMEI(this)));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                        HttpUtil.sendReportUncaughtException(ex, this);
                        sendError(resultCode);
                    }
                }
            } else {
                sendError(resultCode);
            }
        } else {
            sendError(resultCode);
        }

    }

    /**
     * @param resultCode
     */
    private void sendError(int resultCode) {

        Intent returnIntent = new Intent();
        if (resultCode == RESULT_OK) {

            returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.CODIGO_BARRAS_INVALIDO.getCode());
            returnIntent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.CODIGO_BARRAS_INVALIDO.getDescription());
            returnIntent.putExtra(Extras.Out.ESTADO, Status.ERROR);
            setResult(Activity.RESULT_OK, returnIntent);

        } else if (resultCode == RESULT_CANCELED) {
            setResult(Activity.RESULT_CANCELED, returnIntent);
        }
        super.finish();

    }

    /**
     * @param bundle
     */
    private void sendResponse(Bundle bundle) {

        Intent returnIntent = new Intent();
        returnIntent.putExtras(bundle);
        returnIntent.putExtra(Extras.Out.ESTADO, Status.OK);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
        returnIntent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
        setResult(Activity.RESULT_OK, returnIntent);
        super.finish();
    }

    interface Extras {
        interface In {
            String PARAMETER_TYPE = "PARAMETER_TYPE";
            String REVERSE = "REVERSE";
            String PORTRAIT = "PORTRAIT";
        }

        interface Out extends CommonOutExtras {
        }
    }
}
