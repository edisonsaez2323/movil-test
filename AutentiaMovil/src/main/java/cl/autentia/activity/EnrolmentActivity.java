package cl.autentia.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.acepta.android.fetdroid.R;
import com.shuhart.stepview.StepView;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;

@EActivity(R.layout.activity_enrolment)
public class EnrolmentActivity extends AppCompatActivity {


    @ViewById(R.id.step_view)
    StepView stepView;

    @Extra(Extras.In.RUT)
    int mRut = 0;
    @Extra(Extras.In.DV)
    char mDv = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void afterViews(){

        stepView.getState()
                .selectedTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .steps(new ArrayList<String>() {{
                    add("First step");
                    add("Second step");
                    add("Third step");
                }})
                .stepsNumber(3)
                .commit();

    }

    @AfterExtras
    void afterExtras(){


    }


    interface Extras {

        interface In extends CommonInExtras {

            String RUT = "RUT";
            String DV = "DV";

        }

        interface Out extends CommonOutExtras {

            String IDENTIDAD_VERIFICADA = "identidadVerificada";
            String RUT = "rut";
            String DV = "dv";
            String NUMERO_SERIE_HUELLERO = "serialNumber";
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String TIPO_LECTOR = "tipoLector";
            String NOMBRE = "nombre";
            String APELLIDOS = "apellidos";
            String FECHA_NACIMIENTO = "fechaNac";
            String ENROLADO = "enrolado";
            String FECHA_VENCIMIENTO = "fechaVencimiento";
            String NACIONALIDAD = "nacionalidad";
            String SEXO = "sexo";
        }
    }

}
