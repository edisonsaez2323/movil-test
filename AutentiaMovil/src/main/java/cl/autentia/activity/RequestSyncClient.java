package cl.autentia.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import cl.autentia.sync.MyServiceSyncAdapter;

public class RequestSyncClient extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyServiceSyncAdapter.syncImmediately(this);
        finish();
    }
}
