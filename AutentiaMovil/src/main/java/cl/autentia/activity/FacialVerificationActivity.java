package cl.autentia.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.acepta.Utils;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.EActivity;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;

@EActivity//(R.layout.preview_camera_layout)
public class FacialVerificationActivity extends AppCompatActivity{

//    String uri = "https://accounts.dev.autentia.systems/oauth2/auth?scope=address%20email%20mobile_phone%20openid%20profile%20phone&response_type=id_token&client_id=6589e80a-t5y4-11e9-81b4-2a2ae2lkoue4&redirect_uri=autentia://facecallback&acr_values=facial&state=d91dbef70a&nonce=90bcc211b0";
    String uri = "https://itrust-playground.trust.lat/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facialVerification();

    }

    private void facialVerification() {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            Log.d("autentia", "onClick: inTryBrowser");
            startActivity(intent);
            finish();
        } catch (ActivityNotFoundException ex) {
            Log.e("autentia", "onClick: in inCatchBrowser", ex );
            intent.setPackage(null);
            startActivity(intent.createChooser(intent, "Seleccionar Navegador"));
            finish();
        }

    }

    @AfterExtras
    void afterExtras() {

        if (!Utils.isConnectingToInternet(this)) {

            finishActivityWithError(Status.NO_OK,
                    ReturnCode.VERIFICATION_SIN_INTERNET.getCode(),
                    ReturnCode.VERIFICATION_SIN_INTERNET.getDescription());

        }

    }

    void finishActivityWithError(String status, int resultCode, String descripcion) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, false);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, descripcion);
        setResult(RESULT_OK, returnIntent);
        finish();
    }


    interface Extras {

        interface In extends CommonInExtras {

            String RUT = "RUT";
            String DV = "DV";

        }

        interface Out extends CommonOutExtras {

            String IDENTIDAD_VERIFICADA = "identidadVerificada";
            String RUT = "rut";
            String DV = "dv";
            String NUMERO_SERIE_HUELLERO = "serialNumber";
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String TIPO_LECTOR = "tipoLector";
            String NOMBRES = "nombres";
            String APELLIDOS = "apellidos";
            String FECHA_VENCIMIENTO = "fechaVencimiento";
        }
    }

}
