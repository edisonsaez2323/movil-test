package cl.autentia.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.json.JSONObject;

import java.util.Iterator;

import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;


@EActivity
public class BrowseableActivity extends AppCompatActivity {

    private static final String TAG = "BrowseableActivity";

    private static final int REQUEST_VIEW = 0x099;
    private String mQueueId;
    private ProgressDialog mProgressDialog;
    private AutentiaWSClient mAutentiaWSClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAutentiaWSClient = new AutentiaWSClient(this);

        String action = getIntent().getAction();

        if (!Intent.ACTION_VIEW.equals(action)) {

            JSONObject request = getRequest(getIntent());
            autentiaWSCaller(mQueueId,request);
            return;
        }

        Uri data = getIntent().getData();

        if (data == null) {

            JSONObject request = getRequest(getIntent());
            autentiaWSCaller(mQueueId,request);
            return;
        }

        String operacion = data.getQueryParameter("action");

        if (TextUtils.isEmpty(operacion)) {

            JSONObject request = getRequest(getIntent());
            autentiaWSCaller(mQueueId,request);
            return;
        }

        Intent intent = new Intent(operacion);

        try {

            JSONObject json = new JSONObject(data.getQueryParameter("params"));

            Iterator<String> keys = json.keys();

            while (keys.hasNext()) {

                String key = keys.next();

                Object value = json.get(key);

                if(key.equals("data")){
                    intent.putExtra(key, String.valueOf(value));
                }
                if (key.equals("callback")) {
                    mQueueId = (String) value;

                } else if (key.equals("ICON")) {
                    byte[] icon = Base64.decode(key, Base64.DEFAULT);
                    intent.putExtra(key, icon);

                } else if (key.equals("DV")) {
                    intent.putExtra(key, String.valueOf(value).charAt(0));

                } else if (value instanceof Boolean) {
                    intent.putExtra(key, (Boolean) value);

                } else if (value instanceof String) {
                    intent.putExtra(key, (String) value);

                } else if (value instanceof Integer) {
                    intent.putExtra(key, (Integer) value);
                }
            }

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            HttpUtil.sendReportUncaughtException(e, this);
            JSONObject request = getRequest(getIntent());
            autentiaWSCaller(mQueueId,request);
            return;
        }

        startActivityForResult(intent, REQUEST_VIEW);
    }

    private synchronized void autentiaWSCaller(String callback,JSONObject jsonObject){

        mAutentiaWSClient.browseableMethod(callback,jsonObject, new ResponseCallback() {
            @Override
            public void onResponseSuccess(Bundle result) {
                dismissProgressMessage();
                finish();
            }

            @Override
            public void onResponseError(AutentiaMovilException result) {
                dismissProgressMessage();
                finish();
            }
        });

    }

    /**
     *
     * @param intent
     * @return JsonObject
     */
    private JSONObject getRequest(Intent intent) {

        if(intent != null){
            Bundle data = intent.getExtras();
            JSONObject jsonRequest = new JSONObject();
            try {

                Iterator iterator = data.keySet().iterator();
                String msg = "";

                while (iterator.hasNext()) {

                    String key = iterator.next().toString();
                    Object value = data.get(key);

                    jsonRequest.put(key, value);

                    Log.d(TAG, String.format("%s : %s", key, value));
                }

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            return jsonRequest;
        }
        setResult(RESULT_CANCELED);
        return null;
    }

    @OnActivityResult(REQUEST_VIEW)
    void onRequestViewResult(int resultCode, Intent data) {

        if (resultCode == RESULT_CANCELED) {

            JSONObject request = getRequest(data);
            autentiaWSCaller(mQueueId,request);
            return;

        } else {

            if (TextUtils.isEmpty(mQueueId)) {
                JSONObject request = getRequest(data);
                autentiaWSCaller(mQueueId,request);
                return;
            }


            JSONObject request = getRequest(data);
            autentiaWSCaller(mQueueId,request);
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void showProgressMessage(final String title, final String message, boolean cancelable) {
        // por si hubo contador lo cancelamos
        dismissProgressMessage();

        Log.v(TAG, String.format("message: %s: %s", title, message));

        mProgressDialog = new ProgressDialog(BrowseableActivity.this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
        if (cancelable)
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dialogInterface.dismiss();
                    onBackPressed();

                }
            });
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }


    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void dismissProgressMessage() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        Log.v(TAG, "dialog dismissed");
    }
}
