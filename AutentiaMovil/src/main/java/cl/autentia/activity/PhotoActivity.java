package cl.autentia.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Base64;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.google.gson.Gson;
import com.otaliastudios.cameraview.CameraView;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GenAuditResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

@EActivity(R.layout.preview_camera_layout)
public class PhotoActivity extends Activity implements TextureView.SurfaceTextureListener {

    private static final int REQUEST_CODE_SCANNER = 0x0003;
    private static final String CEDULA = "CEDULA";
    private static final String TAG = "CAMERA";
    //    @Extra(Extras.In.HEIGHT)
//    String height;
//    @Extra(Extras.In.WIDTH)
//    String width;
    @Extra("TYPE")
    String type = "";
    @Extra("CAMARA")
    String mOrientacionCamara = "";
    @Extra("RUT")
    int mRut = 0;
    @Extra("DV")
    char mDv = 0;
    @Extra("CANTIDAD")
    int mCant = 0;

    String base64_image;
    Bitmap barcodeBmp;
    private Camera mCamera;
    private TextureView mTextureView;
    private Bitmap bmp_imagen;
    private FrameLayout boxView;
    private CameraView cameraView;
    private boolean mCapturingPicture;
    protected ExecutorService executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_camera_layout);
//        callScanner();
    }

    @AfterExtras
    void afterExtras() {

        if (mDv == 'k') {
            mDv = 'K';
        }

        try {
            if (mRut == 0 || mDv == 0 || !Utils.validaRutDV(mRut, mDv)) {
                throw new AutentiaMovilException(Status.NO_OK,
                        ReturnCode.RUT_INVALIDO);
            }
        }catch (AutentiaMovilException e){
            finishActivityWithError(Status.ERROR, ReturnCode.RUT_INVALIDO.getCode(), ReturnCode.RUT_INVALIDO.getDescription());
        }

        if (checkExtra()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }


    @AfterViews
    void afterViews() {
//        cameraView = (CameraView) findViewById(R.id.camera);
        executor = Executors.newSingleThreadExecutor();
        if (mOrientacionCamara.equals("FRONT")) {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
        } else {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        }

        mTextureView = (TextureView) findViewById(R.id.camera_texture);
        mTextureView.setSurfaceTextureListener(this);

        boxView = (FrameLayout) findViewById(R.id.fl_box_view);
//        drawGuide();

        findViewById(R.id.takePicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSurfaceTextureDestroyed(mTextureView.getSurfaceTexture());
                finishActivity(
                        Status.OK,
                        ReturnCode.EXITO.getCode(),
                        ReturnCode.EXITO.getDescription());
            }
        });
    }

    boolean checkExtra() {
        return !type.isEmpty() && type.equals(CEDULA);
    }

    public byte[] BitmapToJPEG(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        return baos.toByteArray();
    }

    @SuppressLint("NewApi")
    private void drawGuide() {
        Drawable vectorDrawable;

        if (checkExtra()) {
            vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ci_layout_land, null);
        } else {
            vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ci_layout, null);
        }

        Bitmap myLogo = ((BitmapDrawable) vectorDrawable).getBitmap();

        BitmapDrawable br = new BitmapDrawable(getResources(), myLogo);

        int androidVersion = Integer.valueOf(android.os.Build.VERSION.SDK);

        if (androidVersion < 16) {
            boxView.setBackgroundDrawable(br);
        } else {
            boxView.setBackground(br);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
//        cameraView.start();
    }

    public void onPause() {
        super.onPause();
//        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        cameraView.destroy();
    }

    private void releaseCamera() {
//        if (mCamera != null) {
////            mCamera.setPreviewCallback(null);
//            mCamera.release();
//            mCamera = null;
//        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {

        try {
            /* Tell the camera to write onto our textureView mTextureView */
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewTexture(surfaceTexture);

            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            mCamera.setParameters(parameters);

            mCamera.startPreview();
        } catch (IOException ioe) {
            Log.e("camera-reverse", ioe.getMessage());
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        if (null != mCamera) {
            mCamera = null;
//            mCamera.release();
        }
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        barcodeBmp = mTextureView.getBitmap();

        //get pixel array
        int width = barcodeBmp.getWidth();
        int height = barcodeBmp.getHeight();
        int[] pixels = new int[barcodeBmp.getHeight() * barcodeBmp.getWidth()];
        barcodeBmp.getPixels(pixels, 0, width, 0, 0, width, height);
    }

    void finishActivityWithError(String status, int resultCode, String description){

        Intent intent = new Intent();
        intent.putExtra(Extras.Out.ESTADO, status);
        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        intent.putExtra(Extras.Out.DESCRIPCION, description);
        setResult(RESULT_OK, intent);
        finish();

    }


    /**
     * @param status
     * @param resultCode
     * @param description
     */
    private void finishActivity(String status, int resultCode, String description) {
        Bitmap croppedBitmap;
        if (checkExtra()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            croppedBitmap = Bitmap.createBitmap(barcodeBmp, 20, 20, barcodeBmp.getWidth() - 10, barcodeBmp.getHeight() - 10, matrix, true);
        } else {
//            Matrix matrix = new Matrix();
//            matrix.postRotate(90);
            barcodeBmp = Bitmap.createBitmap(barcodeBmp, 0, 0, barcodeBmp.getWidth(), barcodeBmp.getHeight());
            Bitmap resize = Bitmap.createScaledBitmap(barcodeBmp, 720, 1280, true);
            croppedBitmap = Bitmap.createBitmap(resize, 10, 10, resize.getWidth() - 20, resize.getHeight() - 20);
        }

        base64_image = Base64.encodeToString(BitmapToJPEG(croppedBitmap), Base64.DEFAULT).replace("\n", "");


        //create audit
        try {
            AutentiaWSClient mAutentiaWSClient = new AutentiaWSClient(this);
            AutentiaPreferences mPreferences =  new AutentiaPreferences(this);
            String versionApp = AppHelper.getCurrentVersion(this);

            String rutInstit = mPreferences.getString(KeyPreferences.RUN_INSTITUTION);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Captura_fotografia");
            jsonObject.put("rutOperador", rutInstit);
            jsonObject.put("institucion", mPreferences.getString(KeyPreferences.INSTITUTION));
            jsonObject.put("versionApp", versionApp);
            jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
            jsonObject.put("ip", DeviceHelper.getIPAddress(true));
            jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
            jsonObject.put("imei", DeviceHelper.getIMEI(this));
            jsonObject.put("operacion", "FOTOGRAFIA");
            jsonObject.put("origen", "ANDROID");
            jsonObject.put("resultado", "0000");

            mAutentiaWSClient.genAudit(new byte[0],
                    "jpg",
                    String.format("%s-%s", mRut, mDv),
                    "10",
                    "Eikon",
                    mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER),
                    BitmapToJPEG(croppedBitmap),
                    String.valueOf("0"),
                    mPreferences.getString(KeyPreferences.INSTITUTION),
                    jsonObject.toString(),
                    rutInstit,
                    "",
                    "sin datos",
                    "0",
                    "",
                    "",
                    mPreferences.getString(KeyPreferences.INSTITUTION), false,new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
                            Log.d("SignatureActivity", "Create Audit succeed");
                            GenAuditResp gar = new Gson().fromJson(result.getString("result"),GenAuditResp.class);

                            Intent intent = new Intent();
                            if (base64_image != null)
                                intent.putExtra(Extras.Out.BASE64_IMAGEN, base64_image);
                            intent.putExtra("numeroAuditoria", gar.getNroAuditoria());
                            intent.putExtra(Extras.Out.ESTADO, status);
                            intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
                            intent.putExtra(Extras.Out.DESCRIPCION, description);
                            setResult(RESULT_OK, intent);
                            finish();

                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {

                            Intent intent = new Intent();
                            if (base64_image != null)
                                intent.putExtra(Extras.Out.BASE64_IMAGEN, base64_image);
                            intent.putExtra(Extras.Out.ESTADO, status);
                            intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
                            intent.putExtra(Extras.Out.DESCRIPCION, description);
                            setResult(RESULT_OK, intent);
                            finish();

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }



//        Intent intent = new Intent();
//        if (base64_image != null)
//            intent.putExtra(Extras.Out.BASE64_IMAGEN, base64_image);
//        intent.putExtra(Extras.Out.ESTADO, status);
//        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
//        intent.putExtra(Extras.Out.DESCRIPCION, description);
//        setResult(RESULT_OK, intent);
////        cameraView.lis
////        mProgressDialog.dismiss();
//        finish();
    }

    interface Extras {
        interface Out extends CommonOutExtras {
            String BASE64_IMAGEN = "BASE64_IMAGEN";
        }

        interface In extends CommonInExtras {
            String ORIENTACION = "ORIENTACION";
            String HEIGHT = "HEIGHT";
            String WIDTH = "HEIGHT";
            String TYPE = "TYPE";
        }
    }

}
