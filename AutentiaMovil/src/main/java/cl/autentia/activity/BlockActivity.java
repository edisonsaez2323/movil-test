package cl.autentia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

@EActivity(R.layout.activity_block)
public class BlockActivity extends AppCompatActivity implements BlurLockView.OnPasswordInputListener,
        BlurLockView.OnLeftButtonClickListener, View.OnClickListener {

    private AutentiaPreferences preferences;

    @ViewById(R.id.blurlockview)
    BlurLockView _blurLockView;

    @ViewById(R.id.img_block)
    ImageView _imgBlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = new AutentiaPreferences(this);
    }

    @AfterViews
    void afterViews() {

        _blurLockView.setBlurredView(_imgBlock);
        _blurLockView.setCorrectPassword("****");
        _blurLockView.setTitle("Ingrese password para configurar Autentia Movil");
        _blurLockView.setLeftButton("Salir");
        _blurLockView.setRightButton(" ");
        _blurLockView.setType(getPasswordType(), false);

        _blurLockView.setOnLeftButtonClickListener(new BlurLockView.OnLeftButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
        _blurLockView.setBlurRadius(1);
        _blurLockView.setOnPasswordInputListener(this);
    }

    private Password getPasswordType() {
        return Password.NUMBER;
    }

    @Override
    public void correct(String inputPassword) { /* */ }

    @Override
    public void incorrect(String inputPassword) {

        if (isCorrectPassword(inputPassword)) {
            _blurLockView.hide(1000, HideType.FADE_OUT, EaseType.Linear);
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();

        } else {
            Toast.makeText(this,
                    R.string.password_incorrect,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isCorrectPassword(String password) {
        String currentHash = Utils.asHex(
                Utils.sha(
                        String.format("%s/%s/%s",
                                preferences.getString(KeyPreferences.INSTITUTION, ""),
                                preferences.getString(KeyPreferences.ENVIROMENT, ""),
                                password).getBytes()));
        String expectedHash = preferences.getString(KeyPreferences.HASH_CONFIGURATION);
        return expectedHash.equalsIgnoreCase(currentHash);
    }

    @Override
    public void input(String inputPassword) { /* sin codigo */ }

    @Override
    public void onClick(View view) { /* sin codigo */ }

    @Override
    public void onClick() { /* sin codigo */ }
}
