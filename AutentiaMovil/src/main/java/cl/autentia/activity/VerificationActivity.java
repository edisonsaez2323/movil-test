package cl.autentia.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.acepta.eid.EID;
import com.acepta.eid.EIDError;
import com.acepta.iso19794_2.ISO19794_2;
import com.acepta.iso19794_2.ISOError;
import com.acepta.iso19794_2.ISOFMR;
import com.acepta.smartcardio.SmartCardConnection;
import com.acepta.smartcardio.SmartCardError;
import com.acepta.wsq.ImageUtil;
import com.airbnb.lottie.LottieAnimationView;
import com.crashlytics.android.Crashlytics;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.google.gson.Gson;
import com.morpho.android.usb.USBManager;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cl.autentia.adapter.ViewPagerAdapter;
import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.configuration.ConfigurationHandler;
import cl.autentia.data.Pdf417Data;
import cl.autentia.data.QrData;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.helper.LocationHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GenAuditResp;
import cl.autentia.http.parameters.GetStatusNECResp;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.nfc.NFCAccessGrantReceiver;
import cl.autentia.nfc.NFCManager;
import cl.autentia.nfc.NFCReader;
import cl.autentia.pdfUtils.RemotePDFViewPager;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.MyServiceSyncAdapter;
import cl.autentia.sync.onrequest.SyncNow;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;
import io.fabric.sdk.android.Fabric;

import static cl.autentia.http.AutentiaWSClient.URL_TRASPASO;
import static com.acepta.Utils.dedosDrawablesNfc;
import static com.acepta.Utils.getTrackID;

@Fullscreen
@EActivity//(R.layout.activity_capture_fingerprint)
public class VerificationActivity extends BaseActivity
        implements NFCReader.ReaderCallback, NFCAccessGrantReceiver,
        FingerprintReader.ScanCallback, FingerprintReader.InfoCallback, FingerprintGrantReceiver, DownloadFile.Listener {

    public static final String NOT_WORKING_PROPERLY = "The reader is not working properly";
    private static final String TAG = "VerificationActivity";
    private static final int REQUEST_NFC_ACCESS = 0x030;
    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    private static final int DEFAULT_MIN_SCORE = 5;
    private static final String NFC_STATE = "NFC_STATE";
    private static final String FINGERPRINT_STATE = "FINGER_PRINT_STATE";
    private static final String AUDIT_SOURCE = "ANDROID";
    private static final String AUDIT_TYPE = "VALIDACION";
    private static final String AUDIT_TYPE_ERROR = "ERROR";
    private static final String SUCCESS_RESULT = "0000";
    private static final String NOT_SUCCESS_RESULT_QR = "0002";
    private static final String NOT_SUCCESS_RESULT_PDF417 = "0003";
    private static final String HORIZONTAL_ORIENTACION = "HORIZONTAL";
    private static final String UNEXPECTED_CHARACTER = "Unexpected character (<)";
    private static final String CLAVE_TRASPASO = "traspaso";
    private static final String AUDIT_TYPE_REJECT = "RECHAZO";
    private static final int REQUEST_CODE_LOGIN_OPER = 0x0044;
    public static final String URI_STANDARD = "data:image/jpeg;base64,";
    private static final int MANO_DERECHA = 0;
    private static final int MANO_IZQUIERDA = 1;
    AlertDialog optionDialog;
    /***
     * Extras
     */

    @Extra(Extras.In.RUT)
    int mRut = 0;
    @Extra(Extras.In.DV)
    char mDv = 0;
    @Extra(Extras.In.BARCODE)
    String mBarcode = null;
    @Extra(Extras.In.OFFLINE_MODE)
    boolean mOffline = false;
    @Extra(Extras.In.TIMEOUT)
    int mTimeOut = 20;
    @Extra(Extras.In.SKIP_TERMS)
    boolean mSkipTerms = false;
    @Extra(Extras.In.INTENTOS)
    int mMaxAttempts = 3;
    @Extra(Extras.In.ICON)
    byte[] mIcon = new byte[0];
    @Extra(Extras.In.TITLE)
    String mTitle = "";
    @Extra(Extras.In.SUBTITLE)
    String mSubtitle = "";
    @Extra(Extras.In.COLOR_TITLE)
    String mColorTitle = "";
    @Extra(Extras.In.COLOR_SUBTITLE)
    String mColorSubtitle = "";
    @Extra(Extras.In.COLOR_PRIMARY)
    String mColorPrimary = "";
    @Extra(Extras.In.COLOR_PRIMARY_DARK)
    String mColorPrimaryDark = "";
    @Extra(Extras.In.HIDE_RUT)
    boolean mHideRut = false;
    @Extra(Extras.In.PREVIRED)
    boolean mPrevired = false;
    @Extra(Extras.In.TRASPASO)
    boolean mTraspaso = false;
    @Extra(Extras.In.URL_DOCUMENT)
    String mUrlDocument = null;
    @Extra(Extras.In.ORIENTACION)
    String mOrientacion = "";

    @Extra(Extras.In.COD_DOC)
    String mCodigoDocDec = null;
    @Extra(Extras.In.INSTITUCION_DEC)
    String mInstitucionDec = null;

    @Extra(Extras.In.PROPOSITO)
    String mProposito = "";
    @Extra(Extras.In.OTI)
    boolean mOti = false;
    @Extra("operador")
    boolean mOperador = false;

    //OPCIONALES (TEXTO 3)
    @Extra(Extras.In.ADICIONAL)
    String mAdicional = "";

    //Verificar vigencia

    @Extra(Extras.In.VIGENCIA)
    boolean mVigencia = false;
    private String estadoCedula = "";

    /**
     * MOC
     * + EXTENDED INFO
     * VERIFICACION CONTRACEDULA NUEVA + INFORMACION CEDULA
     */
    @Extra(Extras.In.EXTENDED_MOC)
    boolean mMocExtendedInfo = false;
    String extendedInfo = null;

    @Extra(Extras.In.TRACK_ID)
    String trackID = "";

    /**
     * NEW IDENTIFICATION CARD
     * 02-2020
     */
    private boolean mNewIdent = false;

    /**
     * Views
     */

    @ViewById(R.id.toolbar)
    Toolbar _toolbar;
    @ViewById(R.id.place_smartcard)
    LinearLayout _placeSmartcard;
    @ViewById(R.id.wv_terms_and_conditions)
    WebView _disclamer;
    @ViewById(R.id.iv_hands)
    ImageView _ivHands;
    @ViewById(R.id.iv_fingerprint)
    ImageView _ivFingerPrint;
    @ViewById(R.id.ll_attempts)
    LinearLayout _layoutIntentos;
    @ViewById(R.id.layout_fingerprint_capture)
    LinearLayout _contentView;
    @ViewById(R.id.tv_rut)
    TextView _tvRut;
    @ViewById(R.id.tv_help_verification)
    TextView _tvHelpVerification;
    @ViewById(R.id.ll_disclamer_buttons)
    LinearLayout _disclamerButtons;
    @ViewById(R.id.linear_layout_terms)
    LinearLayout _linearLayoutTermsAndConditions;
    @ViewById(R.id.ll_content)
    LinearLayout _linearLayoutVerification;
    @ViewById(R.id.btn_cancel_terms)
    Button _btnCancel;
    @ViewById(R.id.btn_acept_terms)
    Button _btnAceptTerms;
    @ViewById(R.id.btn_cancelar)
    Button _btnCancelar;
    @ViewById(R.id.btn_terms_and_conditions)
    TextView _btnTermsAndConditions;
    LinearLayout _contentViewPDF;
    @ViewById(R.id._tvIntentosNfc)
    TextView _tvNfcAttempts;
    /**
     * States
     */

    @InstanceState
    InternalState mNFCCurrentState = InternalState.JUST_STARTING;
    @InstanceState
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    RemotePDFViewPager remotePDFViewPager;
    PDFPagerAdapter adapter;
    ProgressDialog mProgressWebView;
    Context ctx = this;
    DownloadFile.Listener listener = this;
    int errorCount = 0;
    int errorCountAudit = 0;
    private Engine mEngine;
    private FingerprintImage mFingerprintImage;
    private FingerprintManager mFingerprintManager;
    private FingerprintReader mFingerprintReader;
    private AutentiaWSClient mAutentiaWSClient;
    private AutentiaPreferences mPreferences;
    private QrData mQRData;
    private Pdf417Data mPDF417Data;
    private NFCReader mNfcReader;
    private NFCManager mNfcManager;
    private EID mEid;
    private Fmd mFmdFinger;
    private ProgressDialog mProgressDialog;
    private CountDownTimer mCountDownTimer;
    private Drawable mDrawableIcon;
    private LocationHelper mLocationHelper;
    private String mNombre;
    private String mApellido;
    private String mFechaNac;
    private String mSerialNumber;
    private String mReaderType;
    private String mTransactionId;
    private String mFechaVencimiento;
    private String mNacionalidad;
    private String mSexo;
    private String mPathFingerprintPDF417One;
    private String mPathFingerprintPDF417Two;
    private String mSizeFingerprintPDF417;
    private String mEidInfo;
    private String mInstitution;
    private String mRunInstitution;
    private boolean[] mAvalaibleFinderprints;
    private boolean mSuccessVerification;
    private boolean mSkipReadingNFC = true;
    private boolean mPendingCaptureFingerprint = true;
    private boolean mAcceptMandate;
    private boolean mAcceptTermsAndConditions;
    private int mIntentoActual;
    private byte[] mBarcodeBytes;
    private int[] mFingerISOQR = new int[2];
    private boolean mIsTablet = false;
    private String mAmbiente;
    private boolean pdfError;

    private boolean operVerified = false;

    private List<String> auditList = new ArrayList<>();

    //network
    private static final String LOG_TAG = "CheckNetworkStatus";
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;

    //extras cedula
    String mRetrato;
    String mFirma;


    //adapter nfc
    private ViewPager pager;
    private ImageView arrowLeft, arrowRight;

    //nueva cedula 03-2020
    String respBIR = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());


        if (!TextUtils.isEmpty(mUrlDocument) || !TextUtils.isEmpty(mCodigoDocDec) && !TextUtils.isEmpty(mInstitucionDec)) {
            setContentView(R.layout.activity_capture_fingerprint_mini);
        } else {
            if (!mOrientacion.isEmpty() && mOrientacion.equals(HORIZONTAL_ORIENTACION)) {
                setContentView(R.layout.activity_capture_fingerprint_land);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                setContentView(R.layout.activity_capture_fingerprint);
            }
        }

        try {

//            initControls();


            mAutentiaWSClient = new AutentiaWSClient(this);
            mPreferences = new AutentiaPreferences(this);

            mLocationHelper = new LocationHelper(this);

            mInstitution = mPreferences.getString(KeyPreferences.INSTITUTION);
            mRunInstitution = mPreferences.getString(KeyPreferences.RUN_INSTITUTION);
            mAmbiente = mPreferences.getString(KeyPreferences.ENVIROMENT);

            mFingerprintManager = FingerprintManager.getInstance(this);
            mEngine = UareUGlobal.GetEngine();

            if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager"))
                USBManager.getInstance().initialize(this, "com.morpho.morphosample.USB_ACTION"); //try
            if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager"))
                setPowerFP2(1);

            if (mOffline) {
                int auditSize = Auditoria_.getAuditCount();
                if (auditSize < 1) {

                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.SIN_AUDITORIAS_RESERVADAS);
                }

            } else {

                /**
                 * ONLINE
                 */

                if (!HttpUtil.checkInternetConnection(this)) {
                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.VERIFICATION_SIN_INTERNET));
                    return;
                }
            }

        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            Log.e("exception", e.toString());
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
        }

        String config = mPreferences.getString(KeyPreferences.DATA_CONFIGURATION);
        Log.e("first_start", mPreferences.getString(KeyPreferences.FIRST_START));
        if (!checkForDevices() && config != null && mPreferences.getString(KeyPreferences.FIRST_START).equals("false")) {
            new ConfigurationHandler(this).loadConfiguration();
        }

        if (mPreferences.getString(KeyPreferences.NFC_CONFIGURATION).equals("0")) {
            new ConfigurationHandler(this).checkForNFCDevice();
        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);

    }

    public boolean checkForDevices() {
        UsbManager manager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
        return manager.getDeviceList().isEmpty();
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.v(LOG_TAG, "Receieved notification about network status");
            isNetworkAvailable(context);

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v(LOG_TAG, "Now you are connected to Internet!");
                                isConnected = true;
                                new SyncNow(VerificationActivity.this, receiver);
                                //do your processing here ---
                                //if you need to post any data to the server or get status
                                //update from the server
                            }
                            return true;
                        }
                    }
                }
            }
            Log.v(LOG_TAG, "You are not connected to Internet!");
//            networkStatus.setText("You are not connected to Internet!");
            isConnected = false;
//            changeTextStatus(isConnected);
            return false;
        }
    }


    private void init(Bitmap rightHand, Bitmap leftHand) {
        List<Bitmap> bmpList = new ArrayList<>();
        pager = findViewById(R.id.hand_pager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, bmpList, pager);
        bmpList.add(leftHand);
        bmpList.add(rightHand);
        pager.setAdapter(viewPagerAdapter);
        arrowLeft = findViewById(R.id.arrow_left);
        arrowRight = findViewById(R.id.arrow_right);
        arrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(0, true);
                arrowRight.setVisibility(View.VISIBLE);
                arrowLeft.setVisibility(View.INVISIBLE);
            }
        });
        arrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(1, true);
                arrowRight.setVisibility(View.INVISIBLE);
                arrowLeft.setVisibility(View.VISIBLE);
            }
        });
        pager.setCurrentItem(1);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _ivHands.setVisibility(View.GONE);
                findViewById(R.id.ll_select_hand).setVisibility(View.VISIBLE);
                viewPagerAdapter.moveHands();
            }
        });

    }

    public void hideLeftArrow() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                arrowLeft.setVisibility(View.INVISIBLE);
                arrowRight.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideRightArrow() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                arrowRight.setVisibility(View.INVISIBLE);
                arrowLeft.setVisibility(View.VISIBLE);
            }
        });
    }

    void setPowerFP2(int onOff) {
        //1 ON 2 OFF
//        SledManager sm = (SledManager) this.getSystemService("sled");
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);

        if (onOff == 1) {
            Log.i(TAG, "POWER ON");
            manager.setFingerPrinterPower(true);
        } else if (onOff == 0) {
            Log.i(TAG, "POWER OFF");
            manager.setFingerPrinterPower(false);
        }
    }

//    public void pedirRut() {
//        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(VerificationActivity.this);
//        final View dialogView = getLayoutInflater().inflate(R.layout.view_rut, null);
//        builder.setView(dialogView);
//        builder.setCancelable(false);
//        android.app.AlertDialog alertDialog = builder.create();
//        Button btAceptar = dialogView.findViewById(R.id.bt_aceptar);
//        btAceptar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String rut = ((EditText) dialogView.findViewById(R.id.et_rut)).getText().toString();
//                String dv = ((EditText) dialogView.findViewById(R.id.et_dv)).getText().toString();
//                if (!TextUtils.isEmpty(rut) && !TextUtils.isEmpty(dv)) {
//                    Utils.hideKeyboard(VerificationActivity.this, dialogView);
//                    checkOper(String.format("%s-%s", rut, dv));
//                    alertDialog.dismiss();
//                } else {
//                    pedirRut();
//                }
//
//            }
//        });
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        alertDialog.show();
//    }

//    private boolean checkOper(String rutLogon) {
//        showDialog("Verificando rol operador...");
//        AutentiaWSClient mAutentiaWSClient = new AutentiaWSClient(this);
//        try {
//            mAutentiaWSClient.getOperador(rutLogon, mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), new ResponseCallback() {
//                @Override
//                public void onResponseSuccess(Bundle result) {
//                    if (result.getBoolean("rolVerificado")) {
//                        verificarOperador(rutLogon);
//                    } else {
//                        showAlertDialog("El rut: " + rutLogon + " no tiene perfil de operador en " + mPreferences.getString(KeyPreferences.INSTITUTION));
//                    }
//                }
//
//                @Override
//                public void onResponseError(AutentiaMovilException result) {
////                    dismissDialog();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

//    public void verificarOperador(String result) {
//        String[] rut = result.split("-");
//        Intent intent = new Intent(VerificationActivity.this, VerificationActivity_.class);
//        intent.putExtra("RUT", Integer.valueOf(rut[0]));
//        intent.putExtra("DV", rut[1].charAt(0));
//        intent.putExtra("TITLE", "Login Operador");
//        startActivityForResult(intent, REQUEST_CODE_LOGIN_OPER);
//    }

//    private void showAlertDialog(String message) {
////        dismissDialog();
//        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//        alertDialog.setIcon(R.drawable.autentia_logo_blue);
//        alertDialog.setTitle("Verificacion Operador");
//        alertDialog.setMessage(message);
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//        if (!operVerified) {
//            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Reintentar", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    pedirRut();
//                }
//            });
//        }
//        alertDialog.show();
//    }

//    @OnActivityResult(REQUEST_CODE_LOGIN_OPER)
//    void onLoginResult(int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            Bundle bl = data.getExtras();
//            if (bl != null) {
//                if (bl.getInt("CODIGO_RESPUESTA") == 200) {
//                    mPreferences.setString(KeyPreferences.RUN_INSTITUTION, String.format("%s-%s", bl.getInt("rut"), bl.getChar("dv")));
//                    mPreferences.setBoolean(KeyPreferences.LOGGED_IN, true);
//                    operVerified = true;
//                    showAlertDialog("Operador verificado");
//                } else {
//                    showAlertDialog("Error al verificar operador");
//                }
//            } else {
//                showAlertDialog("Verificacion cancelada");
//            }
//
//        }
//    }

    @Background
    void loadConfiguration() {
        new ConfigurationHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem menuItem = menu.findItem(R.id.action_icon);
        if (mDrawableIcon != null) {
            menuItem.setIcon(mDrawableIcon);
            menuItem.setVisible(true);

        } else {
            menuItem.setVisible(false);
        }

        if (mIsTablet) {
            menuItem.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume init");

        DeviceHelper.hideKeyboard(this, _toolbar);

        //Omite el contador para el TimeOut de lectura NFC
        if (!mSkipReadingNFC) {

            if (mNFCCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mNfcManager.isAdapterPresent()) {
                    createAudit(new byte[0],
                            ReturnCode.DISPOSITIVO_SIN_NFC.getDescription(),
                            ReturnCode.DISPOSITIVO_SIN_NFC.getCode(), false);
                    errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.DISPOSITIVO_SIN_NFC));
                    return;
                }
                if (mNfcManager.isAdapterAccessible()) {

                    mNFCCurrentState = InternalState.READY;
                    mNfcReader = mNfcManager.getReader(this);
                    startReadSmartCard();

                } else {
                    mNFCCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mNfcManager.requestAccess(REQUEST_NFC_ACCESS);
                }
            } else if (mNFCCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                // permiso llegara mediante onActivityResult
            } else if (mNFCCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mNfcReader = mNfcManager.getReader(this);
                startReadSmartCard();
            }
            Log.v(TAG, "onResume done");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(NFC_STATE, mNFCCurrentState.name());
        outState.putString(FINGERPRINT_STATE, mFingerprintCurrentState.name());
        super.onSaveInstanceState(outState);
    }

    public boolean canIContinue(String result) {

        switch (result) {

            case "isValid":
                return true;
            case "isOutdated":
                return false;
            case "isNotTest":
                return true;
        }

        return false;
    }

    @AfterExtras
    void afterExtras() {

        if (mOffline) {
            List<Auditoria_> auditorias = Auditoria_.getAudits();
            if (auditorias.size() < mMaxAttempts) {
                for (int i = 0; i < auditorias.size(); i++) {
                    auditList.add(auditorias.get(i).getNumeroAuditoria());
                }
            } else {
                for (int i = 0; i < mMaxAttempts; i++) {
                    auditList.add(auditorias.get(i).getNumeroAuditoria());
                }
            }
        }

        mTitle = TextUtils.isEmpty(mTitle) ? "Verificación de Identidad" : mTitle;
        mSubtitle = TextUtils.isEmpty(mSubtitle) ? "" : mSubtitle;
        mColorTitle = TextUtils.isEmpty(mColorTitle) ? getResources().getString(android.R.color.white) : mColorTitle;
        mColorSubtitle = TextUtils.isEmpty(mColorSubtitle) ? getResources().getString(android.R.color.white) : mColorSubtitle;
        mColorPrimary = TextUtils.isEmpty(mColorPrimary) ? getResources().getString(R.color.colorPrimary) : mColorPrimary;
        mColorPrimaryDark = TextUtils.isEmpty(mColorPrimaryDark) ? getResources().getString(R.color.colorPrimaryDark) : mColorPrimaryDark;
        mPrevired = !mOffline && mPrevired;
        mPrevired = mBarcode == null && mPrevired;

    }

    @AfterViews
    void afterViews() {
//        boolean pendingLogin = false;
//        boolean loggedIn = false;
//
//        Log.e("pending","enter");
//        try {
//            mPreferences = new AutentiaPreferences(this);
//            if (mPreferences.containsKey(KeyPreferences.PENDING_LOGIN)){
//                pendingLogin = mPreferences.getBoolean(KeyPreferences.PENDING_LOGIN);
//                loggedIn = mPreferences.getBoolean(KeyPreferences.LOGGED_IN);
//                Log.e("pendig ver",String.valueOf(pendingLogin));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        if (pendingLogin && !loggedIn){
//            Log.e("pedir","rut");
//            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//            alertDialog.setIcon(R.drawable.autentia_logo_blue);
//            alertDialog.setTitle("Verificacion Operador");
//            alertDialog.setMessage("Debe verificarse como operador antes de operar");
//            alertDialog.setCancelable(false);
//            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent(VerificationActivity.this,NavegationMenuActivity_.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
//                        }
//                    });
//            alertDialog.show();
//        }else {

        try {

            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);

            if (findViewById(R.id.btn_cancelar).getVisibility() == View.GONE) {
                super.setToolbarWithWhiteArrowBack(_toolbar);

            } else {

                mIsTablet = true;
                setSupportActionBar(_toolbar);
            }
            DeviceHelper.hideKeyboard(this, _toolbar);

            if (mDv == 'k') {
                mDv = 'K';
            }

            if (mRut == 0 || mDv == 0 || !Utils.validaRutDV(mRut, mDv)) {
                throw new AutentiaMovilException(Status.NO_OK,
                        ReturnCode.RUT_INVALIDO);
            }

            if (!trackID.isEmpty())
                mSkipTerms = true;

            initVerification();

        } catch (AutentiaMovilException e) {
            errorHandlerMethod(e.status, new AutentiaMovilException(e.returnCode.getDescription(), e.returnCode));
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
        }
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_icon:

                break;
            default:

                if (!mIsTablet)
                    onBackPressed();
        }
        return false;
    }

    /**
     * Gestión de permisos para el huellero.
     */
    private void prepareCaptureFingerprint() {

        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    Log.e("get info", "yes");
                    getInfo();
                } else {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                Log.e("get info 2", "yes");
                getInfo();
            }
        } catch (AutentiaMovilException e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            errorHandlerMethod(e.status, new AutentiaMovilException(e.returnCode.getDescription(), e.returnCode));
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
        }
    }

    @Background
    void consultaHuellasDisponibles() {

        try {

            showDialog("Obteniendo huellas disponibles...");

            //Consulta huellas disponibles
            String rutPersona = String.format("%s-%s", mRut, mDv);
            mAutentiaWSClient.checkAvalaibleFingerprint(rutPersona, mInstitution, mSerialNumber, new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    try {

                        mNombre = result.getString("nombre", "").trim();
                        mFechaNac = result.getString("fechaNac", "").trim();
                        mAvalaibleFinderprints = result.getBooleanArray("muestras");

                        boolean not_fingers = true;
                        for (boolean mAvalaibleFinderprint : mAvalaibleFinderprints) {
                            if (mAvalaibleFinderprint)
                                not_fingers = false;
                        }
                        if (not_fingers) {
//                            String sinPat = mPreferences.getString("sinPat", null);
                            if (!mOffline)
                                createAudit(new byte[0], ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES.getDescription(), ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES.getCode(), true);
                            else
                                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));

                        }
                        setCombinedHandImage(_ivHands);
                        startFingerprintCapture();

                    } catch (Exception e) {
                        HttpUtil.sendReportUncaughtException(e, mInstitution, getApplicationContext());
                        errorHandlerMethod("", new AutentiaMovilException(String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()), ReturnCode.ERROR_GENERICO));
                    }
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {

                    switch (result.returnCode.getCode()) {

                        case 9000:
                        case 201:
                            errorHandlerMethod("", result);
                            break;
                        case 901:
//                            String sinPat = mPreferences.getString("sinPat", null);
                            if (!mOffline)
                                createAudit(new byte[0], ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES.getDescription(), ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES.getCode(), true);
                            else
                                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));
                            break;
                        case 904:
//                            String noEnro = mPreferences.getString("noEnro", null);
                            if (!mOffline)
                                createAudit(new byte[0], ReturnCode.VERIFICATION_RUT_NO_ENROLADO.getDescription(), ReturnCode.VERIFICATION_RUT_NO_ENROLADO.getCode(), true);
                            else
                                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_RUT_NO_ENROLADO));
                            break;
                        default:
                            errorHandlerMethod(Status.ERROR, result);
                            break;
                    }
                }
            });
        } catch (Exception e) {
            Log.e("SHOWMERESULTHERROR", e.toString());
            errorHandlerMethod("", new AutentiaMovilException(String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()), ReturnCode.ERROR_GENERICO));
        }

    }

    @UiThread()
    void startFingerprintCapture() {
        //Si es Morpho, tenemos que reiniciar el Reader para poder acceder nuevamente al capture sin que se ejecute solo y envíe la misma huella nuevamente.
        if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager")) {
            mFingerprintReader.close();
            mFingerprintReader = mFingerprintManager.getReader(this);
        }
        dismissProgressMessage();
        dismissProgressMessageNFC();
        captureFingerprint();
    }

    @UiThread
    void skipTerms() {
        onAceptDisclamer(null);
    }

    @OnActivityResult(value = REQUEST_NFC_ACCESS)
    protected void onRequestNfcAccessResult(int resultCode, Intent data) {
        mNfcManager.processAccessGrant(resultCode, data, this);
    }

    @OnActivityResult(value = REQUEST_FINGERPRINT_ACCESS)
    protected void onRequestFingerprintAccessResult(int resultCode, Intent data) {
        mFingerprintManager.processAccessGrant(resultCode, data, this);
    }

    void initVerification() throws Exception {

        if (!TextUtils.isEmpty(mBarcode)) {

            if (esQRCedula(mBarcode)) // Cuando es código QR
            {
                try {

                    mNfcManager = NFCManager.getInstance(this);

                    if (mNfcManager == null) {
                        throw new Exception();
                    }

                } catch (Exception e) {
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.DISPOSITIVO_SIN_NFC);
                }

                try {
                    mQRData = new QrData(mBarcode);
                    mSkipReadingNFC = true;

                } catch (UnsupportedEncodingException e) {
                    HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.CODIGO_BARRAS_INVALIDO);

                } catch (Exception e) {
                    HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.ERROR_GENERICO);
                }

                mBarcodeBytes = mBarcode.getBytes();

            } else // Cuando es código PDF417
            {
                //Se separa la data del mBarcode pdf417 en 3 partes
                String[] dataSplited;

                try {

                    dataSplited = mBarcode.split("\\|");

                } catch (Exception e) {
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.CODIGO_BARRAS_INVALIDO);
                }

                byte[] bb;

                try {

                    bb = Base64.decode(dataSplited[0], Base64.DEFAULT);

                    if (bb == null) {
                        throw new UnsupportedEncodingException();
                    }

                    mPathFingerprintPDF417One = dataSplited[1];
                    mPathFingerprintPDF417Two = dataSplited[2];
                    mSizeFingerprintPDF417 = dataSplited[3];

                    mPDF417Data = new Pdf417Data(bb);

                    if (esDistintoRutParametro(mRut, mDv, mPDF417Data.rut, mPDF417Data.dv)) {

                        StringBuilder str = new StringBuilder();
                        str.append("Rut ingresado: ");
                        str.append(mRut);
                        str.append("-");
                        str.append(mDv);
                        str.append(" Es distinto al Rut obtenido de la Cedula de Identidad: ");
                        str.append(mPDF417Data.rut);
                        str.append("-");
                        str.append(mPDF417Data.dv);

                        ReturnCode.VERIFICATION_RUT_CAPTURADO_NO_COINCIDE_CON_CEDULA.setDescription(String.format(ReturnCode.VERIFICATION_RUT_CAPTURADO_NO_COINCIDE_CON_CEDULA.getDescription(), str.toString()));
                        throw new AutentiaMovilException(Status.NO_OK,
                                ReturnCode.VERIFICATION_RUT_CAPTURADO_NO_COINCIDE_CON_CEDULA);
                    }

                    mApellido = mPDF417Data.apPaterno;
                    mFechaVencimiento = mPDF417Data.fechaVencimiento;
                    mNacionalidad = mPDF417Data.codigoPais;

                    boolean[] dedos = new boolean[11];

                    int isoFinger = mPDF417Data.dedoCodificado;

                    dedos[isoFinger] = true;
                    mAvalaibleFinderprints = dedos;
                    setCombinedHandImage(_ivHands);

                } catch (UnsupportedEncodingException e) {
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.CODIGO_BARRAS_INVALIDO);

                } catch (IndexOutOfBoundsException e) {
                    throw new AutentiaMovilException(Status.ERROR,
                            ReturnCode.VERIFICATION_ERROR_OBTENCION_DEDO_DESDE_CODIGO_BARRAS);

                } catch (Exception e) {
                    HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                    throw e;
                }
                mBarcodeBytes = bb;
            }
        }


        //Cambia apariencia de la interfaz de usuario

        if (mIcon.length > 0) {

            Bitmap bmp = BitmapFactory.decodeByteArray(mIcon, 0, mIcon.length);
            _ivFingerPrint.setImageBitmap(bmp);
            mDrawableIcon = new BitmapDrawable(getResources(), bmp);

            if (mIsTablet) {
                _toolbar.setNavigationIcon(mDrawableIcon);
            } else {
                invalidateOptionsMenu();
            }
        }

        _toolbar.setTitle(mTitle);
        _toolbar.setSubtitle(mSubtitle);
        _toolbar.setTitleTextColor(Color.parseColor(mColorTitle));
        _toolbar.setSubtitleTextColor(Color.parseColor(mColorSubtitle));
        _toolbar.setBackgroundColor(Color.parseColor(mColorPrimary));

        _btnCancelar.setBackgroundColor(Color.parseColor(mColorPrimary));

        super.setColorNotificationBar(Color.parseColor(mColorPrimaryDark));

        _btnTermsAndConditions.setText(getResources().getText(R.string.terms_text));
        addIntentos();
        addData();

        if (mSkipTerms)
            skipTerms();


        initControlsStart();
    }

    private boolean esQRCedula(String barcode) {
        return !TextUtils.isEmpty(barcode) &&
                barcode.contains("http") &&
                barcode.contains("mrz");
    }

    private boolean esDistintoRutParametro(int rutParametro, char dvParametro, int rutVerificacion, char dvVerificacion) {
        return (rutParametro != rutVerificacion && dvParametro != dvVerificacion);
    }

    private void addData() {
        String textoRut = String.format("RUT : %s-%s", mRut, mDv);
        _tvRut.setText(textoRut);

        if (mHideRut)
            _tvRut.setVisibility(View.GONE);
    }

    private void addIntentos() {
        for (int i = 0; i < mMaxAttempts; i++) {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(2, 2, 2, 2);

            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.verification_stand_by);
            iv.setLayoutParams(layoutParams);
            iv.setId(i + 10);
            _layoutIntentos.addView(iv);
        }
    }

    @UiThread
    void startReadSmartCard() {
        showProgressMessage("Acerque la cédula al NFC", mTimeOut);
        try {
            mNfcReader.open(this);
        } catch (IOException e) {
            errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.FALLO_APERTURA_NFC));
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void showDialog(String message) {
        if (optionDialog != null)
            if (optionDialog.isShowing())
                optionDialog.dismiss();
        optionDialog = new AlertDialog.Builder(this).create();

        View v = getLayoutInflater().inflate(R.layout.progress_layout, null);
        LottieAnimationView lottie = v.findViewById(R.id.lottie);
        TextView tv = v.findViewById(R.id.tv_message);
        tv.setText(message);
        try {
            lottie.setAnimation(new JSONObject(getResources().getString(R.string.json_animation)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        lottie.playAnimation();
        lottie.setScale(10);
        lottie.loop(true);

        optionDialog.setView(v);
        optionDialog.setCanceledOnTouchOutside(false);
        optionDialog.setCancelable(false);
        optionDialog.show();

    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void showProgressMessage(final String message, final int timeOut) {
        // se cancela cuenta reversa y dialogo anterior por si hubo
        dismissProgressMessage();
        dismissProgressMessageNFC();

        Log.v(TAG, String.format("starting progress: message=%s, timeOut=%d", message, timeOut));

        long timeOutInMiliseconds = TimeUnit.MILLISECONDS.convert(timeOut, TimeUnit.SECONDS);

        mProgressDialog = new ProgressDialog(VerificationActivity.this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(timeOut);
        mProgressDialog.setProgress(timeOut);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressNumberFormat("%1d seg");
        mProgressDialog.setProgressPercentFormat(null);
        mProgressDialog.setTitle(message);
        mProgressDialog.setMessage(String.format("\nQuedan %s seg.", timeOut));
        mProgressDialog.show();

        Log.v(TAG, "progress dialog should be visible");

        mCountDownTimer = new CountDownTimer(timeOutInMiliseconds, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                int secondsLeft = (int) ((millisUntilFinished + 500) / 1000);

                Log.v(TAG, "countdown: " + millisUntilFinished + " ms left");
                updateProgressMessage(
                        String.format("Quedan %s seg.", secondsLeft), secondsLeft);
            }

            @Override
            public void onFinish() {
                createAudit(new byte[0],
                        ReturnCode.NFC_TIMEOUT.getDescription(),
                        ReturnCode.NFC_TIMEOUT.getCode(), false);
                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.NFC_TIMEOUT));
            }

        }.start();

        Log.v(TAG, "CountDownTimer started");
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void updateProgressMessage(final String message, final int secondsLeft) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setProgress(secondsLeft);
                mProgressDialog.setMessage(message);
            }
        });
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void dismissProgressMessage() {
        if (mCountDownTimer != null) mCountDownTimer.cancel();
        if (optionDialog != null && optionDialog.isShowing())
            optionDialog.dismiss();
        Log.v(TAG, "dialog dismissed");
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void dismissProgressMessageNFC() {
        if (mCountDownTimer != null) mCountDownTimer.cancel();
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        Log.v(TAG, "dialog dismissed");
    }

    @UiThread
    void setVisibleTermsAndConditions() {
        _toolbar.setTitle("Autorización");
        _toolbar.setSubtitle("");
        _placeSmartcard.setVisibility(View.GONE);
        _toolbar.setVisibility(View.VISIBLE);
        _btnTermsAndConditions.setVisibility(View.VISIBLE);
        _linearLayoutTermsAndConditions.setVisibility(View.VISIBLE);
    }

    @UiThread
    void setCombinedHandImage(ImageView imageView) {
        Bitmap combinedImage = generateBitmapWithFingersToShow(mAvalaibleFinderprints);
        imageView.setImageBitmap(combinedImage);
    }


    private Bitmap setCombinedHandImageNFC(int dedo) {
        Bitmap bmpHand;
        int mano;
        if (dedo >= 6) {
            mano = MANO_IZQUIERDA;
        } else {
            mano = MANO_DERECHA;
        }

        return generateHandsWithFinger(mano, dedo);
    }

    private void drawResource(Canvas canvas, int resource) {
        Bitmap bmp = BitmapFactory.decodeResource(this.getResources(), resource);
        canvas.drawBitmap(bmp, 0, 0, null);
    }

    private Bitmap generateBitmapWithFingersToShow(final boolean dedos[]) {

        final int dedosDrawables[] = Utils.dedosDrawables;

        Bitmap manos = BitmapFactory.decodeResource(this.getResources(), R.drawable.hands);
        Bitmap bmOverlay = Bitmap.createBitmap(manos.getWidth(), manos.getHeight(),
                manos.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(manos, 0, 0, null);

        if (dedos[0]) {
            drawResource(canvas, R.drawable.hands);

        } else {
            for (int dedoPos = 1; dedoPos <= 10; dedoPos++) {
                if (dedos[dedoPos]) {
                    drawResource(canvas, dedosDrawables[dedoPos]);
                }
            }
        }
        return bmOverlay;
    }

    private Bitmap generateHandsWithFinger(int hand, int finger) {

        final int dedosDrawables[] = Utils.dedosDrawablesNfc;

        Bitmap manos;
        if (hand == MANO_DERECHA) {
            manos = BitmapFactory.decodeResource(this.getResources(), R.drawable.mano_derecha);
        } else {
            manos = BitmapFactory.decodeResource(this.getResources(), R.drawable.mano_izquierda);
        }

        Bitmap bmOverlay = Bitmap.createBitmap(manos.getWidth(), manos.getHeight(),
                manos.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(manos, 0, 0, null);

        drawResource(canvas, dedosDrawablesNfc[finger]);

        return bmOverlay;

    }

    @Click(R.id.btn_acept_terms)
    public void onAceptDisclamer(View v) {

        if (mPrevired && !mAcceptMandate) {
            try {

                _toolbar.setTitle("Firma de Mandato: PREVIRED");
                _toolbar.setSubtitle("");

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            mAcceptMandate = true;
        }

        _toolbar.setTitle(mTitle);
        _toolbar.setSubtitle(mSubtitle);

        _disclamerButtons.setVisibility(LinearLayout.GONE);
        _disclamerButtons.invalidate();

        _linearLayoutTermsAndConditions.setVisibility(ScrollView.GONE);
        _linearLayoutTermsAndConditions.invalidate();

        _linearLayoutVerification.setVisibility(ScrollView.VISIBLE);
        _linearLayoutVerification.invalidate();

        if (mAcceptTermsAndConditions) return;
        mAcceptTermsAndConditions = true;

        _btnCancel.setVisibility(Button.GONE);
        _btnCancel.invalidate();
        _btnAceptTerms.setText("Continuar");


        if (!TextUtils.isEmpty(mUrlDocument) || !TextUtils.isEmpty(mCodigoDocDec) && !TextUtils.isEmpty(mInstitucionDec)) {

            if (mUrlDocument == null) {
                mUrlDocument = createDocumentUrl(mCodigoDocDec, mInstitucionDec);
            }

            mProgressWebView = new ProgressDialog(VerificationActivity.this);
            mProgressWebView.setCancelable(false);
            mProgressWebView.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressWebView.setTitle("Cargando...");
            mProgressWebView.setMessage("Por favor, espere...");
            mProgressWebView.show();

            downloadPdf();

        } else if (mVigencia && mBarcode != null) {
            Pdf417Data mPdf417Data;
            String serie = "";

            if (mQRData != null) {
                serie = mQRData.getSerial();

            } else {
                byte[] bb = Base64.decode(mBarcode, Base64.DEFAULT);
                try {
                    mPdf417Data = new Pdf417Data(bb);
                    serie = mPdf417Data.numeroSerie;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            verificarVigencia(String.format("%s-%s", mRut, mDv), serie);
            showDialog("Verificando vigencia...");
        } else {
            startProcess();
        }
    }

    @Background
    void verificarVigencia(String rut, String serie) {
        if (optionDialog != null)
            if (optionDialog.isShowing())
                optionDialog.dismiss();
        try {
            mAutentiaWSClient.getStatusNEC(rut, serie, mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), mPreferences.getString(KeyPreferences.INSTITUTION), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    try {
                        GetStatusNECResp ger = new Gson().fromJson(result.getString("result"), GetStatusNECResp.class);
                        String statusNEC = ger.getEstado();
                        if (statusNEC.equals("VIGENTE")) {
                            estadoCedula = statusNEC;
                        } else if (statusNEC.equals("NO VIGENTE")) {
                            estadoCedula = statusNEC;
                        } else if (statusNEC.equals("BLOQUEO: DEFINITIVO")) {
                            estadoCedula = statusNEC;
                        } else if (statusNEC.equals("BLOQUEO: TEMPORAL")) {
                            estadoCedula = statusNEC;
                        } else if (statusNEC.equals("CONTINGENCIA")) {
                            estadoCedula = statusNEC;
                        } else if (statusNEC.equals("DOCUMENTO VIGENTE")) {
                            estadoCedula = statusNEC;
                        } else {
                            Log.e("getGlosa", ger.getGlosa());
                            estadoCedula = "Error al obtener Estado Cedula";
                        }

                    } catch (Exception e) {
                        estadoCedula = "Error al obtener Estado Cedula";
                    } finally {
                        startProcess();
                    }
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    estadoCedula = "Error al obtener Estado Cedula";
                    startProcess();
                }
            });
        } catch (Exception e) {
            estadoCedula = "Error al obtener Estado Cedula";
            e.printStackTrace();
        }

    }


    private String createDocumentUrl(String code, String institution) {
        String URL_BASE_DEC;
        String DOCUMENT_VIEWER = "Document/viewer/";

        if (mAmbiente.equals("QA")) {
            URL_BASE_DEC = "https://5cap.dec.cl/";
        } else {
            URL_BASE_DEC = "https://5.dec.cl/";
        }

        String codeWithInstitution = String.format("%s|;_%s", code, institution);
        String codeBase64 = Base64.encodeToString(codeWithInstitution.getBytes(), Base64.DEFAULT);
        return String.format("%s%s%s", URL_BASE_DEC, DOCUMENT_VIEWER, codeBase64.replace("=", ""));
    }

    void startProcess() {
        if (mQRData != null) {
            mSkipReadingNFC = false;
            onResume();
        } else {
            prepareCaptureFingerprint();
        }
    }

    @Click(R.id.btn_cancel_terms)
    public void onCancelDisclamer(View v) {

//        String noAcept = mPreferences.getString("noAcep", null);
        if (!mOffline)
            createAudit(new byte[0],
                    ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES.getDescription(),
                    ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES.getCode(), true);
        else
            errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES));

        HttpUtil.sendReportUncaughtException(new Exception(ReturnCode.VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES.getDescription()), this);

    }

    synchronized void validateFingerprint(FingerprintImage pfingerprintImage) {

        showDialog("Validando Huella...");

        mFingerprintImage = pfingerprintImage;

        if (mFingerprintImage != null) {
            _ivFingerPrint.setImageBitmap(mFingerprintImage.getBitmap());
        }

        try {
            validateFingerprintAccordingVerificationType(mFingerprintImage.getImageWSQ());

        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
        }
    }

    /***
     * Validación de huella en | tipos de casos:
     * - Cédula antigua
     * - Cédula nueva
     * - Contra base Autentia
     *
     */
    @Background
    void validateFingerprintAccordingVerificationType(final byte[] fingerWSQ) {
        if (trackID.isEmpty()) {
            trackID = getTrackID();
        }

        if (mPDF417Data != null) {

            showDialog("Validando localmente...");

            if (mOffline) {

                /**
                 * 2017.07.20 iroman:
                 * Verificación contra cédula (Antigua) OFFLINE, generará una Auditoria por cada
                 * intento de verificación, registrandola como "No exitosa en el servidor"
                 */

                Bundle bundle = new Bundle();
                bundle.putBoolean("match", false);
                bundle.putString("nroAuditoria", null);
                bundle.putByteArray("fingerprint", fingerWSQ);
                bundle.putInt("dedoCodificado", mPDF417Data.dedoCodificado);

                try {
                    String audit = auditList.get(mIntentoActual);

                    boolean validationResponse = validatePDF417Finger(audit);
                    if (validationResponse) bundle.putString("status", "0");
                    else bundle.putString("status", "1");
                    bundle.putBoolean("match", validationResponse);
                    bundle.putString("nroAuditoria", audit);

                } catch (IndexOutOfBoundsException e) {
                    errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.SIN_AUDITORIAS_RESERVADAS));
                } catch (Exception e) {
                    e.printStackTrace();
//                    HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                    if (e.getLocalizedMessage().contains("null")) {
                        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.SIN_AUDITORIAS_RESERVADAS));
                    } else {
                        errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                    }
                }
                showValidationResponse(bundle);

            } else {

                boolean validatedFingerprint = validatePDF417Finger(null);

                if (!validatedFingerprint) {

                    /**
                     * La verificación contra Cédula (Antigua) OFFLINE considera la verificación de:
                     *
                     * - Huella viva contra huella impresa en reverso de cédula
                     * - Huella viva contra PC1 obtenido del código de barras enviada al servicio NEC.
                     *
                     * Si una de ambas no cumple la verificación no es exitosa.                     *
                     */
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("match", false);
                    bundle.putString("nroAuditoria", null);
                    bundle.putByteArray("fingerprint", fingerWSQ);
                    bundle.putInt("dedoCodificado", mPDF417Data.dedoCodificado);
                    showValidationResponse(bundle);

                } else {

                    try {

                        String versionApp = AppHelper.getCurrentVersion(this);
                        String ubicacion = Arrays.toString(mLocationHelper.getCoordinates());
                        String textoAdjunto = "";
                        String valorAdjunto = "";

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", "Verificacion_contra_cedula_Antigua_ONLINE");
                        jsonObject.put("rut", mRut);
                        jsonObject.put("dv", String.valueOf(mDv));
                        jsonObject.put("nombre", mPDF417Data.apPaterno);
                        jsonObject.put("muestraWidth", mFingerprintImage.getWidth());
                        jsonObject.put("muestraHeight", mFingerprintImage.getHeight());
                        jsonObject.put("numeroSerieLector", mSerialNumber);
                        jsonObject.put("tipoLector", mReaderType);
                        jsonObject.put("rutOperador", mRunInstitution);
                        jsonObject.put("institucion", mInstitution);
                        jsonObject.put("versionApp", versionApp);
                        jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
                        jsonObject.put("ip", DeviceHelper.getIPAddress(true));
                        jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
                        jsonObject.put("imei", DeviceHelper.getIMEI(this));
                        jsonObject.put("ubicacion", ubicacion);
                        jsonObject.put("presicionUbicacion", Math.round(mLocationHelper.getAccuracy()));
                        jsonObject.put("operacion", AUDIT_TYPE);
                        jsonObject.put("origen", AUDIT_SOURCE);
                        jsonObject.put("trackId", trackID);
                        jsonObject.put("REINTENTOS", mIntentoActual + 1);
                        if (!mProposito.equals("")) {
                            jsonObject.put("proposito", mProposito);
                        } else {
                            jsonObject.put("proposito", "4");
                        }

                        // NEC requiere huella en orientacion punta del dedo al sur
                        byte[] fingerprintWSQByteArray = mFingerprintImage.getRotated180().getImageWSQ();

                        byte[] bmp = new byte[0];
                        if (mFingerprintImage != null) {
                            bmp = Utils.bitmapToByteArray(mFingerprintImage.getBitmap());
                        }

                        showDialog("Validando en Servidor...");

                        // Verificación contra servicio NEC
                        mAutentiaWSClient.validNEC(
                                String.format("%s-%s", mRut, mDv),
                                String.format("%s", mPDF417Data.dedoCodificado - 1),
                                mPDF417Data.PC1, fingerprintWSQByteArray,
                                mSerialNumber, jsonObject.toString(), mRunInstitution, versionApp,
                                ubicacion, textoAdjunto, valorAdjunto, mReaderType, mInstitution, mOti, bmp, new ResponseCallback() {
                                    @Override
                                    public void onResponseSuccess(Bundle result) {
                                        Log.d(TAG, "Verificación en NEC");
                                        showValidationResponse(result);
                                    }

                                    @Override
                                    public void onResponseError(AutentiaMovilException result) {
                                        Log.e(TAG, "Verificación en NEC HERROR");
                                        if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201) {
                                            errorHandlerMethod("", result);
                                        }
                                        errorHandlerMethod(Status.ERROR, result);
                                    }
                                });
                    } catch (Exception e) {
                        HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                        errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                    }
                }
            }

        } else if (mQRData != null) {

            byte[] fingerprintRaw = mFingerprintImage.getRawImage();
            mPendingCaptureFingerprint = false;

            int rawFinger_w = mFingerprintImage.getWidth();
            int rawFinger_h = mFingerprintImage.getHeight();

            try {
                mFmdFinger = mEngine.CreateFmd(fingerprintRaw, rawFinger_w, rawFinger_h,
                        500, 0, 0, Fmd.Format.ISO_19794_2_2005);

            } catch (UareUException e) {
                HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                // hubo un problema en la extraccion de minucias,
                // los mas probable porque la calidad no es suficiente
                mFmdFinger = null;
            }

            if (!mPendingCaptureFingerprint) {
                try {
                    validateQRFinger();
                } catch (Throwable throwable) {
                    HttpUtil.sendReportUncaughtException(throwable, this);
                    startReadSmartCard();
                }
            }

        } else {

            try {

                String versionApp = AppHelper.getCurrentVersion(this);
                String ubicacion = Arrays.toString(mLocationHelper.getCoordinates());

                String porFirmar = mPrevired ? "1" : "0";
                if (mTraspaso) {
                    porFirmar = String.format("%s:%s", "1", CLAVE_TRASPASO);
                }

                String textoAdjunto = "";
                String valorAdjunto = "";

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", "Verificacion_contra_base_Autentia");
                jsonObject.put("rut", mRut);
                jsonObject.put("dv", String.valueOf(mDv));
                jsonObject.put("muestraWidth", mFingerprintImage.getWidth());
                jsonObject.put("muestraHeight", mFingerprintImage.getHeight());
                jsonObject.put("numeroSerieLector", mSerialNumber);
                jsonObject.put("tipoLector", mReaderType);
                jsonObject.put("rutOperador", mRunInstitution);
                jsonObject.put("institucion", mInstitution);
                jsonObject.put("versionApp", versionApp);
                jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
                jsonObject.put("ip", DeviceHelper.getIPAddress(true));
                jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
                jsonObject.put("imei", DeviceHelper.getIMEI(this));
                jsonObject.put("ubicacion", ubicacion);
                jsonObject.put("presicionUbicacion", Math.round(mLocationHelper.getAccuracy()));
                jsonObject.put("operacion", AUDIT_TYPE);
                jsonObject.put("origen", AUDIT_SOURCE);
                jsonObject.put("trackId", trackID);
                jsonObject.put("REINTENTOS", mIntentoActual + 1);
                if (!mProposito.equals("")) {
                    jsonObject.put("proposito", mProposito);
                } else {
                    jsonObject.put("proposito", "4");
                }

                Bitmap fingerprintBitmap = mFingerprintImage.getBitmap();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                fingerprintBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] byteArray = stream.toByteArray();

                mAutentiaWSClient.ValidFingerprintBitmap(
                        String.format("%s-%s", mRut, mDv),
                        mFingerprintImage.getImageWSQ(),
                        byteArray,
                        mReaderType,
                        mTransactionId,
                        mSerialNumber,
                        jsonObject.toString(),
                        mRunInstitution,
                        versionApp,
                        ubicacion,
                        porFirmar,
                        textoAdjunto,
                        valorAdjunto,
                        mInstitution,
                        mOti,
                        /*opcional*/
                        mAdicional,
                        new ResponseCallback() {
                            @Override
                            public void onResponseSuccess(Bundle result) {
                                Log.e("onResponseSuccess", "SUCCESS");
                                result.putByteArray("fingerprint", fingerWSQ);
                                showValidationResponse(result);
                            }

                            @Override
                            public void onResponseError(AutentiaMovilException result) {
                                if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201) {
                                    errorHandlerMethod("", result);
                                }
                                Log.e("status", result.status);
                                errorHandlerMethod(Status.ERROR, result);
                            }
                        });

            } catch (Exception e) {
                HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
            }
        }
    }


    /**
     * Verificación de huella viva contra impresa en cédula antigua
     * Si se le pasa un código de auditoria almacena la evidencia y la postea en el servidor.
     *
     * @param audit
     * @return
     */
    boolean validatePDF417Finger(String audit) {

        try {

            byte[] rawFinger = mFingerprintImage.getRawImage();
            byte[] bmpFinger = Utils.bitmapToByteArray(mFingerprintImage.getBitmap());
            int rawFingerWidth = mFingerprintImage.getWidth();
            int rawFingerHeight = mFingerprintImage.getHeight();

            //Huella derecha
            byte[] rawFingerPrintLeft = Utils.readByteFile(mPathFingerprintPDF417One);
            int rawFingerPrintWidthLeft = Math.round(Float.parseFloat(mSizeFingerprintPDF417));
            int rawFingerPrintHeightLeft = Math.round(Float.parseFloat(mSizeFingerprintPDF417));

            int fingerDPI = 500;
            int testFingerPrintDPI = (int) (rawFingerPrintWidthLeft / (0.74803));
            int fingerPrintDPI = testFingerPrintDPI < 400 ? 400 : fingerDPI;

            int score = Integer.MAX_VALUE;

            try {
                score = compareFinger(rawFinger, rawFingerWidth,
                        rawFingerHeight, fingerDPI, rawFingerPrintLeft,
                        rawFingerPrintWidthLeft, rawFingerPrintHeightLeft,
                        fingerPrintDPI);

            } catch (Exception ex) {
                HttpUtil.sendReportUncaughtException(ex, mInstitution, this);
                Log.e("Error: ", ex.getMessage());
            }

            int puntaje = com.acepta.Utils.simpleScore(score);
            int minScore = DEFAULT_MIN_SCORE;
            boolean validationResult = puntaje >= minScore;

            Log.d(TAG, "huella 1");
            Log.d(TAG, String.format("Score: Crudo [%s] - Procesado [%s] - MinScore: [%s]", score, puntaje, minScore));
            Log.d(TAG, String.format("Verificado: %s", validationResult));

            boolean huellaIzquierdaExitosa = validationResult;

            //Huella invertida
            byte[] rawFingerPrintRight = Utils.readByteFile(mPathFingerprintPDF417Two);
            int rawFingerPrintWidthRight = Math.round(Float.parseFloat(mSizeFingerprintPDF417));
            int rawFingerPrintHeightRight = Math.round(Float.parseFloat(mSizeFingerprintPDF417));

            if (!validationResult) {

                testFingerPrintDPI = (int) (rawFingerPrintWidthRight / (0.74803));
                fingerPrintDPI = testFingerPrintDPI < 400 ? 400 : fingerDPI;

                try {
                    score = compareFinger(rawFinger, rawFingerWidth,
                            rawFingerHeight, fingerDPI, rawFingerPrintRight,
                            rawFingerPrintWidthRight, rawFingerPrintHeightRight,
                            fingerPrintDPI);

                } catch (Exception ex) {
                    HttpUtil.sendReportUncaughtException(ex, mInstitution, this);
                    Log.e("Error: ", ex.getMessage());
                }

                puntaje = com.acepta.Utils.simpleScore(score);
                validationResult = puntaje >= minScore;
                huellaIzquierdaExitosa = validationResult;

                Log.d(TAG, "huella 2");
                Log.d(TAG, String.format("Score: Crudo [%s] - Procesado [%s] - MinScore: [%s]", score, puntaje, minScore));
                Log.d(TAG, String.format("Verificado: %s", validationResult));
            }

            if (mOffline) {
                storeEvidencePDFData(
                        rawFinger,
                        bmpFinger,
                        rawFingerWidth,
                        rawFingerHeight,
                        fingerDPI,
                        huellaIzquierdaExitosa ? rawFingerPrintLeft : rawFingerPrintRight,
                        huellaIzquierdaExitosa ? rawFingerPrintWidthLeft : rawFingerPrintWidthRight,
                        huellaIzquierdaExitosa ? rawFingerPrintHeightLeft : rawFingerPrintHeightRight,
                        fingerPrintDPI,
                        validationResult ? SUCCESS_RESULT : NOT_SUCCESS_RESULT_PDF417,
                        audit);
            }
            return validationResult;
        } catch (Exception ex) {
            Log.e("ERROR", ex.getMessage());
            HttpUtil.sendReportUncaughtException(ex, mInstitution, this);
        }
        return false;
    }

    /**
     * Obtiene el resultado de la comparación de huellas con la interfaz NFC
     *
     * @throws ISOError
     * @throws EIDError
     * @throws SmartCardError
     */
    void validateQRFinger() {

        try {
            boolean resultComparison = compareFingerprintWithSmartCard();
            endingProcessCompareFinger(resultComparison);
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            throw new RuntimeException(e);
        }
    }

    /**
     * Comparación de huellas
     *
     * @return
     * @throws SmartCardError
     * @throws EIDError
     * @throws ISOError
     */
    private boolean compareFingerprintWithSmartCard() throws SmartCardError, EIDError, ISOError {
        // fallo extraccion de minucias por mala calidad de imagen (u otro error de UareU)
        // devolvemos error de verificacion
        if (mFmdFinger == null)
            return false;

        ISOFMR fmr = new ISOFMR(mFmdFinger.getData());
        fmr.convertToCC();

        int bitMatched = 0;
        if (!mNewIdent) {
            // validamos la huella contra la ranura 1 de la cedula
            if (mEid.verifyBIR(1, fmr.ccStraight).equals("true"))
                bitMatched = 1;

            // si ranura 1 rechazo la huella, validamos la huella contra la ranura 2 de la cedula
            if (bitMatched == 0 && mEid.verifyBIR(2, fmr.ccStraight).equals("true"))
                bitMatched = 2;
        } else {
            int position = pager.getCurrentItem() == 0 ? 2 : 1;

            respBIR = mEid.verifyBIR(position, fmr.ccStraight);

            Log.e("resp bir", respBIR);

            if (respBIR.equals("true")) {
                bitMatched = position;
            } else if (Utils.checkBlocked(Integer.parseInt(respBIR))) {
                if (!mOffline) {
                    try {
                        createAudit(mFingerprintImage.getImageWSQ(),
                                ReturnCode.VERIFICACION_CEDULA_IDENTIDAD_BLOQUEADA.getDescription(),
                                ReturnCode.VERIFICACION_CEDULA_IDENTIDAD_BLOQUEADA.getCode(), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                String textNfcAttempts = String.format("Intentos de cedula restantes : %s ", Utils.returnCurrentAttempt(Integer.parseInt(respBIR)));
                showAttemptsNfc(textNfcAttempts);
            }
        }

        String fingerIdISO = Integer.toString(mFingerISOQR[(bitMatched - 1) < 0 ? 0 : (bitMatched - 1)]);

        mQRData.setDedoCodificado(String.valueOf(fingerIdISO));

        // devolvemos true si huella fue reconocida, false si no
        return (bitMatched > 0);
    }

    public void showAttemptsNfc(String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _tvNfcAttempts.setText(text);
                if (_tvNfcAttempts.getVisibility() == View.GONE)
                    _tvNfcAttempts.setVisibility(View.VISIBLE);
            }
        });
    }

    public int compareFinger(byte[] rawFinger, int rawFinger_w,
                             int rawFinger_h, int FingerDPI, byte[] rawFingerPrint,
                             int rawFingerPrint_w, int rawFingerPrint_h, int FingerPrintDPI)
            throws UareUException {

        Fmd fmdFinger = mEngine.CreateFmd(rawFinger, rawFinger_w, rawFinger_h,
                FingerDPI, 0, 0, Fmd.Format.ISO_19794_2_2005);

        FingerprintImage fpi = new FingerprintImage(rawFingerPrint, rawFingerPrint_w, rawFingerPrint_h, (int) (rawFingerPrint_h * 500.0 / 394));
        FingerprintImage scaled = fpi.scaleToDPI(500);
        Fmd fmdFingerPrint;
        try {
            fmdFingerPrint = mEngine.CreateFmd(scaled.getRawImage(),
                    scaled.getWidth(), scaled.getHeight(), scaled.getDpi(), 0, 0,
                    Fmd.Format.ISO_19794_2_2005);
        } catch (Exception e) {
            fmdFingerPrint = mEngine.CreateFmd(fpi.getRawImage(),
                    fpi.getWidth(), fpi.getHeight(), fpi.getDpi(), 0, 0,
                    Fmd.Format.ISO_19794_2_2005);
        }

        return mEngine.Compare(fmdFinger, 0, fmdFingerPrint, 0);
    }

    /**
     * Traduce el resultado de la comparación de huella contra NFC, en caso de ser un proceso
     * OFFLINE almacena la evidencia, en caso de proceso ONLINE, genera Auditoria
     *
     * @param finalBitMatched
     */
    @Background
    void endingProcessCompareFinger(boolean finalBitMatched) {
        if (trackID.isEmpty()) {
            trackID = getTrackID();
        }

        Bundle result = new Bundle();
        result.putBoolean("match", finalBitMatched);
        result.putString("nroAuditoria", null);
        result.putString("status", "1");
        mSuccessVerification = finalBitMatched;

        String resultado = mSuccessVerification ? SUCCESS_RESULT : NOT_SUCCESS_RESULT_QR;
        String run = mQRData.getRun();
        int dedo = Integer.parseInt(mQRData.getDedoCodificado());//Modificacion de formato iso

        if (!finalBitMatched) {
            showValidationResponse(result);

        } else {
            try {

                byte[] fingerprintRaw = mFingerprintImage.getRawImage();
                byte[] fingerprintWSQByteArray = mFingerprintImage.getImageWSQ();
                byte[] fingerprintBMPByteArray = Utils.bitmapToByteArray(mFingerprintImage.getBitmap());
                /**
                 * Si es una operación offline, se almacena la evidencia,
                 * en caso contrario se envia la data al servidor para generar auditoria.
                 */
                if (mOffline) {

                    try {

                        String numeroAuditoria = auditList.get(mIntentoActual);

                        Bundle bundle = new Bundle();
                        bundle.putBoolean("match", mSuccessVerification);
                        bundle.putString("nroAuditoria", numeroAuditoria);
                        if (mSuccessVerification) bundle.putString("status", "0");
                        else bundle.putString("status", "1");

                        /**
                         * 2017.07.20
                         * Por cada intento de verificación contra cédula (Nueva) OFFLINE, se almacenará
                         * evidencia y se generará auditoria "No Exitosa"
                         */
                        storeEvidenceQRData(fingerprintWSQByteArray, fingerprintBMPByteArray, mRut, mDv, dedo,
                                resultado, mNombre, numeroAuditoria);

                        if (!mSuccessVerification) {
                            mPendingCaptureFingerprint = true;
                        }
                        showValidationResponse(bundle);

                    } catch (IndexOutOfBoundsException e) {
                        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.SIN_AUDITORIAS_RESERVADAS));
                    } catch (Exception e) {
                        if (e.getMessage().equals("Index: 1, Size: 1")) {
                            errorHandlerMethod("", new AutentiaMovilException(ReturnCode.SIN_AUDITORIAS_RESERVADAS.getDescription(), ReturnCode.SIN_AUDITORIAS_RESERVADAS));
                        }

                        HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                        errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                    }

                } else {

                    if (fingerprintWSQByteArray == null || run == null
                            || mReaderType == null || mSerialNumber == null
                            || fingerprintRaw == null || resultado == null
                            || mNombre == null) {
                        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.PARAMETROS_INVALIDOS));
                        return;
                    }

                    String versionApp = AppHelper.getCurrentVersion(this);
                    String ubicacion = Arrays.toString(mLocationHelper.getCoordinates());
                    String porFirmar = mPrevired ? "1" : "0";
                    String textoAdjunto = "";
                    String valorAdjunto = "";

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", "Verificacion_contra_Cedula_Nueva");
                    jsonObject.put("rut", mRut);
                    jsonObject.put("dv", String.valueOf(mDv));
                    jsonObject.put("nombre", mNombre);
                    jsonObject.put("muestraWidth", mFingerprintImage.getWidth());
                    jsonObject.put("muestraHeight", mFingerprintImage.getHeight());
                    jsonObject.put("numeroSerieLector", mSerialNumber);
                    jsonObject.put("tipoLector", mReaderType);
                    jsonObject.put("rutOperador", mRunInstitution);
                    jsonObject.put("institucion", mInstitution);
                    jsonObject.put("versionApp", versionApp);
                    jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
                    jsonObject.put("ip", DeviceHelper.getIPAddress(true));
                    jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
                    jsonObject.put("imei", DeviceHelper.getIMEI(this));
                    jsonObject.put("ubicacion", ubicacion);
                    jsonObject.put("intento", mIntentoActual);
                    jsonObject.put("presicionUbicacion", Math.round(mLocationHelper.getAccuracy()));
                    jsonObject.put("operacion", AUDIT_TYPE);
                    jsonObject.put("origen", AUDIT_SOURCE);
                    jsonObject.put("resultado", resultado);
                    jsonObject.put("trackId", trackID);
                    jsonObject.put("REINTENTOS", mIntentoActual + 1);
                    //se agregan datos extra
                    jsonObject.put("fechaNac", mFechaNac);
                    jsonObject.put("fechaVenc", mFechaVencimiento);
                    jsonObject.put("sexo", mSexo);
                    jsonObject.put("nacionalidad", mNacionalidad);
                    if (mMocExtendedInfo) {
                        textoAdjunto = URI_STANDARD + mRetrato + ";" + URI_STANDARD + mFirma;
                    }

                    if (!mProposito.equals("")) {
                        jsonObject.put("proposito", mProposito);
                    } else {
                        jsonObject.put("proposito", "4");
                    }

                    //Generación de auditoria para la verificación contra cédula (Nueva) ONLINE
                    mAutentiaWSClient.genAudit(fingerprintWSQByteArray,
                            "wsq",
                            run,
                            String.valueOf(dedo - 1),
                            mReaderType,
                            mSerialNumber,
                            fingerprintBMPByteArray,
                            resultado,
                            mNombre,
                            jsonObject.toString(),
                            mRunInstitution,
                            versionApp,
                            ubicacion,
                            porFirmar,
                            textoAdjunto,
                            valorAdjunto,
                            mInstitution,
                            mOti, new ResponseCallback() {
                                @Override
                                public void onResponseSuccess(Bundle result) {

                                    GenAuditResp gar = new Gson().fromJson(result.getString("result"), GenAuditResp.class);
                                    Log.e("RESPONSE", result.getString("result"));
                                    Log.e("RESPONSE", gar.getNroAuditoria());
                                    Log.e("RESPONSE", String.valueOf(gar.getStatus()));
                                    String genAuditResult = gar.getNroAuditoria();
                                    Bundle bundle = new Bundle();
                                    if (mOti) {
                                        bundle.putString("urlOti", gar.getOti().getUrlOti());
                                        bundle.putString("identificacionProveedor", gar.getOti().getValidacion().getVerificacion().getIdentificacionProveedor());
                                        bundle.putString("fechaVerificacion", gar.getOti().getValidacion().getVerificacion().getFechaVerificacion());
                                        bundle.putString("tipoVerificacion", gar.getOti().getValidacion().getVerificacion().getTipoVerificacion());
                                    }
                                    bundle.putBoolean("match", mSuccessVerification);
                                    bundle.putString("nroAuditoria", genAuditResult);
                                    bundle.putString("status", String.valueOf(gar.getStatus()));
                                    if (gar.getGlosa() != null)
                                        bundle.putString("glosa", String.valueOf(gar.getGlosa()));
                                    if (extendedInfo != null) {
                                        Log.e("if - enter", "entra");
                                        try {
                                            JSONObject extendedDoc = new JSONObject(extendedInfo);
                                            bundle.putByteArray("retrato",
                                                    Base64.decode(extendedDoc.getString("retrato"), Base64.DEFAULT));

                                            bundle.putByteArray("firma",
                                                    Base64.decode(extendedDoc.getString("firma"), Base64.DEFAULT));
                                            Log.i(TAG, "retornando informacion extendida");
                                        } catch (Exception e) {
                                            Log.e("Error", "problemas al obetener informacion extendida");
                                        }
                                    }
                                    showValidationResponse(bundle);
                                }

                                @Override
                                public void onResponseError(AutentiaMovilException result) {
                                    if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201) {
                                        errorHandlerMethod("", result);
                                    }
                                    errorHandlerMethod(Status.ERROR, result);

                                }
                            });
                }
            } catch (Exception e) {
                HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                if (e.toString().contains(UNEXPECTED_CHARACTER)) {
                    if (errorCountAudit < 3) {
                        errorCountAudit++;
                        endingProcessCompareFinger(true);
                    } else {
                        errorHandlerMethod("", new AutentiaMovilException(String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()), ReturnCode.ERROR_GENERICO));
                    }
                } else {
                    errorHandlerMethod("", new AutentiaMovilException(String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()), ReturnCode.ERROR_GENERICO));
                }

            } finally {
                dismissProgressMessageNFC();
            }
        }
    }

    private String getLocalAudit() {
        String numeroAuditoria = Auditoria_.getAudit();
        Auditoria_.deleteAudit(numeroAuditoria);
        return numeroAuditoria;
    }

//    private void deleteAudit(String numeroAuditoria) {
//        Realm realm = Realm.getDefaultInstance();
//        realm.executeTransaction();
//
//    }

    /**
     * @param muestraWSQ
     * @param rut
     * @param dv
     * @param dedo
     * @param resultado
     * @param nombre
     * @param numeroAuditoria
     */
    private void storeEvidenceQRData(byte[] muestraWSQ, byte[] muestraBMP, int rut, char dv, int dedo, String resultado,
                                     String nombre, String numeroAuditoria) {

        try {

            //Almacenamiento de datos
            List<byte[]> contents = new ArrayList<>();
            contents.add(mBarcodeBytes);

            byte[] zipBytes = Utils.zipBytes(new String[]{"barcode.txt"}, contents);

            String type = "Verificacion_contra_Cedula_Nueva_OFFLINE";

            JSONObject jsonEvidence = generateJsonEvidence(
                    type
                    , rut
                    , dv
                    , nombre == null ? mApellido : String.format("%s %s", nombre, mApellido)
                    , Utils.traslateFingerIdToFingerDescription(dedo)
                    , mFingerprintImage.getWidth()
                    , mFingerprintImage.getHeight()
                    , mSerialNumber
                    , mPreferences.getString(KeyPreferences.RUN_INSTITUTION)
                    , mInstitution
                    , numeroAuditoria
                    , new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
                    , AppHelper.getCurrentVersion(this)
                    , DeviceHelper.getOperator(this)
                    , DeviceHelper.getIPAddress(true)
                    , DeviceHelper.getAndroidId(this)
                    , DeviceHelper.getIMEI(this)
                    , Arrays.toString(mLocationHelper.getCoordinates())
                    , Math.round(mLocationHelper.getAccuracy())
                    , AUDIT_TYPE
                    , AUDIT_SOURCE
                    , mReaderType
                    , resultado);

            Evidence_ evidence = new Evidence_();
            evidence.setJsonData(jsonEvidence.toString());
            evidence.setFingerprint(muestraWSQ);
            evidence.setFingerprintBmp(muestraBMP);
            evidence.setAttachments(zipBytes);
            evidence.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
            evidence.setType(type);
            evidence.setAudit(numeroAuditoria);
            if (mFirma != null)
                evidence.setFirma(mFirma);
            if (mRetrato != null)
                evidence.setRetrato(mRetrato);
            Evidence_.addAuditoria(evidence);

            Auditoria_.deleteAudit(numeroAuditoria);

            MyServiceSyncAdapter.syncImmediately(this);

        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) unregisterReceiver(receiver);
        dismissProgressMessage();
        dismissProgressMessageNFC();
        if (mNfcReader != null)
            mNfcReader.close();
        super.onDestroy();
    }

    /**
     * @param rawFinger
     * @param rawFinger_w
     * @param rawFinger_h
     * @param FingerDPI
     * @param rawFingerPrint
     * @param rawFingerPrint_w
     * @param rawFingerPrint_h
     * @param FingerPrintDPI
     * @param resultado
     * @param numeroAuditoria
     */
    private void storeEvidencePDFData(byte[] rawFinger, byte[] bmpFinger, int rawFinger_w, int rawFinger_h,
                                      int FingerDPI, byte[] rawFingerPrint, int rawFingerPrint_w,
                                      int rawFingerPrint_h, int FingerPrintDPI, String resultado, String numeroAuditoria) {


        try {

            //Almacenamiento de datos
            byte[] muestraWSQ = ImageUtil.rawToWSQ(rawFinger, rawFinger_w,
                    rawFinger_h, FingerDPI, 8);

            byte[] huellaCedula = ImageUtil.rawToWSQ(rawFingerPrint,
                    rawFingerPrint_w, rawFingerPrint_h, FingerPrintDPI, 8);

            List<byte[]> contents = new ArrayList<>();
            contents.add(mBarcodeBytes);
            contents.add(huellaCedula);

            byte[] zipBytes = Utils.zipBytes(new String[]{"mBarcode.txt", "huellaCedula.wsq"}, contents);

            String type = "Verificacion_contra_Cedula_Antigua_OFFLINE";

            JSONObject jsonEvidence = generateJsonEvidence(
                    type
                    , mRut
                    , mDv
                    , mPDF417Data.apPaterno
                    , Utils.traslateFingerIdToFingerDescription(mPDF417Data.dedoCodificado)
                    , mFingerprintImage.getWidth()
                    , mFingerprintImage.getHeight()
                    , mSerialNumber
                    , mPreferences.getString(KeyPreferences.RUN_INSTITUTION)
                    , mPreferences.getString(KeyPreferences.INSTITUTION)
                    , numeroAuditoria
                    , new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
                    , AppHelper.getCurrentVersion(this)
                    , DeviceHelper.getOperator(this)
                    , DeviceHelper.getIPAddress(true)
                    , DeviceHelper.getAndroidId(this)
                    , DeviceHelper.getIMEI(this)
                    , Arrays.toString(mLocationHelper.getCoordinates())
                    , Math.round(mLocationHelper.getAccuracy())
                    , AUDIT_SOURCE
                    , AUDIT_TYPE
                    , mReaderType
                    , resultado);

            Evidence_ evidence = new Evidence_();
            evidence.setJsonData(jsonEvidence.toString());
            evidence.setFingerprint(muestraWSQ);
            evidence.setFingerprintBmp(bmpFinger);
            evidence.setAttachments(zipBytes);
            evidence.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
            evidence.setType(type);
            evidence.setAudit(numeroAuditoria);
            Evidence_.addAuditoria(evidence);

            Auditoria_.deleteAudit(numeroAuditoria);

            MyServiceSyncAdapter.syncImmediately(this);

        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
        }
    }

    /**
     * @param type
     * @param rut
     * @param dv
     * @param name
     * @param finderDescription
     * @param sampleWidth
     * @param sampleHeight
     * @param serialNumber
     * @param runInstitution
     * @param institution
     * @param codeAudit
     * @param fingerDate
     * @param versionApp
     * @param operator
     * @param ip
     * @param androidId
     * @param imei
     * @param location
     * @param presition
     * @param operation
     * @param origin
     * @param readerType
     * @param result
     * @return
     * @throws JSONException
     */
    private JSONObject generateJsonEvidence(String type, int rut, char dv, String name,
                                            String finderDescription, int sampleWidth, int sampleHeight,
                                            String serialNumber, String runInstitution, String institution,
                                            String codeAudit, String fingerDate, String versionApp,
                                            String operator, String ip, String androidId, String imei, String location,
                                            long presition, String operation, String origin, String readerType,
                                            String result) throws JSONException {

        if (trackID.isEmpty()) {
            trackID = getTrackID();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type);
        jsonObject.put("rut", rut);
        jsonObject.put("dv", String.valueOf(dv));
        jsonObject.put("nombre", name);
        jsonObject.put("dedoCodificado", finderDescription);
        jsonObject.put("muestraWidth", sampleWidth);
        jsonObject.put("muestraHeight", sampleHeight);
        jsonObject.put("numeroSerieLector", serialNumber);
        jsonObject.put("tipoLector", readerType);
        jsonObject.put("rutOperador", runInstitution);
        jsonObject.put("institucion", institution);
        jsonObject.put("codigoAuditoria", codeAudit);
        jsonObject.put("dedoFecha", fingerDate);
        jsonObject.put("versionApp", versionApp);
        jsonObject.put("operadorMovil", operator);
        jsonObject.put("ip", ip);
        jsonObject.put("androidId", androidId);
        jsonObject.put("imei", imei);
        jsonObject.put("ubicacion", location);
        jsonObject.put("presicionUbicacion", presition);
        jsonObject.put("operacion", operation);
        jsonObject.put("origen", origin);
        jsonObject.put("resultado", result);
        jsonObject.put("trackId", trackID);
        jsonObject.put("REINTENTOS", mIntentoActual + 1);
        if (!mProposito.equals("")) {
            jsonObject.put("proposito", mProposito);
        } else {
            jsonObject.put("proposito", "4");
        }
        jsonObject.put("fechaNac", mFechaNac);
        jsonObject.put("fechaVenc", mFechaVencimiento);
        jsonObject.put("sexo", mSexo);
        jsonObject.put("nacionalidad", mNacionalidad);

        return jsonObject;
    }

    /**
     * Procesa la respuesta de la verificación en pantalla.
     *
     * @param result
     */
    @UiThread
    void showValidationResponse(Bundle result) {

        boolean identidadVerificada = false;
        String codigoAuditoria = "";
        byte[] fingerWSQ = new byte[0];
        int dedo = 10;

        Iterator iterator = result.keySet().iterator();
        String msg = "";

        while (iterator.hasNext()) {

            String key = iterator.next().toString();
            Object value = result.get(key);

            msg += String.format("%s : %s\n\n", key, value);
            Log.d(TAG, String.format("%s : %s", key, value));
        }
        Log.e("showValidationRsponse", result.keySet().toString());
        Log.e("showValidationRsponse", result.getString("status", "-1"));

        fingerWSQ = result.getByteArray("fingerprint");
        dedo = result.getInt("dedoCodificado");
        if (result.getString("status", "-1").equals("0")) {
            Log.e("status", "true");
            codigoAuditoria = result.getString("nroAuditoria");
            identidadVerificada = true;
        } else {
            Log.e("status", "false");
            identidadVerificada = false;
        }

        ImageView iv = _layoutIntentos
                .findViewById(mIntentoActual + 10);

        mIntentoActual++;

        if (!identidadVerificada) {

            if (iv != null) {
                iv.setImageResource(R.drawable.verification_error);
            }
            if (mIntentoActual >= mMaxAttempts) {
//                String maxInt = mPreferences.getString("maxInt", null);
                if (!mOffline)
                    createAudit(fingerWSQ,
                            ReturnCode.VERIFICATION_SUPERADO_INTENTOS_VERIFICACION.getDescription(),
                            ReturnCode.VERIFICATION_SUPERADO_INTENTOS_VERIFICACION.getCode(), true);
                else
                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.VERIFICATION_SUPERADO_INTENTOS_VERIFICACION));
            } else {

                mPendingCaptureFingerprint = true;

                dismissProgressMessage();
                dismissProgressMessageNFC();

                String messageRevalidation = "Identidad no verificada. Coloque nuevamente su dedo sobre el lector de huellas";

                _tvHelpVerification.setText(messageRevalidation);
                _tvHelpVerification.setTextColor(Color.RED);

                String messageRevalidation_snackbar = "Identidad no verificada";

                showSnackBar(messageRevalidation_snackbar);


                startFingerprintCapture();
            }
        } else {

            dismissProgressMessage();
            dismissProgressMessageNFC();

            if (iv != null) {
                iv.setImageResource(R.drawable.verification_ok);
            }

            Intent returnIntent = new Intent();
            returnIntent.putExtra(Extras.Out.RUT, mRut);
            returnIntent.putExtra(Extras.Out.DV, mDv);
            returnIntent.putExtra(Extras.Out.ESTADO, Status.OK);
            returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
            returnIntent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
            returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, identidadVerificada);
            returnIntent.putExtra(Extras.Out.NUMERO_SERIE_HUELLERO, mSerialNumber);
            returnIntent.putExtra(Extras.Out.CODIGO_AUDITORIA, codigoAuditoria);
            returnIntent.putExtra(Extras.Out.TIPO_LECTOR, mReaderType);
            returnIntent.putExtra(Extras.Out.TRACK_ID, trackID);
            if (mTransactionId != null && !mTransactionId.isEmpty())
                returnIntent.putExtra(Extras.Out.TRANSACTION_ID, mTransactionId);
            if (!TextUtils.isEmpty(mNombre))
                returnIntent.putExtra(Extras.Out.NOMBRE, mNombre);
            if (!TextUtils.isEmpty(mApellido))
                returnIntent.putExtra(Extras.Out.APELLIDOS, mApellido);
            if (!TextUtils.isEmpty(mFechaNac))
                returnIntent.putExtra(Extras.Out.FECHA_NACIMIENTO, mFechaNac);
            if (mQRData == null && mPDF417Data == null)
                returnIntent.putExtra(Extras.Out.ENROLADO, true);
            if (!TextUtils.isEmpty(mFechaVencimiento))
                returnIntent.putExtra(Extras.Out.FECHA_VENCIMIENTO, mFechaVencimiento);
            if (result.containsKey(URL_TRASPASO))
                returnIntent.putExtra(URL_TRASPASO, result.getString(URL_TRASPASO));
            if (result.getByteArray("retrato") != null) {
                returnIntent.putExtra("retrato", result.getByteArray("retrato"));
            }
            if (result.getByteArray("firma") != null) {
                returnIntent.putExtra("firma", result.getByteArray("firma"));
            }

            if (mNacionalidad != null) {
                returnIntent.putExtra(Extras.Out.NACIONALIDAD, mNacionalidad);
            }
            if (mSexo != null) {
                returnIntent.putExtra(Extras.Out.SEXO, mSexo);
            }
            if (mQRData != null) {
                returnIntent.putExtra("CEDULA", "NUEVA");
            } else if (mPDF417Data != null) {
                returnIntent.putExtra("CEDULA", "ANTIGUA");
            }
            if (mOti) {
                returnIntent.putExtra("URL_OTI", result.getString("urlOti"));
                returnIntent.putExtra("identificacion_proveedor", result.getString("identificacionProveedor"));
                returnIntent.putExtra("fecha_verificacion", result.getString("fechaVerificacion"));
                returnIntent.putExtra("tipo_verificacion", result.getString("tipoVerificacion"));
            }
            if (mVigencia) {
                returnIntent.putExtra("estado_cedula", estadoCedula);
            }
            setResult(RESULT_OK, returnIntent);
            finish();

        }
    }

    public void showSnackBar(String texto) {
        Snackbar snackbar = Snackbar.make(_contentView, texto, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();

        snackbarView.setBackgroundColor(Color.RED);
        snackbar.show();
    }


    @Override
    public void onNFCAccessGranted() {
        mNfcReader = mNfcManager.getReader(this);
        startReadSmartCard();
    }

    @Override
    public void onNFCAccessDenied() {
        createAudit(new byte[0],
                ReturnCode.ACCESO_NFC_DENEGADO.getDescription(),
                ReturnCode.ACCESO_NFC_DENEGADO.getCode(), false);
        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.ACCESO_NFC_DENEGADO));
    }

    @Override
    public void onNFCAccessError() {
        createAudit(new byte[0],
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_NFC.getDescription(),
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_NFC.getCode(), false);
        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_NFC));
    }

    @Override
    public synchronized void onNewConnection(SmartCardConnection connection) {
        final boolean primeraLectura = mEidInfo == null;

        showDialog("Comunicación en curso...");

        try {

            if (!primeraLectura && mPendingCaptureFingerprint)
                dismissProgressMessage();
            dismissProgressMessageNFC();

            mEid = new EID(connection);
            mEid.selectMRTD();
            mEid.openSecureChannel(mQRData.getMrz());


            if (primeraLectura) {
                mEidInfo = mEid.getDocumentInfo();
                if (mMocExtendedInfo) {
                    mCountDownTimer.cancel();
                    extendedInfo = mEid.getExtendedDocumentInfo();
                }

            }

            mNewIdent = mEid.selectMOC();

            if (primeraLectura) {

                mEidInfo = mEidInfo.replace(EID.BIT1_VALUE, Integer.toString(ISO19794_2
                        .decodeMOCFingerPos(mEid.getBIT(1))));
                mEidInfo = mEidInfo.replace(EID.BIT2_VALUE, Integer.toString(ISO19794_2
                        .decodeMOCFingerPos(mEid.getBIT(2))));

                JSONObject jsonObject = new JSONObject(mEidInfo);

                // Validación de mRut
                String[] rutSplited = mQRData.getRun().split("-");
                if (esDistintoRutParametro(mRut, mDv, Integer.parseInt(rutSplited[0]), rutSplited[1].charAt(0))) {

                    StringBuilder str = new StringBuilder();
                    str.append("Rut ingresado: ");
                    str.append(mRut);
                    str.append("-");
                    str.append(mDv);
                    str.append(" Es distinto al Rut obtenido de la Cedula de Identidad: ");
                    str.append(rutSplited[0]);
                    str.append("-");
                    str.append(rutSplited[1].charAt(0));
                    errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(str.toString(), ReturnCode.VERIFICATION_RUT_CAPTURADO_NO_COINCIDE_CON_CEDULA));
                    return;
                }

                int scFinger1 = Integer.parseInt(jsonObject.getString("bit1"));
                int scFinger2 = Integer.parseInt(jsonObject.getString("bit2"));

                mFingerISOQR[0] = scFinger1;
                mFingerISOQR[1] = scFinger2;

                boolean[] dedos = ISO19794_2.traslateIsoToBooleanArray(scFinger1, scFinger2);

                mNombre = jsonObject.getString("nombres");
                mApellido = jsonObject.getString("apellidos");
                mFechaNac = jsonObject.getString("fch_nac");
                mFechaVencimiento = jsonObject.getString("fch_exp");
                mNacionalidad = jsonObject.getString("nacionalidad");
                mSexo = jsonObject.getString("sexo");

                if (extendedInfo != null) {
                    JSONObject objExtended = new JSONObject(extendedInfo);

                    mFirma = objExtended.getString("firma");
                    mRetrato = objExtended.getString("retrato");
                }

                mAvalaibleFinderprints = dedos;
                if (mNewIdent) {
                    Bitmap bRightH = setCombinedHandImageNFC(scFinger1);
                    Bitmap bLeftH = setCombinedHandImageNFC(scFinger2);

                    init(bRightH, bLeftH);
                } else {
                    setCombinedHandImage(_ivHands);
                }

                prepareCaptureFingerprint();

            } else {

                if (!mPendingCaptureFingerprint) {
                    try {
                        validateQRFinger();
                    } catch (Throwable throwable) {
                        HttpUtil.sendReportUncaughtException(throwable, this);
                        startReadSmartCard();
                    }
                }
            }
        } catch (final Throwable throwable) {
            HttpUtil.sendReportUncaughtException(throwable, this);
            startReadSmartCard();
        }
    }

    @Override
    public void onFingerprintAccessGranted() {
        mPreferences.setString(KeyPreferences.FIRST_START, "ready");
        mFingerprintReader = mFingerprintManager.getReader(this);
        Log.e("get info 3", "yes");
        getInfo();
    }

    @Override
    public void onFingerprintAccessDenied() {
        errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.ACCESO_HUELLERO_DENEGADO));
    }

    @Override
    public void onFingerprintAccessError() {
        errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO));
    }

    @Background
    void getInfo() {
        try {
            Log.e("asdf", "asdfasdf");
            showDialog("Cargando...");
            mFingerprintReader.getInfo(this);
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
        }
    }

    @Background
    void captureFingerprint() {
        try {
            mFingerprintReader.capture(this);
        } catch (Exception e) {
            HttpUtil.sendReportUncaughtException(e, mInstitution, this);

            if (e.toString().contains(NOT_WORKING_PROPERLY)) {
                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.ERROR_HUELLERO_DESCONECTADO));
            } else {
                errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
            }
        }
    }

    @Override
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void scanResult(FingerprintImage image) {
        validateFingerprint(image);
    }

    @Override
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void infoResult(FingerprintInfo info) {

        mSerialNumber = info.serialNumber;
        mReaderType = info.readerType;
        Log.e("READERTYPE", mReaderType);

        //save data fingerprint reader
        mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, mSerialNumber);
        mPreferences.setString("mReaderType", mReaderType);

        if (!mOffline && TextUtils.isEmpty(mBarcode))// Cuando es ONLINE sin cédula "Contra Base"
        {
            consultaHuellasDisponibles();

        } else if (mPDF417Data != null || mQRData != null) //Cuando es contra cédula
        {
            startFingerprintCapture();

        } else {
            errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.CODIGO_BARRAS_INVALIDO));
        }
    }

    /**
     * @param status
     * @param descripcion
     */
    //@UiThread(propagation = UiThread.Propagation.REUSE)
    void finishActivityWithError(String status, int resultCode, String descripcion, String mAudRechazo) {

        finishProcess();

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.RUT, mRut);
        returnIntent.putExtra(Extras.Out.DV, mDv);
        returnIntent.putExtra(Extras.Out.IDENTIDAD_VERIFICADA, false);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, descripcion);
        if (!trackID.isEmpty()) {
            returnIntent.putExtra(Extras.Out.TRACK_ID, trackID);
        }
        if (mAudRechazo != null) {
            returnIntent.putExtra(Extras.Out.CODIGO_AUDITORIA, mAudRechazo);
        }
        setResult(RESULT_OK, returnIntent);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishProcess();
        createAudit(new byte[0], ReturnCode.VERIFICACION_CANCELADA.getDescription(), ReturnCode.VERIFICACION_CANCELADA.getCode(), false);
        setResult(RESULT_CANCELED);
        finish();
    }

    public void onCancelPressed(View view) {

        finishProcess();
        createAudit(new byte[0], ReturnCode.VERIFICACION_CANCELADA.getDescription(), ReturnCode.VERIFICACION_CANCELADA.getCode(), false);
        setResult(RESULT_CANCELED);
        finish();

    }

    /*
     * Crear auditoria de rechazo
     * @param byte huella wsq
     * @param descripcion error
     */

    @Background
    void createAudit(byte[] baseImagen, String error, int codigoError, boolean reject) {

        String versionApp = null;
        JSONObject jsonObject = new JSONObject();
        try {
            versionApp = AppHelper.getCurrentVersion(this);

            jsonObject.put("type", error);
            jsonObject.put("rut", String.valueOf(mRut));
            jsonObject.put("dv", String.valueOf(mDv));
            jsonObject.put("versionApp", versionApp);
            jsonObject.put("operadorMovil", DeviceHelper.getOperator(this));
            jsonObject.put("ip", DeviceHelper.getIPAddress(true));
            jsonObject.put("androidId", DeviceHelper.getAndroidId(this));
            jsonObject.put("imei", DeviceHelper.getIMEI(this));
            if (!mProposito.equals("")) {
                jsonObject.put("proposito", mProposito);
            } else {
                jsonObject.put("proposito", "3");
            }

            if (reject) {
                jsonObject.put("operacion", AUDIT_TYPE_REJECT);
            } else {
                jsonObject.put("operacion", AUDIT_TYPE_ERROR);
            }
//            jsonObject.put("operacion", AUDIT_TYPE_ERROR);
            jsonObject.put("origen", AUDIT_SOURCE);
            if (trackID != null) {
                jsonObject.put("trackId", trackID);
            }
            if (mNewIdent && !respBIR.equals("") && reject) {
                jsonObject.put("intentosRestantes", Utils.returnCurrentAttempt(Integer.parseInt(respBIR)));
            }

            if (mSerialNumber == null) {
                try {
                    mSerialNumber = mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
                    mReaderType = mPreferences.getString("mReaderType");
                } catch (Exception e) {
                    HttpUtil.sendReportUncaughtException(e, mInstitution, this);
                }
            }

            byte[] bmp = new byte[0];
            if (mFingerprintImage != null) {
                bmp = Utils.bitmapToByteArray(mFingerprintImage.getBitmap());
            }

            //Obtención número de auditoria
            mAutentiaWSClient.genAudit(baseImagen,
                    "wsq",
                    String.format("%s-%s", mRut, mDv),
                    "10",
                    mReaderType,
                    mSerialNumber,
                    bmp,
                    String.valueOf(codigoError),
                    mNombre,
                    jsonObject.toString(),
                    mRunInstitution,
                    versionApp,
                    "sin datos",
                    "0",
                    "",
                    "",
                    mInstitution, mOti, new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
                            Log.d(TAG, "Create Audit succeed");
                            if (reject) {
                                GenAuditResp gar = new Gson().fromJson(result.getString("result"), GenAuditResp.class);
                                finishActivityWithError(Status.NO_OK, codigoError, error, gar.getNroAuditoria());
                            }

                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            Log.e(TAG, "Create Audit HERROR");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void finishProcess() {
        try {
            if (mFingerprintReader != null) {
                mFingerprintReader.cancelCapture();
                mFingerprintReader.close();
            }
        } catch (Exception e) {
        }
        if (mCountDownTimer != null) mCountDownTimer.cancel();
        if (mEid != null) mEid.close();
    }

    //Visualizar PDF
    protected void downloadPdf() {
        new ConectionStatus().execute(mUrlDocument.replaceAll("\\s+", ""));
    }

    /*
     * Validar url del documento
     */

    public boolean checkResponse(String myUrl) {
        URL url;
        boolean status = false;
        try {
            url = new URL(myUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            status = connection.getResponseCode() == 200;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public void onSuccess(String s, String s1) {
        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(s));
        remotePDFViewPager.setAdapter(adapter);
        if (!pdfError) {
            updateLayout();
        }
    }

    public void updateLayout() {
        _contentViewPDF = findViewById(R.id.ll_pdf);
        _contentViewPDF.removeAllViewsInLayout();
        _contentViewPDF.addView(remotePDFViewPager, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (mProgressWebView != null && mProgressWebView.isShowing()) {
            mProgressWebView.dismiss();
        }
        startProcess();
    }

    @Override
    public void onFailure(Exception e) {
        pdfError = true;
        HttpUtil.sendReportUncaughtException(e, mInstitution, this);
        finishActivityWithError("Error pdf", ReturnCode.ERROR_GENERICO.getCode(), "Url no valida", null);
        if (mProgressWebView != null) {
            mProgressWebView.dismiss();
        }
    }

    @Override
    public void onProgressUpdate(int i, int i1) {
        mProgressWebView.setMax(100);
        int currentProgress = (i * 100) / i1;
        if (currentProgress < 0) {
            if (currentProgress == 100) {
                return;
            } else {
                currentProgress = currentProgress + 100;
            }
        }
        mProgressWebView.setProgress(currentProgress);
    }

    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        Log.e("ameRC", "" + ame.returnCode.getCode());
        Log.e("INVALID_TOKENRC", "" + ReturnCode.INVALID_TOKEN.getCode());

        if (ame.returnCode.getCode() == ReturnCode.INVALID_TOKEN.getCode()) {
            Log.d(TAG, "borrando número de serie...");
            mPreferences.deleteKey(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
        }

        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            default:
                if (ame.returnCode.getCode() == 9000) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status, null);
                }
                if (ame.returnCode.getCode() == 5007) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status, null);
                } else {
                    Log.d("status.ERROR", Status.ERROR);
                    Log.d("getCode()", "" + ReturnCode.ERROR_GENERICO.getCode());
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status, null);
                }
                break;
        }
    }

    int errTerms = 0;

    private void initControlsStart() {

        Log.e("initcontrolstart", "1");

        mPreferences = new AutentiaPreferences(this);
        mAutentiaWSClient = new AutentiaWSClient(this);
        if (mPreferences.containsKey(KeyPreferences.TERMS_AND_CONDITIONS)) {
            if (!mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS).isEmpty()) {
                Log.e("initcontrolstart", "2");
                TerminosParam tp = new Gson().fromJson(mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS), TerminosParam.class);
                Log.e("initcontrolstart", tp.getTerminos());
                final String texto;
                if (mPrevired) texto = tp.getTerminos() + " " + getString(R.string.terms_previred);
                else texto = tp.getTerminos();
                if (texto.isEmpty())
                    errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
                final AlertDialog.Builder builder = new AlertDialog.Builder(VerificationActivity.this);
                @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.layout_terms_in, null);
                builder.setView(v);
                TextView tvTermsText = v.findViewById(R.id.tv_text_terms);
                Button btAceptar = v.findViewById(R.id.btn_acept_terms);
                Button btCancel = v.findViewById(R.id.btn_cancel_terms);
                tvTermsText.setText(texto);
                builder.setCancelable(false);
                builder.create();
                final AlertDialog show = builder.show();
                btAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        show.dismiss();
                        onAceptDisclamer(view);
                        mPreferences.setBoolean(KeyPreferences.FIRST_TRY, false);
                        initControls();
                    }
                });
                btCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCancelDisclamer(view);
                    }
                });

            } else {
                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
            }
        } else {
            errTerms++;
            showDialog("Obteniendo Términos y Condiciones");
            mAutentiaWSClient.getTyCforApplication(mPreferences.getString(KeyPreferences.INSTITUTION), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    dismissProgressMessage();
                    if (errTerms < 2)
                        initControlsStart();
                    else
                        errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
                }
            });
        }


    }

    private void initControls() {
        if (!mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS).equalsIgnoreCase("")) {
            TerminosParam tp = new Gson().fromJson(mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS), TerminosParam.class);
            final String texto = tp.getTerminos();
            if (texto.isEmpty())
                errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
            _btnTermsAndConditions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(VerificationActivity.this);
                    @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.layout_terms, null);
                    builder.setView(v);
                    TextView tvTermsText = v.findViewById(R.id.tv_text_terms);
                    Button btAceptar = v.findViewById(R.id.btn_acept_terms);
                    tvTermsText.setText(texto);
                    builder.create();
                    final AlertDialog show = builder.show();
                    btAceptar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            show.dismiss();
                        }
                    });
                }
            });
        } else {
            errorHandlerMethod(Status.ERROR, new AutentiaMovilException(ReturnCode.VERIFICATION_NO_EXISTEN_TERMINOS_Y_CONDICIONES));
        }
    }

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {

        interface In extends CommonInExtras {

            String RUT = "RUT";
            String DV = "DV";
            String OFFLINE_MODE = "OFFLINE_MODE";
            String BARCODE = "BARCODE";
            String INTENTOS = "INTENTOS";
            String TIMEOUT = "TIMEOUT";
            String SKIP_TERMS = "SKIP_TERMS"; //******* Revisar si corresponde o no este parámetro
            String PREVIRED = "PREVIRED";
            String URL_DOCUMENT = "URL_DOCUMENT";
            String HIDE_RUT = "HIDE_RUT";
            String COD_DOC = "COD_DOCUMENTO";
            String INSTITUCION_DEC = "INSTITUCION_DEC";
            String ORIENTACION = "ORIENTACION";
            String TRASPASO = "TRASPASO";
            String EXTENDED_MOC = "EXTENDED_MOC";
            String PROPOSITO = "PROPOSITO";
            String OTI = "OTI";
            //opcionales
            String ADICIONAL = "ADICIONAL";//(TEXTO3)
            String VIGENCIA = "VIGENCIA";
            //trackid
            String TRACK_ID = "trackId";
        }

        interface Out extends CommonOutExtras {

            String IDENTIDAD_VERIFICADA = "identidadVerificada";
            String RUT = "rut";
            String DV = "dv";
            String NUMERO_SERIE_HUELLERO = "serialNumber";
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String TIPO_LECTOR = "tipoLector";
            String TRANSACTION_ID = "idtx";
            String NOMBRE = "nombre";
            String APELLIDOS = "apellidos";
            String FECHA_NACIMIENTO = "fechaNac";
            String ENROLADO = "enrolado";
            String FECHA_VENCIMIENTO = "fechaVencimiento";
            String NACIONALIDAD = "nacionalidad";
            String SEXO = "sexo";
            String TRACK_ID = "trackId";
        }
    }

    private class ConectionStatus extends AsyncTask<Object, Object, Boolean> {

        @Override
        protected Boolean doInBackground(Object... voids) {
            try {
                return checkResponse(voids[0].toString());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean isConected) {

            if (isConected) {
                remotePDFViewPager = new RemotePDFViewPager(ctx, mUrlDocument.replaceAll("\\s+", ""), listener);
                remotePDFViewPager.setId(R.id.pdfViewPager);
            } else {
                finishActivityWithError("Error pdf", ReturnCode.ERROR_GENERICO.getCode(), "Url no valida", null);
                if (mProgressWebView != null) {
                    mProgressWebView.dismiss();
                }
            }
        }
    }

}
