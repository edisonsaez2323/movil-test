package cl.autentia.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;
import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.morpho.android.usb.USBManager;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.data.Pdf417Data;
import cl.autentia.data.QrData;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GetStatusNECResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;

/**
 *
 *
 * Activity no debe ser expuesta para todos los clientes, este
 *
 */
@EActivity
public class IdentityCardStatusActivity extends AppCompatActivity  implements FingerprintReader.InfoCallback{

    private static final String TAG = "EstadoCedula";

    private FingerprintReader mFingerprintReader;
    private FingerprintManager mFingerprintManager;
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    AlertDialog optionDialog;
    String mRut;
    String mNs;



    private AutentiaWSClient mAutentiaWSClient;
    private QrData mQRData;
    private Pdf417Data mPdf417Data;
//    private ProgressDialog mProgressDialog;

    @Extra(Extras.In.BARCODE)
    String mBarcode;
    private String mSerialNumber;
    private AutentiaPreferences mPreferences;
    UsbManager usbManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAutentiaWSClient = new AutentiaWSClient(this);
        mFingerprintManager = FingerprintManager.getInstance(this);
        mPreferences = new AutentiaPreferences(this);
//        mProgressDialog = new ProgressDialog(this);
//        mProgressDialog.setTitle("Obteniendo estado cédula...");
//        mProgressDialog.setMessage("Por favor, espere...");
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.show();
        showDialog("Obteniendo estado cédula...");
        if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager")) {
            usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            USBManager.getInstance().initialize(this,"com.morpho.morphosample.USB_ACTION");
            setPowerFP2(1);
        }
    }

    void setPowerFP2(int onOff) {
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (onOff == 1) {
            Log.i(TAG, "POWER ON");
            usbManager.setFingerPrinterPower(true);
        } else if (onOff == 0) {
            Log.i(TAG, "POWER OFF");
            usbManager.setFingerPrinterPower(false);
        }
    }

    @AfterExtras
    void afterExtras() {

        try {

            if (TextUtils.isEmpty(mBarcode)) {
                throw new Exception();
            }

            if (esQRCedula(mBarcode)) {
                mQRData = new QrData(mBarcode);
            } else {

                byte[] bb = Base64.decode(mBarcode, Base64.DEFAULT);

                if (bb == null) {
                    throw new UnsupportedEncodingException();
                }
                mPdf417Data = new Pdf417Data(bb);
            }

        } catch (Exception e) {
            finishActivity(Status.ERROR,
                    ReturnCode.CODIGO_BARRAS_INVALIDO.getCode(),
                    ReturnCode.CODIGO_BARRAS_INVALIDO.getDescription(),
                    "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {

            String rut = "";
            String serie = "";

            if (mQRData != null) {

                rut = mQRData.getRun();
                serie = mQRData.getSerial();

            } else {

                rut = String.format("%s-%s", mPdf417Data.rut, mPdf417Data.dv);
                serie = mPdf417Data.numeroSerie;
            }

            if (!DeviceHelper.hasConnection(this)) {
                throw new AutentiaMovilException(Status.ERROR,
                        ReturnCode.VERIFICATION_SIN_INTERNET);
            }

            mRut = rut;
            mNs = serie;
            prepareFingerprintReader();
//            getStatusNEC(rut, serie);


        } catch (AutentiaMovilException e) {
            finishActivity(e.status,
                    e.returnCode.getCode(),
                    e.returnCode.getDescription(),
                    "");

        } catch (Exception e) {
            finishActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()),
                    "");
        }
    }

    @Override
    protected void onDestroy() {

        Log.e("onDestroy","burn it down!");
        dismissProgressMessage();
        if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager"))setPowerFP2(0);
        if(mFingerprintReader != null) mFingerprintReader.close();
        if(mFingerprintManager != null) mFingerprintManager = null;
        super.onDestroy();

    }

    @Background
    void getStatusNEC(String rut, String serie) {
        try {
            mAutentiaWSClient.getStatusNEC(rut, serie,mSerialNumber,mPreferences.getString(KeyPreferences.INSTITUTION), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    try {
                    GetStatusNECResp ger = new Gson().fromJson(result.getString("result"),GetStatusNECResp.class);
                    String statusNEC = ger.getEstado();
                    if(statusNEC.equals("VIGENTE")){
                        finishActivity(Status.OK,
                                ReturnCode.EXITO.getCode(),
                                ReturnCode.EXITO.getDescription(),
                                statusNEC);
                    }else if(statusNEC.equals("NO VIGENTE")){
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_CEDULA_NO_VIGENTE.getCode(),
                                ReturnCode.ERROR_CEDULA_NO_VIGENTE.getDescription(),
                                statusNEC);
                    }else if(statusNEC.equals("BLOQUEO: DEFINITIVO")){
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_CEDULA_BLOQUEADA.getCode(),
                                ReturnCode.ERROR_CEDULA_BLOQUEADA.getDescription(),
                                statusNEC);
                    }else if (statusNEC.equals("BLOQUEO: TEMPORAL")){
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_CEDULA_BLOQUEADA_TEMPORAL.getCode(),
                                ReturnCode.ERROR_CEDULA_BLOQUEADA_TEMPORAL.getDescription(),
                                statusNEC);
                    }else if (statusNEC.equals("CONTINGENCIA")){
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_SERVICIO_EN_CONTINGENCIA.getCode(),
                                ReturnCode.ERROR_SERVICIO_EN_CONTINGENCIA.getDescription(),
                                statusNEC);
                    }else if(statusNEC.equals("DOCUMENTO VIGENTE")){
                        finishActivity(Status.OK,
                                ReturnCode.EXITO.getCode(),
                                ReturnCode.EXITO.getDescription(),
                                statusNEC);
                    }else {
                        Log.e("getGlosa",ger.getGlosa());
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_GENERICO.getCode(),
                                ger.getGlosa(),
                                statusNEC);
                    }

                    } catch (Exception e) {
                        finishActivity(Status.ERROR,
                                ReturnCode.ERROR_GENERICO.getCode(),
                                String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()),
                                "");
                    } finally {
                        dismissProgressMessage();
//                        mProgressDialog.dismiss();
                    }
                }
                @Override
                public void onResponseError(AutentiaMovilException result) {
                    if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201) {
                        errorHandlerMethod("", result);
                    }else errorHandlerMethod(Status.ERROR, result);

                }
            });
        } catch (Exception e) {
            Log.e("HERROR","ME DESANGRO");
            e.printStackTrace();
        }

    }

    private boolean esQRCedula(String barcode) {
        return !TextUtils.isEmpty(barcode) &&
                barcode.contains("http") &&
                barcode.contains("mrz");
    }

    private void finishActivity(String status, int resultCode, String description, String
            statusID) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, description);
        returnIntent.putExtra(Extras.Out.ESTADO_CEDULA, statusID);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void infoResult(FingerprintInfo info) {
        mSerialNumber = info.serialNumber;
        //save data fingerprint reader
        Log.e("infoResult","TRIGGERED");
        Log.e("mSerialNumber",mSerialNumber);
        mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, mSerialNumber);
        getStatusNEC(mRut, mNs);
    }

    //    @Background
    void getInfo() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    showDialog("Cargando...");
                    mFingerprintReader.getInfo(IdentityCardStatusActivity.this);
                } catch (Exception e) {
                    errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });
    }

    private void prepareFingerprintReader() {

        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    getInfo();
                } else {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                getInfo();
            }
        } catch (AutentiaMovilException e) {
            errorHandlerMethod(e.status, new AutentiaMovilException(e.returnCode.getDescription(), e.returnCode));
        } catch (Exception e) {
            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
        }
    }

    /**
     *
     * INTERFACES.
     *
     * */

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {

        interface In extends CommonInExtras {


            String RUT = "RUT";
            String DV = "DV";
            String OFFLINE_MODE = "OFFLINE_MODE";
            String BARCODE = "BARCODE";
            String INTENTOS = "INTENTOS";
            String TIMEOUT = "TIMEOUT";
            String SKIP_TERMS = "SKIP_TERMS"; //******* Revisar si corresponde o no este parámetro
            String PREVIRED = "PREVIRED";
            String URL_DOCUMENT = "URL_DOCUMENT";
            String HIDE_RUT = "HIDE_RUT";
            String COD_DOC = "COD_DOCUMENTO";
            String INSTITUCION_DEC = "INSTITUCION_DEC";
            String ORIENTACION = "ORIENTACION";
            String TRASPASO = "TRASPASO";
        }

        interface Out extends CommonOutExtras {

            String IDENTIDAD_VERIFICADA = "identidadVerificada";
            String ESTADO_CEDULA = "estado_cedula";
            String RUT = "rut";
            String DV = "dv";
            String NUMERO_SERIE_HUELLERO = "serialNumber";
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String TIPO_LECTOR = "tipoLector";
            String TRANSACTION_ID = "idtx";
            String NOMBRE = "nombre";
            String APELLIDOS = "apellidos";
            String FECHA_NACIMIENTO = "fechaNac";
            String ENROLADO = "enrolado";
            String FECHA_VENCIMIENTO = "fechaVencimiento";
        }
    }


    public void showDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                dismissProgressMessage();
                if (optionDialog != null)
                    if (optionDialog.isShowing())
                        optionDialog.dismiss();
                optionDialog = new AlertDialog.Builder(IdentityCardStatusActivity.this).create();

                View v = getLayoutInflater().inflate(R.layout.progress_layout, null);
                LottieAnimationView lottie = (LottieAnimationView) v.findViewById(R.id.lottie);
                TextView tv = v.findViewById(R.id.tv_message);
                tv.setText(message);
                try {
                    lottie.setAnimation(new JSONObject(getResources().getString(R.string.json_animation)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lottie.playAnimation();
                lottie.setScale(10);
                lottie.loop(true);

                optionDialog.setView(v);
                optionDialog.setCanceledOnTouchOutside(false);
                optionDialog.setCancelable(false);
                optionDialog.show();
            }
        });


    }


    void finishActivityWithError(String status, int resultCode, String descripcion) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, descripcion);
        setResult(RESULT_OK, returnIntent);
        finish();

    }


    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        dismissProgressMessage();

        Log.e("ameRC",""+ame.returnCode.getCode());
        Log.e("INVALID_TOKENRC",""+ReturnCode.INVALID_TOKEN.getCode());

        if(ame.returnCode.getCode() == ReturnCode.INVALID_TOKEN.getCode()){
            Log.d(TAG,"borrando número de serie...");
            mPreferences.deleteKey(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
        }

        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            default:
                if (ame.returnCode.getCode() == 9000){
                    Log.e("finishwitherrorif",ame.status);
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status);
                }else {
                    Log.e("finishwitherrorelse",ame.status);
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status);
                }
                break;
        }
    }

    protected void dismissProgressMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (optionDialog != null && optionDialog.isShowing())
                    optionDialog.dismiss();
                Log.v(TAG, "dialog dismissed");
            }
        });

    }


}