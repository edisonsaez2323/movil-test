package cl.autentia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.preferences.AutentiaPreferences;

@EActivity
public class InstitutionPropertiesActivity extends AppCompatActivity {

    private static final String TAG = "Get_Properties";

    private AutentiaPreferences mPreferences;

    interface Extras {

        interface In {
            String SENDER = "SENDER";
        }

        interface Out extends CommonOutExtras {
            String PROPERTIES = "PROPERTIES";
        }
    }

    @Extra(Extras.In.SENDER)
    String sender = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences = new AutentiaPreferences(this);

        String propertiesInstitution = mPreferences.getString(sender);

        Log.d(TAG, "Properties: " + propertiesInstitution);

        boolean hasDataInstitution = propertiesInstitution != null;

        finishActivity(Status.OK,
                hasDataInstitution ? ReturnCode.EXITO.getCode() : ReturnCode.INSTITUTION_PROP_SIN_PROPERTIES.getCode(),
                hasDataInstitution ? ReturnCode.EXITO.getDescription() : sender ,//ReturnCode.INSTITUTION_PROP_SIN_PROPERTIES.getDescription(),
                propertiesInstitution);

    }

    @AfterExtras
    void afterExtras() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {

            sender = this.getReferrer().getHost();

        } else {

            if (TextUtils.isEmpty(sender)) {
                finishActivity(Status.ERROR,
                        ReturnCode.INSTITUTION_PROP_FALTA_SENDER.getCode(),
                        ReturnCode.INSTITUTION_PROP_FALTA_SENDER.getDescription(),
                        null);
                return;
            }
        }
    }

    /**
     * @param status
     * @param resultCode
     * @param description
     * @param properties
     */
    private void finishActivity(String status, int resultCode, String description, String properties) {
        Intent intent = new Intent();
        intent.putExtra(Extras.Out.ESTADO, status);
        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        intent.putExtra(Extras.Out.DESCRIPCION, description);
        intent.putExtra(Extras.Out.PROPERTIES, properties);
        setResult(RESULT_OK, intent);
        finish();
    }
}
