package cl.autentia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;

public class ModifyConfigActivity extends AppCompatActivity {

    private AutentiaPreferences mPreferences;
    String _baseUrl;
    String _ambiente;
    String _institucion;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_modify_configuration);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Autentia Movil");
        mPreferences = new AutentiaPreferences(this);

        _baseUrl = getIntent().getStringExtra("baseUrl");
        _institucion = getIntent().getStringExtra("institucion");
        _ambiente = getIntent().getStringExtra("ambiente");

        try {
            mPreferences.setString(KeyPreferences.DATA_CONFIGURATION,"00003");
            mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER,null);
            mPreferences.setString(KeyPreferences.URL_BASE,_baseUrl);
            mPreferences.setString(KeyPreferences.INSTITUTION,_institucion);
            mPreferences.setString(KeyPreferences.ENVIROMENT,_ambiente);
            mPreferences.setString(KeyPreferences.HASH_CONFIGURATION, createHash());
            //borramos toda la informacion que podríamos terner guardada en la app
            Auditoria_.deleteAllAudit();
            Evidence_.deleteAllEvidences();
            Intent intent = new Intent(ModifyConfigActivity.this, NavegationMenuActivity_.class);
            intent.putExtra("mKill","true");
            startActivity(intent);
            finish();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private String createHash() throws UnsupportedEncodingException, NoSuchAlgorithmException {

        return Utils.SHA1(String.format("%s/%s/1760", _institucion,_ambiente));
    }

}
