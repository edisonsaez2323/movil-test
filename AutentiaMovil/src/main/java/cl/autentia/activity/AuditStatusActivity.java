package cl.autentia.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.acepta.android.fetdroid.R;
import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.json.JSONException;
import org.json.JSONObject;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GetInfoAuditResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;


@EActivity
public class AuditStatusActivity extends AppCompatActivity{

    private static final String TAG = "AuditStatusActivity";

    private static final String SUCCESS_RESULT = "0000";
    private FingerprintReader mFingerprintReader;
    private FingerprintManager mFingerprintManager;
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    private String mSerialNumber;
    private AutentiaPreferences mPreferences;
    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    AlertDialog optionDialog;

    private AutentiaWSClient mAutentiaWSClient;

    @Extra(Extras.In.AUDITORIA)
    String mAudit = "";
    @Extra(Extras.In.BACKGROUND)
    boolean mBackground = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mAutentiaWSClient = new AutentiaWSClient(this);
        mFingerprintManager = FingerprintManager.getInstance(this);
        mPreferences = new AutentiaPreferences(this);
        if (!mBackground)
            showDialog("Consultando auditoria...");

    }

    @AfterExtras
    void afterExtras() {

        try {

            if (TextUtils.isEmpty(mAudit)) {
                throw new Exception();
            }

            if (!DeviceHelper.hasConnection(this)) {
                throw new AutentiaMovilException(Status.ERROR,
                        ReturnCode.VERIFICATION_SIN_INTERNET);
            }

        } catch (AutentiaMovilException e) {
            finishActivity(e.status,
                    e.returnCode.getCode(),
                    e.returnCode.getDescription()
                    , "");
        } catch (Exception e) {
            finishActivity(Status.ERROR,
                    ReturnCode.PARAMETROS_INVALIDOS.getCode(),
                    ReturnCode.PARAMETROS_INVALIDOS.getDescription(),
                    "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
//            prepareFingerprintReader();
            getAudit(mAudit);

        } catch (Exception e) {
            finishActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()),
                    "");
        }
    }

    @Background
    void getAudit(String audit) {
        try {

            mAutentiaWSClient.getStatusAudit(audit,mPreferences.getString(KeyPreferences.INSTITUTION),mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {

                    GetInfoAuditResp gir = new Gson().fromJson(result.getString("result"),GetInfoAuditResp.class);

                    String statusAudit = gir.getResultado();

                    if (gir.getStatus() == 5000 || gir.getStatus() == 5007){
                        statusAudit = Extras.Return.NO_REGISTRADA;
                    }else if (statusAudit.equals(SUCCESS_RESULT)) {
                        statusAudit = Extras.Return.REGISTRADA;
                    }

                    finishActivity(Status.OK,
                            ReturnCode.EXITO.getCode(),
                            ReturnCode.EXITO.getDescription(),
                            statusAudit);
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    if (result.returnCode.getCode() == 9000 || result.returnCode.getCode() == 201){
                        errorHandlerMethod("", result);
                    }else errorHandlerMethod(Status.ERROR, result);
                }
            });

        } catch (Exception e) {
            finishActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()),
                    "");
        } finally {
            dismissProgressMessage();
        }
    }

    private void finishActivity(String status, int resultCode, String description,
                                String statusAudit) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, description);
        returnIntent.putExtra(Extras.Out.ESTADO_AUDITORIA, statusAudit);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

//    @Override
//    public void infoResult(FingerprintInfo info) {
//        mSerialNumber = info.serialNumber;
//        mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, mSerialNumber);
//        getAudit(mAudit);
//
//    }

//    void getInfo() {
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    if (!mBackground){
//                    showDialog("Cargando...");
//                    }
//                    mFingerprintReader.getInfo(AuditStatusActivity.this);
//                } catch (Exception e) {
//                    errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
//                }
//            }
//        });
//    }


//    private void prepareFingerprintReader() {
//
//        try {
//
//            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
//                if (!mFingerprintManager.isUsbPresent()) {
//                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
//                }
//                if (mFingerprintManager.isUsbAccessible()) {
//                    mFingerprintCurrentState = InternalState.READY;
//                    mFingerprintReader = mFingerprintManager.getReader(this);
//                    getInfo();
//                } else {
//                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
//                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
//                }
//            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
//                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
//                // permiso llegara mediante onActivityResult
//            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
//                // tengo impresion que nunca deberiamos llegar aqui
//                mFingerprintReader = mFingerprintManager.getReader(this);
//                getInfo();
//            }
//        } catch (AutentiaMovilException e) {
//            errorHandlerMethod(e.status, new AutentiaMovilException(e.returnCode.getDescription(), e.returnCode));
//        } catch (Exception e) {
//            errorHandlerMethod("", new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
//        }
//    }

    /**
     *
     * HELPING METHODS
     *
     * */

    public void showDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (optionDialog != null)
                    if (optionDialog.isShowing())
                        optionDialog.dismiss();
                optionDialog = new AlertDialog.Builder(AuditStatusActivity.this).create();

                View v = getLayoutInflater().inflate(R.layout.progress_layout, null);
                LottieAnimationView lottie = (LottieAnimationView) v.findViewById(R.id.lottie);
                TextView tv = v.findViewById(R.id.tv_message);
                tv.setText(message);
                try {
                    lottie.setAnimation(new JSONObject(getResources().getString(R.string.json_animation)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lottie.playAnimation();
                lottie.setScale(10);
                lottie.loop(true);

                optionDialog.setView(v);
                optionDialog.setCanceledOnTouchOutside(false);
                optionDialog.setCancelable(false);
                optionDialog.show();
            }
        });


    }


    void finishActivityWithError(String status, int resultCode, String descripcion) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(IdentityCardStatusActivity.Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(IdentityCardStatusActivity.Extras.Out.ESTADO, status);
        returnIntent.putExtra(IdentityCardStatusActivity.Extras.Out.DESCRIPCION, descripcion);
        setResult(RESULT_OK, returnIntent);
        finish();

    }


    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        if (!mBackground){
            dismissProgressMessage();
        }
            Log.e("ameRC",""+ame.returnCode.getCode());
            Log.e("INVALID_TOKENRC",""+ReturnCode.INVALID_TOKEN.getCode());

        if(ame.returnCode.getCode() == ReturnCode.INVALID_TOKEN.getCode()){
            Log.d(TAG,"borrando número de serie...");
            mPreferences.deleteKey(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
        }


        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription());
                break;
            default:
                if (ame.returnCode.getCode() == 9000){
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status);
                }else {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status);
                }
                break;
        }
    }

    protected void dismissProgressMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (optionDialog != null && optionDialog.isShowing())
                    optionDialog.dismiss();
                Log.v(TAG, "dialog dismissed");
            }
        });

    }

    /**
     *
     * INTERFACES.
     *
     * */

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {

        interface In extends CommonInExtras {

            String AUDITORIA = "AUDITORIA";
            String BARCODE = "BARCODE";

            String BACKGROUND = "BACKGROUND";
        }

        interface Out extends CommonOutExtras {

            String ESTADO_AUDITORIA = "ESTADO_AUDITORIA";

        }
        interface Return {
            String REGISTRADA = "REGISTRADA";
            String NO_REGISTRADA = "NO_REGISTRADA";
        }
    }

}