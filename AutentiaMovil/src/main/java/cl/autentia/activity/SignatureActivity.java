package cl.autentia.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.acepta.wsq.ImageUtil;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.beyka.tiffbitmapfactory.CompressionScheme;
import org.beyka.tiffbitmapfactory.Orientation;
import org.beyka.tiffbitmapfactory.TiffSaver;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GenAuditResp;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

@EActivity //(R.layout.activity_signature)
public class SignatureActivity extends AppCompatActivity {

    private static final String DEFAULT_ORIENTATION = "VERTICAL";
    @ViewById(R.id.signature_pad)
    SignaturePad _signaturePad;
    @ViewById(R.id.reintentar_button)
    Button _reintentarButton;
    @ViewById(R.id.finalizar_button)
    Button _finalizarButton;
    @ViewById(R.id.txt_permisos)
    TextView _txtTerms;
    @ViewById(R.id.toolbar)
    Toolbar _toolbar;
    @Extra(Extras.In.ICON)
    byte[] mIcon = new byte[0];
    @Extra(Extras.In.COLOR_PRIMARY)
    String mColorPrimary = "";
    @Extra(Extras.In.TERMS)
    String mTerms = "";
    @Extra(Extras.In.ORIENTATION)
    String mOrientarion = DEFAULT_ORIENTATION;
    File file_ = null;
    private File cacheFolder;

    @Extra(VerificationActivity.Extras.In.RUT)
    int mRut = 0;
    @Extra(VerificationActivity.Extras.In.DV)
    char mDv = 0;

    private AutentiaWSClient mAutentiaWSClient;
    private AutentiaPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (mOrientarion.equals(DEFAULT_ORIENTATION)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_signature);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_signature_land);
        }

        _finalizarButton.setEnabled(false);
        _reintentarButton.setEnabled(false);

        cacheFolder = new File(Environment.getExternalStorageDirectory(), "imagenFirma");
        if (!cacheFolder.exists())
            cacheFolder.mkdir();

        mAutentiaWSClient = new AutentiaWSClient(this);
        mPreferences = new AutentiaPreferences(this);

    }

    private void cleanFolder(File cacheFolder) {
        for (File aFile : cacheFolder.listFiles()) {
            if (aFile.isFile())
                aFile.delete();
        }
    }

    @AfterExtras
    void afterExtras() {
        mColorPrimary = TextUtils.isEmpty(mColorPrimary) ? getString(R.color.colorPrimary) : mColorPrimary;
    }

    @AfterViews
    void afterViews() {

        if (mIcon.length > 0) {

            Bitmap bmp = BitmapFactory.decodeByteArray(mIcon, 0, mIcon.length);

            Drawable drawable = new BitmapDrawable(getResources(), bmp);
        }

        _txtTerms.setText(mTerms);
        //_txtTerms.setText(getResources().getString(R.string.texto_signer));
        _toolbar.setTitle("Firma Holográfica");
        _toolbar.setVisibility(View.VISIBLE);
        _signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener()


        {
            @Override
            public void onStartSigning() {
                Log.e("onStartSigning","HERE");
            }

            @Override
            public void onSigned() {
                Log.e("onSigned","HERE");
                _finalizarButton.setEnabled(true);
                _reintentarButton.setEnabled(true);
                //_reintentarButton.setBackgroundColor(Color.parseColor(mColorPrimary));
                //_finalizarButton.setBackgroundColor(Color.parseColor(mColorPrimary));
            }

            @Override
            public void onClear() {
                Log.e("onClear","HERE");
                _finalizarButton.setEnabled(false);
                _reintentarButton.setEnabled(false);
                //_reintentarButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                //_finalizarButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
            }

        });

        _reintentarButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                _signaturePad.clear();
            }
        });
        _finalizarButton.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View view) {
                sendResponseSignature();
            }
        });

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }


    private void sendResponseSignature() {
//        byte[] signatureSVG = _signaturePad.getSignatureSvg().getBytes();
        byte[] signaturePNG = new byte[0];

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        _signaturePad.getSignatureBitmap().compress(Bitmap.CompressFormat.JPEG, 1, stream);
        _signaturePad.setMaxWidth(183);
//        signaturePNG = stream.toByteArray();

        TiffSaver.SaveOptions options = new TiffSaver.SaveOptions();
        options.compressionScheme = CompressionScheme.CCITTFAX4;
        options.orientation = Orientation.UNAVAILABLE;
        options.author = "Autentia S.A.";
        options.copyright = "Autentia Movil";
        cleanFolder(cacheFolder);
        String name = String.valueOf(System.currentTimeMillis()) + ".tif";
        boolean saved = TiffSaver.appendBitmap(
                cacheFolder + "/" + name,
                scaleAndDraw(_signaturePad.getSignatureBitmap()),
                options);
        if (saved) {
            file_ = new File(cacheFolder + "/" + name);
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("operacion", "FIRMA");
            obj.put("rut", String.valueOf(mRut));
            obj.put("dv", String.valueOf(mDv));
            obj.put("proposito", "3");
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.e("json",obj.toString());

        byte [] firmaBmp = Utils.bitmapToByteArray(_signaturePad.getSignatureBitmap());

        //Obtención número de auditoria
        try {
            mAutentiaWSClient.genAudit(new byte[0],
                    "bmp",
                    String.format("%s-%s", mRut, mDv),
                    "10",
                    "Eikon",
                    mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER),
                    firmaBmp,
                    String.valueOf("0"),
                    mPreferences.getString(KeyPreferences.INSTITUTION),
                    obj.toString(),
                    mPreferences.getString(KeyPreferences.RUN_INSTITUTION),
                    "",
                    "sin datos",
                    "0",
                    "",
                    "",
                    mPreferences.getString(KeyPreferences.INSTITUTION), false,new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
                            Log.d("SignatureActivity", "Create Audit succeed");
                            GenAuditResp gar = new Gson().fromJson(result.getString("result"),GenAuditResp.class);
                            Log.e("result",gar.getNroAuditoria());
                            Intent intent = new Intent();
                            //        intent.putExtra(Extras.Out.SIGNATURE_SVG, signatureSVG);
                            intent.putExtra(Extras.Out.SIGNATURE_PNG, signaturePNGresized(_signaturePad.getSignatureBitmap()));
                            intent.putExtra(Extras.Out.SIGNATURE_TIF, Base64.encodeToString(fileToBytes(file_.getAbsolutePath()), Base64.DEFAULT).replace("\n", ""));
                            intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                            intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                            intent.putExtra("numeroAuditoria", gar.getNroAuditoria());
                            intent.putExtra(Extras.Out.ESTADO, Status.OK);
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            Log.e("SignatureActivity", "Create Audit HERROR");
                            Log.e("SignatureActivity", result.status);
                            Log.e("SignatureActivity", result.returnCode.getDescription());
                            Log.e("SignatureActivity", result.returnCode.getCode()+"");
                            Intent intent = new Intent();
                            //        intent.putExtra(Extras.Out.SIGNATURE_SVG, signatureSVG);
                            intent.putExtra(Extras.Out.SIGNATURE_PNG, signaturePNGresized(_signaturePad.getSignatureBitmap()));
                            intent.putExtra(Extras.Out.SIGNATURE_TIF, Base64.encodeToString(fileToBytes(file_.getAbsolutePath()), Base64.DEFAULT).replace("\n", ""));
                            intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                            intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                            intent.putExtra(Extras.Out.ESTADO, Status.OK);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


//        Intent intent = new Intent();
////        intent.putExtra(Extras.Out.SIGNATURE_SVG, signatureSVG);
//        intent.putExtra(Extras.Out.SIGNATURE_PNG, signaturePNGresized(_signaturePad.getSignatureBitmap()));
//        intent.putExtra(Extras.Out.SIGNATURE_TIF, Base64.encodeToString(fileToBytes(file_.getAbsolutePath()), Base64.DEFAULT).replace("\n", ""));
//        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
//        intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
//        intent.putExtra(Extras.Out.ESTADO, Status.OK);
//        setResult(RESULT_OK, intent);

    }

    public String signaturePNGresized(Bitmap bitmap_) {
        Bitmap bitmap = Bitmap.createScaledBitmap(bitmap_, 256, 180, true);
        return Base64.encodeToString(ImageUtil.BitmapToPNG(bitmap), Base64.DEFAULT).replace("\n","");
    }

    protected Bitmap scaleAndDraw(Bitmap srcBitmap) {
        int borderWidth = 15;
        int borderColor = Color.WHITE;

        Bitmap dstBitmap = Bitmap.createBitmap(
                srcBitmap.getWidth() + borderWidth * 2,
                srcBitmap.getHeight() + borderWidth * 2,
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(dstBitmap);
//        canvas.rotate(90);

        Paint paint = new Paint();
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);

        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvas.getWidth() - borderWidth / 2,
                canvas.getHeight() - borderWidth / 2);

        canvas.drawRect(rect, paint);
        canvas.drawBitmap(srcBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();

        return Bitmap.createScaledBitmap(dstBitmap, 193, 94, true);
    }

    public byte[] fileToBytes(String path) {
        byte[] getBytes = {};
        try {
            File file = new File(path);
            getBytes = new byte[(int) file.length()];
            InputStream is = new FileInputStream(file);
            is.read(getBytes);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getBytes;
    }

    interface Extras {

        interface In extends CommonInExtras {
            String TERMS = "TERMS";
            String ORIENTATION = "ORIENTATION";
        }

        interface Out extends CommonOutExtras {
            String SIGNATURE_SVG = "SIGNATURE_SVG";
            String SIGNATURE_PNG = "SIGNATURE_PNG";
            String SIGNATURE_TIF = "SIGNATURE_TIF";

        }
    }

    public void errorHandlerMethod(String status, AutentiaMovilException ame) {

        Log.e("ameRC", "" + ame.returnCode.getCode());
        Log.e("INVALID_TOKENRC", "" + ReturnCode.INVALID_TOKEN.getCode());

        if (ame.returnCode.getCode() == ReturnCode.INVALID_TOKEN.getCode()) {
//            Log.d(TAG, "borrando número de serie...");
            mPreferences.deleteKey(KeyPreferences.FINGERPRINT_SERIAL_NUMBER);
        }

        switch (status) {
            case Status.OK: //CHECK
                finishActivityWithError(Status.OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.NO_OK: //CHECK
                finishActivityWithError(Status.NO_OK,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.ERROR: //CHECK
                finishActivityWithError(Status.ERROR,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.PENDIENTE: //CHECK
                finishActivityWithError(Status.PENDIENTE,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            case Status.CANCELADO: //CHECK
                finishActivityWithError(Status.CANCELADO,
                        ame.returnCode.getCode(),
                        ame.returnCode.getDescription(), null);
                break;
            default:
                if (ame.returnCode.getCode() == 9000) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status, null);
                }
                if (ame.returnCode.getCode() == 5007) {
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.INVALID_TOKEN.getCode(),
                            ame.status, null);
                } else {
                    Log.d("status.ERROR", Status.ERROR);
                    Log.d("getCode()", "" + ReturnCode.ERROR_GENERICO.getCode());
                    finishActivityWithError(Status.ERROR,
                            ReturnCode.ERROR_GENERICO.getCode(),
                            ame.status, null);
                }
                break;
        }
    }

    void finishActivityWithError(String status, int resultCode, String descripcion, String mAudRechazo) {

//        finishProcess();

        Intent returnIntent = new Intent();
//        returnIntent.putExtra(VerificationActivity.Extras.Out.RUT, mRut);
//        returnIntent.putExtra(VerificationActivity.Extras.Out.DV, mDv);
//        returnIntent.putExtra(VerificationActivity.Extras.Out.IDENTIDAD_VERIFICADA, false);
        returnIntent.putExtra(VerificationActivity.Extras.Out.CODIGO_RESPUESTA, resultCode);
        returnIntent.putExtra(VerificationActivity.Extras.Out.ESTADO, status);
        returnIntent.putExtra(VerificationActivity.Extras.Out.DESCRIPCION, descripcion);
//        if (mAudRechazo != null) {
//            returnIntent.putExtra(VerificationActivity.Extras.Out.CODIGO_AUDITORIA, mAudRechazo);
//        }
        setResult(RESULT_OK, returnIntent);
        finish();
    }

}