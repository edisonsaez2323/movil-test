package cl.autentia.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.helper.DeviceHelper;
import cl.autentia.helper.LocationHelper;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.http.parameters.GenAuditResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.MyServiceSyncAdapter;
import cl.autentia.view.VerticalStepperFormLayout;
import ernestoyaquello.com.verticalstepperform.interfaces.VerticalStepperForm;

import static cl.autentia.http.HttpUtil.sendReportUncaughtException;
import static com.acepta.Utils.getTrackID;

/**
 * Created by Edison Saez on 04-04-2017.
 */

public class SelectorEvidenceActivity extends AppCompatActivity implements VerticalStepperForm {

    private static final String TAG = "SelectorEvidence";

    private static final String IZQUIERDA = "Izquierda";
    private static final String DERECHA = "Derecha";
    private static final String AUDIT_SOURCE = "ANDROID";
    private static final String AUDIT_TYPE = "EVIDENCIA";
    private static final int REQUEST_EVIDENCE_CODE = 0x0233;

    private VerticalStepperFormLayout verticalStepperForm;
    private AutentiaPreferences mAutentiaPreferences;
    private LocationHelper mLocationHelper;
    private ImageButton mano_im, mano_ia, mano_ime, mano_ii, mano_ip, mano_i, mano_d,
            mano_dm, mano_da, mano_dme, mano_di, mano_dp;
    private LinearLayout manosLayout, dedosLayout, finishLayout;

    private String mano_seleccionada = "";
    private HashMap<Integer, byte[]> mapa_evidencia;
    private List<byte[]> listBitmap;
    private int idFinger, fingerIso;
    private int cantidadEvidencia;
    private int rut;
    private char dv;
    private AutentiaWSClient mAutentiaWSClient;
    public String numeroAuditoria = "";

    private String trackID = "";

//    private AutentiaPreferences mPreferences;

    interface Extras {

        interface In extends CommonOutExtras {
            String CANTIDAD_EVIDENCIAS = "CANTIDAD_EVIDENCIAS";
            String RUT = "RUT";
            String DV = "DV";
            String TRACK_ID = "trackId";
        }

        interface Out extends CommonOutExtras {
            String CODIGO_AUDITORIA = "codigoAuditoria";
            String TRACK_ID = "trackId";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selector_evidence);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_evidencia);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Toma de evidencia");
            toolbar.setNavigationIcon(R.mipmap.ic_autentia_white);
        }

        try {
            mAutentiaPreferences = new AutentiaPreferences(this);
            mLocationHelper = new LocationHelper(this);
            mAutentiaWSClient = new AutentiaWSClient(this);

            if (!Utils.isConnectingToInternet(this)){
                int auditSize = Auditoria_.getAuditCount();
                if (auditSize < 1) {

                    finishActivity(Status.ERROR,
                            ReturnCode.SIN_AUDITORIAS_RESERVADAS.getCode(),
                            ReturnCode.SIN_AUDITORIAS_RESERVADAS.getDescription());

                }
            }

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            sendReportUncaughtException(e, this);
            finishActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cantidadEvidencia = extras.getInt(Extras.In.CANTIDAD_EVIDENCIAS, 1);
            rut = extras.getInt(Extras.In.RUT, 0);
            dv = extras.getChar(Extras.In.DV, Character.MIN_VALUE);

        } else {
            cantidadEvidencia = 1;
        }

        try {
            if (extras.containsKey(Extras.In.TRACK_ID)){
                trackID = extras.getString(Extras.In.TRACK_ID);
                Log.e("trackID", trackID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        if (rut == 0 || dv == Character.MIN_VALUE) {

            finishActivity(Status.ERROR,
                    ReturnCode.RUT_INVALIDO.getCode(),
                    ReturnCode.RUT_INVALIDO.getDescription());
        }

        mapa_evidencia = new HashMap<>();
        listBitmap = new ArrayList<>();

        String[] mySteps = {"Seleccionar mano", "Seleccionar dedo"};
        int colorPrimary = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
        int colorPrimaryDark = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark);

        verticalStepperForm = findViewById(R.id.vertical_stepper_form);

//        mPreferences = new AutentiaPreferences(this);
//        boolean pendingLogin = false;
//        boolean loggedIn = false;
//
//        Log.e("pending","enter");
//        try {
//            mPreferences = new AutentiaPreferences(this);
//            if (mPreferences.containsKey(KeyPreferences.PENDING_LOGIN)){
//                pendingLogin = mPreferences.getBoolean(KeyPreferences.PENDING_LOGIN);
//                loggedIn = mPreferences.getBoolean(KeyPreferences.LOGGED_IN);
//                Log.e("pendig ver",String.valueOf(pendingLogin));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        if (pendingLogin && !loggedIn){
//            Log.e("pedir","rut - selector");
//            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(this).create();
//            alertDialog.setIcon(R.drawable.autentia_logo_blue);
//            alertDialog.setTitle("Verificacion Operador");
//            alertDialog.setMessage("Debe verificarse como operador antes de operar");
//            alertDialog.setCancelable(false);
//            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "Aceptar",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent(SelectorEvidenceActivity.this,NavegationMenuActivity_.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
//                        }
//                    });
//            alertDialog.show();
//        }else {

        // Incializacion formulario
        VerticalStepperFormLayout.Builder.newInstance(verticalStepperForm, mySteps, this, this)
                .primaryColor(colorPrimary)
                .primaryDarkColor(colorPrimaryDark)
                .displayBottomNavigation(false)
                .init();
//        }
    }

    /**
     * @param status
     * @param resultCode
     * @param description
     */
    private void finishActivity(String status, int resultCode, String description) {
        Intent intent = new Intent();
        intent.putExtra(Extras.Out.ESTADO, status);
        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, resultCode);
        intent.putExtra(Extras.Out.DESCRIPCION, description);
        intent.putExtra(Extras.Out.TRACK_ID, trackID);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public View createStepContentView(int stepNumber) {
        //Se instancian las vistas de cada paso
        View view = null;
        switch (stepNumber) {
            case 0:
                view = seleccionarManos();
                break;
            case 1:
                view = seleccionarDedos();
                break;
            case 2:
                view = finishProcessLayout();
                break;
        }
        return view;
    }

    public void callCaptureEvidence(int dedoIso) {

        Intent intent = new Intent(getApplicationContext(), CaptureEvidenceActivity_.class);
        intent.putExtra(CaptureEvidenceActivity.Extras.In.DEDO, dedoIso);
        startActivityForResult(intent, REQUEST_EVIDENCE_CODE);
    }

    private View seleccionarManos() {
        LayoutInflater inflater = LayoutInflater.from(getBaseContext());
        manosLayout = (LinearLayout) inflater.inflate(R.layout.view_selector_hands, null, false);
        return manosLayout;
    }

    private View.OnClickListener onClickFingerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            idFinger = (int) view.getTag();

            switch (idFinger) {

                case 1:
                    fingerIso = 10;
                    mano_ip.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_pulgar));
                    mano_ii.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_indice));
                    mano_ime.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_medio));
                    mano_ia.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_anular));
                    mano_im.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_menique_selected));
                    callCaptureEvidence(fingerIso);
                    break;
                case 2:
                    fingerIso = 9;
                    mano_ip.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_pulgar));
                    mano_ii.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_indice));
                    mano_ime.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_medio));
                    mano_ia.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_anular_selected));
                    mano_im.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 3:
                    fingerIso = 8;
                    mano_ip.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_pulgar));
                    mano_ii.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_indice));
                    mano_ime.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_medio_selected));
                    mano_ia.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_anular));
                    mano_im.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 4:
                    fingerIso = 7;
                    mano_ip.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_pulgar));
                    mano_ii.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_indice_selected));
                    mano_ime.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_medio));
                    mano_ia.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_anular));
                    mano_im.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 5:
                    fingerIso = 6;
                    mano_ip.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_pulgar_selected));
                    mano_ii.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_indice));
                    mano_ime.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_medio));
                    mano_ia.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_anular));
                    mano_im.setImageDrawable(getResources().getDrawable(R.drawable.mano_izquierda_menique));
                    callCaptureEvidence(fingerIso);
                    break;

                case 6:
                    fingerIso = 1;
                    mano_dp.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_pulgar_selected));
                    mano_di.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_indice));
                    mano_dme.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_medio));
                    mano_da.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_anular));
                    mano_dm.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 7:
                    fingerIso = 2;
                    mano_dp.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_pulgar));
                    mano_di.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_indice_selected));
                    mano_dme.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_medio));
                    mano_da.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_anular));
                    mano_dm.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 8:
                    fingerIso = 3;
                    mano_dp.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_pulgar));
                    mano_di.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_indice));
                    mano_dme.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_medio_selected));
                    mano_da.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_anular));
                    mano_dm.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 9:
                    fingerIso = 4;
                    mano_dp.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_pulgar));
                    mano_di.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_indice));
                    mano_dme.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_medio));
                    mano_da.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_anular_selected));
                    mano_dm.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_menique));
                    callCaptureEvidence(fingerIso);
                    break;
                case 10:
                    fingerIso = 5;
                    mano_dp.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_pulgar));
                    mano_di.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_indice));
                    mano_dme.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_medio));
                    mano_da.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_anular));
                    mano_dm.setImageDrawable(getResources().getDrawable(R.drawable.mano_derecha_menique_selected));
                    callCaptureEvidence(fingerIso);
                    break;
            }
        }
    };


    private View seleccionarDedos() {
        // In this case we generate the view by inflating a XML file
        LayoutInflater inflater = LayoutInflater.from(getBaseContext());
        dedosLayout = (LinearLayout) inflater.inflate(R.layout.view_selector_finger, null, false);
        return dedosLayout;
    }

    @Override
    public void onStepOpening(int stepNumber) {
        switch (stepNumber) {
            case 0:
                checkManos();
                break;
            case 1:
                checkFinger();
                break;
            case 2:
                finishProcess(null, null);
                break;
        }
    }

    private void finishProcess(String serialNumber, String readerType) {

        cantidadEvidencia = cantidadEvidencia - 1;
        if (cantidadEvidencia == 0) {

            //Obtención número de auditoria

            if (!Utils.isConnectingToInternet(this)) {

                try {

                    numeroAuditoria = Auditoria_.getAudit();
                    Auditoria_.deleteAudit(numeroAuditoria);

                } catch (Exception e) {
                    sendReportUncaughtException(e, this);
                    Log.d(TAG, e.getMessage());
                }

            }

            try {

                List<String> fileNames = new ArrayList<>();
                List<byte[]> contentEvidence = new ArrayList<>();
                JSONArray jsonArray = new JSONArray();

                Iterator<Map.Entry<Integer, byte[]>> iterator = mapa_evidencia.entrySet().iterator();

                while (iterator.hasNext()) {

                    Map.Entry<Integer, byte[]> next = iterator.next();
                    int idFingerIso = next.getKey();

                    // Añade el content a la lista.
                    contentEvidence.add(next.getValue());
                    fileNames.add(String.format("finger_iso_%s.wsq", idFingerIso));

                    jsonArray.put(idFingerIso - 1);
                }

                String type = "Evidencia";

                JSONObject jsonEvidence = generateJsonEvidence(
                        type
                        , rut
                        , dv
                        , ""
                        , jsonArray.getString(0)
                        , 0
                        , 0
                        , serialNumber
                        , mAutentiaPreferences.getString(KeyPreferences.RUN_INSTITUTION)
                        , mAutentiaPreferences.getString(KeyPreferences.INSTITUTION)
                        , numeroAuditoria
                        , new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
                        , AppHelper.getCurrentVersion(this)
                        , DeviceHelper.getOperator(this)
                        , DeviceHelper.getIPAddress(true)
                        , DeviceHelper.getAndroidId(this)
                        , DeviceHelper.getIMEI(this)
                        , Arrays.toString(mLocationHelper.getCoordinates())
                        , Math.round(mLocationHelper.getAccuracy())
                        , AUDIT_TYPE
                        , AUDIT_SOURCE
                        , readerType
                        , "0000");


                if (!numeroAuditoria.equals("")) {


                    byte[] zipBytes = Utils.zipBytes(fileNames.toArray(new String[fileNames.size()]), contentEvidence);

                    Evidence_ evidence = new Evidence_();
                    evidence.setJsonData(jsonEvidence.toString());
                    evidence.setFingerprint(contentEvidence.get(0));
                    evidence.setFingerprintBmp(listBitmap.get(0));
                    evidence.setAttachments(zipBytes);
                    evidence.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                    evidence.setType(type);
                    evidence.setAudit(numeroAuditoria);
                    Evidence_.addAuditoria(evidence);

                    MyServiceSyncAdapter.syncImmediately(this);

                    Intent intent = new Intent();
                    intent.putExtra(Extras.Out.ESTADO, Status.OK);
                    intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                    intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                    intent.putExtra(Extras.Out.CODIGO_AUDITORIA, numeroAuditoria);
                    intent.putExtra(Extras.Out.TRACK_ID, trackID);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {

                    String dedoId = jsonArray.getString(0);

                    mAutentiaWSClient.genAudit(contentEvidence.get(0),
                            "wsq",
                            String.format("%s-%s", rut, dv),
                            dedoId,
                            readerType,
                            serialNumber,
                            listBitmap.get(0),
                            "0000",
                            "",
                            jsonEvidence.toString(),
                            mAutentiaPreferences.getString(KeyPreferences.RUN_INSTITUTION),
                            AppHelper.getCurrentVersion(this),
                            "sin datos",
                            "0",
                            "",
                            "",
                            mAutentiaPreferences.getString(KeyPreferences.INSTITUTION),
                            false,
                            new ResponseCallback() {
                                @Override
                                public void onResponseSuccess(Bundle result) {

                                    GenAuditResp gar = new Gson().fromJson(result.getString("result"), GenAuditResp.class);

                                    Intent intent = new Intent();
                                    intent.putExtra(Extras.Out.ESTADO, Status.OK);
                                    intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                                    intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                                    intent.putExtra(Extras.Out.CODIGO_AUDITORIA, gar.getNroAuditoria());
                                    intent.putExtra(Extras.Out.TRACK_ID, trackID);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }

                                @Override
                                public void onResponseError(AutentiaMovilException result) {
                                    Log.e(TAG, "Create Audit HERROR");
                                    try {

                                        byte[] zipBytes = Utils.zipBytes(fileNames.toArray(new String[fileNames.size()]), contentEvidence);

                                        Evidence_ evidence = new Evidence_();
                                        evidence.setJsonData(jsonEvidence.toString());
                                        evidence.setFingerprint(contentEvidence.get(0));
                                        evidence.setFingerprintBmp(listBitmap.get(0));
                                        evidence.setAttachments(zipBytes);
                                        evidence.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                                        evidence.setType(type);
                                        evidence.setAudit(numeroAuditoria);
                                        Evidence_.addAuditoria(evidence);

                                        MyServiceSyncAdapter.syncImmediately(SelectorEvidenceActivity.this);

                                        Intent intent = new Intent();
                                        intent.putExtra(Extras.Out.ESTADO, Status.OK);
                                        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                                        intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                                        intent.putExtra(Extras.Out.CODIGO_AUDITORIA, numeroAuditoria);
                                        intent.putExtra(Extras.Out.TRACK_ID, trackID);
                                        setResult(RESULT_OK, intent);
                                        finish();

                                    }catch (Exception e){

                                    }

                                }
                            });

                }

            } catch (Exception e) {
                sendReportUncaughtException(e, this);
                finishActivity(Status.ERROR,
                        ReturnCode.ERROR_GENERICO.getCode(),
                        String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
            }
        } else {
            verticalStepperForm.goToStep(0, false);
        }
    }

    /**
     * @param type
     * @param rut
     * @param dv
     * @param name
     * @param finderDescription
     * @param sampleWidth
     * @param sampleHeight
     * @param serialNumber
     * @param runInstitution
     * @param institution
     * @param codeAudit
     * @param fingerDate
     * @param versionApp
     * @param operator
     * @param ip
     * @param androidId
     * @param imei
     * @param location
     * @param presition
     * @param operation
     * @param origin
     * @param readerType
     * @param result
     * @return
     * @throws JSONException
     */
    private JSONObject generateJsonEvidence(String type, int rut, char dv, String name,
                                            String finderDescription, int sampleWidth, int sampleHeight,
                                            String serialNumber, String runInstitution, String institution,
                                            String codeAudit, String fingerDate, String versionApp,
                                            String operator, String ip, String androidId, String imei, String location,
                                            long presition, String operation, String origin, String readerType,
                                            String result) throws JSONException {

        if (trackID.isEmpty()){
            trackID = getTrackID();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type);
        jsonObject.put("rut", rut);
        jsonObject.put("dv", String.valueOf(dv));
        jsonObject.put("nombre", name);
        jsonObject.put("dedoCodificado", finderDescription);
        jsonObject.put("muestraWidth", sampleWidth);
        jsonObject.put("muestraHeight", sampleHeight);
        jsonObject.put("numeroSerieLector", serialNumber);
        jsonObject.put("tipoLector", readerType);
        jsonObject.put("rutOperador", runInstitution);
        jsonObject.put("institucion", institution);
        jsonObject.put("codigoAuditoria", codeAudit);
        jsonObject.put("dedoFecha", fingerDate);
        jsonObject.put("versionApp", versionApp);
        jsonObject.put("operadorMovil", operator);
        jsonObject.put("ip", ip);
        jsonObject.put("androidId", androidId);
        jsonObject.put("imei", imei);
        jsonObject.put("ubicacion", location);
        jsonObject.put("presicionUbicacion", presition);
        jsonObject.put("operacion", operation);
        jsonObject.put("origen", origin);
        jsonObject.put("resultado", result);
        jsonObject.put("proposito", "5");
        jsonObject.put("trackId", trackID);

        return jsonObject;
    }

    private View finishProcessLayout() {
        LayoutInflater inflater = LayoutInflater.from(getBaseContext());
        finishLayout = (LinearLayout) inflater.inflate(R.layout.view_selector_finish, null, false);
        verticalStepperForm.setUpStepLayoutAsConfirmationStepLayout(finishLayout);
        return finishLayout;
    }

    private void checkManos() {
        mano_i = (ImageButton) manosLayout.findViewById(R.id.mano_izq);
        mano_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mano_seleccionada = IZQUIERDA;
                (dedosLayout.findViewById(R.id.mano_der_layout)).setVisibility(View.GONE);
                (dedosLayout.findViewById(R.id.mano_izq_layout)).setVisibility(View.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Drawable drawable = getResources().getDrawable(R.drawable.mano_izquierda_check);
                    ((ImageButton) (manosLayout.findViewById(R.id.mano_izq))).setImageDrawable(drawable);
                    Drawable drawable2 = getResources().getDrawable(R.drawable.mano_derecha);
                    ((ImageButton) (manosLayout.findViewById(R.id.mano_der))).setImageDrawable(drawable2);
                }
                verticalStepperForm.setActiveStepAsCompleted();
                verticalStepperForm.goToNextStep();

            }
        });
        mano_d = (ImageButton) manosLayout.findViewById(R.id.mano_der);
        mano_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mano_seleccionada = DERECHA;
                (dedosLayout.findViewById(R.id.mano_der_layout)).setVisibility(View.VISIBLE);
                (dedosLayout.findViewById(R.id.mano_izq_layout)).setVisibility(View.GONE);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Drawable drawable = getResources().getDrawable(R.drawable.mano_derecha_check);
                    ((ImageButton) (manosLayout.findViewById(R.id.mano_der))).setImageDrawable(drawable);
                    Drawable drawable2 = getResources().getDrawable(R.drawable.mano_izquierda);
                    ((ImageButton) (manosLayout.findViewById(R.id.mano_izq))).setImageDrawable(drawable2);

                }
                verticalStepperForm.setActiveStepAsCompleted();
                verticalStepperForm.goToNextStep();
            }
        });
    }

    private void checkFinger() {

        if (mano_seleccionada.equals(IZQUIERDA)) {

            (dedosLayout.findViewById(R.id.mano_der_layout)).setVisibility(View.GONE);

            mano_im = dedosLayout.findViewById(R.id.mano_izq_menique);
            mano_im.setTag(1);
            mano_im.setOnClickListener(onClickFingerListener);

            mano_ia = dedosLayout.findViewById(R.id.mano_izq_anular);
            mano_ia.setTag(2);
            mano_ia.setOnClickListener(onClickFingerListener);

            mano_ime = dedosLayout.findViewById(R.id.mano_izq_medio);
            mano_ime.setTag(3);
            mano_ime.setOnClickListener(onClickFingerListener);

            mano_ii = dedosLayout.findViewById(R.id.mano_izq_indice);
            mano_ii.setTag(4);
            mano_ii.setOnClickListener(onClickFingerListener);

            mano_ip = dedosLayout.findViewById(R.id.mano_izq_pulgar);
            mano_ip.setTag(5);
            mano_ip.setOnClickListener(onClickFingerListener);

        } else {

            mano_dp = dedosLayout.findViewById(R.id.mano_der_pulgar);
            mano_dp.setTag(6);
            mano_dp.setOnClickListener(onClickFingerListener);

            mano_di = dedosLayout.findViewById(R.id.mano_der_indice);
            mano_di.setTag(7);
            mano_di.setOnClickListener(onClickFingerListener);

            mano_dme = dedosLayout.findViewById(R.id.mano_der_medio);
            mano_dme.setTag(8);
            mano_dme.setOnClickListener(onClickFingerListener);

            mano_da = dedosLayout.findViewById(R.id.mano_der_anular);
            mano_da.setTag(9);
            mano_da.setOnClickListener(onClickFingerListener);

            mano_dm = dedosLayout.findViewById(R.id.mano_der_menique);
            mano_dm.setTag(10);
            mano_dm.setOnClickListener(onClickFingerListener);
        }
    }

    @Override
    public void sendData() {/*sin datos*/}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_EVIDENCE_CODE) {

            if (requestCode == RESULT_CANCELED) {
                verticalStepperForm.goToStep(0, false);
                return;
            }

            if (data != null) {

                if (data.hasExtra(Extras.In.ESTADO)) {

                    finishActivity(data.getStringExtra(Extras.In.ESTADO),
                            data.getIntExtra(Extras.Out.CODIGO_RESPUESTA, 0),
                            data.getStringExtra(Extras.In.DESCRIPCION));
                    return;
                }

                final byte[] bb = data.getByteArrayExtra("EVIDENCIA_HUELLA");
                final byte[] bbmp = data.getByteArrayExtra("EVIDENCIA_HUELLA_BMP");
                if (mapa_evidencia.containsKey(fingerIso)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectorEvidenceActivity.this);
                    builder.setTitle("Alerta");
                    builder.setMessage("¿Desea reemplazar la huella capturada anteriormente?");
                    builder.setPositiveButton("Reemplazar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            cantidadEvidencia++;
                            mapa_evidencia.put(fingerIso, bb);
                            listBitmap.add(bbmp);
                            verticalStepperForm.setStepAsCompleted(2);
                            verticalStepperForm.goToNextStep();
                        }
                    });
                    builder.setNegativeButton("Nuevo dedo", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            verticalStepperForm.goToStep(0, false);
                        }
                    });
                    builder.create().show();
                    return;
                } else {

                    String serialNumber = data.getStringExtra(CaptureEvidenceActivity.Extras.Out.SERIE_LECTOR);
                    String readerType = data.getStringExtra(CaptureEvidenceActivity.Extras.Out.READER_TYPE);
                    mapa_evidencia.put(fingerIso, bb);
                    listBitmap.add(bbmp);
                    finishProcess(serialNumber, readerType);
                }
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}