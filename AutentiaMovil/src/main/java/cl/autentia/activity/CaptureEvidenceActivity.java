package cl.autentia.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.acepta.iso19794_2.ISOFMR;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUGlobal;
import com.morpho.android.usb.USBManager;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import cl.autentia.common.CommonInExtras;
import cl.autentia.common.CommonOutExtras;
import cl.autentia.common.ReturnCode;
import cl.autentia.common.Status;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.HttpUtil;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintImage;
import cl.autentia.reader.FingerprintInfo;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;

import static cl.autentia.activity.VerificationActivity.NOT_WORKING_PROPERLY;

@EActivity(R.layout.activity_capture_evidence)
public class CaptureEvidenceActivity extends AppCompatActivity implements FingerprintReader.ScanCallback, FingerprintReader.InfoCallback, FingerprintGrantReceiver {

    private static final String TAG = "CaptureEvidence";

    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;

    private FingerprintManager mFingerprintManager;
    private FingerprintReader mFingerprintReader;
    private Engine mEngine;

    private ProgressDialog mProgressDialog;
    private CountDownTimer mCountDownTimer;
    private boolean[] huellasDisponibles;
    private String mSerialNumber;
    private String mReaderType;
    private AutentiaPreferences mPreferences;

    /**
     * Extras
     */

    Object dedosObtenidos;

    @Extra(Extras.In.RUN)
    String rut = "";

    @Extra(Extras.In.ICON)
    byte[] mIcon = new byte[0];

    @Extra(Extras.In.TITLE)
    String mTitle = "";

    @Extra(Extras.In.SUBTITLE)
    String mSubtitle = "";

    @Extra(Extras.In.COLOR_TITLE)
    String mColorTitle = "";

    @Extra(Extras.In.COLOR_SUBTITLE)
    String mColorSubtitle = "";

    @Extra(Extras.In.COLOR_PRIMARY)
    String mColorPrimary = "";

    @Extra(Extras.In.COLOR_PRIMARY_DARK)
    String mColorPrimaryDark = "";

    @Extra(Extras.In.INTENTOS)
    int maxIntentos = 3;

    @Extra(Extras.In.INTENTO_EN_CURSO)
    int intentoEnCurso = 0;

    /**
     * Views
     */

    @ViewById(R.id.iv_hands_evidence)
    ImageView _ivHands;

    @ViewById(R.id.tv_rut)
    TextView _tvRut;

    @ViewById(R.id.ll_attempts)
    LinearLayout _layoutIntentos;

    @ViewById(R.id.toolbar)
    Toolbar _toolbar;

    @InstanceState
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {
        interface In extends CommonInExtras {
            String DEDO = "DEDO";
            String RUN = "RUN";
            String INTENTO_EN_CURSO = "INTENTO_EN_CURSO";
            String INTENTOS = "INTENTOS";
        }

        interface Out extends CommonOutExtras {
            String EVIDENCIA_HUELLA = "EVIDENCIA_HUELLA";
            String EVIDENCIA_HUELLA_BMP = "EVIDENCIA_HUELLA_BMP";
            String SERIE_LECTOR = "SERIE_LECTOR";
            String READER_TYPE = "READER_TYPE";
        }
    }

    UsbManager usbManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreferences = new AutentiaPreferences(this);
        try {

            Bundle extras = getIntent().getExtras();

            dedosObtenidos = extras.get(Extras.In.DEDO);

            mFingerprintManager = FingerprintManager.getInstance(this);
            mEngine = UareUGlobal.GetEngine();

            if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager")) {
                usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                USBManager.getInstance().initialize(this,"com.morpho.morphosample.USB_ACTION");
                setPowerFP2(1);
            }

        } catch (Exception e) {
            sendReportUncaughtException(e);
            Log.d(TAG, e.getMessage());
            setResultActivity(Status.ERROR, ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
        }
    }


    void setPowerFP2(int onOff) {
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (onOff == 1) {
            Log.i(TAG, "POWER ON");
            usbManager.setFingerPrinterPower(true);
        } else if (onOff == 0) {
            Log.i(TAG, "POWER OFF");
            usbManager.setFingerPrinterPower(false);
        }
    }

    public void sendReportUncaughtException(Exception exception) {
        HttpUtil.sendReportUncaughtException(exception, this);
    }

    @AfterExtras
    void afterExtras() {

        mTitle = TextUtils.isEmpty(mTitle) ? "Toma de evidencia" : mTitle;
        mSubtitle = TextUtils.isEmpty(mSubtitle) ? "" : mSubtitle;
        mColorTitle = TextUtils.isEmpty(mColorTitle) ? getResources().getString(android.R.color.white) : mColorTitle;
        mColorSubtitle = TextUtils.isEmpty(mColorSubtitle) ? getResources().getString(android.R.color.white) : mColorSubtitle;
        mColorPrimary = TextUtils.isEmpty(mColorPrimary) ? getResources().getString(R.color.colorPrimary) : mColorPrimary;
        mColorPrimaryDark = TextUtils.isEmpty(mColorPrimaryDark) ? getResources().getString(R.color.colorPrimaryDark) : mColorPrimaryDark;
    }

    @AfterViews
    void afterViews() {

        if (mIcon.length > 0) {

            Bitmap bmp = BitmapFactory.decodeByteArray(mIcon, 0, mIcon.length);
            Drawable drawable = new BitmapDrawable(getResources(), bmp);
            _toolbar.setNavigationIcon(drawable);
        }

        _tvRut.setText(rut);

        _toolbar.setTitle(mTitle);
        _toolbar.setSubtitle(mSubtitle);
        _toolbar.setTitleTextColor(Color.parseColor(mColorTitle));
        _toolbar.setSubtitleTextColor(Color.parseColor(mColorSubtitle));
        _toolbar.setBackgroundColor(Color.parseColor(mColorPrimary));
        setColorNotificationBar(Color.parseColor(mColorPrimaryDark));

        addIntentos();

        ImageView iv = (ImageView) _layoutIntentos
                .findViewById(intentoEnCurso + 9);

        if (intentoEnCurso > 0) {

            if (iv != null) {
                iv.setImageResource(R.drawable.verification_error);
            }
        }

        boolean[] dedos = new boolean[11];

        if (dedosObtenidos instanceof Integer) {

            int dedo = (Integer) dedosObtenidos;

            if (dedo < 1) {
                setResultActivity(Status.NO_OK,
                        ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getCode(),
                        ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getDescription());

            } else {
                dedos[dedo] = true;
            }

        } else if (dedosObtenidos instanceof ArrayList) {

            ArrayList<Integer> dedosArray = (ArrayList<Integer>) dedosObtenidos;

            if (dedosArray.isEmpty())
                setResultActivity(Status.NO_OK,
                        ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getCode(),
                        ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getDescription());

            else {
                for (int i = 0; i < dedosArray.size(); i++) {
                    dedos[dedosArray.get(i)] = true;
                }
            }

        } else {
            setResultActivity(Status.NO_OK,
                    ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getCode(),
                    ReturnCode.CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR.getDescription());
        }
        huellasDisponibles = dedos;
        setCombinedHandImage(_ivHands);

        prepareCaptureFingerprint();
    }

    /**
     * Solicitud de permisos para el huellero.
     */
    private void prepareCaptureFingerprint() {

        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    getInfo();
                } else {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                getInfo();
            }
        } catch (AutentiaMovilException e) {
            setResultActivity(e.status,
                    e.returnCode.getCode(),
                    e.returnCode.getDescription());

        } catch (Exception e) {
            setResultActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
        }
    }

    private void setColorNotificationBar(int color) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(color);
        }
    }

    private void addIntentos() {

        for (int i = 0; i < maxIntentos; i++) {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(2, 2, 2, 2);

            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.verification_stand_by);
            iv.setLayoutParams(layoutParams);
            iv.setId(i + 10);
            _layoutIntentos.addView(iv);
        }
    }

    /**
     * @param status
     * @param responseCode
     * @param description
     */
    private void setResultActivity(String status, int responseCode, String description) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Extras.Out.CODIGO_RESPUESTA, responseCode);
        returnIntent.putExtra(Extras.Out.ESTADO, status);
        returnIntent.putExtra(Extras.Out.DESCRIPCION, description);
        setResult(RESULT_OK, returnIntent);
    }

    private void setCombinedHandImage(ImageView imageView) {
        Bitmap combinedImage = combinar(huellasDisponibles);
        imageView.setImageBitmap(combinedImage);
    }

    private Bitmap combinar(final boolean dedos[]) {

        final int dedosDrawables[] = Utils.dedosDrawables;

        Bitmap manos = BitmapFactory.decodeResource(this.getResources(), R.drawable.hands);
        Bitmap bmOverlay = Bitmap.createBitmap(manos.getWidth(), manos.getHeight(),
                manos.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(manos, 0, 0, null);

        if (dedos[0]) {
            drawResource(canvas, 0, 0, R.drawable.hands);
        } else {
            for (int dedoPos = 1; dedoPos <= 10; dedoPos++) {
                if (dedos[dedoPos]) {
                    drawResource(canvas, 0, 0, dedosDrawables[dedoPos]);
                }
            }
        }
        return bmOverlay;
    }

    @Override
    public void onFingerprintAccessGranted() {
        mFingerprintReader = mFingerprintManager.getReader(this);
        getInfo();
    }

    @Override
    public void onFingerprintAccessDenied() {
        setResultActivity(Status.NO_OK,
                ReturnCode.ACCESO_HUELLERO_DENEGADO.getCode(),
                ReturnCode.ACCESO_HUELLERO_DENEGADO.getDescription());
    }

    @Override
    public void onFingerprintAccessError() {
        setResultActivity(Status.NO_OK,
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO.getCode(),
                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO.getDescription());
    }


    @Override
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void scanResult(FingerprintImage image) {
        try {
            if (image != null && image.getRawImage() != null) {

                byte[] byteArray = image.getImageWSQ();
                byte[] byteArrayBitmap = Utils.bitmapToByteArray(image.getBitmap());

                int minuciaQualityCount = 0;
                int minuciaCount = 0;

                try {

                    Fmd fmd = mEngine.CreateFmd(
                            image.getRawImage(),
                            image.getWidth(),
                            image.getHeight(),
                            500,
                            0,
                            0,
                            Fmd.Format.ISO_19794_2_2005);
                    ISOFMR isofmr = new ISOFMR(fmd.getData());

                    for (int i = 0; i < isofmr.views.length; i++) {

                        for (int j = 0; j < isofmr.views[i].minutiae.length; j++) {

                            Log.d("Quality." + j, String.valueOf(isofmr.views[i].minutiae[j].quality));
                            minuciaCount++;
                            if (isofmr.views[i].minutiae[j].quality > 65) {
                                minuciaQualityCount++;
                            }
                        }
                    }

                } catch (Exception e) {
                    sendReportUncaughtException(e);
                }

                if (minuciaCount > 15) {

                    if (((minuciaQualityCount * 100) / minuciaCount) < (minuciaCount * 0.66)) {
                        AlertDialog.Builder al = new AlertDialog.Builder(
                                CaptureEvidenceActivity.this)
                                .setTitle(R.string.error)
                                .setMessage("Baja calidad de huella capturada")
                                .setPositiveButton(R.string.reintentar,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                captureFingerprint();
                                            }
                                        })
                                .setNegativeButton(R.string.cancelar,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int which) {

                                                setResult(RESULT_CANCELED);
                                                finish();
                                            }
                                        });
                        al.show();

                    } else {

                        mFingerprintReader = null;

                        Intent intent = new Intent();
                        intent.putExtra(Extras.Out.EVIDENCIA_HUELLA, byteArray);
                        intent.putExtra(Extras.Out.EVIDENCIA_HUELLA_BMP, byteArrayBitmap);
                        intent.putExtra(Extras.Out.SERIE_LECTOR, mSerialNumber);
                        intent.putExtra(Extras.Out.READER_TYPE, mReaderType);
                        intent.putExtra(Extras.Out.CODIGO_RESPUESTA, ReturnCode.EXITO.getCode());
                        intent.putExtra(Extras.Out.DESCRIPCION, ReturnCode.EXITO.getDescription());
                        setResult(RESULT_OK, intent);
                        mCountDownTimer.cancel();
                        finish();
                    }
                } else {

                    LinearLayout rv_capture = (LinearLayout) findViewById(R.id.layout_fingerprint_capture);
                    Snackbar snackbar = Snackbar.make(rv_capture,
                            "Reposicionar la huella en el lector", Snackbar.LENGTH_INDEFINITE);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.RED);
                    snackbar.show();
                    captureFingerprint();
                }
            }
        } catch (Exception e) {
            sendReportUncaughtException(e);
        }
    }

    @OnActivityResult(value = REQUEST_FINGERPRINT_ACCESS)
    protected void onRequestFingerprintAccessResult(int resultCode, Intent data) {
        mFingerprintManager.processAccessGrant(resultCode, data, this);
    }

    @Override
    public void infoResult(FingerprintInfo info) {

        mSerialNumber = info.serialNumber;
        mReaderType = info.readerType;

        captureFingerprint();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void showProgressMessage(final String title, final String message, boolean cancelable) {
        // por si hubo contador lo cancelamos
        dismissProgressMessage();

        Log.v(TAG, String.format("message: %s: %s", title, message));

        int timeOut = 20;
        long timeOutInMiliseconds = TimeUnit.MILLISECONDS.convert(timeOut, TimeUnit.SECONDS);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        if (cancelable)
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dialogInterface.dismiss();
                    onBackPressed();

                }
            });
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(timeOut);
        mProgressDialog.setProgress(timeOut);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setProgressNumberFormat("%1d seg");
        mProgressDialog.setProgressPercentFormat(null);
        mProgressDialog.setMessage(String.format("\nQuedan %s seg.", timeOut));
        mProgressDialog.show();

        Log.v(TAG, "progress dialog should be visible");

        mCountDownTimer = new CountDownTimer(timeOutInMiliseconds, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                int secondsLeft = (int) ((millisUntilFinished + 500) / 1000);

                Log.v(TAG, "countdown: " + millisUntilFinished + " ms left");
                updateProgressMessage(
                        String.format("Quedan %s seg.", secondsLeft), secondsLeft);
            }

            @Override
            public void onFinish() {
                onBackPressed();
            }

        }.start();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void updateProgressMessage(final String message, final int secondsLeft) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setProgress(secondsLeft);
                mProgressDialog.setMessage(message);
            }
        });
    }


    @Background
    void getInfo() {
        try {
            showProgressMessage("Cargando...", "por favor, espere...", false);
            mFingerprintReader.getInfo(this);
        } catch (Exception e) {
            sendReportUncaughtException(e);
            setResultActivity(Status.ERROR,
                    ReturnCode.ERROR_GENERICO.getCode(),
                    String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    protected void dismissProgressMessage() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        Log.v(TAG, "dialog dismissed");
    }

    @Background
    void captureFingerprint() {
        try {
            showProgressMessage("Esperando huella", "Coloque el dedo seleccionado sobre el lector...", true);
            mFingerprintReader.capture(this);
        } catch (Exception e) {
            sendReportUncaughtException(e);

            if (e.toString().contains(NOT_WORKING_PROPERLY)) {
                setResultActivity(Status.ERROR,
                        ReturnCode.ERROR_HUELLERO_DESCONECTADO.getCode(),
                        ReturnCode.ERROR_HUELLERO_DESCONECTADO.getDescription());
            } else {
                setResultActivity(Status.ERROR,
                        ReturnCode.ERROR_GENERICO.getCode(),
                        String.format(ReturnCode.ERROR_GENERICO.getDescription(), e.getMessage()));
            }

        }
    }

    @Override
    public void onBackPressed() {

        try {
            if (mFingerprintReader != null) {
                if (mFingerprintManager.getClass().getName().equals("cl.autentia.reader.usb.morpho.MorphoManager"))setPowerFP2(0);
                mFingerprintReader.cancelCapture();
                mFingerprintReader.close();
            }
        } catch (Exception e) {
        }
        setResult(RESULT_CANCELED);
        finish();
    }

    private void drawResource(Canvas canvas, int left, int top, int resource) {
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), resource);
        canvas.drawBitmap(bmp, left, top, null);
    }
}