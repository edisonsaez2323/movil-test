package cl.autentia.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.acepta.Utils;
import com.acepta.android.fetdroid.R;
import com.airbnb.lottie.LottieAnimationView;
import com.jakewharton.processphoenix.ProcessPhoenix;
import com.morpho.android.usb.USBManager;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import cl.autentia.AutentiaMovilApplication;
import cl.autentia.barcode.QRActivity;
import cl.autentia.configuration.CheckForUpdates;
import cl.autentia.configuration.ConfigurationHandler;
import cl.autentia.configuration.fragment.AboutFragment;
import cl.autentia.configuration.fragment.AboutFragment_;
import cl.autentia.configuration.fragment.CurrentConfigurationFragment;
import cl.autentia.configuration.fragment.CurrentConfigurationFragment_;
import cl.autentia.configuration.fragment.EvidenceOfflineFragment;
import cl.autentia.configuration.fragment.EvidenceOfflineFragment_;
import cl.autentia.configuration.fragment.FingerprintReaderConfigurationFragment;
import cl.autentia.configuration.fragment.FingerprintReaderConfigurationFragment_;
import cl.autentia.configuration.fragment.NFCReaderConfigurationFragment;
import cl.autentia.configuration.fragment.NFCReaderConfigurationFragment_;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.HttpUtil;
import cl.autentia.http.ResponseCallback;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;
import cl.autentia.reader.FingerprintGrantReceiver;
import cl.autentia.reader.FingerprintManager;
import cl.autentia.reader.FingerprintReader;
import cl.autentia.realmObjects.Auditoria_;
import cl.autentia.realmObjects.Evidence_;
import cl.autentia.sync.MyServiceSyncAdapter;
import cl.autentia.sync.onrequest.SyncNow;
import cl.autentia.sync.onrequest.SyncNow_;
import cl.autentia.test.AutentiaTester_;
import cl.autentia.usb.UsbPermissionManager;

@EActivity(R.layout.activity_navegation_menu)
public class NavegationMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FingerprintGrantReceiver {


    private static final String MORPHORT080_FINGERPRINT_CLASS = "cl.autentia.reader.usb.morpho.MorphoManager";
    public static final String NETWORK_ERROR = "Se ha superado el tiempo de respuesta de la petición o no es posible conectar con el servidor.";
    public static final String BROADCAST_NOTIFICACION_FINISH = "cl.autentia.android.broadcast.notificationfinish";
    private static final String URL_PROD = "https://movil.autentia.cl/autentiaws/";
    private static final String TAG = "NavegationMenuActivity";
    private static final int REQUEST_BLOCK_CODE_NEW_CONFIG = 0x010;
    private static final int REQUEST_BLOCK_CODE_FINGERPRINT = 0x011;
    private static final int REQUEST_BLOCK_CODE_NFC = 0x012;
    private static final int REQUEST_BLOCK_CODE_EVIDENCE_OFFLINE = 0x013;
    private static final int REQUEST_CAMERA_BARCODE = 0x014;
    private static final int REQUEST_PERMISSION = 0x015;
    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    public static final String INICIO_AUTOMATICO = "4";
    public static final String PEDIR_RUT = "1";
    private static final int REQUEST_CODE_LOGIN_OPER = 0x032;
    private static final int REQUEST_PERMISSION_INSTALL = 0x033;

    public UsbManager usbManager;

    private boolean operVerified = false;

    //Updatefrombuttonimplemmentation
    private FingerprintManager mFingerprintManager;
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    AutentiaWSClient mAutentiaWSClient;

    static Activity mActivityFinal;
    ProgressDialog mProgressDialog;
    static AlertDialog optionDialog;
    static AlertDialog alertDialog;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.nav_view)
    NavigationView navigationView;
    private String[] permission = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private AutentiaPreferences mPreferences;
    private boolean mKillProcess = false;
    private Uri mUri = null;
    /**
     *
     */
    private BroadcastReceiver receiverDownloadConfigFinish = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle extras = intent.getExtras();

            if (extras != null) {

                boolean alertDownload = extras.getBoolean("alertDownload");
                boolean wasDownload = extras.getBoolean("wasDownload");

                if (alertDownload) {

                    new AlertDialog.Builder(NavegationMenuActivity.this)
                            .setMessage(wasDownload ? "Configuración actualizada" : "Sin cambios")
                            .setTitle("Sincronización terminada")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", null)
                            .create().show();

                    mPreferences.setBoolean("alertDownload", false);

                } else {

                    if (wasDownload) {
                        if (navigationView != null) {

                            Toast.makeText(context, "Configuración actualizada", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            } else if (navigationView != null) {
                navigationView.setCheckedItem(R.id.nav_properties);
                onNavigationItemSelected(navigationView.getMenu().getItem(0));
            }

        }
    };
    private FingerprintReader mFingerprintReader;

    public static void showProgressAlert(final String message) {

        if (mActivityFinal != null) {
            mActivityFinal.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (optionDialog != null)
                        if (optionDialog.isShowing())
                            optionDialog.dismiss();
                    if (alertDialog != null)
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();
                    alertDialog = new AlertDialog.Builder(mActivityFinal).create();
                    alertDialog.setTitle("Error de configuración");
                    alertDialog.setMessage(message);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            });
        }


    }

    public static void showProgressNoUpgrade() {

        if (mActivityFinal != null)
            mActivityFinal.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (alertDialog != null)
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();
                    alertDialog = new AlertDialog.Builder(mActivityFinal).create();
                    alertDialog.setTitle("Actualización AutentiaMovil");
                    alertDialog.setMessage("Su aplicación está actualizada a la última versión.");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            });
    }

    public static void showDialog(final String message) {

        if (mActivityFinal != null)
            mActivityFinal.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (optionDialog != null)
                        if (optionDialog.isShowing())
                            optionDialog.dismiss();
                    optionDialog = new AlertDialog.Builder(mActivityFinal).create();

                    View v = mActivityFinal.getLayoutInflater().inflate(R.layout.progress_layout, null);
                    LottieAnimationView lottie = v.findViewById(R.id.lottie);
                    TextView tv = v.findViewById(R.id.tv_message);
                    tv.setText(message);
                    try {
                        lottie.setAnimation(new JSONObject(mActivityFinal.getResources().getString(R.string.json_animation)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    lottie.playAnimation();
                    lottie.setScale(10);
                    lottie.loop(true);

                    optionDialog.setView(v);
                    optionDialog.setCanceledOnTouchOutside(false);
                    optionDialog.setCancelable(false);
                    optionDialog.show();
                }
            });
    }

    private void requestPermission(String[] permissions, int requestCode) {

        List<String> permissionToGranted = new ArrayList();

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionToGranted.add(permission);
            }
        }

        if (permissionToGranted.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionToGranted.toArray(new String[permissionToGranted.size()]), requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_PERMISSION:

                List<String> permissionToGranted = new ArrayList();
                for (int i = 0; i < permissions.length; i++) {

                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        permissionToGranted.add(permission);
                    }
                }

                if (permissionToGranted.size() > 0) {

                    ActivityCompat.requestPermissions(this, permissionToGranted.toArray(new String[permissionToGranted.size()]), requestCode);
                } else {

                    mKillProcess = true;
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (mKillProcess) {

            File directory = new File(new File(Environment.getExternalStorageDirectory() + "/" + AutentiaMovilApplication.AUTENTIA_MOVIL_PROPERTIES_PATH).getParent());
            if (!directory.exists())
                directory.mkdirs();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (usbManager == null) usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.durga.action.close");
        mIntentFilter.addAction("com.durga.action.reserve");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);

        mPreferences = new AutentiaPreferences(NavegationMenuActivity.this);

        mAutentiaWSClient = new AutentiaWSClient(this);
        mActivityFinal = this;
        mFingerprintManager = null;

        try {
            mFingerprintManager = FingerprintManager.getInstance(this);
        } catch (Exception e) {
            showDialogError(e.getMessage());
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra("mKill"))
                mKillProcess = true;
        }

        String config = mPreferences.getString(KeyPreferences.DATA_CONFIGURATION);

        if (config.equals("00003") && !mPreferences.containsKey(KeyPreferences.TERMS_AND_CONDITIONS)) {
            new SyncNow().getTyC(this);
        }

        if (mPreferences.getString(KeyPreferences.FIRST_START).equals("false")) {
            if (getIntent().hasExtra("IMFROMSERVICE")) {
                if (getIntent().getStringExtra("IMFROMSERVICE").equals("TRUE")) {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                } else {
                    if (mFingerprintManager.getClass().getName().equals(MORPHORT080_FINGERPRINT_CLASS)) {
                        Log.e("Morpho", "here");
                        USBManager.getInstance().initialize(this, "com.morpho.morphosample.USB_ACTION", false);
//                        USBManager.getInstance().initialize(this, "com.morpho.morphosample.USB_ACTION"); //try
                        setPowerFP2(1);
                    } else callerFingerprintAccess();
                }
            }
        }

        requestPermission(permission, REQUEST_PERMISSION);
    }

    @Background
    void loadConfiguration() {
        new ConfigurationHandler(this).loadConfiguration();
    }

    public boolean checkForDevices() {
        UsbManager manager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
        return manager.getDeviceList().isEmpty();
    }

    void setPowerFP2(int onOff) {
        //1 ON 2 OFF
//        SledManager sm = (SledManager) this.getSystemService("sled");
//        UsbManager m_usb_manager;
        if (usbManager == null) usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (onOff == 1) {
            Log.i(TAG, "POWER ON");
            usbManager.setFingerPrinterPower(true);
            new ConfigurationHandler(this);
            //started = true;
        } else if (onOff == 0) {
            Log.i(TAG, "POWER OFF");
            usbManager.setFingerPrinterPower(false);
            //started = false;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
    }

    public void callerFingerprintAccess() {
        try {
            if (mFingerprintCurrentState == InternalState.READY) {
                forceFinishProcess("Ready");
            } else {
                Log.e("callerFingerprintAccess", "here");
                mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.GONE);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_properties);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

//        checkFingerprintOper();
    }

    public void checkFingerprintOper() {
        try {
//            Log.e("checking", "Oper");
            mPreferences = new AutentiaPreferences(this);
            String conf = mPreferences.getString(KeyPreferences.DATA_CONFIGURATION);
            String first = mPreferences.getString(KeyPreferences.FIRST_START);
            boolean loggedIn = mPreferences.getBoolean(KeyPreferences.LOGGED_IN);
            Log.e("looggedin", String.valueOf(loggedIn));
            if (conf.equals("00002") && (first.equals("false") || first.equals("asdf")) && Utils.isConnectingToInternet(this) && !loggedIn) {

                AutentiaWSClient mClient = new AutentiaWSClient(this);
                try {
                    mClient.getFingerprintInfo(mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), new ResponseCallback() {
                        @Override
                        public void onResponseSuccess(Bundle result) {
                            switch (result.getString("tipoLogon")) {
                                case INICIO_AUTOMATICO:
                                    Log.e("login", "inicio automatico");
                                    mPreferences.setString(KeyPreferences.TIPO_LOGON, INICIO_AUTOMATICO);
                                    mPreferences.setBoolean(KeyPreferences.LOGGED_IN, true);
                                    break;
                                case PEDIR_RUT:
                                    mPreferences.setString(KeyPreferences.TIPO_LOGON, PEDIR_RUT);
                                    Log.e("pedir rut", "enter");
                                    mPreferences.setBoolean(KeyPreferences.PENDING_LOGIN, true);
                                    Log.e("pending", String.valueOf(mPreferences.getBoolean(KeyPreferences.PENDING_LOGIN)));
                                    pedirRut();
                                    break;
                                case "0":
                                case "2":
                                case "3":

                                    mPreferences.setString(KeyPreferences.TIPO_LOGON, result.getString("tipoLogon"));
//                                    Log.e("login - check", "enter");
                                    mPreferences.setString("RUT_OPER", result.getString("rutLogon"));
                                    checkOper(result.getString("rutLogon"));
                                    break;
                            }
                        }

                        @Override
                        public void onResponseError(AutentiaMovilException result) {
                            Log.e("error", result.status);
                        }
                    });
                } catch (Exception e) {
                    Log.e("ERROOOOOOR: ", e.getMessage());
                    Log.e("ERROOOOOOR: ", e.toString());
                    Log.e("stack erorr", "sddasdasdasdas");
                    e.printStackTrace();
                }

            } else if (mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("1") && mPreferences.getBoolean(KeyPreferences.PENDING_LOGIN) && !Utils.isConnectingToInternet(this)) {
                showAlertDialog("Verificacion de operador requiere conexion a internet");
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Log.e("ERROR", e.getLocalizedMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();


        Log.d("Prueba", "Resume");

        if (mKillProcess) {

            new AlertDialog.Builder(NavegationMenuActivity.this)
                    .setMessage("Se ha aplicado la configuración de permisos, tras presionar \"Aceptar\", Autentia Móvil se reiniciará.")
                    .setTitle("Configuración de permisos")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            forceFinishProcess("none");
                        }
                    })
                    .create().show();

        } else {

            IntentFilter filter = new IntentFilter();
            filter.addAction(NavegationMenuActivity.BROADCAST_NOTIFICACION_FINISH);
            registerReceiver(receiverDownloadConfigFinish, filter);

            MyServiceSyncAdapter.syncImmediately(this);
        }
    }

    public void verificarOperador(String result) {
        dismissDialog();
        mPreferences.setBoolean(KeyPreferences.PENDING_LOGIN, false);
        String[] rut = result.split("-");
        Intent intent = new Intent(NavegationMenuActivity.this, VerificationActivity_.class);
        intent.putExtra("RUT", Integer.valueOf(rut[0]));
        intent.putExtra("DV", rut[1].charAt(0));
        intent.putExtra("TITLE", "Login Operador");
        startActivityForResult(intent, REQUEST_CODE_LOGIN_OPER);
    }

    public void pedirRut() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(NavegationMenuActivity.this);
        final View dialogView = getLayoutInflater().inflate(R.layout.view_rut, null);
        builder.setView(dialogView);
        builder.setCancelable(false);
        android.app.AlertDialog alertDialog = builder.create();
        Button btAceptar = dialogView.findViewById(R.id.bt_aceptar);
        btAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rut = ((EditText) dialogView.findViewById(R.id.et_rut)).getText().toString();
                String dv = ((EditText) dialogView.findViewById(R.id.et_dv)).getText().toString();
                if (!TextUtils.isEmpty(rut) && !TextUtils.isEmpty(dv)) {
                    Utils.hideKeyboard(NavegationMenuActivity.this, dialogView);
                    checkOper(String.format("%s-%s", rut, dv));
                    alertDialog.dismiss();
                } else {
                    alertDialog.dismiss();
                    pedirRut();
                }

            }
        });
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private boolean checkOper(String rutLogon) {
        showDialog("Verificando rol operador...");
        AutentiaWSClient mAutentiaWSClient = new AutentiaWSClient(this);
        try {
            mAutentiaWSClient.getOperador(rutLogon, mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), new ResponseCallback() {
                @Override
                public void onResponseSuccess(Bundle result) {
                    if (result.getBoolean("rolVerificado")) {
                        verificarOperador(rutLogon);
                    } else {
                        showAlertDialog("El rut: " + rutLogon + " no tiene perfil de operador en " + mPreferences.getString(KeyPreferences.INSTITUTION));
                    }
                }

                @Override
                public void onResponseError(AutentiaMovilException result) {
                    dismissDialog();
                    if (result.status.equals(NETWORK_ERROR)) {
                        showAlertDialog("La verificacion requiere conexion a internet");
                    } else {
                        showAlertDialog("El rut: " + rutLogon + " no tiene perfil de operador en " + mPreferences.getString(KeyPreferences.INSTITUTION));
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void showAlertDialog(String message) {
        if (optionDialog != null) {
            dismissDialog();
        }
        AlertDialog alertDialog = new AlertDialog.Builder(mActivityFinal).create();
        alertDialog.setIcon(R.drawable.autentia_logo_blue);
        alertDialog.setTitle("Verificacion Operador");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (!mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("1")) {
                            mPreferences.setBoolean(KeyPreferences.LOGGED_IN, true);
                        }
                    }
                });
        if (!operVerified) {
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Reintentar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String rutLogon = mPreferences.getString("RUT_OPER", null);
                    if (rutLogon != null) {
                        verificarOperador(rutLogon);
                    } else {
                        pedirRut();
                    }

                }
            });
        }
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(receiverDownloadConfigFinish);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_properties) {

            CurrentConfigurationFragment currentConfigurationFragment = new CurrentConfigurationFragment_();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, currentConfigurationFragment, "configuracion_actual").commit();

        } else if (id == R.id.nav_properties_config) {

            //utilizar caracteristicas protegidas abre BlockActivity
            Intent intent = new Intent(NavegationMenuActivity.this, BlockActivity_.class);
            startActivityForResult(intent, REQUEST_BLOCK_CODE_NEW_CONFIG);

        } else if (id == R.id.nav_fingerprints_config) {

            //utilizar caracteristicas protegidas abre BlockActivity
            Intent intent = new Intent(NavegationMenuActivity.this, BlockActivity_.class);
            startActivityForResult(intent, REQUEST_BLOCK_CODE_FINGERPRINT);

        } else if (id == R.id.nav_nfc_config) {

            //utilizar caracteristicas protegidas abre BlockActivity
            Intent intent = new Intent(NavegationMenuActivity.this, BlockActivity_.class);
            startActivityForResult(intent, REQUEST_BLOCK_CODE_NFC);

        } else if (id == R.id.nav_data) {

            //utilizar caracteristicas protegidas abre BlockActivity
            Intent intent = new Intent(NavegationMenuActivity.this, BlockActivity_.class);
            startActivityForResult(intent, REQUEST_BLOCK_CODE_EVIDENCE_OFFLINE);

        } else if (id == R.id.nav_get_config) {

            mPreferences.setBoolean("alertDownload", true);
            MyServiceSyncAdapter.syncImmediately(this);

        } else if (id == R.id.nav_test) {

            Intent intent = new Intent(NavegationMenuActivity.this, AutentiaTester_.class);
            startActivity(intent);

        } else if (id == R.id.nav_upd) {

            //TODO: CREATE METHOD TO CONTROL THIS SHIT
            showNotification();

        } else if (id == R.id.nav_change_log) {

            initControls();

        } else if (id == R.id.nav_about) {

            Fragment fragment = getSupportFragmentManager().findFragmentByTag("acerca_de");
            if (fragment == null) {
                AboutFragment aboutFragment = new AboutFragment_();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, aboutFragment, "acerca_de").commit();
            }
        }
//        if (id == R.id.nav_login_oper){
//            if(mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("1"))
//                mPreferences.setBoolean(KeyPreferences.LOGGED_IN, false);
//                checkOper(mPreferences.getString("RUT_OPER"));
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnActivityResult(REQUEST_CODE_LOGIN_OPER)
    void onLoginResult(int resultCode, Intent data) {
        String tipoLogon = mPreferences.getString(KeyPreferences.TIPO_LOGON);
        if (resultCode == RESULT_OK) {
            Bundle bl = data.getExtras();
            if (bl != null) {
                if (bl.getInt("CODIGO_RESPUESTA") == 200) {
                    mPreferences.setString(KeyPreferences.RUN_INSTITUTION, String.format("%s-%s", bl.getInt("rut"), bl.getChar("dv")));
                    mPreferences.setBoolean(KeyPreferences.LOGGED_IN, true);
                    operVerified = true;
//                    if (mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("0") || mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("2") || mPreferences.getString(KeyPreferences.TIPO_LOGON).equals("3")){
//                        findViewById(R.id.nav_login_oper).setVisibility(View.GONE);
//                    }
                    showAlertDialog("Operador verificado");
                } else {
                    showAlertDialog("Error al verificar operador");
                    if (tipoLogon.equals("1")) {
                        mPreferences.setBoolean(KeyPreferences.PENDING_LOGIN, true);
                    }
                }
            } else {
                showAlertDialog("Verificacion cancelada");
                if (tipoLogon.equals("1")) {
                    mPreferences.setBoolean(KeyPreferences.PENDING_LOGIN, true);
                }
            }

        } else {
            if (tipoLogon.equals("1")) {
                mPreferences.setBoolean(KeyPreferences.PENDING_LOGIN, true);
            }
        }
    }

    @OnActivityResult(REQUEST_CAMERA_BARCODE)
    void onBarcodeResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {

                String contents = data.getStringExtra("barcode");
                if (contents == null) {
                    throw new Exception("Error en lectura de QR para configuración");
                }

                byte[] decodedproperties = Base64.decode(contents, Base64.DEFAULT);
                String stringProperties = new String(decodedproperties);
                Properties properties = getProperties(stringProperties);
                processProperties(properties);

            } catch (Exception e) {
                sendReportUncaughtException(e);
            }
        }
    }

    @OnActivityResult(REQUEST_BLOCK_CODE_NEW_CONFIG)
    void onBlockNewConfigResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(NavegationMenuActivity.this, QRActivity.class);
            intent.putExtra("TYPE", "QR");
            intent.putExtra("PORTRAIT", true);
            startActivityForResult(intent, REQUEST_CAMERA_BARCODE);
        }
    }

    @OnActivityResult(REQUEST_BLOCK_CODE_FINGERPRINT)
    void onBlockFingerPrintResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            try {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag("seleccion_huellero");
                        if (fragment == null) {
                            FingerprintReaderConfigurationFragment fingerprintReaderConfigurationFragment = new FingerprintReaderConfigurationFragment_();
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, fingerprintReaderConfigurationFragment, "seleccion_huellero").commit();
                        }
                    }
                });

            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
                sendReportUncaughtException(e);
            }
        }
    }

    @OnActivityResult(REQUEST_BLOCK_CODE_NFC)
    void onBlockNFCResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            try {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag("seleccion_nfc");
                        if (fragment == null) {
                            NFCReaderConfigurationFragment nfcReaderConfigurationFragment = new NFCReaderConfigurationFragment_();
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, nfcReaderConfigurationFragment, "seleccion_nfc").commit();
                        }
                    }
                });

            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
                sendReportUncaughtException(e);
            }
        }
    }

    @OnActivityResult(REQUEST_BLOCK_CODE_EVIDENCE_OFFLINE)
    void onBlockEvidenceOfflineResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            try {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag("evidence_offline");
                        if (fragment == null) {
                            EvidenceOfflineFragment evidenceOfflineFragment = new EvidenceOfflineFragment_();
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, evidenceOfflineFragment, "evidence_offline").commit();
                        }
                    }
                });

            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
                sendReportUncaughtException(e);
            }
        }
    }

    private Properties getProperties(String prop) throws IOException {
        InputStream stream = new ByteArrayInputStream(prop.getBytes(StandardCharsets.UTF_8));

        Properties properties = new Properties();
        properties.load(stream);

        return properties;
    }

    private void updatePreferences(Properties properties) throws Exception {

        if (properties.size() >= 0) {

//            mFingerprintReader.close();
            mPreferences.deleteAll();
            Auditoria_.deleteAllAudit();
            Evidence_.deleteAllEvidences();
            for (Map.Entry<Object, Object> entry : properties
                    .entrySet()) {
                mPreferences.setString(String.valueOf(entry.getKey()),
                        String.valueOf(entry.getValue()));
            }
            UsbPermissionManager.getInstance().updateStatus(mFingerprintManager.findSuitableDevice(), UsbPermissionManager.Permission.PENDING);
            mFingerprintManager = null;
            mPreferences.setString(KeyPreferences.FIRST_START, "false");
            mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
        }
    }


    @UiThread(propagation = UiThread.Propagation.REUSE)
    void showProgressDownload() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle("Descargando configuración...");
        mProgressDialog.setMessage("Por favor, espere...");
        mProgressDialog.show();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void dismissProgressDownload() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void processProperties(Properties properties) {
        String urlDescripcion = "Properties Cargado exitosamente";

        if (properties == null) {
            Toast.makeText(NavegationMenuActivity.this, "Error al cargar configuración de Autentia Móvil", Toast.LENGTH_LONG).show();
            return;
        } else {
            Toast.makeText(NavegationMenuActivity.this, urlDescripcion, Toast.LENGTH_LONG).show();
        }

        try {
            updatePreferences(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
        forceFinishProcess("GetTyC");
        MyServiceSyncAdapter.syncImmediately(this);
    }

    void forceFinishProcess(String conditionFlag) {

//        int pid = android.os.Process.myPid();
//        AlarmManager alm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent newIntent = new Intent(NavegationMenuActivity.this, NavegationMenuActivity_.class);

        //conditions to determinate different flows
        switch (conditionFlag) {
            case "Service":
                mPreferences.setString(KeyPreferences.FIRST_START, "false");
                newIntent.putExtra("IMFROMSERVICE", "TRUE");
                break;
            case "Ready":
                mPreferences.setString(KeyPreferences.DATA_CONFIGURATION, "00002");
                mPreferences.setString(KeyPreferences.FIRST_START, "asdf");
                newIntent.putExtra("IMFROMSERVICE", "FALSE");
                break;
            case "BadConfig":
                Auditoria_.deleteAllAudit();
                Evidence_.deleteAllEvidences();
                mPreferences.setString(KeyPreferences.URL_BASE, URL_PROD);
                mPreferences.setString(KeyPreferences.ENVIROMENT, "PRODUCCION");
                mPreferences.setString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, null);
                mPreferences.setString(KeyPreferences.DATA_CONFIGURATION, "00001");
                mPreferences.setString(KeyPreferences.FIRST_START, "false");
                break;
            case "GetTyC":
                newIntent.putExtra("TyC", true);
                mPreferences.setString(KeyPreferences.FIRST_START, "false");
                break;
            default:
                newIntent.putExtra("IMFROMSERVICE", "FALSE");
                mPreferences.setString(KeyPreferences.FIRST_START, "false");
                break;
        }
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        ProcessPhoenix.triggerRebirth(this, newIntent);

//        alm.set(AlarmManager.RTC, System.currentTimeMillis()+100, PendingIntent.getActivity(NavegationMenuActivity.this, 0, newIntent, 0));
//        android.os.Process.killProcess(pid);
    }

    public void sendReportUncaughtException(Exception exception) {
        HttpUtil.sendReportUncaughtException(exception, this);
    }

    /**
     * AUTO UPDATE NOTIFICATION
     */

    public void showNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!getPackageManager().canRequestPackageInstalls()) {
                Log.e("error", "notgranted");
                Intent canI = new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:com.acepta.android.fetdroid"));
                startActivityForResult(canI, REQUEST_PERMISSION_INSTALL);
            } else {
                new CheckForUpdates(this).checkCurrentVersion();
            }
        } else {
            new CheckForUpdates(this).checkCurrentVersion();
        }

    }

    @OnActivityResult(REQUEST_PERMISSION_INSTALL)
    void onInstallRequest(int resultCode, Intent data) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            if (!getPackageManager().canRequestPackageInstalls()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(NavegationMenuActivity.this);
                @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.layout_terms, null);
                builder.setView(v);
                TextView tvTermsText = (TextView) v.findViewById(R.id.tv_text_terms);
                TextView tvtandCTitle = (TextView) v.findViewById(R.id.TandCTitle);
                tvtandCTitle.setText("Permiso");
                Button btAceptar = (Button) v.findViewById(R.id.btn_acept_terms);
                tvTermsText.setText("Permiso no aceptado no se\npuede instalar actualización");
                builder.create();
                final AlertDialog show = builder.show();
                btAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        show.dismiss();
                    }
                });
            } else {
                new CheckForUpdates(this).checkCurrentVersion();
            }
        }

    }

    /***
     *
     * CHECK LOG IMPLEMENTATION
     *
     * */

    private void initControls() {
        try {
            InputStream in = getAssets().open("changeLog");
            String texto = convertStreamToString(in);
            //TextView tvTermsLink = findViewById(R.id.tv_terms_link);

            final AlertDialog.Builder builder = new AlertDialog.Builder(NavegationMenuActivity.this);
            @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.layout_terms, null);
            builder.setView(v);
            TextView tvTermsText = (TextView) v.findViewById(R.id.tv_text_terms);
            TextView tvtandCTitle = (TextView) v.findViewById(R.id.TandCTitle);
            tvtandCTitle.setText("ChangeLog");
            Button btAceptar = (Button) v.findViewById(R.id.btn_acept_terms);
            tvTermsText.setText(texto);
            builder.create();
            final AlertDialog show = builder.show();
            btAceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    show.dismiss();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    public void onFingerprintAccessGranted() {
        mFingerprintCurrentState = InternalState.READY;
        mFingerprintReader = mFingerprintManager.getReader(this);
        new ConfigurationHandler(this).loadConfiguration();
    }

    @Override
    public void onFingerprintAccessDenied() {
        Log.i(TAG, "DENIEEEED");

//        errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.ACCESO_HUELLERO_DENEGADO));
//        finishActivityWithError(Status.NO_OK,
//                ReturnCode.ACCESO_HUELLERO_DENEGADO.getCode(),
//                ReturnCode.ACCESO_HUELLERO_DENEGADO.getDescription());

    }

    @Override
    public void onFingerprintAccessError() {
        Log.e(TAG, "accesserror");

//        errorHandlerMethod(Status.NO_OK, new AutentiaMovilException(ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO));
//        finishActivityWithError(Status.NO_OK,
//                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO.getCode(),
//                ReturnCode.ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO.getDescription());

    }

    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    @OnActivityResult(value = REQUEST_FINGERPRINT_ACCESS)
    protected void onRequestFingerprintAccessResult(int resultCode, Intent data) {
        Log.e("REQUEST_FINGER_ACCESS", "IM HERE!");
        mFingerprintManager.processAccessGrant(resultCode, data, this);
    }

    LocalBroadcastManager mLocalBroadcastManager;
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("mBroadcastReceiver", "HERE");
            if (intent.getAction().equals("com.durga.action.close")) {
                Log.i("onReceive", "I HAVE TO CLOSE NOW :CCCC");
                mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                mPreferences.setString(KeyPreferences.FIRST_START, "false");
                forceFinishProcess("Service");
            } else if (intent.getAction().equals("com.durga.action.reserve")) {
                Log.e("reserve", "IM HERE");
                showErrorConfigAlert(intent.getStringExtra("message"));
            }
        }
    };


    public void showErrorConfigAlert(final String message) {

        if (mActivityFinal != null)
            mActivityFinal.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog alertDialog = new AlertDialog.Builder(mActivityFinal).create();
                    alertDialog.setTitle("Error de configuración");
                    alertDialog.setMessage(message + " La aplicación se reiniciará con su configuración inicial.");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    forceFinishProcess("BadConfig");
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            });
    }

    private void showDialogError(String message) {
        if (optionDialog != null) {
            dismissDialog();
        }
        AlertDialog alertDialog = new AlertDialog.Builder(mActivityFinal).create();
        alertDialog.setIcon(R.drawable.autentia_logo_blue);
        alertDialog.setTitle("Verificacion Operador");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void dismissDialog() {

        if (mActivityFinal != null)
            mActivityFinal.runOnUiThread(() -> {
                if (optionDialog.isShowing())
                    optionDialog.dismiss();
            });
    }
}