package cl.autentia.FirebaseService;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import cl.autentia.FirebaseService.params.TyCValuesParams;
import cl.autentia.configuration.parameters.autoUpdateResp;
import cl.autentia.helper.AppHelper;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

import static cl.autentia.sync.MyServiceSync.JOB_ID;

public class FirebaseValueEventListenerService extends JobIntentService {

    private AutentiaPreferences mAutentiaPreferences;
//    private ServiceHandler mServiceHandler;
    private static final String TAG = "FbValueEventService";
    Context mContext = this;
    private DatabaseReference mDatabaseActualizacion;
    private DatabaseReference mDatabaseTyC;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, FirebaseValueEventListenerService.class, JOB_ID, work);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        mAutentiaPreferences = new AutentiaPreferences(mContext);
        if(mDatabaseActualizacion == null){
            mDatabaseActualizacion = FirebaseDatabase.getInstance().getReference("Actualizacion");
        }
        if(mDatabaseTyC == null){
            mDatabaseTyC = FirebaseDatabase.getInstance().getReference("TerminosCondiciones");
        }


        mDatabaseActualizacion.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot == null) return;

                autoUpdateResp aur = dataSnapshot.getValue(autoUpdateResp.class);
                assert aur != null;

                if (checkCreatedAt(aur.getCreatedAt()) && aur.getAmbiente().equals(mAutentiaPreferences.getString(KeyPreferences.ENVIROMENT))){
                    mAutentiaPreferences.setLong(KeyPreferences.FIREBASE_CREATED_AT_VALUE,aur.getCreatedAt());
                    if (checkVersion(aur.getVersion())) {
                        if (aur.getInstitucion() != null) {
                            if (aur.getInstitucion().equals(mAutentiaPreferences.getString(KeyPreferences.INSTITUTION)) || aur.getInstitucion().equals("TODAS")) {//si existen actualizaciones
                                Log.d(TAG, "Es necesario actualizar");
                                Bundle bundle = new Bundle();
                                bundle.putString("apkUrl",aur.getUrl());
                                bundle.putString("institucion",aur.getInstitucion());
                                bundle.putString("version",aur.getVersion());
                                bundle.putInt("priority",aur.getPriority());
                                bundle.putLong("createdat",aur.getCreatedAt());
                                Intent dialogIntent = new Intent(mContext, DialogActivity.class);
                                dialogIntent.putExtra("data",bundle);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(dialogIntent);
                            }
                        }
                    } else {
                        Log.d(TAG, "No se necesita actualizar");
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Failed to read value
                Log.e(TAG, "Failed to read value.", databaseError.toException());
            }
        });

        mDatabaseTyC.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue()==null) return;

                if(mAutentiaPreferences.containsKey(KeyPreferences.TERMS_AND_CONDITIONS)){
                    TerminosParam tp = new Gson().fromJson(mAutentiaPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS),TerminosParam.class);

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        TyCValuesParams mTyCvalues = postSnapshot.getValue(TyCValuesParams.class);

                        assert mTyCvalues != null;
                        if(mTyCvalues.getDefaultInstit()!=null) {
                            if (mTyCvalues.getInstitucion().equalsIgnoreCase(mAutentiaPreferences.getString(KeyPreferences.INSTITUTION)) && !mTyCvalues.getDefaultInstit().equalsIgnoreCase(tp.getDefaultInstit())) {
                                tp.setDefaultInstit(mTyCvalues.getDefaultInstit());
                            }
                        }else {
                            if (mTyCvalues.getInstitucion().equals(mAutentiaPreferences.getString(KeyPreferences.INSTITUTION))) {
                                tp.setDefaultInstit("");
                            }
                        }
                    }

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        TyCValuesParams mTyCvalues = postSnapshot.getValue(TyCValuesParams.class);

                        if(tp.getDefaultInstit().equals("")){ //sino dependo de otros TyC
                            assert mTyCvalues != null;
                            if (mTyCvalues.getInstitucion().equals(mAutentiaPreferences.getString(KeyPreferences.INSTITUTION))){
                                if(mTyCvalues.getCreatedAt() > Long.parseLong(tp.getCreatedAt())){ //necesito actualizar los terminos
                                    updateTyC(mTyCvalues,tp);
                                }else{ //no necesito actualizar los terminos
                                }

                            }
                        }else{ //Si dependo de otros TyC

                            assert mTyCvalues != null;
                            if (mTyCvalues.getInstitucion().equals(tp.getDefaultInstit())){
                                if(mTyCvalues.getCreatedAt() != Long.parseLong(tp.getCreatedAt())){ //necesito actualizar los terminos
                                    updateTyC(mTyCvalues,tp);
                                }else{ //no necesito actualizar los terminos
                                    Log.e("Last TyC","Updated");
                                }

                            }
                        }
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Failed to read value
                Log.e(TAG, "Failed to read value.", databaseError.toException());

            }
        });

    }

    private void updateTyC(TyCValuesParams mTyCvalues, TerminosParam tp) {

        if(mTyCvalues.getCreatedAt() != null)  tp.setCreatedAt(mTyCvalues.getCreatedAt().toString());
        else tp.setCreatedAt("0");

        if(tp.getDefaultInstit() == null) tp.setDefaultInstit("");
        else tp.setDefaultInstit(tp.getDefaultInstit());

        tp.setInstitucion(mAutentiaPreferences.getString(KeyPreferences.INSTITUTION));
        tp.setTerminos(mTyCvalues.getTerminos());
        tp.setGlosa("");
        mAutentiaPreferences.setString(KeyPreferences.TERMS_AND_CONDITIONS,new Gson().toJson(tp));

    }

    @Override
    public void onDestroy() {
//        Log.d(TAG, "service done");
        Intent broadcastIntent = new Intent(this, FbValueEventReceiver.class);
        sendBroadcast(broadcastIntent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service starting");
        return START_STICKY;
    }

    private boolean checkVersion(String apkVersion) {
        Double versionWS = Double.parseDouble(apkVersion.replace(".", ""));
        Double versionAndroid = null;
        try {
            versionAndroid = Double.parseDouble(AppHelper.getCurrentVersion(mContext).replace(".", ""));
            if (versionWS > versionAndroid) {
                return true; //nueva version
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false; //no es necesario la actualizacion
    }

    private boolean checkCreatedAt(long checkout){

        if (mAutentiaPreferences.containsKey(KeyPreferences.FIREBASE_CREATED_AT_VALUE)) {
//            Log.e("if", "here");
//            Log.e("if", ""+ mAutentiaPreferences.getLong(KeyPreferences.FIREBASE_CREATED_AT_VALUE));
//            Log.e("if", ""+ checkout);
            return mAutentiaPreferences.getLong(KeyPreferences.FIREBASE_CREATED_AT_VALUE) != checkout;
        }else{
//            Log.e("else","here");
//            Log.e("else", ""+ mAutentiaPreferences.getLong(KeyPreferences.FIREBASE_CREATED_AT_VALUE));
//            Log.e("else", ""+ checkout);
            mAutentiaPreferences.setLong(KeyPreferences.FIREBASE_CREATED_AT_VALUE,checkout);
            return false;
        }
    }


}
