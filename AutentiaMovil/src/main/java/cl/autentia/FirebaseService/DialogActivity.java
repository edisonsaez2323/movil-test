package cl.autentia.FirebaseService;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.acepta.android.fetdroid.R;

import java.util.Calendar;

import cl.autentia.configuration.CheckForUpdates;

public class DialogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        if (getIntent().hasExtra("data")){
            final Bundle bundle = getIntent().getBundleExtra("data");
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            View v = getLayoutInflater().inflate(R.layout.progress_dialog_upd, null);
            alertDialog.setTitle("Nueva actualización");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new CheckForUpdates(DialogActivity.this).downloadAndUpdate(bundle.getString("apkUrl"));
                    finish();
                }
            });
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (bundle.getInt("priority",-1)){
                        case 1: //HIGH PRIORITY
                            Log.d("switch","priority 1");
                            alarmManagerTask(0);
                            //todo: determinar comportamiento.
                            break;
                        case 2: //MIDDLE PRIORITY
                            Log.d("switch","priority 2");
                            alarmManagerTask(24);
                            break;
                        case 3: //LOW PRIORITY
                            Log.d("switch","priority 3"); //no fijamos un nuevo recordatorio si el usuario cancela.
//                            alarmManagerTask(5);
                            int pid = android.os.Process.myPid();
                            android.os.Process.killProcess(pid);
                            break;
                        default:
                            Log.d("switch","default");
                            break;
                    }

                }
            });

            alertDialog.setView(v);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            alertDialog.setIcon(R.drawable.ic_notification_icon);
            alertDialog.show();
        }else{ //sino viene el bundle
            Log.e("DialogActivity","falta bundle Data");
        }

    }

    public void alarmManagerTask(int hour){

        int pid = android.os.Process.myPid();
        Calendar c = Calendar.getInstance();
        long howMany;
        if(hour == 0){
            c.setTimeInMillis(System.currentTimeMillis());
            c.add(Calendar.HOUR, 1);
            howMany = (c.getTimeInMillis()-System.currentTimeMillis());
            Log.e("Diff in milliseconds",""+howMany);
        }else{
//            c.add(Calendar.DAY_OF_MONTH,minute);
            c.setTimeInMillis(System.currentTimeMillis());
            c.add(Calendar.HOUR, hour);
            howMany = (c.getTimeInMillis()-System.currentTimeMillis());
            Log.e("Diff in minutes",""+howMany/1000);
        }

        final Bundle bundle = getIntent().getBundleExtra("data");
        Intent intent = new Intent(DialogActivity.this,DialogActivity.class);
        intent.putExtra("data",bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        AlarmManager alm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alm.set(AlarmManager.RTC, c.getTimeInMillis(), PendingIntent.getActivity(DialogActivity.this, 0, intent, 0));
        android.os.Process.killProcess(pid);

    }

}
