package cl.autentia.FirebaseService.params;

public class TyCValuesParams {

    Long createdAt;
    String terminos;
    String defaultInstit;
    String institucion;
    Long status;

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getTerminos() {
        return terminos;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    public String getDefaultInstit() {
        return defaultInstit;
    }

    public void setDefaultInstit(String defaultInstit) {
        this.defaultInstit = defaultInstit;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
