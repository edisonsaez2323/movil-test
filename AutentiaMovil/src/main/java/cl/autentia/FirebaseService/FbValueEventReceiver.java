package cl.autentia.FirebaseService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class FbValueEventReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        FirebaseValueEventListenerService.enqueueWork(context,new Intent(context, FirebaseValueEventListenerService.class));
    }

}
