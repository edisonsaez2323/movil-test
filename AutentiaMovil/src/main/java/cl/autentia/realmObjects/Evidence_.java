package cl.autentia.realmObjects;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Edison on 07-02-2019.
 */

public class Evidence_ extends RealmObject {

    @PrimaryKey
    public int idVerification;
    public String jsonData;
    public String audit;
    public String type;
    public byte[] fingerprint;
    public byte[] attachments;
    public byte[] fingerprintBmp;
    public String createDate;
    //
    public String firma;
    public String retrato;


    private final static int calculateIndex() {
        try (Realm realm = Realm.getDefaultInstance()) {

            Number currentIdNum = realm.where(Evidence_.class).max("idVerification");
            int nextId;
            if (currentIdNum == null) {
                nextId = new Random().nextInt();
            } else {
                nextId = currentIdNum.intValue() + 1;
            }
            return nextId;
        }

    }

    public final static void addAuditoria(final Evidence_ evidence) {
        Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int index = Evidence_.calculateIndex();
                    Evidence_ evidence_ = realm.createObject(Evidence_.class, index);
                    evidence_.setJsonData(evidence.getJsonData());
                    evidence_.setAudit(evidence.getAudit());
                    evidence_.setType(evidence.getType());
                    evidence_.setFingerprint(evidence.getFingerprint());
                    evidence_.setFingerprintBmp(evidence.getFingerprintBmp());
                    evidence_.setAttachments(evidence.getAttachments());
                    evidence_.setCreateDate(evidence.getCreateDate());
                    if (evidence.getFirma()!= null)
                        evidence_.setFirma(evidence.getFirma());
                    if (evidence.getRetrato() != null)
                        evidence_.setRetrato(evidence.getRetrato());
                }
            });

    }

    public static int getEvicenceCount() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Evidence_> Evicences = realm.where(Evidence_.class).findAll();
        return Evicences.size();
    }

    public static List<Evidence_> getAllEvicences() {
        Realm realm = Realm.getDefaultInstance();
        List<Evidence_> evidences = realm.where(Evidence_.class).findAll().subList(0, getEvicenceCount());
        return evidences;
    }

    public static void deleteAllEvidences() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Evidence_> EvidencesDelete = realm.where(Evidence_.class).findAll();
                EvidencesDelete.deleteAllFromRealm();
            }
        });

    }

    public static void deleteEvidence(final String audit) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Evidence_> EvidenceDelete = realm.where(Evidence_.class).equalTo("audit", audit).findAll();
                EvidenceDelete.deleteAllFromRealm();
            }
        });
    }


    public int getIdVerification() {
        return idVerification;
    }

    public void setIdVerification(int idVerification) {
        this.idVerification = idVerification;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getAudit() {
        return audit;
    }

    public void setAudit(String audit) {
        this.audit = audit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(byte[] fingerprint) {
        this.fingerprint = fingerprint;
    }

    public byte[] getAttachments() {
        return attachments;
    }

    public void setAttachments(byte[] attachments) {
        this.attachments = attachments;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public byte[] getFingerprintBmp() {
        return fingerprintBmp;
    }

    public void setFingerprintBmp(byte[] fingerprintBmp) {
        this.fingerprintBmp = fingerprintBmp;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getRetrato() {
        return retrato;
    }

    public void setRetrato(String retrato) {
        this.retrato = retrato;
    }
}
