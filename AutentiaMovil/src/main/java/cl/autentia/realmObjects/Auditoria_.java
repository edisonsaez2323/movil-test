package cl.autentia.realmObjects;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Edison on 07-02-2019.
 */

public class Auditoria_ extends RealmObject {

    @PrimaryKey
    public int idAuditoria;
    public String numeroAuditoria;
    public boolean isUsed;


    private final static int calculateIndex() {
        Realm realm = Realm.getDefaultInstance();
        Number currentIdNum = realm.where(Auditoria_.class).max("idAuditoria");
        int nextId;
        if (currentIdNum == null) {
            nextId = 0;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        return nextId;
    }

    public final static void addAuditoria(final Auditoria_ auditoria_) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                int index = Auditoria_.calculateIndex();
                Log.i("Audit - index", String.valueOf(index));
                Auditoria_ auditoria = realm.createObject(Auditoria_.class, index);
                auditoria.setNumeroAuditoria(auditoria_.getNumeroAuditoria());
                auditoria.setUsed(false);
                Log.i("Auditoria", auditoria_.getNumeroAuditoria());
            }
        });
        realm.close();
    }

    public static List<Auditoria_> getAudits() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Auditoria_.class)
                .sort("idAuditoria", Sort.ASCENDING)
                .findAll().subList(0,getAuditCount());
    }

    public static int getAuditCount() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Auditoria_> Auditorias = realm.where(Auditoria_.class).findAll();
        return Auditorias.size();
    }

    public static String getAudit() {
        Realm realm = Realm.getDefaultInstance();
        Auditoria_ auditoria_ = realm.where(Auditoria_.class).findFirst();

        return auditoria_.getNumeroAuditoria();
    }

    public static void deleteAudit(final String auditoria) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Auditoria_> auditDelete = realm.where(Auditoria_.class).equalTo("numeroAuditoria", auditoria).findAll();
                auditDelete.deleteAllFromRealm();
            }
        });
//        realm.close();
    }

    public static void deleteAllAudit() {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Auditoria_> auditDelete = realm.where(Auditoria_.class).findAll();
                auditDelete.deleteAllFromRealm();
            }
        });
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public int getIdAuditoria() {
        return idAuditoria;
    }

    public void setIdAuditoria(int idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public String getNumeroAuditoria() {
        return numeroAuditoria;
    }

    public void setNumeroAuditoria(String numeroAuditoria) {
        this.numeroAuditoria = numeroAuditoria;
    }
}
