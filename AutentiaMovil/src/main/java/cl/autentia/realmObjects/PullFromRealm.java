package cl.autentia.realmObjects;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import cl.autentia.http.parameters.AuditoriaData;
import cl.autentia.http.parameters.UpdateAuditParam;

public class PullFromRealm extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        retrieveData();
    }

    public void retrieveData() {

        try {

            ArrayList<String> urls = new ArrayList<>();

            List<Evidence_> evidencesOffline = Evidence_.getAllEvicences();

            for (Evidence_ offline : evidencesOffline) {

                JSONObject jsonEvidence = new JSONObject(offline.jsonData);
                String tipoEvidencia = jsonEvidence.getString("type");
                String numAuditoria = jsonEvidence.getString("codigoAuditoria");
                String institution = jsonEvidence.getString("institucion");
                String origen = jsonEvidence.getString("origen");
                String operacion = jsonEvidence.getString("operacion");
                String numSerieLector = jsonEvidence.getString("numeroSerieLector");
                String rutOperador = jsonEvidence.getString("rutOperador");
                String resultadoVerificacion = jsonEvidence.getString("resultado");
                String descripcionJsonData = jsonEvidence.toString();
                String versionApp = jsonEvidence.getString("versionApp");
                String rutPersona = String.format("%s-%s", jsonEvidence.getString("rut"), jsonEvidence.getString("dv"));
                String nombre = jsonEvidence.getString("nombre");
                String dedoId = jsonEvidence.getString("dedoCodificado");
                String dedoFecha = jsonEvidence.getString("dedoFecha");
                String ubicacion = jsonEvidence.getString("ubicacion");
                String tipoLector = jsonEvidence.getString("tipoLector");
                String texto1 = "";
                String texto2 = "";
                if (tipoEvidencia.equals("Evidencia")) {
                    texto1 = "indicio";
                    texto2 = "Toma de Evidencia, LA VERIFICACION ES RESPONSABILIDAD DEL CLIENTE";
                }

                AuditoriaData auditoriaData = new Gson().fromJson(descripcionJsonData, AuditoriaData.class);

                UpdateAuditParam uap = new UpdateAuditParam();
                uap.setAuditoria(numAuditoria);
                uap.setInstitucion(institution);
                uap.setOrigen(origen);
                uap.setOperacion(operacion);
                uap.setNroSerie(numSerieLector);
                uap.setTipoLector(tipoLector);
                uap.setRutOperador(rutOperador);
                uap.setResultado(resultadoVerificacion);
                uap.setDescripcion(auditoriaData);
                uap.setVersion(versionApp);
                uap.setRutPersona(rutPersona);
                uap.setNombre(nombre);
                uap.setDedoId(dedoId);
                uap.setDedoFecha(dedoFecha);
                uap.setUbicacion(ubicacion);
                uap.setTexto1(texto1);
                uap.setMuestra(Base64.encodeToString(offline.fingerprint, 0).replace("\n", ""));
                uap.setBitmap(Base64.encodeToString(offline.fingerprintBmp, 0).replace("\n", ""));
                uap.setTexto2(texto2);


                org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(uap));

                Writer output = null;
                File file = new File(Environment.getExternalStorageDirectory()+"/AutentiaMovil/" + numAuditoria + ".json");
                output = new BufferedWriter(new FileWriter(file));
                output.write(jsonObject.toString());
                output.close();

                urls.add(file.getAbsolutePath());

            }

            Intent intent = new Intent();
            intent.putStringArrayListExtra("evidencias",urls);
            setResult(RESULT_OK, intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
