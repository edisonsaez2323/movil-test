package cl.autentia.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cl.autentia.configuration.CheckForUpdates;
import cl.autentia.sync.MyServiceSync;

public class RebootReceiver extends BroadcastReceiver {

    private static final String TAG = RebootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {

            Intent serviceIntent = new Intent(context, MyServiceSync.class);
            MyServiceSync.enqueueWork(context,serviceIntent);

            Log.d(TAG, "Reboot complete!");

        }
    }
}
