package cl.autentia.http;

public class UrlList {

    static String GET_TOKEN = "getToken";
    static String GET_AVA_FING = "muestrasDisponibles";
    static String GET_VAL_NEC = "validarNec";
    static String GET_MUESTRA = "validarMuestra";
    static String GET_UPD_AUD = "actualizarAuditoria";
    static String GET_PRE_AUD = "preAuditoria";
    static String GEN_AUDIT = "generarAuditoria";
    static String GET_STA_NEC = "consultarCedula";
    static String GET_INFO_AUD = "infoAuditoria";
    static String GET_AUTO_CONFIG_DATA = "getInstitInfo";
    static String CHECK_VERSION_AUTO_UPDATE = "checkVersion";
    static String GET_AUD_RECH = "audRech";
    static String GET_OPERADOR = "verificaRol";
    static String VERIFICA_SENSOR = "verificaSensor";
    //transunion
    public static String POST_ANSWERS = "enviarRespuestas";
    public static String GET_QUESTIONS = "obtenerPreguntas";
    public static String REGISDEVICE = "registraDisp";
    //facial
    public static String OAUTHFACIALAUDIT = "auditFacial";
    //terminos y condiciones
    static String TERMS_AND_CONDITION_URL = "obtenerTerminos";

}
