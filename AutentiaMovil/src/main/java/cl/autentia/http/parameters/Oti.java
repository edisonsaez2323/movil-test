package cl.autentia.http.parameters;

public class Oti {

   private Validacion validacion;
   private String urlOti;

    public String getUrlOti() {
        return urlOti;
    }

    public void setUrlOti(String urlOti) {
        this.urlOti = urlOti;
    }

    public Validacion getValidacion() {
        return validacion;
    }

    public void setValidacion(Validacion validacion) {
        this.validacion = validacion;
    }
}
