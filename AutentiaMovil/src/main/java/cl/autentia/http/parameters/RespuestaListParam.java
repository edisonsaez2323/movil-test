package cl.autentia.http.parameters;

import java.util.ArrayList;
import java.util.List;

public class RespuestaListParam {

    private String rut;
    private String institucion;
    private String origen;
    private String operacion;
    private String rutOperador;
    private String versionApp;
    private String numeroSerieLector;
    private String nroSerieCedula;
    private String idCredencials;
    private String proposito;
    private List<RespuestaParam> respuestas = new ArrayList<RespuestaParam>();
    private String trackId;

    public String getProposito() {
        return proposito;
    }

    public void setProposito(String proposito) {
        this.proposito = proposito;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public List<RespuestaParam> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<RespuestaParam> respuestas) {
        this.respuestas = respuestas;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getNumeroSerieLector() {
        return numeroSerieLector;
    }

    public void setNumeroSerieLector(String numeroSerieLector) {
        this.numeroSerieLector = numeroSerieLector;
    }

    public String getNroSerieCedula() {
        return nroSerieCedula;
    }

    public void setNroSerieCedula(String nroSerieCedula) {
        this.nroSerieCedula = nroSerieCedula;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getIdCredencials() {
        return idCredencials;
    }

    public void setIdCredencials(String idCredencials) {
        this.idCredencials = idCredencials;
    }

    public String getTrackID() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }
}
