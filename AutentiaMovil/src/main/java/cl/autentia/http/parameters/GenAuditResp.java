package cl.autentia.http.parameters;

public class GenAuditResp extends GenericResp {

    String nroAuditoria;
    Oti oti;

    public Oti getOti() {
        return oti;
    }

    public void setOti(Oti oti) {
        this.oti = oti;
    }

    public String getNroAuditoria() {
        return nroAuditoria;
    }

    public void setNroAuditoria(String nroAuditoria) {
        this.nroAuditoria = nroAuditoria;
    }
}
