
package cl.autentia.http.parameters;


public class OperadorResp {

    private Integer status;
    private String glosa;
    private Boolean rolVerificado;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public Boolean getRolVerificado() {
        return rolVerificado;
    }

    public void setRolVerificado(Boolean rolVerificado) {
        this.rolVerificado = rolVerificado;
    }

}
