package cl.autentia.http.parameters;

public class ValidFingerprintResp extends GenericResp {

    private String nroAuditoria;
    private String nombre;
    private String fechaNac;
    private String jsonUrl;
    private Oti oti;

    public Oti getOti() {
        return oti;
    }

    public void setOti(Oti oti) {
        this.oti = oti;
    }

    public String getNroAuditoria() {
        return nroAuditoria;
    }

    public void setNroAuditoria(String nroAuditoria) {
        this.nroAuditoria = nroAuditoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getJsonUrl() {
        return jsonUrl;
    }

    public void setJsonUrl(String jsonUrl) {
        this.jsonUrl = jsonUrl;
    }

}
