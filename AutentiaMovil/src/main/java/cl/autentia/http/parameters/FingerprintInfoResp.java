
package cl.autentia.http.parameters;


public class FingerprintInfoResp {

    private Integer status;
    private String glosa;
    private String institucion;
    private String codLugar;
    private String descripcion;
    private String tipoLogon;
    private String rutLogon;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getCodLugar() {
        return codLugar;
    }

    public void setCodLugar(String codLugar) {
        this.codLugar = codLugar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoLogon() {
        return tipoLogon;
    }

    public void setTipoLogon(String tipoLogon) {
        this.tipoLogon = tipoLogon;
    }

    public String getRutLogon() {
        return rutLogon;
    }

    public void setRutLogon(String rutLogon) {
        this.rutLogon = rutLogon;
    }

}
