package cl.autentia.http.parameters;

public class GenericResp {

    private int status = -1;
    private String glosa = "Error Genérico";

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }
}
