
package cl.autentia.http.parameters;


public class Validacion {

    private String codigoVerificacion;
    private UsuarioVerificado usuarioVerificado;
    private String codigoInstitucion;
    private Verificacion verificacion;

    public String getCodigoVerificacion() {
        return codigoVerificacion;
    }

    public void setCodigoVerificacion(String codigoVerificacion) {
        this.codigoVerificacion = codigoVerificacion;
    }

    public UsuarioVerificado getUsuarioVerificado() {
        return usuarioVerificado;
    }

    public void setUsuarioVerificado(UsuarioVerificado usuarioVerificado) {
        this.usuarioVerificado = usuarioVerificado;
    }

    public String getCodigoInstitucion() {
        return codigoInstitucion;
    }

    public void setCodigoInstitucion(String codigoInstitucion) {
        this.codigoInstitucion = codigoInstitucion;
    }

    public Verificacion getVerificacion() {
        return verificacion;
    }

    public void setVerificacion(Verificacion verificacion) {
        this.verificacion = verificacion;
    }

}
