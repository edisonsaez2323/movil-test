
package cl.autentia.http.parameters;


public class Operador {

    private String rutOperador;
    private String institucion;
    private String rol;

    public String getRutOperador() {
        return rutOperador;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
