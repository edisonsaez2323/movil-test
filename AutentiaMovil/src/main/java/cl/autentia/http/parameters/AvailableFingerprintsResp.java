package cl.autentia.http.parameters;

import java.util.ArrayList;
import java.util.List;

public class AvailableFingerprintsResp extends GenericResp {

    private String id;
    private String nombre;
    private String fechaNac;
    private List<String> muestrasString = new ArrayList<String>();
    //private String muestras;

    public AvailableFingerprintsResp() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public List<String> getMuestrasString() {
        return muestrasString;
    }

    public void setMuestrasString(List<String> muestrasString) {
        this.muestrasString = muestrasString;
    }
}
