
package cl.autentia.http.parameters;


public class Terminos {

    private Integer status;
    private String glosa;
    private String institucion;
    private String defaultInstit;
    private String terminos;
    private Long createdAt;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getDefaultInstit() {
        return defaultInstit;
    }

    public void setDefaultInstit(String defaultInstit) {
        this.defaultInstit = defaultInstit;
    }

    public String getTerminos() {
        return terminos;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

}
