package cl.autentia.http.parameters;

public class ValidFingerprintParam {

    private String run;
    private AuditoriaData descripcion;
    private String rutOperador;
    private String versionApp;
    private String ubicacion;
    private String porFirmar;
    private String muestra;
    private String muestraBitmap;
    private String tipoLector;
    private String textoAdjunto;
    private String valorAdjunto;
    private String institucion;
    private String nroSerie;
    private String oti;

    //OPCIONAL
    private String texto3;

    public String getTexto3() {
        return texto3;
    }

    public void setTexto3(String texto3) {
        this.texto3 = texto3;
    }

    //FIN OPCIONAL

    public String getOti() {
        return oti;
    }

    public void setOti(String oti) {
        this.oti = oti;
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public AuditoriaData getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(AuditoriaData descripcion) {
        this.descripcion = descripcion;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getPorFirmar() {
        return porFirmar;
    }

    public void setPorFirmar(String porFirmar) {
        this.porFirmar = porFirmar;
    }

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    public String getMuestraBitmap() {
        return muestraBitmap;
    }

    public void setMuestraBitmap(String muestraBitmap) {
        this.muestraBitmap = muestraBitmap;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public String getTextoAdjunto() {
        return textoAdjunto;
    }

    public void setTextoAdjunto(String textoAdjunto) {
        this.textoAdjunto = textoAdjunto;
    }

    public String getValorAdjunto() {
        return valorAdjunto;
    }

    public void setValorAdjunto(String valorAdjunto) {
        this.valorAdjunto = valorAdjunto;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }
}
