package cl.autentia.http.parameters;

public class GetConfigDataParam {

    String nroSerie;

    public String getNroSerie() {
        return nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }
}
