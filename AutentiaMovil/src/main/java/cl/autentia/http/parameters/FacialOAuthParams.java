package cl.autentia.http.parameters;

public class FacialOAuthParams {

    String idToken;
    String nroSerieLector;
    String tipoLector;
    String resultado;
    String operacion;
    String institucion;
    String rutInstitucion;
    String versionApp;

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getRutInstitucion() {
        return rutInstitucion;
    }

    public void setRutInstitucion(String rutInstitucion) {
        this.rutInstitucion = rutInstitucion;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getNroSerieLector() {
        return nroSerieLector;
    }

    public void setNroSerieLector(String nroSerieLector) {
        this.nroSerieLector = nroSerieLector;
    }
}
