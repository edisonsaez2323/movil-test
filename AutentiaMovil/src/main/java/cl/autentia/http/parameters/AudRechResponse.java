
package cl.autentia.http.parameters;


public class AudRechResponse {

    private Integer status;
    private String glosa;
    private Integer todos;
    private Integer sinPat;
    private Integer maxInt;
    private Integer noEnro;
    private Integer noAcep;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public Integer getTodos() {
        return todos;
    }

    public void setTodos(Integer todos) {
        this.todos = todos;
    }

    public Integer getSinPat() {
        return sinPat;
    }

    public void setSinPat(Integer sinPat) {
        this.sinPat = sinPat;
    }

    public Integer getMaxInt() {
        return maxInt;
    }

    public void setMaxInt(Integer maxInt) {
        this.maxInt = maxInt;
    }

    public Integer getNoEnro() {
        return noEnro;
    }

    public void setNoEnro(Integer noEnro) {
        this.noEnro = noEnro;
    }

    public Integer getNoAcep() {
        return noAcep;
    }

    public void setNoAcep(Integer noAcep) {
        this.noAcep = noAcep;
    }

}
