package cl.autentia.http.parameters;

public class GetStatusNECResp extends GenericResp {

    private String estado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
