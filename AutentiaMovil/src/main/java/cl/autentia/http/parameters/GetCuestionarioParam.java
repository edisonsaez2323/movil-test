package cl.autentia.http.parameters;

public class GetCuestionarioParam {

    String rut;
    String nroSerie;
    String nroSerieDocumento;
    String inquirySource;
    String tipoDocumento;
    private String idCredencials;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public String getNroSerieDocumento() {
        return nroSerieDocumento;
    }

    public void setNroSerieDocumento(String nroSerieDocumento) {
        this.nroSerieDocumento = nroSerieDocumento;
    }

    public String getInquirySource() {
        return inquirySource;
    }

    public void setInquirySource(String inquirySource) {
        this.inquirySource = inquirySource;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getIdCredencials() {
        return idCredencials;
    }

    public void setIdCredencials(String idCredencials) {
        this.idCredencials = idCredencials;
    }
}
