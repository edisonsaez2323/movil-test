package cl.autentia.http.parameters;

public class RespuestaParam {

    private String idPregunta;
    private String idRespuesta;

    public RespuestaParam(String idPregunta, String idRespuesta) {
        this.idPregunta = idPregunta;
        this.idRespuesta = idRespuesta;
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(String idRespuesta) {
        this.idRespuesta = idRespuesta;
    }
}
