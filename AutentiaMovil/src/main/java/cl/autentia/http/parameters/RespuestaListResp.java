package cl.autentia.http.parameters;

public class RespuestaListResp extends GenericResp {

    String codigoAuditoria;
    String decision;
    String auditoria;

    public String getCodigoAuditoria() {
        return codigoAuditoria;
    }

    public void setCodigoAuditoria(String codigoAuditoria) {
        this.codigoAuditoria = codigoAuditoria;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getAuditoria() {
        return auditoria;
    }

    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }
}
