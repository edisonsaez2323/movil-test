package cl.autentia.http.parameters;

public class VolleyRespError extends GenericResp {

    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
