package cl.autentia.http.parameters;

public class ValidNecParam {

    private String wsq;
    private String pdf417;
    private String formatoMuestra;
    private String run;
    private String dedo;
    private String tipoLector;
    private String nroSerie;
    private AuditoriaData descripcion;
    private String rutOperador;
    private String versionApp;
    private String ubicacion;
    private String textoAdjunto;
    private String valorAdjunto;
    private String institucion;
    private String oti;
    private String bitmap;

    public String getBitmap() {
        return bitmap;
    }

    public void setBitmap(String bitmap) {
        this.bitmap = bitmap;
    }

    public String getOti() {
        return oti;
    }

    public void setOti(String oti) {
        this.oti = oti;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public void setDedo(String dedo) {
        this.dedo = dedo;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public void setDescripcion(AuditoriaData descripcion) {
        this.descripcion = descripcion;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setTextoAdjunto(String textoAdjunto) {
        this.textoAdjunto = textoAdjunto;
    }

    public void setValorAdjunto(String valorAdjunto) {
        this.valorAdjunto = valorAdjunto;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public void setFormatoMuestra(String formatoMuestra) {
        this.formatoMuestra = formatoMuestra;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public void setWsq(String wsq) {
        this.wsq = wsq;
    }

    public void setPdf417(String pdf417) {
        this.pdf417 = pdf417;
    }

    public String getRun() {
        return run;
    }

    public String getDedo() {
        return dedo;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public AuditoriaData getDescripcion() {
        return descripcion;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getTextoAdjunto() {
        return textoAdjunto;
    }

    public String getValorAdjunto() {
        return valorAdjunto;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public String getFormatoMuestra() {
        return formatoMuestra;
    }

    public String getInstitucion() {
        return institucion;
    }

    public String getWsq() {
        return wsq;
    }

    public String getPdf417() {
        return pdf417;
    }
}
