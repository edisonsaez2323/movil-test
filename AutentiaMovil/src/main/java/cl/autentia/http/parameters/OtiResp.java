
package cl.autentia.http.parameters;


public class OtiResp {

    private Integer codigoRespuesta;
    private Object mensajeRespuesta;
    private Validacion validacion;

    public Integer getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(Integer codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public Object getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    public void setMensajeRespuesta(Object mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }

    public Validacion getValidacion() {
        return validacion;
    }

    public void setValidacion(Validacion validacion) {
        this.validacion = validacion;
    }

}
