package cl.autentia.http.parameters;

public class AudRechParams {

    private String institucion;

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }
}
