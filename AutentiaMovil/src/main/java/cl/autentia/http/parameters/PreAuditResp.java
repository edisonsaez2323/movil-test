package cl.autentia.http.parameters;

import java.util.ArrayList;
import java.util.List;

public class PreAuditResp {

    private List<String> auditorias = new ArrayList<String>();

    public List<String> getAuditorias() {
        return auditorias;
    }

    public void setAuditorias(List<String> auditorias) {
        this.auditorias = auditorias;
    }

}
