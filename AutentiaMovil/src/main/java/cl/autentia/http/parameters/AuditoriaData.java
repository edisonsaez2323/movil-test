package cl.autentia.http.parameters;

public class AuditoriaData {

    private String type;
    private Integer rut;
    private String dv;
    private Integer muestraWidth;
    private Integer muestraHeight;
    private String numeroSerieLector;
    private String tipoLector;
    private String rutOperador;
    private String institucion;
    private String versionApp;
    private String operadorMovil;
    private String ip;
    private String androidId;
    private String imei;
    private String ubicacion;
    private Integer presicionUbicacion;
    private String operacion;
    private String origen;
    private String trackId;
    private String proposito;
    private String sexo;
    private String nacionalidad;
    private String fechaNac;
    private String fechaVenc;
    private Integer intentosRestantes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getProposito() {
        return proposito;
    }

    public Integer getMuestraWidth() {
        return muestraWidth;
    }

    public void setMuestraWidth(Integer muestraWidth) {
        this.muestraWidth = muestraWidth;
    }

    public Integer getMuestraHeight() {
        return muestraHeight;
    }

    public void setMuestraHeight(Integer muestraHeight) {
        this.muestraHeight = muestraHeight;
    }

    public String getNumeroSerieLector() {
        return numeroSerieLector;
    }

    public void setNumeroSerieLector(String numeroSerieLector) {
        this.numeroSerieLector = numeroSerieLector;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getOperadorMovil() {
        return operadorMovil;
    }

    public void setOperadorMovil(String operadorMovil) {
        this.operadorMovil = operadorMovil;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Integer getPresicionUbicacion() {
        return presicionUbicacion;
    }

    public void setPresicionUbicacion(Integer presicionUbicacion) {
        this.presicionUbicacion = presicionUbicacion;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public void setProposito(String proposito) {
        this.proposito = proposito;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(String fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    public Integer getIntentosRestantes() {
        return intentosRestantes;
    }

    public void setIntentosRestantes(Integer intentosRestantes) {
        this.intentosRestantes = intentosRestantes;
    }
}
