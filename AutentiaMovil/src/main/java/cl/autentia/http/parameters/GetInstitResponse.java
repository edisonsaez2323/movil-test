
package cl.autentia.http.parameters;


public class GetInstitResponse {

    private Integer status;
    private String glosa;
    private String rutInstitucion;
    private String institucion;
    private Terminos terminos;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getRutInstitucion() {
        return rutInstitucion;
    }

    public void setRutInstitucion(String rutInstitucion) {
        this.rutInstitucion = rutInstitucion;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public Terminos getTerminos() {
        return terminos;
    }

    public void setTerminos(Terminos terminos) {
        this.terminos = terminos;
    }

}
