package cl.autentia.http.parameters;

public class TerminosParam extends GenericResp{

//    {"status":0,"glosa":null,"institucion":"AUTENTIA","defaultInstit":"ACEPTA","terminos":"Términos y condiciones AUTENTIA","createdAt":1587067549027}
    String institucion;
    String defaultInstit;
    String terminos;
    String createdAt;


    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getTerminos() {
        return terminos;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    public String getDefaultInstit() {
        return defaultInstit;
    }

    public void setDefaultInstit(String defaultInstit) {
        this.defaultInstit = defaultInstit;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
