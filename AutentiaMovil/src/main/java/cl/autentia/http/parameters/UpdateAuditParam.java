package cl.autentia.http.parameters;

public class UpdateAuditParam {

    private String auditoria;
    private String institucion;
    private String origen;
    private String operacion;
    private String nroSerie;
    private String rutOperador;
    private String resultado;
    private AuditoriaData descripcion;
    private String version;
    private String rutPersona;
    private String nombre;
    private String dedoId;
    private String dedoFecha;
    private String ubicacion;
    private String tipoLector;
    private String muestra;
    private String bitmap;
    private String texto1;
    private String texto2;
    private String trackId;

    public String getAuditoria() {
        return auditoria;
    }

    public String getInstitucion() {
        return institucion;
    }

    public String getOrigen() {
        return origen;
    }

    public String getOperacion() {
        return operacion;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public String getResultado() {
        return resultado;
    }

    public AuditoriaData getDescripcion() {
        return descripcion;
    }

    public String getVersion() {
        return version;
    }

    public String getRutPersona() {
        return rutPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDedoId() {
        return dedoId;
    }

    public String getDedoFecha() {
        return dedoFecha;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public String getMuestra() {
        return muestra;
    }

    public String getTexto1() {
        return texto1;
    }

    public String getTexto2() {
        return texto2;
    }

    public String getBitmap() {
        return bitmap;
    }

    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public void setDescripcion(AuditoriaData descripcion) {
        this.descripcion = descripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setRutPersona(String rutPersona) {
        this.rutPersona = rutPersona;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDedoId(String dedoId) {
        this.dedoId = dedoId;
    }

    public void setDedoFecha(String dedoFecha) {
        this.dedoFecha = dedoFecha;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public void setTexto2(String texto2) {
        this.texto2 = texto2;
    }

    public void setBitmap(String bitmap) {
        this.bitmap = bitmap;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }
}
