package cl.autentia.http.parameters;

public class PreAuditParam {

    private String cantidad;
    private String institucion;
    private String rutInstitucion;

    public String getCantidad() {
        return cantidad;
    }

    public String getInstitucion() {
        return institucion;
    }

    public String getRutInstitucion() {
        return rutInstitucion;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public void setRutInstitucion(String rutInstitucion) {
        this.rutInstitucion = rutInstitucion;
    }
}
