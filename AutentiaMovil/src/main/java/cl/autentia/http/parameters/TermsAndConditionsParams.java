package cl.autentia.http.parameters;

public class TermsAndConditionsParams extends GenericResp{

    String institucion;
    TerminosParam terminos;

    public TerminosParam getTerminos() {
        return terminos;
    }

    public void setTerminos(TerminosParam terminos) {
        this.terminos = terminos;
    }
}
