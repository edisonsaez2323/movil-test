package cl.autentia.http.parameters;

public class GenAuditParam {

    private String muestra; // FileItem
    private String evidencia; // FileItem
    private String formatoMuestra;
    private String run;
    private String dedo;
    private String tipoLector;
    private String nroSerie;
    private String resultado;
    private String nombre;
    private AuditoriaData descripcion;
    private String rutOperador;
    private String versionApp;
    private String ubicacion;
    private String porFirmar;
    private String textoAdjunto;
    private String valorAdjunto;
    private String institucion;
    private String oti;

    public String getOti() {
        return oti;
    }

    public void setOti(String oti) {
        this.oti = oti;
    }

    public String getMuestra() {
        return muestra;
    }

    public String getEvidencia() {
        return evidencia;
    }

    public String getFormatoMuestra() {
        return formatoMuestra;
    }

    public String getRun() {
        return run;
    }

    public String getDedo() {
        return dedo;
    }

    public String getTipoLector() {
        return tipoLector;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public String getResultado() {
        return resultado;
    }

    public String getNombre() {
        return nombre;
    }

    public AuditoriaData getDescripcion() {
        return descripcion;
    }

    public String getRutOperador() {
        return rutOperador;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getPorFirmar() {
        return porFirmar;
    }

    public String getTextoAdjunto() {
        return textoAdjunto;
    }

    public String getValorAdjunto() {
        return valorAdjunto;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    public void setEvidencia(String evidencia) {
        this.evidencia = evidencia;
    }

    public void setFormatoMuestra(String formatoMuestra) {
        this.formatoMuestra = formatoMuestra;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public void setDedo(String dedo) {
        this.dedo = dedo;
    }

    public void setTipoLector(String tipoLector) {
        this.tipoLector = tipoLector;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(AuditoriaData descripcion) {
        this.descripcion = descripcion;
    }

    public void setRutOperador(String rutOperador) {
        this.rutOperador = rutOperador;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setPorFirmar(String porFirmar) {
        this.porFirmar = porFirmar;
    }

    public void setTextoAdjunto(String textoAdjunto) {
        this.textoAdjunto = textoAdjunto;
    }

    public void setValorAdjunto(String valorAdjunto) {
        this.valorAdjunto = valorAdjunto;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }
}
