package cl.autentia.http.parameters;

public class TokenParam {

    private String institucion;
    private String deviceId;
    private String nroSerie;

    public TokenParam(String institucion, String deviceId, String nroSerie) {
        this.institucion = institucion;
        this.deviceId = deviceId;
        this.nroSerie = nroSerie;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }
}

