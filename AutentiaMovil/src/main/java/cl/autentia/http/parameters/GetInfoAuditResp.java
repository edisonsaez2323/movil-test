package cl.autentia.http.parameters;

public class GetInfoAuditResp extends GenericResp {

    private String institucion;
    private String origen;
    private String operacion;
    private String estacion;
    private String rutOper;
    private String resultado;
    private String descripcion;
    private String version;
    private String rut;
    private String nombre;
    private String dedoId;
    private String dedoWsq;
    private String bmp;
    private String sSerie;
    private String texto1;
    private String valor1;

    public String getBmp() {
        return bmp;
    }

    public void setBmp(String bmp) {
        this.bmp = bmp;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public String getRutOper() {
        return rutOper;
    }

    public void setRutOper(String rutOper) {
        this.rutOper = rutOper;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDedoId() {
        return dedoId;
    }

    public void setDedoId(String dedoId) {
        this.dedoId = dedoId;
    }

    public String getDedoWsq() {
        return dedoWsq;
    }

    public void setDedoWsq(String dedoWsq) {
        this.dedoWsq = dedoWsq;
    }

    public String getsSerie() {
        return sSerie;
    }

    public void setsSerie(String sSerie) {
        this.sSerie = sSerie;
    }

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public String getValor1() {
        return valor1;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }
}
