package cl.autentia.http.parameters;

public class TokenResp extends GenericResp{

    private String token;
    private long expires;

    public TokenResp() {
        super();
    }

    public TokenResp(String token, long expires) {
        super();
        this.token = token;
        this.expires = expires;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public long getExpires() {
        return expires;
    }
    public void setExpires(long expires) {
        this.expires = expires;
    }

}