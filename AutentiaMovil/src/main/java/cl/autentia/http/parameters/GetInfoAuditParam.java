package cl.autentia.http.parameters;

public class GetInfoAuditParam {

    private String auditoria;

    public String getAuditoria() {
        return auditoria;
    }

    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }

}
