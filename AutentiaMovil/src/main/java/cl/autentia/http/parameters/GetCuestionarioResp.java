package cl.autentia.http.parameters;

import java.util.ArrayList;
import java.util.List;

public class GetCuestionarioResp extends GenericResp {

    private List<PreguntaParam> preguntas = new ArrayList<PreguntaParam>();

    public List<PreguntaParam> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<PreguntaParam> preguntas) {
        this.preguntas = preguntas;
    }
}
