
package cl.autentia.http.parameters;


public class Verificacion {

    private String identificacionProveedor;
    private String fechaVerificacion;
    private String urlEvidenciasPdf;
    private String tipoVerificacion;
    private Boolean verificado;
    private Integer porcentaje;

    public String getIdentificacionProveedor() {
        return identificacionProveedor;
    }

    public void setIdentificacionProveedor(String identificacionProveedor) {
        this.identificacionProveedor = identificacionProveedor;
    }

    public String getFechaVerificacion() {
        return fechaVerificacion;
    }

    public void setFechaVerificacion(String fechaVerificacion) {
        this.fechaVerificacion = fechaVerificacion;
    }

    public String getUrlEvidenciasPdf() {
        return urlEvidenciasPdf;
    }

    public void setUrlEvidenciasPdf(String urlEvidenciasPdf) {
        this.urlEvidenciasPdf = urlEvidenciasPdf;
    }

    public String getTipoVerificacion() {
        return tipoVerificacion;
    }

    public void setTipoVerificacion(String tipoVerificacion) {
        this.tipoVerificacion = tipoVerificacion;
    }

    public Boolean getVerificado() {
        return verificado;
    }

    public void setVerificado(Boolean verificado) {
        this.verificado = verificado;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

}
