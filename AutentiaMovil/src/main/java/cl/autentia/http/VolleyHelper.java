package cl.autentia.http;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import cl.autentia.http.parameters.VolleyRespError;

public class VolleyHelper {

    private Map<String, String> headers;
    private RequestQueue queue;
    private Context mContext;
    private String url;
    public static int DEFAULT_MAX_RETRIES = 0;

    public VolleyHelper(Context context, Map<String, String> headers, String url) {
        this.headers = headers;
        this.mContext = context;
        this.queue = getRequestQueue();
        this.url = url;
    }


    public RequestQueue getRequestQueue() {
        if (queue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            queue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return queue;
    }


    public synchronized void postRequestVolley(JSONObject jsonObject, final onResponseMethod am){

        final String requestBody = jsonObject.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                am.action(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("err", String.valueOf(error.networkResponse.statusCode));
//                Log.e("err",error.getMessage());

                VolleyRespError vre = new VolleyRespError();

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError || error instanceof ServerError) {
                    vre.setCode("404");
                    vre.setGlosa("Se ha superado el tiempo de respuesta de la petición o no es posible conectar con el servidor.");
                    vre.setStatus(404);
                } else if (error instanceof AuthFailureError) {
                    vre.setCode("401");
                    vre.setGlosa("Token no válido o expirado: "+ error.getMessage());
                    vre.setStatus(401);
                } else if (error instanceof ParseError) {
                    vre.setCode("900");
                    vre.setGlosa("Error de parse de la data obtenida: "+ error.getMessage());
                    vre.setStatus(900);
                }else{
                    if(error.getMessage() != null && !error.getMessage().equals("")){
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico: "+error.getMessage());
                    }else{
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico");
                    }
                }
                am.action(new Gson().toJson(vre));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000,
                DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public synchronized void getRequestVolley(JSONObject jsonObject, final onResponseMethod am){

        final String requestBody = jsonObject.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                am.action(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("err", String.valueOf(error.networkResponse.statusCode));
//                Log.e("err",error.getMessage());

                VolleyRespError vre = new VolleyRespError();

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError || error instanceof ServerError) {
                    vre.setCode("404");
                    vre.setGlosa("Se ha superado el tiempo de respuesta de la petición o no es posible conectar con el servidor.");
                    vre.setStatus(404);
                } else if (error instanceof AuthFailureError) {
                    vre.setCode("401");
                    vre.setGlosa("Token no válido o expirado: "+ error.getMessage());
                    vre.setStatus(401);
                } else if (error instanceof ParseError) {
                    vre.setCode("900");
                    vre.setGlosa("Error de parse de la data obtenida: "+ error.getMessage());
                    vre.setStatus(900);
                }else{
                    if(error.getMessage() != null && !error.getMessage().equals("")){
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico: "+error.getMessage());
                    }else{
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico");
                    }
                }
                am.action(new Gson().toJson(vre));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000,
                DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public synchronized void getByteDownload(String url, final onResponseByte am){


        InputStreamRequest inputStreamRequest = new InputStreamRequest(url, new Response.Listener<byte[]>() {
            @Override
            public void onResponse(byte[] response) {
                am.action(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyRespError vre = new VolleyRespError();

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError || error instanceof ServerError) {
                    vre.setCode("404");
                    vre.setGlosa("Se ha superado el tiempo de respuesta de la petición o no es posible conectar con el servidor.");
                    vre.setStatus(404);
                } else if (error instanceof AuthFailureError) {
                    vre.setCode("401");
                    vre.setGlosa("Token no válido o expirado: "+ error.getMessage());
                    vre.setStatus(401);
                } else if (error instanceof ParseError) {
                    vre.setCode("900");
                    vre.setGlosa("Error de parse de la data obtenida: "+ error.getMessage());
                    vre.setStatus(900);
                }else{
                    if(error.getMessage() != null && !error.getMessage().equals("")){
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico: "+error.getMessage());
                    }else{
                        vre.setStatus(201);
                        vre.setGlosa("Error Genérico");
                    }
                }
                am.action(new Gson().toJson(vre));

            }
        });

        queue.add(inputStreamRequest);

    }

}
