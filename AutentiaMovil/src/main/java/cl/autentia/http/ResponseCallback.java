package cl.autentia.http;

import android.os.Bundle;

import cl.autentia.helper.AutentiaMovilException;

public interface ResponseCallback {

    void onResponseSuccess(Bundle result);

    void onResponseError(AutentiaMovilException result);

}
