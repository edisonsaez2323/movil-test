package cl.autentia.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cl.autentia.helper.AppHelper;
import cl.autentia.helper.DeviceHelper;

public class HttpUtil {

    final protected static char[] hexArray = {
            '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'};

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        return activeInfo != null && activeInfo.isConnected();
    }

    public static String hash(String input, String metod)
            throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance(metod);
        messageDigest.update(input.getBytes());
        byte[] digest = messageDigest.digest();
        return HttpUtil.byteArrayToHexString(digest);
    }

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    public static void sendReportUncaughtException(Exception exception, String mInstitution, Context ctx) {
        try {
            Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
                    .putCustomAttribute("Error", exception.getMessage())
                    .putCustomAttribute("Stacktrace", getStringStacktrace(exception))
                    .putCustomAttribute("Institucion", mInstitution)
                    .putCustomAttribute("App version", AppHelper.getCurrentVersion(ctx))
                    .putCustomAttribute("IMEI", DeviceHelper.getIMEI(ctx)));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void sendReportUncaughtException(Exception exception, Context ctx) {
        try {
            Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
                    .putCustomAttribute("Error", exception.getMessage())
                    .putCustomAttribute("Stacktrace", getStringStacktrace(exception))
                    .putCustomAttribute("App version", AppHelper.getCurrentVersion(ctx))
                    .putCustomAttribute("IMEI", DeviceHelper.getIMEI(ctx)));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void sendReportUncaughtException(Throwable throwable, Context ctx){
        try {
            Answers.getInstance().logCustom(new CustomEvent("Excepcion no controlada")
                    .putCustomAttribute("Error",  throwable.getMessage())
                    .putCustomAttribute("Stacktrace", getStringStacktrace(throwable))
                    .putCustomAttribute("App version", AppHelper.getCurrentVersion(ctx))
                    .putCustomAttribute("IMEI", DeviceHelper.getIMEI(ctx)));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static String getStringStacktrace(Exception exception){
        Writer writer = new StringWriter();
        exception.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    private static String getStringStacktrace(Throwable throwable){
        Writer writer = new StringWriter();
        throwable.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

}