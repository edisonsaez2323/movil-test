package cl.autentia.http;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import com.acepta.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.autentia.common.ReturnCode;
import cl.autentia.configuration.parameters.autoUpdateParams;
import cl.autentia.configuration.parameters.autoUpdateResp;
import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.parameters.AuditoriaData;
import cl.autentia.http.parameters.AvailableFingerprintsParam;
import cl.autentia.http.parameters.AvailableFingerprintsResp;
import cl.autentia.http.parameters.DeviceRegistrationParam;
import cl.autentia.http.parameters.FacialOAuthParams;
import cl.autentia.http.parameters.FingerprintInfoResp;
import cl.autentia.http.parameters.GenAuditParam;
import cl.autentia.http.parameters.GetConfigDataParam;
import cl.autentia.http.parameters.GetCuestionarioParam;
import cl.autentia.http.parameters.GetCuestionarioResp;
import cl.autentia.http.parameters.GetInfoAuditParam;
import cl.autentia.http.parameters.GetStatusNECParam;
import cl.autentia.http.parameters.Operador;
import cl.autentia.http.parameters.OperadorResp;
import cl.autentia.http.parameters.PreAuditParam;
import cl.autentia.http.parameters.RespuestaListParam;
import cl.autentia.http.parameters.RespuestaParam;
import cl.autentia.http.parameters.TerminosParam;
import cl.autentia.http.parameters.TermsAndConditionsParams;
import cl.autentia.http.parameters.TokenParam;
import cl.autentia.http.parameters.TokenResp;
import cl.autentia.http.parameters.UpdateAuditParam;
import cl.autentia.http.parameters.UpdateAuditResp;
import cl.autentia.http.parameters.ValidFingerprintParam;
import cl.autentia.http.parameters.ValidFingerprintResp;
import cl.autentia.http.parameters.ValidNecParam;
import cl.autentia.http.parameters.ValidNecResp;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

import static android.provider.Settings.Secure.ANDROID_ID;

/**
 *
 */
public class AutentiaWSClient {

    private static final String TAG = "AutentiaWSClient";
    public static final String URL_JSON_TRASPASO = "jsonUrl";
    public static final String URL_TRASPASO = "urlTraspaso";

    private AutentiaPreferences mPreferences;
    private int errorCount = 0;

    private Context mContext;
    private String mUrlBase;

    private String tokenMsgError;

    /**
     * Constructor por defecto
     */
    public AutentiaWSClient(Context context) {
        mPreferences = new AutentiaPreferences(context);
        mContext = context;
        mUrlBase = mPreferences.getString(KeyPreferences.URL_BASE, "");
    }

    /**
     * Método para obtener token de acceso para los servicios de AutentiaWs
     *
     * @return devuelve token de acceso
     */

    public void getToken(String institucion, final onResponseMethod onResponseMethod) {
        try {
            @SuppressLint("HardwareIds") TokenParam tp = new TokenParam(institucion, getAndroidID(), mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER, ""));
            org.json.JSONObject json = new org.json.JSONObject(new Gson().toJson(tp));
            VolleyHelper vh = new VolleyHelper(mContext, new HashMap<String, String>(), mUrlBase + UrlList.GET_TOKEN);
            vh.postRequestVolley(json, new onResponseMethod() {
                @Override
                public void action(String response) {
                    TokenResp token = new Gson().fromJson(response, TokenResp.class);
                    tokenMsgError = token.getGlosa();
                    if (token.getToken() != null) mPreferences.setString("token", token.getToken());
                    else mPreferences.setString("token", "");
                    mPreferences.setLong("expires", token.getExpires());
                    onResponseMethod.action("");

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();//TODO:Kill with AutentiaError.
        }
    }


    /**
     * Lista de header necesarios para las consultas al WS
     *
     * @param token
     * @param serialNumber
     * @param institucion
     * @return
     */
    private Map<String, String> getHeaders(String token, String serialNumber, String institucion) {
        Map<String, String> basicHeaders = new HashMap<String, String>();
        basicHeaders.put("Authorization", "Bearer " + token);
        basicHeaders.put("nroSerie", serialNumber);
        basicHeaders.put("institucion", institucion);
        basicHeaders.put("deviceId", getAndroidID());
        return basicHeaders;
    }

    /**
     * Método para obtener el androidID del dispositivo
     *
     * @return el androidID del dispositivo.
     */
    @SuppressLint("HardwareIds")
    private String getAndroidID() {
        return Settings.Secure.getString(mContext.getContentResolver(), ANDROID_ID);
    }

    /**
     *
     * @param rutPersona
     * @param institucion
     * @param serialNumber
     * @param responseCallback
     */
    public void checkAvalaibleFingerprint(final String rutPersona, final String institucion, final String serialNumber, final ResponseCallback responseCallback) {

        String url = mUrlBase + UrlList.GET_AVA_FING;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), serialNumber, institucion);

        AvailableFingerprintsParam afp = new AvailableFingerprintsParam();
        afp.setInstitucion(institucion);
        afp.setNroSerie(serialNumber);
        afp.setRun(rutPersona);

        try {
            final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(afp));
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    try {
                        org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                        AvailableFingerprintsResp mdp;
                        mdp = new Gson().fromJson(response, AvailableFingerprintsResp.class);
                        if (mdp.getGlosa() != null && mdp.getStatus() != 0) {
                            String cause = jsonResp.getString("glosa");
                            if (mdp.getStatus() == 401 || mdp.getStatus() == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(institucion, new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            checkAvalaibleFingerprint(rutPersona, institucion, serialNumber, responseCallback);
                                        }
                                    });
                                } else {
                                    responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                                }
                            } else {
                                if (mdp.getStatus() == 5000)
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));
                                else if (mdp.getGlosa().equals("Esta persona no tiene datos"))
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_RUT_NO_ENROLADO));
                                else if (mdp.getStatus() == 404)
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                                else if (mdp.getStatus() == 900)
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                                else
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                            }
                        } else { //si es exitosa la respuesta
                            Bundle data = new Bundle();
                            if (!mdp.getMuestrasString().isEmpty()) {
                                data.putString("nombre", mdp.getNombre());
                                data.putString("fechaNac", mdp.getFechaNac());
                                boolean[] huellasDisponibles = new boolean[11];
                                huellasDisponibles[0] = mdp.getMuestrasString().contains("unknown");
                                huellasDisponibles[1] = mdp.getMuestrasString().contains("rthumb");
                                huellasDisponibles[2] = mdp.getMuestrasString().contains("rindex");
                                huellasDisponibles[3] = mdp.getMuestrasString().contains("rmiddle");
                                huellasDisponibles[4] = mdp.getMuestrasString().contains("rring");
                                huellasDisponibles[5] = mdp.getMuestrasString().contains("rpinkie");
                                huellasDisponibles[6] = mdp.getMuestrasString().contains("lthumb");
                                huellasDisponibles[7] = mdp.getMuestrasString().contains("lindex");
                                huellasDisponibles[8] = mdp.getMuestrasString().contains("lmiddle");
                                huellasDisponibles[9] = mdp.getMuestrasString().contains("lring");
                                huellasDisponibles[10] = mdp.getMuestrasString().contains("lpinkie");
                                data.putBooleanArray("muestras", huellasDisponibles);
                            }

                            String[] rut = rutPersona.split("-");
                            data.putInt("rut", rut[0].charAt(0) /*Integer.parseInt(mdp.getId().split("-")[0])*/);
                            data.putChar("dv", rut[1].charAt(0));

                            data.putString("serialNumber", serialNumber);
                            responseCallback.onResponseSuccess(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                    }
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     *
     * @param rutPersona
     * @param dedo
     * @param pdf417
     * @param fingerprint
     * @param serialNumber
     * @param descripcion
     * @param rutEmpresa
     * @param versionApp
     * @param ubicacion
     * @param textoAdjunto
     * @param valorAdjunto
     * @param readerType
     * @param institucion
     * @param oti
     * @param bitmap
     * @param responseCallback
     * @throws Exception
     */
    public void validNEC(final String rutPersona, final String dedo, final byte[] pdf417, final byte[] fingerprint,
                         final String serialNumber, final String descripcion, final String rutEmpresa,
                         final String versionApp, final String ubicacion, final String textoAdjunto,
                         final String valorAdjunto, final String readerType, final String institucion, boolean oti, byte[] bitmap,final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_VAL_NEC;

        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), serialNumber, institucion);
        AuditoriaData ad = new Gson().fromJson(descripcion, AuditoriaData.class);

        final ValidNecParam vnp = new ValidNecParam();
        vnp.setRun(rutPersona);
        vnp.setDedo(dedo);
        vnp.setNroSerie(serialNumber);
        vnp.setDescripcion(ad);
        vnp.setRutOperador(rutEmpresa);
        vnp.setVersionApp(versionApp);
        vnp.setUbicacion(ubicacion);
        vnp.setTextoAdjunto(textoAdjunto);
        vnp.setValorAdjunto(valorAdjunto);
        vnp.setTipoLector(readerType);
        vnp.setInstitucion(institucion);
        vnp.setWsq(Base64.encodeToString(fingerprint, 0).replace("\n", ""));
        vnp.setPdf417(Base64.encodeToString(pdf417, 0).replace("\n", ""));
        vnp.setBitmap(Base64.encodeToString(bitmap, 0).replace("\n", ""));
        if (oti) {
            vnp.setOti("true");
        }


        try {
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(vnp));
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {

                @Override
                public void action(String response) {
                    try {
                        Log.e("response", response);
                        ValidNecResp vnr = new ValidNecResp();
                        vnr = new Gson().fromJson(response, ValidNecResp.class);
                        if (vnr.getGlosa() != null && (vnr.getStatus() != 0 && vnr.getStatus() != 1)) {
                            final String cause = vnr.getGlosa();
                            if (vnr.getStatus() == 401 || vnr.getStatus() == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(institucion, new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            try {
                                                validNEC(rutPersona, dedo, pdf417, fingerprint,
                                                        serialNumber, descripcion, rutEmpresa,
                                                        versionApp, ubicacion, textoAdjunto,
                                                        valorAdjunto, readerType, institucion, oti, bitmap,responseCallback);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                            }
                                        }
                                    });
                                } else {
                                    responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                                }
                            } else {
                                if (vnr.getStatus() == 7) { //TODO: preguntar los status de los errores.
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.HUELLERO_NO_REGISTRADO));
                                } else {
                                    if (vnr.getStatus() == 202)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));
                                    else if (vnr.getStatus() == 404)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                                    else if (vnr.getStatus() == 900)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                                    else
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));

                                }
                            }

                        } else {  //status 0 : match, status 1: no match
                            Bundle data = new Bundle();
                            if (vnr.getNroAuditoria() != null) {
                                data.putString("nroAuditoria", vnr.getNroAuditoria());
                            } else {
                                data.putString("nroAuditoria", null);
                            }

                            if (oti) {
                                data.putString("urlOti", vnr.getOti().getUrlOti());
                                data.putString("identificacionProveedor", vnr.getOti().getValidacion().getVerificacion().getIdentificacionProveedor());
                                data.putString("fechaVerificacion", vnr.getOti().getValidacion().getVerificacion().getFechaVerificacion());
                                data.putString("tipoVerificacion", vnr.getOti().getValidacion().getVerificacion().getTipoVerificacion());
                            }

                            data.putString("status", String.valueOf(vnr.getStatus()));
                            data.putInt("rut", Integer.parseInt(rutPersona.split("-")[0]));
                            data.putChar("dv", rutPersona.split("-")[1].charAt(0));
                            data.putString("serialNumber", serialNumber);
                            responseCallback.onResponseSuccess(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param rutPersona
     * @param wsq
     * @param bitmap
     * @param readerType
     * @param transactionId
     * @param serialNumber
     * @param descripcion
     * @param rutEmpresa
     * @param versionApp
     * @param ubicacion
     * @param porFirmar
     * @param textoAdjunto
     * @param valorAdjunto
     * @param institucion
     * @param oti
     * @param texto3
     * @param responseCallback
     * @throws Exception
     */

    public void ValidFingerprintBitmap(final String rutPersona, final byte[] wsq, final byte[] bitmap,
                                       final String readerType, final String transactionId, final String serialNumber,
                                       final String descripcion, final String rutEmpresa, final String versionApp,
                                       final String ubicacion, final String porFirmar, final String textoAdjunto,
                                       final String valorAdjunto, final String institucion, boolean oti,/*Opcional*/String texto3 ,final ResponseCallback responseCallback)
            throws Exception {

        String url = mUrlBase + UrlList.GET_MUESTRA;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), serialNumber, institucion);

        AuditoriaData ad = new Gson().fromJson(descripcion, AuditoriaData.class);
        ValidFingerprintParam vfp = new ValidFingerprintParam();
        vfp.setRun(rutPersona);
        vfp.setRutOperador(rutEmpresa);
        vfp.setTipoLector(readerType);
        vfp.setInstitucion(institucion);
        vfp.setValorAdjunto(valorAdjunto);
        vfp.setUbicacion(ubicacion);
        vfp.setVersionApp(versionApp);
        vfp.setNroSerie(serialNumber);
        vfp.setTextoAdjunto(textoAdjunto);
        vfp.setDescripcion(ad);
        vfp.setMuestra("wsq");
        vfp.setPorFirmar(porFirmar);
        vfp.setMuestra(Base64.encodeToString(wsq, 0).replace("\n", ""));
        vfp.setMuestraBitmap(Base64.encodeToString(bitmap, 0).replace("\n", ""));
        if (oti) vfp.setOti("true");
        if (!texto3.isEmpty()){
            vfp.setTexto3(texto3);
        }
        try {
            org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(vfp));
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    Log.e("resp", response);

                    try {
                        org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                        ValidFingerprintResp vfr = new ValidFingerprintResp();
                        vfr = new Gson().fromJson(jsonResp.toString(), ValidFingerprintResp.class);
                        if (vfr.getGlosa() != null && (vfr.getStatus() != 0 && vfr.getStatus() != 1)) {
                            final String cause = vfr.getGlosa();
                            if (vfr.getStatus() == 401 || vfr.getStatus() == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(institucion, new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            try {
                                                ValidFingerprintBitmap(rutPersona, wsq, bitmap,
                                                        readerType, transactionId, serialNumber,
                                                        descripcion, rutEmpresa, versionApp,
                                                        ubicacion, porFirmar, textoAdjunto,
                                                        valorAdjunto, institucion, oti, texto3,responseCallback);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.e(TAG, "1");
                                                responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                            }
                                        }
                                    });
                                } else {
                                    responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                                }
                            } else {
                                if (vfr.getStatus() == 7) { //TODO: preguntar los status de los errores.
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.HUELLERO_NO_REGISTRADO));
                                } else {
                                    if (vfr.getStatus() == 202)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));
                                    else if (vfr.getStatus() == 404)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                                    else if (vfr.getStatus() == 900)
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                                    else
                                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                                }
                            }
                        } else {
                            Bundle data = new Bundle();
                            if (vfr.getNroAuditoria() != null) {
                                data.putString("result", vfr.getNroAuditoria());
                                data.putString("identidadVerificada", "true");
                                if (vfr.getJsonUrl() != null && !vfr.getJsonUrl().equals("")) {
                                    String urlTras = vfr.getJsonUrl();
                                    data.putString("urlTraspaso", new JSONObject(urlTras).get("Url").toString());
                                }
                                if (oti) {
                                    data.putString("urlOti", vfr.getOti().getUrlOti());
                                    data.putString("identificacionProveedor", vfr.getOti().getValidacion().getVerificacion().getIdentificacionProveedor());
                                    data.putString("fechaVerificacion", vfr.getOti().getValidacion().getVerificacion().getFechaVerificacion());
                                    data.putString("tipoVerificacion", vfr.getOti().getValidacion().getVerificacion().getTipoVerificacion());
                                }
                            } else {
                                data.putString("result", null);
                                data.putString("identidadVerificada", "false");
                            }
                            data.putInt("rut", Integer.parseInt(rutPersona.split("-")[0]));
                            data.putChar("dv", rutPersona.split("-")[1].charAt(0));
                            data.putString("transactionId", transactionId);
                            data.putString("serialNumber", serialNumber);
                            data.putString("status", String.valueOf(vfr.getStatus()));
                            data.putString("nroAuditoria", vfr.getNroAuditoria());
                            data.putString("jsonUrl", vfr.getJsonUrl());
                            data.putString("nombre", vfr.getNombre());
                            data.putString("fechaNac", vfr.getFechaNac());
                            responseCallback.onResponseSuccess(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param auditoria
     * @param institution
     * @param origen
     * @param operacion
     * @param numSerieLector
     * @param tipoLector
     * @param rutOperador
     * @param resultadoVerificacion
     * @param descripcionJsonData
     * @param versionApp
     * @param rutPersona
     * @param nombre
     * @param dedoId
     * @param dedoFecha
     * @param ubicacion
     * @param muestra
     * @param texto1
     * @param texto2
     * @param responseCallback
     * @throws Exception
     */

    public void updateAudit(final String auditoria,
                            final String institution,
                            final String origen,
                            final String operacion,
                            final String numSerieLector,
                            final String tipoLector,
                            final String rutOperador,
                            final String resultadoVerificacion,
                            final String descripcionJsonData,
                            final String versionApp,
                            final String rutPersona,
                            final String nombre,
                            final String dedoId,
                            final String dedoFecha,
                            final String ubicacion,
                            final byte[] muestra,
                            final byte[] bitmap,
                            final String texto1,
                            final String texto2, final String trackId,final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_UPD_AUD;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), numSerieLector, institution);

        AuditoriaData auditoriaData = new Gson().fromJson(descripcionJsonData, AuditoriaData.class);

        UpdateAuditParam uap = new UpdateAuditParam();
        uap.setAuditoria(auditoria);
        uap.setInstitucion(institution);
        uap.setOrigen(origen);
        uap.setOperacion(operacion);
        uap.setNroSerie(numSerieLector);
        uap.setTipoLector(tipoLector);
        uap.setRutOperador(rutOperador);
        uap.setResultado(resultadoVerificacion);
        uap.setDescripcion(auditoriaData);
        uap.setVersion(versionApp);
        uap.setRutPersona(rutPersona);
        uap.setNombre(nombre);
        uap.setDedoId(dedoId);
        uap.setDedoFecha(dedoFecha);
        uap.setUbicacion(ubicacion);
        uap.setTexto1(texto1);
        uap.setMuestra(Base64.encodeToString(muestra, 0).replace("\n", ""));
        uap.setBitmap(Base64.encodeToString(bitmap, 0).replace("\n", ""));
        uap.setTexto2(texto2);
        org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(uap));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                UpdateAuditResp updateAuditResp = new Gson().fromJson(response, UpdateAuditResp.class);
                if (updateAuditResp.getGlosa() != null && updateAuditResp.getErr() != 0) {
                    final String cause = updateAuditResp.getGlosa();
                    if (updateAuditResp.getErr() == 5007) {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    } else if (updateAuditResp.getErr() == 401 || updateAuditResp.getErr() == 5002) { //token invalido
                        if (errorCount < 3) {
                            errorCount++;
                            getToken(institution, new onResponseMethod() {
                                @Override
                                public void action(String response) {
                                    try {
                                        updateAudit(auditoria,
                                                institution,
                                                origen,
                                                operacion,
                                                numSerieLector,
                                                tipoLector,
                                                rutOperador,
                                                resultadoVerificacion,
                                                descripcionJsonData,
                                                versionApp,
                                                rutPersona,
                                                nombre,
                                                dedoId,
                                                dedoFecha,
                                                ubicacion,
                                                muestra,
                                                bitmap,
                                                texto1,
                                                texto2,
                                                trackId,
                                                responseCallback);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                    }
                                }
                            });
                        } else {
                            responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                        }
                    } else {
                        if (updateAuditResp.getErr() == 7) { //TODO: preguntar los status de los errores.
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.HUELLERO_NO_REGISTRADO));
                        }
                        if (updateAuditResp.getErr() == 202)
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES));
                        else if (updateAuditResp.getErr() == 404)
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                        else if (updateAuditResp.getErr() == 900)
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                        else
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                    }
                } else {
                    Bundle data = new Bundle();
                    data.putString("result", response);
                    responseCallback.onResponseSuccess(data);
                }
            }
        });
    }


    /**
     *
     * Asignación de pre-Auditorias
     *
     * @param cantidad
     * @param institucion
     * @param rutInstitucion
     * @param responseCallback
     * @throws Exception
     */

    public synchronized void getPreAudit(final String cantidad, final String institucion, final String rutInstitucion, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_PRE_AUD;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER), institucion);
        PreAuditParam pap = new PreAuditParam();
        pap.setCantidad(cantidad);
        pap.setInstitucion(institucion);
        pap.setRutInstitucion(rutInstitucion);
        try {
            final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(pap));
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
//                    Log.e("result pre audit",response);
                    try {
                        org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                        if (jsonResp.getInt("status") != 0) {
                            if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(institucion, new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            try {
                                                getPreAudit(cantidad, institucion, rutInstitucion, responseCallback);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                            }
                                        }
                                    });
                                } else {
                                    if (tokenMsgError != null) {
                                        responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                                    } else {
                                        responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.CANT_GET_AUDIT));
                                    }
                                }
                            } else {
                                if (jsonResp.has("glosa")) {
                                    if (jsonResp.getString("status") == null || jsonResp.getInt("status") != 0) {
                                        if (jsonResp.getInt("status") == 404)
                                            responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.VERIFICATION_SIN_INTERNET));
                                        else if (jsonResp.getInt("status") == 900)
                                            responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.VOLLEY_PARSE));
                                        else
                                            responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                                    }
                                }
                            }

                        } else {
                            Bundle data = new Bundle();
                            data.putString("result", response);
                            responseCallback.onResponseSuccess(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /**
     *
     * @param muestra
     * @param formatoMuestra
     * @param run
     * @param dedo
     * @param tipoLector
     * @param nroSerieLector
     * @param evidencia
     * @param resultado
     * @param nombre
     * @param descripcion
     * @param rutEmpresa
     * @param versionApp
     * @param ubicacion
     * @param porFirmar
     * @param textoAdjunto
     * @param valorAdjunto
     * @param institucion
     * @param oti
     * @param responseCallback
     * @throws Exception
     */

    public void genAudit(final byte[] muestra, final String formatoMuestra, final String run, final String dedo,
                         final String tipoLector, final String nroSerieLector, final byte[] evidencia,
                         final String resultado, final String nombre, final String descripcion,
                         final String rutEmpresa, final String versionApp, final String ubicacion, final String porFirmar,
                         final String textoAdjunto, final String valorAdjunto, final String institucion, boolean oti, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GEN_AUDIT;
//        String url = "http://192.168.1.142:8888/";
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nroSerieLector, institucion);
        AuditoriaData auditoriaData = new Gson().fromJson(descripcion, AuditoriaData.class);

        GenAuditParam gap = new GenAuditParam();
        gap.setDedo(dedo);
        gap.setFormatoMuestra(formatoMuestra);
        gap.setRun(run);
        gap.setTipoLector(tipoLector);
        gap.setNroSerie(nroSerieLector);
        gap.setResultado(resultado);
        gap.setNombre(nombre);
        gap.setDescripcion(auditoriaData);
        gap.setRutOperador(rutEmpresa);
        gap.setTextoAdjunto(textoAdjunto);
        gap.setVersionApp(versionApp);
        gap.setUbicacion(ubicacion);
        gap.setPorFirmar(porFirmar);
        gap.setValorAdjunto(valorAdjunto);
        gap.setInstitucion(institucion);
        if (muestra == null || muestra.length == 0) {
            gap.setMuestra(Base64.encodeToString(new byte[0], 0));
        } else {
            gap.setMuestra(Base64.encodeToString(muestra, 0).replace("\n", ""));
        }
        if (evidencia == null || evidencia.length == 0) {
            gap.setEvidencia(Base64.encodeToString(new byte[0], 0));
        } else {
            gap.setEvidencia(Base64.encodeToString(evidencia, 0).replace("\n", ""));
        }
        if (oti)
            gap.setOti("true");

        final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(gap));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        final String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(institucion, new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            genAudit(muestra, formatoMuestra, run, dedo,
                                                    tipoLector, nroSerieLector, evidencia,
                                                    resultado, nombre, descripcion,
                                                    rutEmpresa, versionApp, ubicacion, porFirmar,
                                                    textoAdjunto, valorAdjunto, institucion, oti, responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                        }

                    } else {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });

    }

    /**
     *
     * @param rutPersona
     * @param numeroSerieCedula
     * @param nSerialnumber
     * @param institucion
     * @param responseCallback
     * @throws Exception
     */
    public void getStatusNEC(final String rutPersona, final String numeroSerieCedula, final String nSerialnumber, final String institucion, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_STA_NEC;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nSerialnumber, institucion);


        GetStatusNECParam gsn = new GetStatusNECParam();
        gsn.setNroSerie(numeroSerieCedula);
        gsn.setRun(rutPersona);

        org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(gsn));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        final String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(institucion, new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            getStatusNEC(rutPersona, numeroSerieCedula, nSerialnumber, institucion, responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                        }
                    } else {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });
    }

    /**
     *
     * @param audit
     * @param institucion
     * @param nSerialnumber
     * @param responseCallback
     * @throws Exception
     */

    public void getStatusAudit(final String audit, final String institucion, final String nSerialnumber, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_INFO_AUD;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nSerialnumber, institucion);

        GetInfoAuditParam gip = new GetInfoAuditParam();
        gip.setAuditoria(audit);
        org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(gip));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        final String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(institucion, new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            getStatusAudit(audit, nSerialnumber, institucion, responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                        }
                    } else {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });
    }

    /**
     *
     * @param nroSerie
     * @param responseCallback
     * @throws Exception
     */

    public void getConfigData(final String nroSerie, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.GET_AUTO_CONFIG_DATA;

        GetConfigDataParam gcp = new GetConfigDataParam();
        gcp.setNroSerie(nroSerie);
        org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(gcp));
        VolleyHelper vh = new VolleyHelper(mContext, new HashMap<String, String>(), url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                Log.e("resp instit", response);
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                    } else {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });
    }

    /**
     *
     * @param rutOperador
     * @param nSerialnumber
     * @param responseCallback
     * @throws Exception
     */

    public void getOperador(final String rutOperador, String nSerialnumber, final ResponseCallback responseCallback) throws Exception {

//        String url = "http://192.168.1.142:8888";
        String url = mUrlBase + UrlList.GET_OPERADOR;
//        String url = "http://192.168.1.117:8080/verificaRol";
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nSerialnumber, mPreferences.getString(KeyPreferences.INSTITUTION));

        Operador operador = new Operador();
        operador.setInstitucion(mPreferences.getString(KeyPreferences.INSTITUTION));
        operador.setRutOperador(rutOperador);
        operador.setRol("Oper");

        org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(operador));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                    } else {
                        OperadorResp oResp = new Gson().fromJson(response, OperadorResp.class);
                        Log.e("result - getOper", response);
                        Bundle data = new Bundle();
                        data.putBoolean("rolVerificado", oResp.getRolVerificado());
                        responseCallback.onResponseSuccess(data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });
    }

    /**
     *
     * @param nSerialnumber
     * @param responseCallback
     * @throws Exception
     */

    public void getFingerprintInfo(String nSerialnumber, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.VERIFICA_SENSOR;
//        Log.e("url",url);
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nSerialnumber, mPreferences.getString(KeyPreferences.INSTITUTION));

        org.json.JSONObject jsonObject = new org.json.JSONObject();
        jsonObject.put("nroSerie", nSerialnumber);
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);

        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    JSONObject jsonResp = new JSONObject(response);

                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        final String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 403) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(mPreferences.getString(KeyPreferences.INSTITUTION), new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            getFingerprintInfo(nSerialnumber, responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                        }
                    } else {
                        try {
                            if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                            } else {
                                FingerprintInfoResp fir = new Gson().fromJson(response, FingerprintInfoResp.class);
                                Bundle data = new Bundle();
                                data.putString("tipoLogon", fir.getTipoLogon());
                                data.putString("rutLogon", fir.getRutLogon());
                                responseCallback.onResponseSuccess(data);
                            }
                        } catch (Exception e) {
                            Log.e("on error", "fingerprint info");
                            e.printStackTrace();
                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     *
     * TRANSUNION METHODS
     *
     * @param rutPersona
     * @param ns_cedula
     * @param institucion
     * @param serialNumber
     * @param id_credencials
     * @param responseCallback
     * @throws Exception
     */

    public synchronized void getQuestions(final String rutPersona, final String ns_cedula, final String institucion, final String serialNumber, final String id_credencials, final ResponseCallback responseCallback) throws Exception {


        String url = mUrlBase + UrlList.GET_QUESTIONS;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), serialNumber, institucion);

        GetCuestionarioParam gcp = new GetCuestionarioParam();
        gcp.setRut(rutPersona);
        gcp.setNroSerie(serialNumber);
        gcp.setNroSerieDocumento(ns_cedula);
        gcp.setInquirySource("IDV-WEBSERVICE");
        gcp.setTipoDocumento("C");
        if (id_credencials != null) {
            gcp.setIdCredencials(id_credencials);
        }
        try {
            final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(gcp));
//            Log.e("JSON",jsonObject.toString());
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    try {
                        org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                        GetCuestionarioResp gqr;
                        gqr = new Gson().fromJson(response, GetCuestionarioResp.class);
                        if (gqr.getGlosa() != null && gqr.getStatus() != 0) {
                            String cause = jsonResp.getString("glosa");
                            if (gqr.getStatus() == 401 || gqr.getStatus() == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(institucion, new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            try {
                                                getQuestions(rutPersona, ns_cedula, institucion, serialNumber, id_credencials, responseCallback);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
                                            }
                                        }
                                    });
                                } else {
                                    responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                                }
                            } else {
                                if (gqr.getStatus() == 404)
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET_TRANSUNION));
                                else if (gqr.getStatus() == 900)
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                                else
                                    responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO_TRANSUNION));
                            }
                        } else { //si es exitosa la respuesta
                            if (gqr.getPreguntas().size() == 0) {
                                responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.NO_QUESTIONS_FOR_RUT_TRANSUNION));
                            }
                            Bundle data = new Bundle();
                            data.putString("result", response);
                            responseCallback.onResponseSuccess(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO_TRANSUNION));
                    }
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }


    }

    /**
     *
     * Respuestas transunion
     *
     * @param hashMap
     * @param rut
     * @param institucion
     * @param serialNumber
     * @param ns_cedula
     * @param mId_credencial
     * @param responseCallback
     * @throws Exception
     */

    public synchronized void sendResponse(final HashMap<String, String> hashMap, final String rut, final String institucion, final String serialNumber, final String ns_cedula, final String mId_credencial, String trackId,final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.POST_ANSWERS;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), serialNumber, institucion);

        RespuestaParam resp;
        List<RespuestaParam> respList = new ArrayList<>();

        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            resp = new RespuestaParam(key, value);
            respList.add(resp);
        }

        RespuestaListParam rlp = new RespuestaListParam();
        rlp.setRut(rut);
        rlp.setInstitucion(institucion);
        rlp.setOrigen("ANDROID");
        rlp.setRutOperador(mPreferences.getString(KeyPreferences.RUN_INSTITUTION));
        rlp.setOperacion("VERIFICACION_TRANSUNION");
        rlp.setNumeroSerieLector(serialNumber);
        rlp.setProposito("6");
        rlp.setNroSerieCedula(ns_cedula);
        rlp.setTrackId(trackId);
        rlp.setVersionApp(AppHelper.getCurrentVersion(mContext));
        if (mId_credencial != null) {
            rlp.setIdCredencials(mId_credencial);
        }
        rlp.setRespuestas(respList);
        final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(rlp));
        Log.e("dd", jsonObject.toString());

        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);

                    if (jsonResp.getInt("status") == 0 || jsonResp.getInt("status") == 1) {
                        //si es exitosa la respuesta
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    } else {
                        String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(institucion, new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            sendResponse(hashMap, rut, institucion, serialNumber, ns_cedula, mId_credencial, trackId,responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET_TRANSUNION));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });

    }


    /**
     *
     * AutoUpdate
     *
     * @param responseCallback
     * @throws Exception
     */

    public synchronized void getCheckVersion(final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.CHECK_VERSION_AUTO_UPDATE;

        autoUpdateParams aup = new autoUpdateParams();
        aup.setVersion(AppHelper.getCurrentVersion(mContext));
        final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(aup));
        VolleyHelper vh = new VolleyHelper(mContext, new HashMap<String, String>(), url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                autoUpdateResp aur;
                aur = new Gson().fromJson(response, autoUpdateResp.class);
                if (aur.getStatus() == 0) {
                    if (aur.getPriority() > 0 || aur.getPriority() < 4) {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    } else {
                        //TODO: DETERMINAR COMPORTAMIENTO
                        responseCallback.onResponseError(new AutentiaMovilException("No se puede obtener actualización", ReturnCode.ERROR_GENERICO));
                    }
                } else {
                    String cause = aur.getGlosa();
                    if (aur.getStatus() == 404)
                        responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_TIMEOUT));
                    else if (aur.getStatus() == 900)
                        responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_PARSE));
                    else
                        responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                }

            }
        });
    }


    public synchronized void browseableMethod(final String url, final JSONObject jsonObject, final ResponseCallback responseCallback) {

        Map<String, String> basicHeaders = new HashMap<String, String>();
        basicHeaders.put("Content-Type", "application/json; charset=utf-8");

        if (jsonObject != null) {
            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    Log.e("browseableMethod", response);
                    Bundle data = new Bundle();
                    data.putString("result", response);
                    responseCallback.onResponseSuccess(data);
                }
            });
        } else {
            responseCallback.onResponseError(new AutentiaMovilException("empty", ReturnCode.ERROR_GENERICO));
        }
    }


    /**
     * Registro de dispositivo en firebase
     */


    public synchronized void deviceRegistration() {

        String url = mUrlBase + UrlList.REGISDEVICE;

        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"),
                mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER),
                mPreferences.getString(KeyPreferences.INSTITUTION));

        StringBuilder builder = new StringBuilder();
        builder.append("android : ").append(Build.VERSION.RELEASE);

        String osName = "";

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                osName = fieldName;
            }
        }

        try {
            DeviceRegistrationParam drp = new DeviceRegistrationParam();
            drp.setDeviceId(Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID));
            drp.setInstitucion(mPreferences.getString(KeyPreferences.INSTITUTION));
            drp.setHuellero(mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER));
            drp.setSdkVersion(String.valueOf(Build.VERSION.SDK_INT));
            drp.setAppVersion(AppHelper.getCurrentVersion(mContext));
            drp.setOsName(String.format("%s-%s", Utils.getDeviceName(), osName));

            JSONObject jsonObject = new JSONObject(new Gson().toJson(drp));

            VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    try {
                        org.json.JSONObject jsonResp = new org.json.JSONObject(response);
//                        Log.e("resp reg",jsonResp.toString());


                        if (jsonResp.getInt("status") == 0) {
                            //si es exitosa la respuesta

                        } else {
                            String cause = jsonResp.getString("glosa");
                            if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                                if (errorCount < 3) {
                                    errorCount++;
                                    getToken(mPreferences.getString(KeyPreferences.INSTITUTION), new onResponseMethod() {
                                        @Override
                                        public void action(String response) {
                                            try {
                                                deviceRegistration();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            } else {
                                Log.e("error", "status:" + jsonResp.getString("status") + "; glosa:" + jsonResp.getString("glosa"));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error", e.getMessage());
                    }
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e("NameNotFoundException", e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", e.getMessage());
        }

    }

    public synchronized void getOtiInfo(String mUrl, final ResponseCallback responseCallback) {

        Map<String, String> basicHeaders = new HashMap<String, String>();
        basicHeaders.put("Authorization", "Basic bW92aWw6MzY1YTc4NzgwMGQ0MWYyZg==");

        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, mUrl);
        vh.getRequestVolley(new JSONObject(), new onResponseMethod() {
            @Override
            public void action(String response) {
                Log.e("resp", response);

                if (response.contains("code")) {
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response));

                        if (jsonObject.getString("code").equals("404"))
                            responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_TIMEOUT));
                        else if (jsonObject.getString("code").equals("900"))
                            responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_PARSE));
                        else
                            responseCallback.onResponseError(new AutentiaMovilException(jsonObject.getString("cause"), ReturnCode.ERROR_GENERICO));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Bundle bl = new Bundle();
                    bl.putString("otiResp", response);
                    responseCallback.onResponseSuccess(bl);
                }
            }
        });

    }

    public void setOAuthAudit(String id_token, String nroSerieLector, String institucion, final String tipoLector, final String resultado,
                              final String operacion, final String rutInstitucion, final String versionApp, final ResponseCallback responseCallback) throws Exception {

        String url = mUrlBase + UrlList.OAUTHFACIALAUDIT;
//        String url = "http://192.168.1.117:8080/"+UrlList.OAUTHFACIALAUDIT;
        Map<String, String> basicHeaders = getHeaders(mPreferences.getString("token"), nroSerieLector, institucion);

        FacialOAuthParams facialOAuthParams = new FacialOAuthParams();
        facialOAuthParams.setIdToken(id_token);
        facialOAuthParams.setNroSerieLector(nroSerieLector);
        facialOAuthParams.setInstitucion(institucion);
        facialOAuthParams.setResultado(resultado);
        facialOAuthParams.setRutInstitucion(rutInstitucion);
        facialOAuthParams.setVersionApp(versionApp);
        facialOAuthParams.setOperacion(operacion);
        facialOAuthParams.setTipoLector(tipoLector);


        final org.json.JSONObject jsonObject = new org.json.JSONObject(new Gson().toJson(facialOAuthParams));
        VolleyHelper vh = new VolleyHelper(mContext, basicHeaders, url);
        vh.postRequestVolley(jsonObject, new onResponseMethod() {
            @Override
            public void action(String response) {
                Log.e("RESPONSE", response);
                try {
                    org.json.JSONObject jsonResp = new org.json.JSONObject(response);
                    if (jsonResp.has("status") && jsonResp.getInt("status") != 0) {
                        final String cause = jsonResp.getString("glosa");
                        if (jsonResp.getInt("status") == 401 || jsonResp.getInt("status") == 5002) { //token invalido
                            if (errorCount < 3) {
                                errorCount++;
                                getToken(institucion, new onResponseMethod() {
                                    @Override
                                    public void action(String response) {
                                        try {
                                            setOAuthAudit(id_token, nroSerieLector, institucion, tipoLector, resultado,
                                                    operacion, rutInstitucion, versionApp, responseCallback);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                                        }
                                    }
                                });
                            } else {
                                responseCallback.onResponseError(new AutentiaMovilException(tokenMsgError, ReturnCode.INVALID_TOKEN));
                            }
                        } else {
                            if (jsonResp.getInt("status") == 404)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VERIFICATION_SIN_INTERNET));
                            else if (jsonResp.getInt("status") == 900)
                                responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.VOLLEY_PARSE));
                            else
                                responseCallback.onResponseError(new AutentiaMovilException(jsonResp.getString("glosa"), ReturnCode.ERROR_GENERICO));
                        }

                    } else {
                        Bundle data = new Bundle();
                        data.putString("result", response);
                        responseCallback.onResponseSuccess(data);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    responseCallback.onResponseError(new AutentiaMovilException(e.getMessage(), ReturnCode.ERROR_GENERICO));
                }
            }
        });

    }

    public synchronized void getTyCforApplication(String institucion, ResponseCallback responseCallback){

        String url = mUrlBase + UrlList.TERMS_AND_CONDITION_URL;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("institucion",institucion);
            VolleyHelper vh = new VolleyHelper(mContext, new HashMap<String, String>(), url);
            vh.postRequestVolley(jsonObject, new onResponseMethod() {
                @Override
                public void action(String response) {
                    Log.e("RESPONSE",response);
                    TerminosParam tyCp;
                    tyCp = new Gson().fromJson(response, TerminosParam.class);
                    if (tyCp.getStatus() == 0) {
                        mPreferences.setString(KeyPreferences.TERMS_AND_CONDITIONS,new Gson().toJson(tyCp));
                        Log.e("success","we made it: "+ mPreferences.getString(KeyPreferences.TERMS_AND_CONDITIONS));
                        Bundle data = new Bundle();
                        data.putString("result", tyCp.getTerminos().toString());
                        responseCallback.onResponseSuccess(data);
                    } else {
                        String cause = tyCp.getGlosa();
                        if (tyCp.getStatus() == 404)
                            responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_TIMEOUT));
                        else if (tyCp.getStatus() == 900)
                            responseCallback.onResponseError(new AutentiaMovilException(ReturnCode.VOLLEY_PARSE));
                        else
                            responseCallback.onResponseError(new AutentiaMovilException(cause, ReturnCode.ERROR_GENERICO));
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}