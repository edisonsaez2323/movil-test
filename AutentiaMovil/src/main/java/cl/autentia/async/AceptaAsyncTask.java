package cl.autentia.async;

/**
 * Funciona en referencia cruzada con una instancia de
 * {@link AceptaAsyncTaskCallbacks}
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 * @author gustavo
 */
public class AceptaAsyncTask<Params, Progress, Result> extends
        android.os.AsyncTask<Params, Progress, Result> {
    private AceptaAsyncTaskCallbacks<Params, Progress, Result> callbacks;
    private boolean finishedWithError;
    private int errorCode;
    private Object[] errorArguments;
    private Exception exception;

    public AceptaAsyncTask(
            AceptaAsyncTaskCallbacks<Params, Progress, Result> callbacks) {
        if (callbacks != null) {
            this.callbacks = callbacks;
            callbacks.setAsyncTask(this);
        }
    }

    public boolean isFinishedWithError() {
        return finishedWithError;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public Object[] getErrorArguments() {
        return errorArguments;
    }

    public Exception getException() {
        return exception;
    }

    public void clearError() {
        this.errorCode = 0;
        this.exception = null;
        this.errorArguments = null;
        this.finishedWithError = false;
    }

    public void setError(int errorCode) {
        this.errorCode = errorCode;
        this.finishedWithError = true;
    }

    public void setError(int errorCode, Exception exception) {
        this.errorCode = errorCode;
        this.exception = exception;
        this.finishedWithError = true;
    }

    public void setError(int errorCode, Object[] errorArguments) {
        this.errorCode = errorCode;
        this.errorArguments = errorArguments;
        this.finishedWithError = true;
    }

    @Override
    protected Result doInBackground(Params... params) {
        return null;
    }

    @Override
    protected void onCancelled() {
        if (callbacks != null)
            callbacks.onCancelled();
    }

    @Override
    protected void onCancelled(Result result) {
        if (callbacks != null)
            callbacks.onCancelled(result);
    }

    @Override
    protected void onPostExecute(Result result) {
        if (callbacks != null)
            callbacks.onPostExecute(result);
    }

    @Override
    protected void onPreExecute() {
        if (callbacks != null)
            callbacks.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Progress... values) {
        if (callbacks != null)
            callbacks.onProgressUpdate(values);
    }

}