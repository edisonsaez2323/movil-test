package cl.autentia.async;

/**
 * Funciona en referencia cruzada con una instancia de {@link AceptaAsyncTask}
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 * @author gustavo
 */
public class AceptaAsyncTaskCallbacks<Params, Progress, Result> {
    protected AceptaAsyncTask<Params, Progress, Result> asyncTask;

    public AceptaAsyncTask<Params, Progress, Result> getAsyncTask() {
        return asyncTask;
    }

    void setAsyncTask(AceptaAsyncTask<Params, Progress, Result> asyncTask) {
        this.asyncTask = asyncTask;
    }

    protected void onCancelled() {
    }

    protected void onCancelled(Result result) {
    }

    protected void onPostExecute(Result result) {
    }

    protected void onPreExecute() {
    }

    protected void onProgressUpdate(Progress... values) {
    }
}
