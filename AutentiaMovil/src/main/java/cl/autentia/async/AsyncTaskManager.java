package cl.autentia.async;

import android.os.AsyncTask;


public abstract class AsyncTaskManager<T> extends AsyncTask<Object, Integer, T> {

    private Exception exception;

    public abstract T doWork(Object... objects) throws Exception;

    public abstract void onResult(T result);

    public abstract void onError(Exception exception);

    public AsyncTaskManager() {

    }

    @Override
    protected T doInBackground(Object... params) {

        try {

            return doWork(params);
        } catch (Exception e) {
            e.printStackTrace();
            exception = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(T result) {
        super.onPostExecute(result);

        if (exception == null)
            onResult(result);
        else
            onError(exception);
    }
}
