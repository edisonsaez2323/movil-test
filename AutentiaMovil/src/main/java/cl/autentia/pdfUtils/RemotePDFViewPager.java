package cl.autentia.pdfUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.io.File;

import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.remote.DownloadFileUrlConnectionImpl;
import es.voghdev.pdfviewpager.library.util.FileUtil;

/**
 * Created by Edison Saez on 19-01-2018.
 */

public class RemotePDFViewPager extends ViewPagerFixed implements DownloadFile.Listener{

    protected Context context;
    protected DownloadFile.Listener listener;

    public RemotePDFViewPager(Context context, String pdfUrl, DownloadFile.Listener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
        this.init(pdfUrl);
    }

    public RemotePDFViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.init(attrs);
    }

    private void init(String pdfUrl) {
        DownloadFile downloadFile = new DownloadFileUrlConnectionImpl(this.context, new Handler(), this);
        downloadFile.download(pdfUrl, (new File(this.context.getCacheDir(), FileUtil.extractFileNameFromURL(pdfUrl))).getAbsolutePath());
    }

    private void init(AttributeSet attrs) {
        if(attrs != null) {
            TypedArray a = this.context.obtainStyledAttributes(attrs, es.voghdev.pdfviewpager.library.R.styleable.PDFViewPager);
            String pdfUrl = a.getString(es.voghdev.pdfviewpager.library.R.styleable.PDFViewPager_pdfUrl);
            if(pdfUrl != null && pdfUrl.length() > 0) {
                this.init(pdfUrl);
            }

            a.recycle();
        }

    }

    public void onSuccess(String url, String destinationPath) {
        this.listener.onSuccess(url, destinationPath);
    }

    public void onFailure(Exception e) {
        this.listener.onFailure(e);
    }

    public void onProgressUpdate(int progress, int total) {
        this.listener.onProgressUpdate(progress, total);
    }

    public class NullListener implements DownloadFile.Listener {
        public NullListener() {
        }

        public void onSuccess(String url, String destinationPath) {
        }

        public void onFailure(Exception e) {
        }

        public void onProgressUpdate(int progress, int total) {
        }
    }

}




