package cl.autentia.FacialOAuth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.acepta.android.fetdroid.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import cl.autentia.helper.AppHelper;
import cl.autentia.helper.AutentiaMovilException;
import cl.autentia.http.AutentiaWSClient;
import cl.autentia.http.ResponseCallback;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

public class FaceCallback extends AppCompatActivity {

    AutentiaWSClient mAutentiaWSClient;
    AutentiaPreferences mPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.face_callback);

        mAutentiaWSClient = new AutentiaWSClient(this);
        mPreferences = new AutentiaPreferences(this);

        String data = getIntent().getData().toString();

        if (data.contains("id_token")){
//            String msg= String.format("%s : %s\n\n", "resultado", "Exito al verificar rostro");
//            msg+= String.format("%s : %s\n\n", "status", "0");
//            msg+= String.format("%s : %s\n\n", "glosa", "Proceso finalizado correctamente");
//            showMessage(msg);
            String val = data.replace("autentia://facecallback#id_token=", "");
            try {
                oAuthAuditCaller(val, "0000");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                String msg= String.format("%s : %s\n\n", "resultado", "Error al verificar");
                msg+= String.format("%s : %s\n\n", "status", "1");
                msg+= String.format("%s : %s\n\n", "glosa", "Rostro no reconocido");
                showMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle("Resultado Verificacion:")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        openTest();
                        finish();
                    }
                })
                .create().show();
    }

    public void openTest(){
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage("cl.autentia.testerautentiamovil");
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }
    }


    private void oAuthAuditCaller(String id_token, String resultado) throws Exception {

        mAutentiaWSClient.setOAuthAudit(id_token, mPreferences.getString(KeyPreferences.FINGERPRINT_SERIAL_NUMBER),
                mPreferences.getString(KeyPreferences.INSTITUTION),mPreferences.getString("mReaderType"),resultado,
                "BIOMETRIA FACIAL",mPreferences.getString(KeyPreferences.RUN_INSTITUTION),
                AppHelper.getCurrentVersion(FaceCallback.this)
                , new ResponseCallback() {
                    @Override
                    public void onResponseSuccess(Bundle result) {
//                Log.e("RESPONSERIGHT", result.getString("result"));
//                FacialOAuthActivity.this.finish();

                        String msg = "";
                        JSONObject json = null;
                        try {
                            json = new JSONObject(result.get("result").toString());

                            if (json.getInt("status") == 0){
                                msg+= String.format("%s : %s\n\n", "resultado", "Exito al verificar");
                            }else {
                                msg+= String.format("%s : %s\n\n", "resultado", "Error al verificar");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Iterator<String> iter = json.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                Object value = json.get(key);

                                msg += String.format("%s : %s\n\n", key, value);
                            } catch (JSONException e) {
                                // Something went wrong!
                            }
                        }

//                        Iterator iterator = result.keySet().iterator();
//                        String msg = "";
//                        while (iterator.hasNext()) {
//
//                            String key = iterator.next().toString();
//                            Object value = result.get(key);
//
//                            msg += String.format("%s : %s\n\n", key, value);
//
//                        }
                        showMessage(msg);

                    }

                    @Override
                    public void onResponseError(AutentiaMovilException result) {
                        Intent intent = new Intent();
                        intent.putExtra("Error",result.status);
                        intent.putExtra("CODIGO_RESPUESTA",result.returnCode);
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                });

    }
}
