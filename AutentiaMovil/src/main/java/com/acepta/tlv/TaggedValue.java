package com.acepta.tlv;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.acepta.Utils.asHex;
import static com.acepta.tlv.TLVConst.TAG_OBJECT_IDENTIFIER;
import static com.acepta.tlv.TLVConst.TAG_SEQUENCE;

public class TaggedValue {

    public long tag;
    public boolean constructed;
    public long valueLength;
    public byte[] value;
    public List<TaggedValue> tags = new ArrayList<TaggedValue>();

    public TaggedValue() {
    }

    public TaggedValue(long tag, TaggedValue... tlvs) {
        this.tag = tag;
        this.constructed = true;
        this.value = null;
        this.valueLength = 0;
        for (TaggedValue tlv : tlvs) {
            this.tags.add(tlv);
        }
    }

    public boolean exists(long tag) {
        for (TaggedValue item : tags) {
            if (item.tag == tag)
                return true;
        }
        return false;
    }

    public TaggedValue find(long tag) {
        return find(tag, 0);
    }

    public TaggedValue find(long tag, int nth_match) {
        int match = 0;
        for (TaggedValue item : tags) {
            if (item.tag == tag) {
                match++;
                if ((match == nth_match) || (nth_match == 0)) {
                    return item;
                }
            }
        }
        return null;
    }

    public List<TaggedValue> findAll(long tag) {
        ArrayList<TaggedValue> result = new ArrayList<TaggedValue>();
        for (TaggedValue item : tags) {
            if (item.tag == tag) {
                result.add(item);
            }
        }
        return result;
    }

    boolean isText(byte[] data) {
        for (byte b : data) {
            if (b < 32) return false;
        }
        return true;
    }

    public TaggedValue findObject(byte[] anOID) {
        for (TaggedValue item : tags) {
            if (item.tag == TAG_SEQUENCE) {
                if (!item.tags.isEmpty()) {
                    TaggedValue child0 = item.tags.get(0);
                    if ((child0.tag == TAG_OBJECT_IDENTIFIER)
                            && Arrays.equals(child0.value, anOID)) {
                        return item;
                    }
                }
            }
            if (item.constructed) {
                TaggedValue object = item.findObject(anOID);
                if (object != null)
                    return object;
            }
        }
        return null;
    }

    static int VALUE_SAMPLE_LENGTH = 64;

    public void print(int depth, PrintStream ps) {
        String labelConstructed = "", valueContinues, decodedOid = "";
        byte[] valueSample;
        if (constructed) {
            labelConstructed = " constructed";
        } else {
            decodedOid = "";
        }
        if (tag == TAG_OBJECT_IDENTIFIER) {
            decodedOid = OID.asString(value);
        }
        ps.printf("%sTag %x%s [%s] %s\n", margin(depth), tag, labelConstructed, tagLabel(tag), decodedOid);
        ps.printf("%sLength %d (0x%x)\n", margin(depth + 1), valueLength, valueLength);
        if (constructed) {
            // DBG("%sTags:", margin(depth + 1));
            for (TaggedValue item : tags) {
                item.print(depth + 2, ps);
            }
        } else {
            valueSample = Arrays.copyOf(value,
                    Math.min(VALUE_SAMPLE_LENGTH, value.length));
            if (value.length > VALUE_SAMPLE_LENGTH) {
                valueContinues = "...";
            } else {
                valueContinues = "";
            }
            if (isText(valueSample)) {
                ps.printf("%sValue '%s'%s\n", margin(depth + 1), new String(valueSample), valueContinues);
            } else {
                ps.printf("%shexValue %s%s\n", margin(depth + 1), asHex(valueSample), valueContinues);
            }
        }
    }

    private String margin(int depth) {
        StringBuilder sb = new StringBuilder(depth);
        while (depth > 0) {
            sb.append(" ");
            depth--;
        }
        return sb.toString();
    }

    public static String tagLabel(long tag) {
        switch ((int) tag) {
            // universal tags
            case 0x01:
                return "BOOLEAN";
            case 0x02:
                return "INTEGER";
            case 0x03:
                return "BIT STRING";
            case 0x04:
                return "OCTET STRING";
            case 0x05:
                return "NULL";
            case 0x06:
                return "OBJECT IDENTIFIER";
            case 0x0C:
                return "UTF8String";
            case 0x30:
                return "SEQ or SEQ OF";
            case 0x31:
                return "SET or SET OF";
            case 0x13:
                return "PrintableString";
            case 0x14:
                return "T61String";
            case 0x16:
                return "IA5String";
            case 0x17:
                return "UTCTime";

            case 0x5C:
                return "Tag list";
            case 0x5F01:
                return "LDS Version Number";
            case 0x5F08:
                return "Date of birth (truncated)";

            case 0x5F09:
                return "Compressed image (ANSI/NIST-ITL 1-2000)";
            case 0x5F0A:
                return "Security features - Encoded Data";
            case 0x5F0B:
                return "Security features - Structure";
            case 0x5F0C:
                return "Security features";
            case 0x5F0E:
                return "Full name, in national characters";
            case 0x5F0F:
                return "Other names";

            case 0x5F10:
                return "Personal Number";
            case 0x5F11:
                return "Place of birth";
            case 0x5F12:
                return "Telephone";
            case 0x5F13:
                return "Profession";
            case 0x5F14:
                return "Title";
            case 0x5F15:
                return "Personal Summary";
            case 0x5F16:
                return "Proof of citizenship (10918 image)";
            case 0x5F17:
                return "Other valid TD Numbers";
            case 0x5F18:
                return "Custody information";
            case 0x5F19:
                return "Issuing Authority";
            case 0x5F1A:
                return "Other people on document";
            case 0x5F1B:
                return "Endorsement/Observations";
            case 0x5F1C:
                return "Tax/Exit requirements";
            case 0x5F1D:
                return "Image of document front";
            case 0x5F1E:
                return "Image of document rear";
            case 0x5F1F:
                return "MRZ data elements";

            case 0x5F26:
                return "Date of Issue";
            case 0x5F2B:
                return "Date of birth (8 digit)";
            case 0x5F2E:
                return "Biometric data block";

            case 0x5F36:
                return "Unicode Version Level";

            case 0x5F40:
                return "Compressed image template";
            case 0x5F42:
                return "Address";
            case 0x5F43:
                return "Compressed image template";

            case 0x5F50:
                return "Date data recorded";
            case 0x5F51:
                return "Name of person";
            case 0x5F52:
                return "Telephone";
            case 0x5F53:
                return "Address";

            case 0x5F55:
                return "Date and time document personalized";
            case 0x5F56:
                return "Serial number of personalization system";

            case 0x60:
                return "Common data elements";
            case 0x61:
                return "Template for MRZ data group";
            case 0x63:
                return "Template for Finger biometric data group";
            case 0x65:
                return "Template for digitized facial image";
            case 0x67:
                return "Template for digitized Signature or usual mark";
            case 0x68:
                return "Template for Machine Assisted Security - Encoded Data";
            case 0x69:
                return "Template for Machine Assisted Security - Structure";
            case 0x6A:
                return "Template for Machine Assisted Security - Substance";
            case 0x6B:
                return "Template for Additional Personal Details";
            case 0x6C:
                return "Template for Additional Document Details";
            case 0x6D:
                return "Optional details";
            case 0x6E:
                return "Reserved for future use";
            case 0x70:
                return "Person to Notify";
            case 0x75:
                return "Template for facial biometric data group";
            case 0x76:
                return "Template for Iris (eye) biometric template";
            case 0x77:
                return "EF.SOD (EF for security data)";
            case 0x7F2E:
                return "Biometric data block (enciphered)";
            case 0x7F60:
                return "Biometric Information Template";
            case 0x7F61:
                return "Biometric Information Group Template";

            case 0x80:
                return "ICAO header version";
            case 0x81:
                return "Biometric Type";
            case 0x82:
                return "Biometric subtype";
            case 0x83:
                return "Creation date and time";
            case 0x84:
                return "Validity period"; // (revized in nov 2008)
            case 0x85:
                return "Validity period"; // (since 2008)
            case 0x86:
                return "Creator of biometric reference data";
            case 0x87:
                return "Format Owner";
            case 0x88:
                return "Format Type";
            case 0x89:
                return "Context specific tags";
            case 0x8A:
                return "Context specific tags";
            case 0x8B:
                return "Context specific tags";
            case 0x8C:
                return "Context specific tags";
            case 0x8D:
                return "Context specific tags";
            case 0x8E:
                return "Context specific tags";
            case 0x8F:
                return "Context specific tags";

            case 0x90:
                return "Enciphered hash code";

            case 0xA0:
                return "Context specific constructed data objects";

            case 0xA1:
                return "Repeating template, 1 occurrence Biometric header";
            case 0xA2:
                return "Repeating template, 2 occurrence Biometric header";
            case 0xA3:
                return "Repeating template, 3 occurrence Biometric header";
            case 0xA4:
                return "Repeating template, 4 occurrence Biometric header";
            case 0xA5:
                return "Repeating template, 5 occurrence Biometric header";
            case 0xA6:
                return "Repeating template, 6 occurrence Biometric header";
            case 0xA7:
                return "Repeating template, 7 occurrence Biometric header";
            case 0xA8:
                return "Repeating template, 8 occurrence Biometric header";
            case 0xA9:
                return "Repeating template, 9 occurrence Biometric header";
            case 0xAA:
                return "Repeating template, 10 occurrence Biometric header";
            case 0xAB:
                return "Repeating template, 11 occurrence Biometric header";
            case 0xAC:
                return "Repeating template, 12 occurrence Biometric header";
            case 0xAD:
                return "Repeating template, 13 occurrence Biometric header";
            case 0xAE:
                return "Repeating template, 14 occurrence Biometric header";
            case 0xAF:
                return "Repeating template, 15 occurrence Biometric header";

            case 0xB0:
                return "Repeating template, 0 occurrence Biometric header";
            case 0xB1:
                return "Repeating template, 1 occurrence Biometric header";
            case 0xB2:
                return "Repeating template, 2 occurrence Biometric header";
            case 0xB3:
                return "Repeating template, 3 occurrence Biometric header";
            case 0xB4:
                return "Repeating template, 4 occurrence Biometric header";
            case 0xB5:
                return "Repeating template, 5 occurrence Biometric header";
            case 0xB6:
                return "Repeating template, 6 occurrence Biometric header";
            case 0xB7:
                return "Repeating template, 7 occurrence Biometric header";
            case 0xB8:
                return "Repeating template, 8 occurrence Biometric header";
            case 0xB9:
                return "Repeating template, 9 occurrence Biometric header";
            case 0xBA:
                return "Repeating template, 10 occurrence Biometric header";
            case 0xBB:
                return "Repeating template, 11 occurrence Biometric header";
            case 0xBC:
                return "Repeating template, 12 occurrence Biometric header";
            case 0xBD:
                return "Repeating template, 13 occurrence Biometric header";
            case 0xBE:
                return "Repeating template, 14 occurrence Biometric header";
            case 0xBF:
                return "Repeating template, 15 occurrence Biometric header";
            // }

            // DOC9303-2 pg III-40
            // tagMRZtoName = {
            case 0x53:
                return "Optional Data";
            case 0x59:
                return "Date of Expiry or valid Until Date";
            // case 0x02: return "Document Number";

            case 0x5F02:
                return "Check digit - Optional data (ID-3 only)";
            case 0x5F03:
                return "Document Type";
            case 0x5F04:
                return "Check digit - Doc Number";
            case 0x5F05:
                return "Check digit - DOB";
            case 0x5F06:
                return "Expiry date";
            case 0x5F07:
                return "Composite";

            case 0x5F20:
                return "Issuing State or Organization";
            // case 0x5F2B: return "Date of birth";
            case 0x5F2C:
                return "Nationality";

            case 0x5F35:
                return "Sex";
            case 0x5F57:
                return "Date of birth (6 digit)";

            // From DG1 (information tags)
            case 0x5F28:
                return "Issuing State or Organization";
            case 0x5F5B:
                return "Name of Holder"; // version 2006
            case 0x5B:
                return "Name of Holder"; // version 2008
            case 0x5A:
                return "Document Number";

            // }

            // DOC9303-2 pg III-40
            // tagRFUtoName = {
            case 0x5F44:
                return "Country of entry/exit";
            case 0x5F45:
                return "Date of entry/exit";
            case 0x5F46:
                return "Port of entry/exit";
            case 0x5F47:
                return "Entry/Exit indicator";
            case 0x5F48:
                return "Length of stay";
            case 0x5F49:
                return "Category (classification)";
            case 0x5F4A:
                return "Inspector reference";
            case 0x5F4B:
                return "Entry/Exit indicator";
            case 0x71:
                return "Template for Electronic Visas";
            case 0x72:
                return "Template for Border Crossing Schemes";
            case 0x73:
                return "Template for Travel Record Data Group";

            // DataGroup
            /*
			 * $60: Result := 'Index'; $61: Result := 'MRZ'; $75: Result :=
			 * 'Face'; $63: Result := 'Finger'; $76: Result := 'Eye (Iris)';
			 * $65: Result := 'Portrait'; $66: Result := 'Reserved for Future
			 * Use'; $67: Result := 'Signature or Usual Mark'; $68: Result :=
			 * 'Data Features'; $69: Result := 'Structure Features'; $6A: Result
			 * := 'Substance Features'; $6B: Result := 'Additional Personal
			 * Detail(s)'; $6C: Result := 'Additional Documents Detail(s)'; $6D:
			 * Result := 'Optional Details (Country Specific)'; $6E: Result :=
			 * 'Reserved for Future Use'; $6F: Result := 'Active Authentication
			 * Public Key Info'; $70: Result := 'Person to Notify'; $77: Result
			 * := 'Security Object';
			 */
        }
        return "";
    }
}
