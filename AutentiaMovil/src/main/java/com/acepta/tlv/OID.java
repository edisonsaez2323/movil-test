package com.acepta.tlv;

import java.io.ByteArrayOutputStream;

public class OID {

	private static void addPart(StringBuilder sb, int i) {
		if (sb.length() > 0)
			sb.append('.');
		sb.append(Integer.toString(i));
	}
	
	public static String asString(byte[] anOID) {
		StringBuilder result = new StringBuilder();
		int oidPart, aByte;

		addPart(result, (anOID[0] & 0xff) / 40);
		addPart(result, (anOID[0] & 0xff) % 40);

		oidPart = 0;
		for (int i = 1; i < anOID.length; i++) {
			aByte = anOID[i] & 0xff;
			oidPart = oidPart * 0x80;
			oidPart = oidPart + (aByte & 0x7f);
			if ((aByte & 0x80) == 0) {
				addPart(result, oidPart);
				oidPart = 0;
			}
		}
		
		return result.toString();
	}
	
	public static byte[] encode(String s) {
		String[] parts = s.split("[.]");
		Integer[] ints = new Integer[parts.length];
		for(int n = 0; n < ints.length; n++) {
			ints[n] = Integer.parseInt(parts[n]);			
		}
		return encode(ints);
	}
	
	public static byte[] encode(Integer...ints) {
		OIDEncoder e = new OIDEncoder();
		return e.encode(ints);
	}
}

class OIDEncoder {
	
	ByteArrayOutputStream m_encoded = new ByteArrayOutputStream();
	byte[] m_part = new byte[8];

    public OIDEncoder() {}
    
  	byte[] encode(Integer[] anOID) {
  		
  		if (anOID.length < 2) return null;

  		encodePart(anOID[0] * 40 + anOID[1]);

  		for(int n = 2; n < anOID.length; n++) {
  			encodePart(anOID[n]);
  		}

  		return m_encoded.toByteArray();
  	}

	private void encodePart(int aPart) {
		int partLength = length(aPart);
		for (int n = partLength - 1; n >= 0; n--) {
			m_part[n] = (byte) ((aPart % 0x80) | 0x80);
			aPart = aPart / 0x80;
		}
		m_part[partLength - 1] &= 0x7f;
		m_encoded.write(m_part, 0, partLength);
	}

	private int length(int aPart) {
		int result = 0;
		while (aPart > 0) {
			result += 1;
			aPart /= 0x80;
		}
		return result;
	}
}