package com.acepta.tlv;

import java.nio.ByteBuffer;

public class BinaryParser {

	private ByteBuffer m_buffer;
	
	public BinaryParser(ByteBuffer bytes) {
		m_buffer = bytes;
	}

	public short readUInt8() {
		return (short) (m_buffer.get() & 0xff);
	}

	public byte[] readBytes(long length) {
		if (m_buffer.remaining() < length)
			return null;
		byte[] result = new byte[(int) length];
		m_buffer.get(result, 0, (int) length);
		return result;
	}

	public int getPosition() {
		return m_buffer.position();
	}

	public long readUInt32() {
		long result = 0;
		result = readUInt8();
		result = result << 8 | readUInt8();
		result = result << 8 | readUInt8();
		result = result << 8 | readUInt8();
		return result;
	}

	public int readUInt16() {
		int result = 0;
		result = readUInt8();
		result = result << 8 | readUInt8();
		return result;
	}

	private final static long BASE_MASK = 0xffffffffL;
	
	public long getBits(long value, int bitsPos, int bitCount) {
		long mask = BASE_MASK >> (32 - bitCount);
	  	return (value >> bitsPos) & mask;
	}

	public static long setBits(long original, int bitsPos, int bitCount, long value) {
	  long mask = BASE_MASK >> (32 - bitCount);
	  long anti_mask = ~(mask << bitsPos);
	  return (original & anti_mask) | ((value & mask) << bitsPos);
	}
}
