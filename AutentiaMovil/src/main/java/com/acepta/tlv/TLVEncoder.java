package com.acepta.tlv;

public class TLVEncoder {

	public static byte[] encodeLength(long len) {
		byte[] result;
		if (len < 0x80) {
			result = new byte[1];
			result[0] = (byte) len;
		}
		else {
			int usedBytes = 0;
			long tmp = 0; 
		    while (len > 0) {
		    	tmp = (tmp << 8) | (len & 0xff);
		    	usedBytes += 1;
		    	len >>= 8;
		    }
	    	tmp = (tmp << 8) | (0x80 | usedBytes);
	    	result = new byte[usedBytes + 1];
	    	int pos = 0;
	    	while (tmp > 0) {
	    		result[pos] = (byte) (tmp & 0xff);
	    		tmp >>= 8;
	    		pos ++;
	    	}
		}
    	return result;
	}

	public static byte[] encodeTag(long tag) {
		byte[] result = new byte[tagLength(tag)];
		int pos = result.length - 1;
		while (tag > 0) {
			result[pos] = (byte) (tag & 0xff);
			tag = tag >> 8;
			pos --;
		}
		return result;
	}

	private static int tagLength(long tag) {
		int result = 0;
		while (tag > 0) {
			result += 1;
			tag >>= 8;
		}
		return (result == 0) ? 1 : result;
	}
}
