package com.acepta.tlv;

import java.nio.ByteBuffer;

import static com.acepta.tlv.TLVConst.EXT_LENGTH;
import static com.acepta.tlv.TLVConst.LONG_TAG;
import static com.acepta.tlv.TLVConst.TAG_APPLICATIVE;
import static com.acepta.tlv.TLVConst.TAG_CLASS_MASK;
import static com.acepta.tlv.TLVConst.TAG_CONSTRUCTED;
import static com.acepta.tlv.TLVConst.TAG_CONTEXTUAL;
import static com.acepta.tlv.TLVConst.TAG_PRIVATE;
import static com.acepta.tlv.TLVConst.TAG_STRUCT_MASK;
import static com.acepta.tlv.TLVConst.TAG_TAIL;
import static com.acepta.tlv.TLVConst.TAG_TAIL_MASK;
import static com.acepta.tlv.TLVConst.TAG_UNIVERSAL;

public class TLVReader {

	private BinaryParser m_bp;

	public TLVReader(ByteBuffer bytes) {
		m_bp = new BinaryParser(bytes);
	}

	public TLVReader(byte[] bytes) {
		m_bp = new BinaryParser(ByteBuffer.wrap(bytes));
	}

	public static boolean tagIsUniversal(long tag) {
		while (tag > 0xff) tag = tag >> 8;
		return (tag & TAG_CLASS_MASK) == TAG_UNIVERSAL;
	}
	
	public static boolean tagIsApplicative(long tag) {
		while (tag > 0xff) tag = tag >> 8;
		return (tag & TAG_CLASS_MASK) == TAG_APPLICATIVE;
	}
	
	public static boolean tagIsContextual(long tag) {
		while (tag > 0xff) tag = tag >> 8;
		return (tag & TAG_CLASS_MASK) == TAG_CONTEXTUAL;
	}
	
	public static boolean tagIsPrivate(long tag) {
		while (tag > 0xff) tag = tag >> 8;
		return (tag & TAG_CLASS_MASK) == TAG_PRIVATE;
	}
	
	public static boolean tagIsConstructed(long tag) {
		while (tag > 0xff) tag = tag >> 8;
		return (tag & TAG_STRUCT_MASK) == TAG_CONSTRUCTED;
	}
	
	public static boolean tagIsPrimitive(long tag) {
		return !tagIsConstructed(tag);
	}
	
	public long readTag() throws ETLVError {
		short tagBytes;
		long tagSlice;
		long tagValue = m_bp.readUInt8();
		if ((tagValue & LONG_TAG) == LONG_TAG) {
			tagBytes = 1;
			while (true) {
				tagSlice = m_bp.readUInt8();
				tagBytes++;
				if (tagBytes > 4) {
					throw new ETLVError("Tag value too long.");
				} else {
					tagValue = (tagValue << 8) | tagSlice;
				}
				if ((tagSlice & TAG_TAIL_MASK) == TAG_TAIL) {
					break;
				}
			}
		}
		return tagValue;
	}

	public long readLength() throws ETLVError {
		long bytesOfLength;
		int result = m_bp.readUInt8();
		if ((result & EXT_LENGTH) != 0) {
		    bytesOfLength = result & ~EXT_LENGTH;
		    if (bytesOfLength > 4) {
			    throw new ETLVError("TLV length to big: %d", bytesOfLength);
		    }
		    result = 0;
		    while (bytesOfLength > 0) {
			    result = (result << 8) | m_bp.readUInt8();
				bytesOfLength --;
		    }
		}
		return result;
	}

	public byte[] readBytes(long length) {
		return m_bp.readBytes(length);
	}
	
	public int readerPos() {
		return m_bp.getPosition();
	}
	
	public TaggedValue readTaggedValue() throws ETLVError {
		TaggedValue result = new TaggedValue();
		result.tag = readTag();
		// DBG('TTLVReader.readTaggedValue: tag = %x', [Result.tag]);
		result.constructed = tagIsConstructed(result.tag);
		result.valueLength = readLength();
		result.value = readBytes(result.valueLength);
		if (result.value == null)
			throw new ETLVError("buffer underflow");
		if (result.constructed)
		{
			TLVReader childReader = new TLVReader(result.value);
			while (childReader.readerPos() < result.valueLength) {
			    result.tags.add(childReader.readTaggedValue());
			}
		}
		return result;
	}
}
