package com.acepta.tlv;

public interface TLVConst {

	// =========================================
	public static long TAG_TAIL_MASK   = 0x80;       // 0b 1000 0000
	// -----------------------------------------
	public static long TAG_TAIL        = 0x00;       // 0b 0xxx xxxx
  	// -----------------------------------------
	public static long LONG_TAG        = 0x1f;       // 0b 0001 1111
  	// =========================================
	public static long EXT_LENGTH      = 0x80;
  	// =========================================
	public static long TAG_STRUCT_MASK = 0x1  << 5;  // 0b xx1x xxxx
  	// -----------------------------------------
	public static long TAG_PRIMITIVE   = 0x0;        // 0b xx0x xxxx
	public static long TAG_CONSTRUCTED = 0x1  << 5;  // 0b xx1x xxxx
  	// =========================================
	public static long TAG_CLASS_MASK  = 0x3  << 6;  // 0b 1100 0000
  	// -----------------------------------------
	public static long TAG_UNIVERSAL   = 0x0  << 6;  // 0b 00xx xxxx
	public static long TAG_APPLICATIVE = 0x1  << 6;  // 0b 01xx xxxx
	public static long TAG_CONTEXTUAL  = 0x2  << 6;  // 0b 10xx xxxx
	public static long TAG_PRIVATE     = 0x3  << 6;  // 0b 11xx xxxx
  	// =========================================

	public static long TAG_OBJECT_IDENTIFIER = 0x6;
	public static long TAG_SEQUENCE = 0x30;
	public static long TAG_SET = 0x30;
}
