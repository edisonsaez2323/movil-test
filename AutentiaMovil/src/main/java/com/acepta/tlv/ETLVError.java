package com.acepta.tlv;

public class ETLVError extends Exception {

	public ETLVError(String msg) {
		super(msg);
	}

	public ETLVError(String fmt, Object...args) {
		super(String.format(fmt, args));
	}

	private static final long serialVersionUID = 7344315313273087156L;
}
