package com.acepta;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.acepta.android.fetdroid.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ch.boye.httpclientandroidlib.conn.util.InetAddressUtils;
import cl.autentia.preferences.AutentiaPreferences;
import cl.autentia.preferences.KeyPreferences;

import static android.content.Context.WIFI_SERVICE;

public class Utils {


    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final String UTF_8 = "UTF-8";
    public static final int dedosDrawables[] = {
            0,
            R.drawable.green_finger_06, R.drawable.green_finger_07, R.drawable.green_finger_08,
            R.drawable.green_finger_09, R.drawable.green_finger_10, R.drawable.green_finger_05,
            R.drawable.green_finger_04, R.drawable.green_finger_03, R.drawable.green_finger_02,
            R.drawable.green_finger_01
    };

    public static final int dedosDrawablesNfc[] = {
            0,
            R.drawable.d06_right, R.drawable.d07_right, R.drawable.d08_right,
            R.drawable.d09_right, R.drawable.d10_right, R.drawable.d05_left,
            R.drawable.d04_left, R.drawable.d03_left, R.drawable.d02_left,
            R.drawable.d01_left
    };

    static final String HEXDIGITS = "0123456789ABCDEF";

    /**
     * Retorna la descripción del dedo a partir del idDedo en ISO
     * <p>
     * Nota:
     * La descripcion es en formato Digital Persona Legacy
     *
     * @param idFingerIso
     * @return
     */
    public static String traslateFingerIdToFingerDescription(int idFingerIso) {
        switch (idFingerIso) {
            case 1:
                return "PULGAR DERECHO";
            case 2:
                return "INDICE DERECHO";
            case 3:
                return "MEDIO DERECHO";
            case 4:
                return "ANULAR DERECHO";
            case 5:
                return "MEÑIQUE DERECHO";
            case 6:
                return "PULGAR IZQUIERDO";
            case 7:
                return "INDICE IZQUIERDO";
            case 8:
                return "MEDIO IZQUIERDO";
            case 9:
                return "ANULAR IZQUIERDO";
            case 10:
                return "MEÑIQUE IZQUIERDO";
            default:
                return "DEDO INDETERMINADO";
        }
    }

    public static void hideKeyboard(Context mContext, View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static byte[] bitmapToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
//        bmp.recycle();
        return byteArray;
    }

    /**
     * manda al log un arreglo de bytes grande transformandolo a base64
     * e imprimiendo linea por linea
     *
     * @param tag   - log tag
     * @param value - arreglo de bytes a mandar
     */
    public static void dump(String tag, byte[] value) {
        String encoded = Base64.encodeToString(value, Base64.DEFAULT);
        String lines[] = encoded.split("[\r\n\t ]+");
        long ts = System.currentTimeMillis();
        Log.d(tag, "DUMP start: " + ts);
        for (String line : lines) {
            Log.d(tag, line);
        }
        Log.d(tag, "DUMP end:" + ts);
    }

    public static String coalesce(String... alternatives) {
        for (String alternative : alternatives) {
            if (!TextUtils.isEmpty(alternative)) {
                return alternative;
            }
        }
        return null;
    }

    public static byte[] inputStreamtoByteArray(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    public static String join(String joiner, Object... parts) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < parts.length; ++i) {
            builder.append(parts[i]);
            if (i < parts.length - 1) {
                builder.append(joiner);
            }
        }

        return builder.toString();
    }

    public static String convertStreamToString(InputStream is) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("ISO-8859-1")));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } finally {
            is.close();
        }
        return sb.toString();
    }

    /**
     * Recibe un arreglo con los nombres de los archivos contenidos dentro del zip y una lista
     * con el contenido propiamente tal.
     *
     * @param fileName
     * @param input
     * @return
     * @throws Exception
     */
    public static byte[] zipBytes(String[] fileName, List<byte[]> input) throws Exception {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        for (int i = 0; i < input.size(); i++) {

            ZipEntry entry = new ZipEntry(fileName[i]);
            byte[] content = input.get(i);
            entry.setSize(content.length);
            zos.putNextEntry(entry);
            zos.write(content);
            zos.closeEntry();
        }

        zos.close();
        return baos.toByteArray();
    }

    public static <E> Iterator<List<E>> partition(List<E> list, final int batchSize) {

        assert (batchSize > 0);
        assert (list != null);
        assert (list.size() + batchSize <= Integer.MAX_VALUE);

        int idx = 0;

        List<List<E>> result = new ArrayList<List<E>>();

        for (idx = 0; idx + batchSize <= list.size(); idx += batchSize) {
            result.add(list.subList(idx, idx + batchSize));
        }
        if (idx < list.size()) {
            result.add(list.subList(idx, list.size()));
        }

        return result.iterator();
    }

    public static List<String> listOfString(String... strings) {
        List<String> result = new ArrayList<String>(strings.length);
        for (int n = 0; n < strings.length; n++) {
            result.set(n, strings[n]);
        }
        return result;
    }

    public static byte[] invert(byte[] data) {
        byte[] resp = new byte[data.length];
        int end = data.length - 1;
        for (int i = 0; i < data.length; i++) {
            resp[end] = data[i];
            end--;
        }

        return resp;

    }

    public static String saveByteToFile(byte[] buffer, String filename) {
        File dataFile = new File(filename);
        if (dataFile.exists()) {
            dataFile.delete();
        }
        try {
            // SimpleBitmap bm = renderCroppedGreyscaleBitmap(data, width, height);
            FileOutputStream out = new FileOutputStream(dataFile);
            out.write(buffer);
            out.close();
            return dataFile.getPath();

        } catch (java.io.IOException e) {
            return null;
        }

    }

    public static byte[] readByteFile(String filename) {
        File file = new File(filename);
        if (!file.exists()) {
            return null;
        }

        try {
            byte[] data = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            in.read(data);
            in.close();

//            file.delete();

            return data;
        } catch (Exception e) {
            return null;
        }
    }

    public static Boolean isOutdated(Application application) {

        int currentVersion = 0;
        try {
            currentVersion = application.getPackageManager().getPackageInfo(
                    application.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
        }

        AutentiaPreferences preferences = new AutentiaPreferences(application);
        int lastVersionReported = preferences.getInt(KeyPreferences.LAST_VERSION_REPORTED);

        return currentVersion < lastVersionReported;
    }

    public static boolean isNonPrintableData(byte[] bytes) {
        for (byte b : bytes) {
            int c = uint8(b);
            if (!(((c >= 32) && (c <= 255)) ||
                    (c == 9) || (c == 10) ||
                    (c == 11) || (c == 12) ||
                    (c == 13))) return true;
        }
        return false;
    }

    public static String bytesAsHex(byte[] bytes) {
        if (bytes == null)
            return "<NULL>";
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(HEXDIGITS.charAt((b & 0xf0) >> 4));
            sb.append(HEXDIGITS.charAt(b & 0x0f));
        }
        return sb.toString();
    }

    public static byte[] hexToBytes(String hex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int n = 0; n < hex.length(); n += 2) {
            baos.write(Integer.parseInt(hex.substring(n, n + 2), 16));
        }
        return baos.toByteArray();
    }

    public static byte[] concatArrays(byte[]... parts) {
        int totalBytes = 0;
        for (byte[] part : parts) {
            totalBytes += part.length;
        }
        ByteBuffer buf = ByteBuffer.allocate(totalBytes);
        for (byte[] part : parts) {
            buf.put(part);
        }
        return buf.array();
    }

    public static byte[] copySlice(byte[] data, int pos, int count) {
        return Arrays.copyOfRange(data, pos, pos + count);
    }

    public static int uint16(byte hb, byte lb) {
        return (uint8(hb) << 8) | uint8(lb);
    }

    public static int uint16(int hb, int lb) {
        return (uint8(hb) << 8) | uint8(lb);
    }

    public static short uint8(long b) {
        return (short) (0xff & b);
    }

    public static short uint8(int b) {
        return (short) (0xff & b);
    }

    public static short uint8(byte b) {
        return (short) (0xff & b);
    }

    public static String asHex(byte[] bytes) {
        final String hexChars = "0123456789ABCDEF";
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(hexChars.charAt((b & 0xf0) >> 4));
            sb.append(hexChars.charAt(b & 0x0f));
        }
        return sb.toString();
    }

    public static byte[] fromHex(String hex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int n = 0; n < hex.length(); n += 2) {
            baos.write(Integer.parseInt(hex.substring(n, n + 2), 16));
        }
        return baos.toByteArray();
    }

    @SuppressLint("DefaultLocale")
    public static String normalizeRUT(String rut) {
        String tmp = rut.replaceAll("[^0-9kK]+", "").toUpperCase();
        int leftLen = tmp.length() - 1;
        return tmp.substring(0, leftLen) + "-" + tmp.substring(leftLen, leftLen + 1);
    }

    static public byte[] randomBytes(int count) {
        byte[] result = new byte[count];
        new Random().nextBytes(result);
        return result;
    }

    static public byte[] sha(byte[]... blocks) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            for (byte[] block : blocks) {
                digest.update(block);
            }
            return digest.digest();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static public byte[] orBytes(byte[] a, byte[] b) {
        byte[] result = new byte[Math.max(a.length, b.length)];
        for (int n = 0; n < result.length; n++) {
            byte ba = 0, bb = 0;
            if (n < a.length) {
                ba = a[n];
            }
            if (n < b.length) {
                bb = b[n];
            }
            result[n] = (byte) (ba | bb);
        }
        return result;
    }

    static public byte[] xorBytes(byte[] a, byte[] b) {
        byte[] result = new byte[Math.max(a.length, b.length)];
        for (int n = 0; n < result.length; n++) {
            byte ba = 0, bb = 0;
            if (n < a.length) {
                ba = a[n];
            }
            if (n < b.length) {
                bb = b[n];
            }
            result[n] = (byte) (ba ^ bb);
        }
        return result;
    }

    public static byte[] andBytes(byte[] a, byte[] b) {
        byte[] result = new byte[Math.max(a.length, b.length)];
        for (int n = 0; n < result.length; n++) {
            byte ba = 0, bb = 0;
            if (n < a.length) {
                ba = a[n];
            }
            if (n < b.length) {
                bb = b[n];
            }
            result[n] = (byte) (ba & bb);
        }
        return result;
    }

    public static byte[] getResourceContent(String resourceName) throws IOException {
        String className = getCallingClassName(1);
        Class<?> clazz;
        try {
            clazz = Class.forName(className);
            InputStream resourceStream = clazz.getResourceAsStream(resourceName);
            if (resourceStream == null)
                throw new IOException(String.format("resource '%s' not found for class '%s'", resourceName, className));
            return com.acepta.Utils.getContent(resourceStream);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getCallingClassName(int aboveOffset) {
        Throwable t = new Throwable();
        StackTraceElement[] trace = t.getStackTrace();
        return trace[1 + aboveOffset].getClassName();
    }

    public static byte[] getContent(InputStream stream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[16384];
        while (true) {
            int readBytes = stream.read(buf);
            if (readBytes > 0)
                baos.write(buf, 0, readBytes);
            else
                break;
        }
        return baos.toByteArray();
    }

    public static byte[] longToBytes(long curTime) {
        // TODO Auto-generated method stub
        return null;
    }

    public static boolean checkBlocked(int cInfo) {
        if (cInfo == 0x6983)
            return true;
        else
            return false;
    }

    public static int returnCurrentAttempt(int attempt) {

        switch (attempt) {

            case 0x63CF:
                return 15;
            case 0x63CE:
                return 14;
            case 0x63CD:
                return 13;
            case 0x63CC:
                return 12;
            case 0x63CB:
                return 11;
            case 0x63CA:
                return 10;
            case 0x63C9:
                return 9;
            case 0x63C8:
                return 8;
            case 0x63C7:
                return 7;
            case 0x63C6:
                return 6;
            case 0x63C5:
                return 5;
            case 0x63C4:
                return 4;
            case 0x63C3:
                return 3;
            case 0x63C2:
                return 2;
            case 0x63C1:
                return 1;

        }

        return 0;
    }

    public static String stackTrace(Exception e) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream err = new PrintStream(baos, true, "UTF-8");
            e.printStackTrace(err);
            return baos.toString("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            return "error while building stacktrace";
        }
    }

    public static void drawFatalError(String msg, final Activity act) {
        new AlertDialog.Builder(act)
                .setTitle("Error")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                act.finish();
                            }
                        }).show();
    }

    public static void drawError(String msg, final Activity act) {
        new AlertDialog.Builder(act)
                .setTitle("Error")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                //
                            }
                        }).show();
    }

    public static boolean validaRutDV(int rut, char dv) {
        int m = 0, s = 1;
        for (; rut != 0; rut /= 10) {
            s = (s + rut % 10 * (9 - m++ % 6)) % 11;
        }
        return dv == (char) (s != 0 ? s + 47 : 75);
    }

    // private byte[] readFile(String rutaInput) {
    // try {
    // File file = new File(rutaInput);
    //
    // if (file.exists()) {
    // FileInputStream streamIn = new FileInputStream(file);
    // ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    //
    // int nRead;
    // byte[] data = new byte[1024];
    //
    // while ((nRead = streamIn.read(data, 0, data.length)) != -1) {
    // buffer.write(data, 0, nRead);
    // }
    //
    // buffer.flush();
    // streamIn.close();
    // return buffer.toByteArray();
    // } else {
    // Utils.drawFatalError("El archivo no existe en la memoria", this);
    // return null;
    // }
    // } catch (IOException e) {
    // Utils.drawFatalError("Error al leer el archivo", this);
    // return null;
    // }
    // }

    public static boolean isConnectingToInternet(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx
                .getSystemService(ctx.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

//    public static byte[] reverse(byte[] array) {
//        byte[] validData = Arrays.copyOf(array, array.length);
//        for (int i = 0; i < validData.length / 2; i++) {
//            byte temp = validData[i];
//            validData[i] = validData[validData.length - i - 1];
//            validData[validData.length - i - 1] = temp;
//        }
//        return validData;
//    }

    public static int simpleScore(int score) {
        int puntaje = score;
        if (puntaje <= 1) {
            puntaje = 10;
        } else {
            puntaje = (int) Math.ceil(10 - Math.log10(puntaje));
        }
        return puntaje;
    }

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static byte[] readByteFile(String filename, boolean delete) {


        File file = new File(filename);
        if (!file.exists()) {
            return null;
        }

        try {
            byte[] data = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            in.read(data);
            in.close();

            if (delete)
                file.delete();
            return data;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean storeByteFile(String path, byte[] data) {
        if (path == null || data == null)
            return false;
        try {
            File sdCard = new File(Environment.getExternalStorageDirectory(),
                    path);
            File sdCardParent = sdCard.getParentFile();
            if (!sdCardParent.exists()) {
                sdCardParent.mkdirs();
            }
            // File sdCard = new File(path);
            if (sdCard.exists()) {
                sdCard.delete();
            }

            FileOutputStream out = new FileOutputStream(sdCard);
            out.write(data);
            out.close();
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    public static String stringFromInputStream(InputStream is, String charset)
            throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is,
                Charset.forName(charset)));

        StringBuffer buf = new StringBuffer();
        String str;

        while ((str = in.readLine()) != null) {
            buf.append(str);
        }

        in.close();
        return buf.toString();
    }

    /**
     * Convert byte array to hex string
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10)
                sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str
     * @return array of NULL if error was found
     */
    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename
     * @return
     * @throws java.io.IOException
     */
    public static String loadFileAsString(String filename)
            throws java.io.IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(
                filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF
                        && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8")
                    : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName))
                        continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null)
                    return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0)
                    buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*
         * try { // this is so Linux hack return
         * loadFileAsString("/sys/class/net/" +interfaceName +
         * "/address").toUpperCase().trim(); } catch (IOException ex) { return
         * null; }
         */
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf
                        .getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port
                                // suffix
                                return delim < 0 ? sAddr : sAddr.substring(0,
                                        delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static void fixInmmersive(Dialog dialog, Context context) {
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public static byte[] InputStreamToByteArray(InputStream inputStream) {
        byte[] bytes = null;

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte buffer[] = new byte[1024];
            int count;
            while (true) {
                count = inputStream.read(buffer);
                if (count == -1)
                    break;
                bos.write(buffer, 0, count);
            }
            bos.flush();
            bos.close();
            inputStream.close();

            bytes = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static JSONObject BundleToJsonObject(Bundle bundle) {
        JSONObject json = new JSONObject();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                try {
                    json.put(key, JSONObject.wrap(bundle.get(key)));
                } catch (JSONException e) {
                    Log.e("JSON transform", e.getMessage());
                }
            }
        }
        return json;
    }

    public static Bundle JsonObjectToBundle(JSONObject json) {
        Bundle bundle = new Bundle();
        if (json != null) {
            Iterator<String> it = json.keys();
            while (it.hasNext()) {
                String key = it.next();
                String value = null;
                try {
                    value = json.get(key).toString();
                } catch (JSONException e) {
                    Log.e("Bundle transform", e.getMessage());
                }
                bundle.putString(key, value);
            }
        }
        return bundle;
    }

    public static Logger getLogger(Class<?> clazz) {

        Logger log = Logger.getLogger(clazz.getName());
        return log;
    }

    public static Set<String> setOf_Strings(String... elements) {
        Set<String> result = new HashSet<String>();
        for (String element : elements) {
            result.add(element);
        }
        return result;
    }

    public static void reverse(byte[] pixels) {
        int head = 0;
        int tail = pixels.length - 1;
        byte tmp;
        while (head < tail) {
            tmp = pixels[head];
            pixels[head] = pixels[tail];
            pixels[tail] = tmp;
            --tail;
            ++head;
        }
    }

    public static byte[] readAllBytes(InputStream stream) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        int bytesRead = 0;
        while (true) {
            bytesRead = stream.read(buffer);
            if (bytesRead == -1) break;
            result.write(buffer, 0, bytesRead);
        }
        return result.toByteArray();
    }

    public static boolean arrayHeaderMatches(byte[] data, byte[] header) {
        if (header.length >= data.length)
            return false;
        for (int n = 0; n < header.length; n++) {
            if (data[n] != header[n])
                return false;
        }
        return true;
    }

    public static boolean arrayHeaderMatchesAny(byte[] data,
                                                byte[]... headers) {

        for (byte[] header : headers) {
            if (arrayHeaderMatches(data, header))
                return true;
        }
        return false;
    }

    public static String makePath(String basedir, String... parts) {
        StringBuffer buf = new StringBuffer();
        buf.append(basedir);
        for (String part : parts) {
            buf.append(File.separator);
            buf.append(part);
        }
        return buf.toString();
    }

    public static String makePath(File basedir, String... parts) throws IOException {
        return makePath(basedir.getCanonicalPath(), parts);
    }

    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        return sb.toString();
    }

    public static byte[] compressFiles(List<FileHolder> files) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(baos);
        zip.setLevel(Deflater.DEFAULT_COMPRESSION);
        for (FileHolder file : files) {
            if (file.filename != null) {
                zip.putNextEntry(new ZipEntry(file.filename));
                zip.write(file.data);
            }
        }
        zip.close();
        return baos.toByteArray();
    }

    public static List<FileHolder> unCompressFiles(byte[] files) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(files);
        ZipInputStream zipStream = new ZipInputStream(bais);
        ZipEntry entry = null;

        List<FileHolder> list = new ArrayList<FileHolder>();

        while ((entry = zipStream.getNextEntry()) != null) {
            //entelFile.fileName = entry.getName();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int read;
            byte[] buffer = new byte[1024];
            while ((read = zipStream.read(buffer)) != -1) {
                baos.write(buffer, 0, read);
            }
            FileHolder file = new FileHolder(baos.toByteArray(), entry.getName(), null);
            list.add(file);
            zipStream.closeEntry();
        }
        zipStream.close();
        return list;
    }

    public static String getTrackID() {
        return UUID.randomUUID().toString();
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = text.getBytes("iso-8859-1");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public String GetDeviceipMobileData() {
        try {
            for (java.util.Enumeration<java.net.NetworkInterface> en = java.net.NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                java.net.NetworkInterface networkinterface = en.nextElement();
                for (java.util.Enumeration<java.net.InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    java.net.InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Current IP", ex.toString());
        }
        return null;
    }


    public String GetDeviceipWiFiData(Context ctx) {
        android.net.wifi.WifiManager wm = (android.net.wifi.WifiManager) ctx.getSystemService(WIFI_SERVICE);
        @SuppressWarnings("deprecation")
        String ip = android.text.format.Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }

    public static String isValidIntentTestCall(Activity activity) {
        String versionApp = null;
        Intent intent = activity.getIntent();

        try {
            String packageName = activity.getCallingActivity().getPackageName();
            if (packageName.equals("cl.autentia.tester")) {
                versionApp = intent.getExtras().getString("version");
                if (versionApp != null) {

                    if (versionApp.contains("3.0")) {
                        Log.e("test", "isvalid");
                        return "isValid";
                    } else {
                        Log.e("test", "isOut");
                        return "isOutdated";
                    }

                } else {
                    return "isOutdated";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        Log.e("test", "isNot");
        return "isNotTest";
    }

    public static String bundleToJsonString(Intent intent) {

        Bundle data = intent.getExtras();
        JSONObject jsonRequest = new JSONObject();
        try {

            Iterator iterator = data.keySet().iterator();

            while (iterator.hasNext()) {

                String key = iterator.next().toString();
                Object value = data.get(key);

                jsonRequest.put(key, value);
            }

        } catch (Exception e) {
            Log.d("data", e.getMessage());
        }
        return jsonRequest.toString();
    }

}
