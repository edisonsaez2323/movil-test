package com.acepta;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public class JSON {

	private static ObjectMapper m_objectMapperInstance;

	public static ObjectMapper getObjectMapper() {
		if (m_objectMapperInstance == null) {
			synchronized (JSON.class) {
				if (m_objectMapperInstance == null) {
					m_objectMapperInstance = new ObjectMapper();
				}
			}
		}
		return m_objectMapperInstance;
	}
	
	public static String toString(Object obj) {
		try {
			return getObjectMapper().writeValueAsString(obj);
		}
		catch (Exception e) {
			throw new RuntimeException("conversion to JSON failed", e);
		}
	}

	public static Object readObject(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = JSON.getObjectMapper();
		return om.readValue(json, Object.class);
	}

	public static Object readObject(String json, Class<?> clazz) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = JSON.getObjectMapper();
		return om.readValue(json, clazz);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, ?> readMap(String json) throws JsonParseException, JsonMappingException, IOException {
		return (Map<String, ?>) readObject(json);
	}
}
