package com.acepta.eid;

import android.nfc.tech.IsoDep;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.acepta.JJ2000Utils;
import com.acepta.JSON;
import com.acepta.Utils;
import com.acepta.eid.crypto.DESede;
import com.acepta.smartcardio.APDU;
import com.acepta.smartcardio.RespAPDU;
import com.acepta.smartcardio.SmartCard;
import com.acepta.smartcardio.SmartCardConnection;
import com.acepta.smartcardio.SmartCardError;
import com.acepta.tlv.TaggedValue;

import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.acepta.eid.MRTDConst.EF_COM;
import static com.acepta.eid.MRTDConst.EF_DG1;
import static com.acepta.eid.MRTDConst.EF_DG11;
import static com.acepta.eid.MRTDConst.EF_DG2;
import static com.acepta.eid.MRTDConst.EF_DG7;
import static com.acepta.eid.MRTDConst.EF_SOD;

public class EID extends SmartCard {

    public static final String BIT1_VALUE = "BIT1_VALUE";
    public static final String BIT2_VALUE = "BIT2_VALUE";
    public static final int NEW_SIZE_BIT = 28; //LARGO CON MODIFICACION CEDULA IDENTIDAD 02-2020

    public enum Application {
        UNKNOWN, MOC, MRTD
    }

    public static boolean mTagNfcA = false;

    static final int FIRST_READ_SIZE = 4;
    static final byte[] ATR_CHeID = {0x3B, (byte) 0x88, (byte) 0x80, 0x01,
            0x31, (byte) 0xCC, (byte) 0xCC, 0x01, 0x77, (byte) 0x81,
            (byte) 0xC1, 0x00, 0x0E};
    static final byte[] AID_MOC_CHILE = {(byte) 0xE8, 0x28, (byte) 0xBD, 0x08,
            0x0F, (byte) 0xD2, 0x50, 0x43, 0x68, 0x6C, 0x43, 0x43, 0x2D, 0x65,
            0x49, 0x44};
    static final byte[] AID_MRTD = {(byte) 0xA0, 0x00, 0x00, 0x02, 0x47, 0x10,
            0x01};
    // #$00 #$CB #$3F #$FF #$0E
    // #$4D#$0C#$70#$0A#$BF#$82#$10#$06#$7F#$50#$03#$7F#$60#$80 #$00;
    // GET_BIT_DATA ; data:
    // #$4D#$0C#$70#$0A#$BF#$82#$10#$06#$7F#$50#$03#$7F#$60#$80; flags:
    static final APDU CMD_GET_BIT = new APDU(0x00, 0xCB, 0x3F, 0xFF,
            APDU.APDU_HAS_LC | APDU.APDU_HAS_LE);
    static final byte[] CMD_GET_BIT_DATA = {0x4D, 0x0C, 0x70, 0x0A,
            (byte) 0xBF, (byte) 0x82, 0x10, 0x06, 0x7F, 0x50, 0x03, 0x7F, 0x60,
            (byte) 0x80};
    //    static final byte[] CMD_GET_BIT_RESPONSE = {0x70, 0x1a, (byte) 0xbf,
//            (byte) 0x82, 0x00, 0x16, 0x7f, 0x50, 0x13, 0x7f, 0x60, 0x10,
//            (byte) 0xa1, 0x0e, (byte) 0x81, 0x01, 0x08, (byte) 0x82, 0x01,
//            0x00, (byte) 0x87, 0x02, 0x01, 0x01, (byte) 0x88, 0x02, 0x00, 0x05};
    static final byte[] CMD_GET_BIT_RESPONSE = {0x70, 0x1d, (byte) 0xbf,
            (byte) 0x82, 0x00, 0x19, 0x7f, 0x50, 0x16, (byte) 0x80, 0x01, 0x03, 0x7f, 0x60, 0x10,
            (byte) 0xa1, 0x0e, (byte) 0x81, 0x01, 0x08, (byte) 0x82, 0x01,
            0x06, (byte) 0x87, 0x02, 0x01, 0x01, (byte) 0x88, 0x02, 0x00, 0x05, (byte) 0x90, 0x00};
    static final byte[] CMD_GET_BIT_RESPONSE_MASK = {(byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0x00, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, 0x00, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff};
    static final int CMD_GET_BIT_SLOT_POS = 6; // 7 in 1 based array
    static final int CMD_GET_BIT_RESP_FINGER_POS = 19; // 20 in 1 based array
    static final int CMD_GET_BIT_RESP_FINGER_POS_NEW_IDENT = 22; // 20 in 1 based array
    static final int CMD_GET_BIT_RESP_SLOT_POS = 4;
    // #$00#$21#$00#$90
    static final APDU CMD_VERIFY_BIR = new APDU(0x00, 0x21, 0x00, 0x90,
            APDU.APDU_HAS_LC);
    static final byte[] KENC_SEED = {0x00, 0x00, 0x00, 0x01};
    static final byte[] KMAC_SEED = {0x00, 0x00, 0x00, 0x02};
    /*
     * 231, maximum data chunk that can be requested for read, including SM
     * overhead results in 254 bytes long secured RAPDU RFIDIOt uses 118,
     * pypassport uses 223
     */
    static final int MAX_READ_CHUNK = 223;
    private static final String EID = "EID";
    private Application m_activeApp;

    private Map<byte[], TaggedValue> m_mrtdFiles = new HashMap<byte[], TaggedValue>();
    private byte[] m_mrzKey = null;
    private int[] m_bit = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE};

    //TAG IN USE
    private IsoDep tag;

    public EID(@NonNull SmartCardConnection connection) {
        super(connection);
        m_activeApp = Application.UNKNOWN;
    }

    private void requiresApp(Application... appIds) throws EIDError {
        for (Application appId : appIds) {
            Log.d("appId", appId.name());
            Log.d("m_activeApp", m_activeApp.name());
            if (m_activeApp.equals(appId))
                return;
        }
        throw new EIDError("required app not activate: %s, found: %s",
                JSON.toString(appIds), m_activeApp);
    }

    public void selectMRTD() {
//        if (m_activeApp == Application.MRTD)
//            return;
//        try {
//            requiresApp(Application.UNKNOWN, Application.MOC, Application.MRTD);
//        } catch (EIDError eidError) {
//            Log.e("EIDError",eidError.getMessage());
//            eidError.printStackTrace();
//        }

        APDU req = CMD_SELECT_AID.withData(AID_MRTD);
        req.flags |= APDU.APDU_RST_SEC_CHANNEL;
        RespAPDU resp = null;
        try {
            resp = sendCommand(req);
            checkSuccess("selectMRTD", resp);
            m_activeApp = Application.MRTD;
            Log.d(EID, String.format("MRTD selected, time = %d ms", resp.time));
        } catch (SmartCardError smartCardError) {
            Log.e("SmartCardError", smartCardError.getMessage());
            smartCardError.printStackTrace();
        }
    }

    public void openSecureChannel(@NonNull String mrz) throws SmartCardError, EIDError {
        //requiresApp(Application.MRTD);

        byte[] kseed, kenc, kmac;
        byte[] rnd_icc, rnd_ifd, kifd, S, eifd, mifd;
        byte[] rec_ifd, kicc;
        @SuppressWarnings("unused")
        byte[] rec_icc;
        byte[] auth_resp, dec_auth_resp;
        byte[] ksseed;

        try {
            if (mrz != null) {
                m_mrzKey = MRTDTools.buildMRZKey(mrz);
            }
        } catch (MRTDError e) {
            throw new EIDError(e);
        }
        if (m_mrzKey == null) {
            throw new EIDError("mrz not set");
        }
        kseed = Arrays.copyOf(Utils.sha(m_mrzKey), 16);
        kenc = MRTDTools.desKey(kseed, KENC_SEED);
        kmac = MRTDTools.desKey(kseed, KMAC_SEED);
        rnd_icc = iso7816_getChallenge(8);
        rnd_ifd = Utils.randomBytes(8);
        kifd = Utils.randomBytes(16);
        S = Utils.concatArrays(rnd_ifd, rnd_icc, kifd);
        DESede des3 = new DESede(kenc);
        eifd = des3.encrypt(S);
        mifd = MRTDTools.desMac(kmac, MRTDTools.padData(eifd));
        auth_resp = iso7816_externalAuthenticate(
                Utils.concatArrays(eifd, mifd), kmac);
        dec_auth_resp = des3.decrypt(auth_resp);
        rec_icc = Arrays.copyOf(dec_auth_resp, 8);
        rec_ifd = Arrays.copyOfRange(dec_auth_resp, 8, 16);
        if (!Arrays.equals(rnd_ifd, rec_ifd))
            throw new EIDError("rnd_ifd != rec_ifd");
        kicc = Arrays.copyOfRange(dec_auth_resp, 16, 32);
        ksseed = Utils.xorBytes(kifd, kicc);

        byte[] ks_enc = MRTDTools.desKey(ksseed, KENC_SEED);
        byte[] ks_mac = MRTDTools.desKey(ksseed, KMAC_SEED);
        BigInteger ssc = MRTDTools.bytes2UInt64(Utils.concatArrays(
                Arrays.copyOfRange(rnd_icc, 4, 8),
                Arrays.copyOfRange(rnd_ifd, 4, 8)));

        setSecureChannelParams(ks_enc, ks_mac, ssc);
    }

    private ByteBuffer mrtd_readFile(byte[] file_id) throws SmartCardError,
            EIDError {
        //requiresApp(Application.MRTD);

//        if (!hasSecureChannel()) {
//            throw new EIDError("secure channel required");
//        }

        int root_tag_data_remaining, root_tag_length;
        int root_tag_length_size[] = new int[1];

        long started = System.currentTimeMillis();
        iso7816_selectFile(file_id);
        byte[] header = iso7816_readBinary(0, FIRST_READ_SIZE);
        root_tag_length = MRTDTools.asn1Length(Utils.copySlice(header, 1, 3),
                root_tag_length_size);
        root_tag_data_remaining = root_tag_length
                - (FIRST_READ_SIZE - 1 - root_tag_length_size[0]);

        ByteBuffer content = ByteBuffer.allocate(FIRST_READ_SIZE
                + root_tag_data_remaining);
        content.put(header);

        while (content.position() < content.capacity()) {
            int blockSize = Math.min(content.capacity() - content.position(),
                    MAX_READ_CHUNK);
            byte[] blockBytes = iso7816_readBinary(content.position(),
                    blockSize);
            content.put(blockBytes);
        }
        long ended = System.currentTimeMillis();
        Log.d(EID, String.format(
                "mrtd_readFile: file %s of %d bytes read in %d ms.",
                Utils.bytesAsHex(file_id), content.position(), ended - started));
        content.rewind();
        return content;
    }

    private void addInfoTag(XmlSerializer serializer, String name,
                            TaggedValue tlv, long tag) throws IllegalArgumentException,
            IllegalStateException, IOException {
        serializer.startTag("", name);
        serializer.text(new String(tlv.find(tag).value, "UTF-8"));
        serializer.endTag("", name);
    }

    private void addInfoTag(XmlSerializer serializer, String name, String value)
            throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag("", name);
        serializer.text(value);
        serializer.endTag("", name);
    }

    public InternalAuthenticatedData certifyData(byte[] data)
            throws SmartCardError, EIDError, NoSuchAlgorithmException {

        selectMRTD();

        byte[] ef_com = mrtd_readFile(MRTDConst.EF_COM).array();
        byte[] ef_sod = mrtd_readFile(MRTDConst.EF_SOD).array();
        byte[] ef_dg15 = mrtd_readFile(MRTDConst.EF_DG15).array();

        return certifyTimestampedDataSHA256(ef_com, ef_sod, ef_dg15,
                System.currentTimeMillis(), m_mrzKey, data);
    }

    public InternalAuthenticatedData certifyTimestampedDataSHA256(
            byte[] ef_com, byte[] ef_sod, byte[] ef_dg15, long timestamp,
            byte[] mrz, byte[]... data) throws SmartCardError, EIDError,
            NoSuchAlgorithmException {

        long start = System.currentTimeMillis();

        requiresApp(Application.MOC);

        selectMRTD();

        InternalAuthenticatedData certification = new InternalAuthenticatedData();
        certification.timestamp = MRTDTools.int64Bytes(timestamp);
        certification.data = data;

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(certification.timestamp);
        for (int i = 0; i < certification.data.length; i++) {
            digest.update(certification.data[i]);
        }
        byte[] sha256 = digest.digest();

        certification.signature = new byte[4][];
        certification.signature[0] = iso7816_internalAuthenticate(Arrays
                .copyOfRange(sha256, 0, 8));
        certification.signature[1] = iso7816_internalAuthenticate(Arrays
                .copyOfRange(sha256, 8, 16));
        certification.signature[2] = iso7816_internalAuthenticate(Arrays
                .copyOfRange(sha256, 16, 24));
        certification.signature[3] = iso7816_internalAuthenticate(Arrays
                .copyOfRange(sha256, 24, 32));

        selectMOC();
        long end = System.currentTimeMillis();
        certification.running_millis = end - start;
        return certification;
    }

    public String getDocumentInfo() throws SmartCardError, EIDError {
        Log.i(EID, "getDocumentInfo");

        //requiresApp(Application.MRTD);

        checkFile(EF_COM);
        checkFile(EF_DG1);
        checkFile(EF_DG11);
        checkFile(EF_SOD);

        JSONObject document = new JSONObject();
        try {
            TaggedValue dg1 = m_mrtdFiles.get(EF_DG1);
            TaggedValue dg11 = m_mrtdFiles.get(EF_DG11);

            document.put("tipo", "EID");
            document.put("mrz", new String(dg1.find(0x5F1F).value, "UTF-8"));
            document.put("tipo_doc",
                    new String(dg1.find(0x5F03).value, "UTF-8"));
            document.put("pais_emi",
                    new String(dg1.find(0x5F28).value, "UTF-8"));
            document.put("serie", new String(dg1.find(0x5A).value, "UTF-8"));
            document.put("fch_nac", new String(dg1.find(0x5F57).value, "UTF-8"));
            document.put("sexo", new String(dg1.find(0x5F35).value, "UTF-8"));
            document.put("fch_exp", new String(dg1.find(0x59).value, "UTF-8"));
            document.put("nacionalidad", new String(dg1.find(0x5F2C).value,
                    "UTF-8"));
            String rut = new String(dg1.find(0x53, 2).value);
            document.put("run", rut);
            String[] tmp = new String(dg11.find(0x5F0E).value, "UTF-8")
                    .split("[<][<]");
            document.put("apellidos", tmp[0].replace("<", " "));
            document.put("nombres", tmp[1].replace("<", " "));
            document.put("nacio_en", new String(dg11.find(0x5F11).value,
                    "UTF-8"));
            document.put("profesion", new String(dg11.find(0x5F13).value,
                    "UTF-8"));
            document.put("bit1", BIT1_VALUE);
            document.put("bit2", BIT2_VALUE);
            return document.toString();

        } catch (Exception e) {
            throw new EIDError(e);
        }
    }

    public String getExtendedDocumentInfo() throws SmartCardError, EIDError {
        Log.i(EID, "getExtendedDocumentInfo");

        //requiresApp(Application.MRTD);

        checkFile(EF_DG2);
        checkFile(EF_DG7);

        try {
            JSONObject document = new JSONObject();
            TaggedValue dg2 = m_mrtdFiles.get(EF_DG2);
            TaggedValue dg7 = m_mrtdFiles.get(EF_DG7);

            // rescatamos las imagenes
            document.put("retrato", Base64.encodeToString(
                    JJ2000Utils.jp2ToJpeg(dg2.find(0xA1).value),
                    Base64.DEFAULT).replaceAll("\n", ""));

            document.put("firma", Base64.encodeToString(
                    JJ2000Utils.jp2ToJpeg(dg7.find(0x5F43).value),
                    Base64.DEFAULT).replaceAll("\n", ""));
            // retornamos la info extendida
            return document.toString();
        } catch (Exception e) {
            Log.e("ExtendedDocumentInfo()", e.toString());
            throw new EIDError(e);
        }
    }

    protected byte[] AID_MOC = {
            (byte) 0x00,
            (byte) 0xA4,
            (byte) 0x04,
            (byte) 0x0C,
            (byte) 0x10,
            (byte) 0xE8,
            (byte) 0x28,
            (byte) 0xBD,
            (byte) 0x08,
            (byte) 0x0F,
            (byte) 0xD2,
            (byte) 0x50,
            (byte) 0x43,
            (byte) 0x68,
            (byte) 0x6C,
            (byte) 0x43,
            (byte) 0x43,
            (byte) 0x2D,
            (byte) 0x65,
            (byte) 0x49,
            (byte) 0x44

    };

    public boolean selectMOC() throws SmartCardError, EIDError {
//        if (m_activeApp == Application.MOC)
//            return tr
        requiresApp(Application.MRTD);

        APDU req = CMD_SELECT_AID.withData(AID_MOC_CHILE);
        req.flags |= APDU.APDU_REQ_SEC_CHANNEL | APDU.APDU_RST_SEC_CHANNEL;
        RespAPDU resp = sendCommand(req);

        if (resp.sw == 0x9000) {
            m_activeApp = Application.MOC;
            Log.d(EID, String.format("MOC selected, time = %d ms", resp.time));
            return false;
        } else {
            RespAPDU resp_ = _sendCommandMoc(AID_MOC);
            m_activeApp = Application.MOC;
            Log.d(EID, String.format("MOC selected, time = %d ms", resp_.time));
            return true;
        }
    }

    public int getBIT(int slot) throws EIDError, SmartCardError {
        requiresApp(Application.MOC);

        int result = m_bit[slot - 1];
        if (result == Integer.MAX_VALUE) {
            APDU req = CMD_GET_BIT.withData(CMD_GET_BIT_DATA, true);
            req.data[CMD_GET_BIT_SLOT_POS] |= Utils.uint8(slot);
            byte reqSlotId = req.data[CMD_GET_BIT_SLOT_POS];
            RespAPDU resp;
            resp = sendCommand(req);
            checkSuccess("getBIT", resp);

            if (resp.data.length > NEW_SIZE_BIT){
                result = resp.data[CMD_GET_BIT_RESP_FINGER_POS_NEW_IDENT];
            }else {
                result = resp.data[CMD_GET_BIT_RESP_FINGER_POS];
            }

            m_bit[slot - 1] = result;
        }
        return result;
    }

    /*
     * public byte[] certifyBIR(byte[] isocc) throws EIDError, SmartCardError {
     * requiresApp(Application.MOC);
     *
     * selectMRTD(); openSecureChannel(null); long curTime =
     * System.currentTimeMillis(); byte[] timeStamp =
     * MRTDTools.uInt32Bytes(curTime / 60000); byte[] stampedIsocc =
     * Utils.concatArrays(timeStamp, isocc); byte[] kmac_seed =
     * Arrays.copyOf(Utils.sha(stampedIsocc), 16); byte[] kmac =
     * MRTDTools.desKey(kmac_seed, KMAC_SEED); byte[] isocc_mac =
     * MRTDTools.desMac(kmac, stampedIsocc); byte[] certified_mac =
     * iso7816_internalAuthenticate(isocc_mac); selectMOC();
     *
     * return Utils.concatArrays(timeStamp, certified_mac); }
     */

    public byte[][] certifyBIR(byte[] isocc) throws EIDError, SmartCardError {
        requiresApp(Application.MOC);

        selectMRTD();

        long curTime = System.currentTimeMillis();
        byte[] timeStamp = MRTDTools.uInt32Bytes(curTime / 60000);
        byte[] stampedIsocc = Utils.concatArrays(timeStamp, isocc);
        byte[] kmac_seed = Arrays.copyOf(Utils.sha(stampedIsocc), 16);
        byte[] kmac = MRTDTools.desKey(kmac_seed, KMAC_SEED);
        byte[] isocc_mac = MRTDTools.desMac(kmac, stampedIsocc);
        byte[] certified_mac = iso7816_internalAuthenticate(isocc_mac);
        selectMOC();

        // return Utils.concatArrays(timeStamp, certified_mac);
        return new byte[][]{timeStamp, stampedIsocc, kmac_seed, kmac,
                isocc_mac, certified_mac};
    }

    public String verifyBIR(int slot, byte[] isocc) throws SmartCardError,
            EIDError {
        requiresApp(Application.MOC);

        byte[] isocTrim = trim(isocc);

        APDU req = CMD_VERIFY_BIR.withData(isocTrim);
        req.p2 |= slot;
        RespAPDU resp = sendCommand(req);
        if (!checkVerifyFingerResp(resp.sw)) {
            checkSuccess("verifyBIR", resp);
        }
        boolean match = resp.sw == 0x9000;
        Log.d(EID, String.format("verifyBIR slot %d -> %s, %d ms", slot, match,
                resp.time));

        if (match)
            return "true";
        else
            return String.valueOf(resp.sw);
    }

    public boolean checkVerifyFingerResp(int resp){

        switch (resp){

            case 0x9000:
                return true;
            case 0x6300:
                return true;
            case 0x6700:
                return true;
            case 0x63CF:
                return true;
            case 0x63CE:
                return true;
            case 0x63CD:
                return true;
            case 0x63CC:
                return true;
            case 0x63CB:
                return true;
            case 0x63CA:
                return true;
            case 0x63C9:
                return true;
            case 0x63C8:
                return true;
            case 0x63C7:
                return true;
            case 0x63C6:
                return true;
            case 0x63C5:
                return true;
            case 0x63C4:
                return true;
            case 0x63C3:
                return true;
            case 0x63C2:
                return true;
            case 0x63C1:
                return true;

        }
        return false;

    }

    static byte[] trim(byte[] bytes) {
        int i = bytes.length - 1;
        while (i >= 0 && bytes[i] == 0) {
            --i;
        }

        return Arrays.copyOf(bytes, i + 1);
    }

    private void dumpFile(byte[] fileId) {

        Log.d(EID, Utils.bytesAsHex(m_mrtdFiles.get(fileId).value));
    }

    private void checkFile(byte[] fileId) throws EIDError, SmartCardError {

        if (!m_mrtdFiles.containsKey(fileId)) {
            m_mrtdFiles.put(
                    fileId,
                    DataGroupReader.read(mrtd_readFile(fileId)));
        }
    }

    interface FingerPos {

        int LEFT_LITTLE = 0;

    }
}
