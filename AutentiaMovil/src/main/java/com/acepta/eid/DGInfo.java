package com.acepta.eid;

public class DGInfo {

	public long tag;
	public byte[] efID;
	public String name;

	public DGInfo(long tag, byte[] efID, String name) {
		this.tag = tag;
		this.efID = efID;
		this.name = name;
	}
}
