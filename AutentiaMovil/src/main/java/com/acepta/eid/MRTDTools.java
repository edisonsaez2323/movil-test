package com.acepta.eid;

import android.annotation.SuppressLint;

import com.acepta.Utils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import static com.acepta.Utils.uint16;
import static com.acepta.Utils.uint8;

public class MRTDTools {

	private static final byte[] DEFAULT_DES_IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	private static final byte[] DES_PAD = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	private static final BigInteger ULONG_LIMIT = new BigInteger("2").pow(64);
	private static final int DESMAC_SIZE = 8;

	static public byte[] desParity(byte[] in) {
		byte[] result = new byte[in.length];
		for (int n = 0; n < in.length; n++) {
			int y = in[n] & 0xfe;
		    int parity = 0;
		    for (int z = 0; z < 8; z++) {
		    	parity += (y >> z) & 1;
		    }
		    result[n] = (byte) (y + (1 - parity % 2));
		}
		return result;
	}

	static public byte[] desKey(byte[] kseed, byte[] type) {
		byte[] kencsha = Utils.sha(kseed, type);
		return Arrays.copyOf(desParity(kencsha), 16);
	}

	/* ISO 9797-1 Algorithm 3 (Retail MAC) */
	@SuppressLint("TrulyRandom")
	static public byte[] desMac(final byte[] key, byte[] data) {
		
	    Cipher tdesa, tdesb;
	    byte[] mac;
	    byte[] current;
	    
		try {
		    SecretKeyFactory sf = SecretKeyFactory.getInstance("DES");
		    SecretKey aKey = sf.generateSecret(new DESKeySpec(key));
		    SecretKey bKey = sf.generateSecret(new DESKeySpec(key, 8));
		    tdesa = Cipher.getInstance("DES/ECB/NoPadding");
		    tdesa.init(Cipher.ENCRYPT_MODE, aKey);
		    tdesb = Cipher.getInstance("DES/ECB/NoPadding");
		    tdesb.init(Cipher.DECRYPT_MODE, bKey);
		    mac = DEFAULT_DES_IV;
		    for (int y = 0; y < data.length; y += 8) {
		    	current = Arrays.copyOfRange(data, y, y + 8);
		    	mac = Utils.xorBytes(mac, current);
		    	mac = tdesa.update(mac);
		    }
		    mac = tdesb.update(mac);
		    return tdesa.update(mac);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	static int paddingSize(int value, int rounding_step) {
		return value + (rounding_step - (value % rounding_step));
	}
	
	static public byte[] padData(byte[] data) {
		ByteBuffer result = ByteBuffer.allocate(paddingSize(data.length, DES_PAD.length));
		result.put(data);
		result.put(DES_PAD, 0, result.capacity() - data.length);
		return result.array();
	}

	static public byte[] unPadData(byte[] data) {
		int padding_size = 0, n = data.length - 1;
		while ((n >= 0) && (data[n] == 0x00)) {
			padding_size += 1;
			n -= 1;
		}
		if ((0xff & data[n]) == 0x80) {
			padding_size += 1;
		}
		else {
			throw new RuntimeException("unpadding error");
		}
		return Arrays.copyOf(data, data.length - padding_size);
	}

	static public byte[] toAsn1Length(int value) {
		int bytesNeeded = 0, tmpValue = value;
		while (tmpValue > 0) {
			bytesNeeded += 1;
			tmpValue >>= 8;
		}
		byte[] result = new byte[bytesNeeded];
		if (value >= 0 && value <= 0x7F) {
			result[0] = (byte) value;
		}
		else if (value >= 0x80 && value <= 0xFF) {
			result[0] = (byte) 0x81;
			result[1] = (byte) value;
		}
		else if (value >= 0x100 && value <= 0xFFFF) {
			result[0] = (byte) 0x82;
			result[1] = (byte) (value >> 8);
			result[2] = (byte) (value & 0xff);
		}
		else {
			throw new RuntimeException("value is to big");
		}
		return result;
	}

	static public int asn1Length(byte[] data, int[] length_bytes) {
		int b1 = uint8(data[0]);
		int b2 = uint8(data[1]);
		int b3 = uint8(data[2]);
		if (b1 >= 0x00 && b1 <= 0x7f) {
			length_bytes[0] = 1; return b1;
		}
		if (b1 == 0x81) {
			length_bytes[0] = 2; return b2;
		}
		if (b1 == 0x82) {
			length_bytes[0] = 3; return uint16(b2, b3);
		}
		throw new RuntimeException("bad asn1 length bytes");
	}

	static public BigInteger bytes2UInt64(byte[] bytes) {
		if (bytes.length != 8)
			throw new RuntimeException("wrong byte count");
		return new BigInteger(1, bytes);
	}

	static public byte[] uInt64Bytes(BigInteger number) {
		if (number.compareTo(ULONG_LIMIT) >= 0)
			throw new RuntimeException("number too big");
		number = number.or(ULONG_LIMIT);
		byte[] numberBytes = number.toByteArray();
		return Arrays.copyOfRange(numberBytes, 1, numberBytes.length);
	}

	static public long bytes2UInt32(byte[] bytes) {
		long result = 0;
		for (byte b : bytes) {
			result = (result << 8) | uint8(b);
		}
		return result;
	}

	static public byte[] uInt32Bytes(long number) {
		byte[] result = new byte[4];
		result[0] = (byte) uint8(number >> 24);
		result[1] = (byte) uint8(number >> 16);
		result[2] = (byte) uint8(number >>  8);
		result[3] = (byte) uint8(number      );
		return result;
	}

	static public byte[] int64Bytes(long number) {
		byte[] result = new byte[8];
		result[0] = (byte) uint8(number >> 56);
		result[1] = (byte) uint8(number >> 48);
		result[2] = (byte) uint8(number >> 40);
		result[3] = (byte) uint8(number >> 32);
		result[5] = (byte) uint8(number >> 24);
		result[6] = (byte) uint8(number >> 16);
		result[7] = (byte) uint8(number >>  8);
		result[8] = (byte) uint8(number      );
		return result;
	}

	public static boolean macVerify(byte[] data, byte[] key) {
		byte[] msg = Arrays.copyOf(data, data.length - DESMAC_SIZE);
		byte[] received_mac = Arrays.copyOfRange(data, data.length - DESMAC_SIZE, data.length);
		byte[] calculated_mac = desMac(key, padData(msg));
		return Arrays.equals(calculated_mac, received_mac);
	}

    /* 	serial(1, 9)
		chksum(10, 1)
		birth_date(11, 6) -- YYMMDD
		chksum(17, 1)
		expiration_date(18, 6) -- YYMMDD
		chksum(24, 1) */

	static final int MRZ_LENGTH = 9 + 1 + 6 + 1 + 6 + 1;

	public static byte[] buildMRZKey(String mrz) throws MRTDError {
		byte[] mrzBytes = mrz.getBytes();
		byte[] document_serial, expiration_date, birth_date;

		if (mrz.length() != MRZ_LENGTH)
			throw new MRTDError("bad mrz length");

		document_serial = Arrays.copyOfRange(mrzBytes, 0, 9);
		birth_date = Arrays.copyOfRange(mrzBytes, 10, 16);
		expiration_date = Arrays.copyOfRange(mrzBytes, 17, 23);
		return Utils.concatArrays(
				document_serial, checkDigit(document_serial),
				birth_date, checkDigit(birth_date), 
				expiration_date, checkDigit(expiration_date));
	}

	static final byte[] MRZ_WEIGHT = {7, 3, 1};
	
	private static byte[] checkDigit(byte[] data) {
		int cd = 0, value = 0;
		for (int k = 0; k < data.length;  k++) {
			int d = uint8(data[k]);
		    if (d >= 'A' && d <= 'Z') value = d - 55;
		    if (d == '<') value = 0;
		    if (d >= '0' && d <= '9') value = d - '0';
		    cd += value * MRZ_WEIGHT[k % 3];
		}
		byte[] result = new byte[1];
		result[0] = (byte)((cd % 10) + '0');
		return result;
	}
}
