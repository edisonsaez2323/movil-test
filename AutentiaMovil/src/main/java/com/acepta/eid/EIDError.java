package com.acepta.eid;


public class EIDError extends Exception {

	public EIDError(String msg) {
		super(msg);
	}

	public EIDError(Exception e) {
		super(e);
	}

	public EIDError(String fmt, Object...args) {
		super(String.format(fmt, args));
	}

	private static final long serialVersionUID = 1L;
}
