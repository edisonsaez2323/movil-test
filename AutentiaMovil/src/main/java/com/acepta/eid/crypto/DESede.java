package com.acepta.eid.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DESede {

	private static final byte[] DEFAULT_DES_IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	private static final IvParameterSpec DEFAULT_IV_PARAMETER_SPEC = new IvParameterSpec(DEFAULT_DES_IV);
	private Cipher m_cipher;
	private SecretKey m_key;
	
	public DESede(byte[] key) {
		try {
		    m_key = new SecretKeySpec(key, "DESede");
		    m_cipher = Cipher.getInstance("DESede/CBC/NoPadding","BC");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	synchronized
	public byte[] encrypt(byte[] plain) {
		try {
			m_cipher.init(Cipher.ENCRYPT_MODE, m_key, DEFAULT_IV_PARAMETER_SPEC);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return m_cipher.update(plain);
	}

	synchronized
	public byte[] decrypt(byte[] ciphered) {
		try {
			m_cipher.init(Cipher.DECRYPT_MODE, m_key, DEFAULT_IV_PARAMETER_SPEC);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return m_cipher.update(ciphered);
	}
}
