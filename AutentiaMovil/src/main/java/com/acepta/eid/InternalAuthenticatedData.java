package com.acepta.eid;

import android.os.Environment;

import org.spongycastle.asn1.ASN1EncodableVector;
import org.spongycastle.asn1.ASN1Primitive;
import org.spongycastle.asn1.DERApplicationSpecific;
import org.spongycastle.asn1.DEROctetString;
import org.spongycastle.asn1.DERSequence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class InternalAuthenticatedData {
	byte[] timestamp;
	byte[][] data;
	byte[][] signature;
	public long running_millis;
	public int bitMatched;
}

class DataCertificationSerializer {

	public static void saveDER(byte[] ef_com, byte[] ef_sod, byte[] ef_dg15, InternalAuthenticatedData internalAuthenticatedData) {
		String filename;
		{
			StringBuilder sb = new StringBuilder();
			sb.append("certifydatasha256-");
			String hex;
			for (int i = 0; i < internalAuthenticatedData.timestamp.length; i++) {
				hex = Integer.toHexString(internalAuthenticatedData.timestamp[i] & 0xff);
				if (hex.length() == 1) {
					sb.append('0');
				}
				sb.append(hex);
			}
			sb.append('-');
			sb.append(internalAuthenticatedData.running_millis);
			sb.append(".der");
			filename = sb.toString();
		}
		ASN1Primitive asn1primitive = DataCertificationSerializer.toASN1Primitive(ef_com, ef_sod, ef_dg15, internalAuthenticatedData);
		try {
			File baseDir = Environment.getExternalStorageDirectory();
			File file = new File(baseDir, filename);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(asn1primitive.getEncoded());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ASN1Primitive toASN1Primitive(byte[] ef_com, byte[] ef_sod, byte[] ef_dg15, InternalAuthenticatedData internalAuthenticatedData) {
		ASN1Primitive asn1efcom = new DEROctetString(ef_com);
		ASN1Primitive asn1efsod = new DEROctetString(ef_sod);
		ASN1Primitive asn1efdg15 = new DEROctetString(ef_dg15);
		ASN1Primitive asn1iadata;
		{

			ASN1Primitive asn1timestamp = new DEROctetString(internalAuthenticatedData.timestamp);

			DERSequence asn1datasequence;
			{
				ASN1EncodableVector asn1datavector = new ASN1EncodableVector();
				for (int i = 0; i < internalAuthenticatedData.data.length; i++) {
					ASN1Primitive asn1data = new DEROctetString(internalAuthenticatedData.data[i]);
					asn1datavector.add(asn1data);
				}
				asn1datasequence = new DERSequence(asn1datavector);
			}

			DERSequence asn1signaturesequence;
			{
				ASN1EncodableVector asn1signaturevector = new ASN1EncodableVector();
				for (int i = 0; i < internalAuthenticatedData.signature.length; i++) {
					ASN1Primitive asn1signature = new DEROctetString(internalAuthenticatedData.signature[i]);
					asn1signaturevector.add(asn1signature);
				}
				asn1signaturesequence = new DERSequence(asn1signaturevector);
			}

			DERSequence sequence;
			{
				ASN1EncodableVector vector = new ASN1EncodableVector();
				vector.add(asn1timestamp);
				vector.add(asn1datasequence);
				vector.add(asn1signaturesequence);
				sequence = new DERSequence(vector);
			}
			asn1iadata = sequence;
		}

		DERSequence sequence;
		{
			ASN1EncodableVector vector = new ASN1EncodableVector();
			vector.add(asn1efcom);
			vector.add(asn1efsod);
			vector.add(asn1efdg15);
			vector.add(asn1iadata);
			sequence = new DERSequence(vector);
			/* --- */
			try {
				int tag = 0x5050;
				DERApplicationSpecific derApplicationSpecific1 = new DERApplicationSpecific(tag, sequence);
				DERApplicationSpecific derApplicationSpecific2 = new DERApplicationSpecific(tag, vector);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sequence;

	}
}