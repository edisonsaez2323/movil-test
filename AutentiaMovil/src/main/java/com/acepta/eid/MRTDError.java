package com.acepta.eid;

public class MRTDError extends Exception {

	private static final long serialVersionUID = 1L;

	public MRTDError(String msg) {
		super(msg);
	}
}
