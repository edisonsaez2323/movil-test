package com.acepta.eid;

import com.acepta.Utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MRTDConst {

		public static final byte[] EF_COM = { 0x01, 0x1E };
		public static final byte[] EF_DG1 = { 0x01, 0x01 };
		public static final byte[] EF_DG2 = { 0x01, 0x02 };
		public static final byte[] EF_DG3 = { 0x01, 0x03 };
		public static final byte[] EF_DG4 = { 0x01, 0x04 };
		public static final byte[] EF_DG5 = { 0x01, 0x05 };
		public static final byte[] EF_DG6 = { 0x01, 0x06 };
		public static final byte[] EF_DG7 = { 0x01, 0x07 };
		public static final byte[] EF_DG8 = { 0x01, 0x08 };
		public static final byte[] EF_DG9 = { 0x01, 0x09 };
		public static final byte[] EF_DG10 = { 0x01, 0x0A };
		public static final byte[] EF_DG11 = { 0x01, 0x0B };
		public static final byte[] EF_DG12 = { 0x01, 0x0C };
		public static final byte[] EF_DG13 = { 0x01, 0x0D };
		public static final byte[] EF_DG14 = { 0x01, 0x0E };
		public static final byte[] EF_DG15 = { 0x01, 0x0F };
		public static final byte[] EF_DG16 = { 0x01, 0x10 };
		public static final byte[] EF_SOD = { 0x01, 0x1D };

		public static final long TAG_DGCOM = 0x60;
		public static final long TAG_DG1 = 0x61;
		public static final long TAG_DG2 = 0x75;
		public static final long TAG_DG3 = 0x63;
		public static final long TAG_DG4 = 0x76;
		public static final long TAG_DG5 = 0x65;
		public static final long TAG_DG6 = 0x66;
		public static final long TAG_DG7 = 0x67;
		public static final long TAG_DG8 = 0x68;
		public static final long TAG_DG9 = 0x69;
		public static final long TAG_DG10 = 0x6A;
		public static final long TAG_DG11 = 0x6B;
		public static final long TAG_DG12 = 0x6C;
		public static final long TAG_DG13 = 0x6D;
		public static final long TAG_DG14 = 0x6E;
		public static final long TAG_DG15 = 0x6F;
		public static final long TAG_DG16 = 0x70;
		public static final long TAG_DGSOD = 0x77;

		// Data Group 2 Elements
		public static final String BDB = "5f2e";
		public static final String BDB1 = "7f2e";
		public static final String FAC = "46414300";

		public static final Map<String, String> DG2_ELEMENTS;

		public static final long FACE_IMAGE_DATA_MAGIC = 0x46414300;

	static {
			Map<String, String> DG2_ELEMENTS_map = new HashMap<String, String>();
			DG2_ELEMENTS_map.put(Utils.bytesAsHex(EF_COM), "EF.DG2");
			DG2_ELEMENTS_map.put("7f61", "Biometric Information Group Template");
			DG2_ELEMENTS_map.put("02",
					"Integer - Number of instances of this type of biometric");
			DG2_ELEMENTS_map.put("7f60", "1st Biometric Information Template");
			DG2_ELEMENTS_map.put("a1", "Biometric Header Template (BHT)");
			DG2_ELEMENTS_map
					.put("80",
							"ICAO header version [01 00] (Optional) - Version of the CBEFF patron header format");
			DG2_ELEMENTS_map.put("81", "Biometric type (Optional)");
			DG2_ELEMENTS_map
					.put("82",
							"Biometric feature (Optional for DG2, mandatory for DG3, DG4.)");
			DG2_ELEMENTS_map.put("83", "Creation date and time (Optional)");
			DG2_ELEMENTS_map.put("84", "Validity period (from through) (Optional)");
			DG2_ELEMENTS_map.put("86",
					"Creator of the biometric reference data (PID) (Optional)");
			DG2_ELEMENTS_map.put("87", "Format owner (Mandatory)");
			DG2_ELEMENTS_map.put("88", "Format type (Mandatory)");
			DG2_ELEMENTS_map
					.put(BDB,
							"Biometric data (encoded according to Format Owner) also called the biometric data block (BDB).");
			DG2_ELEMENTS_map
					.put(BDB1,
							"Biometric data (encoded according to Format Owner) also called the biometric data block (BDB).");
			DG2_ELEMENTS_map.put("7f60", "2nd Biometric Information Template");
			DG2_ELEMENTS_map.put(FAC, "Format Identifier ASCII FAC");
			DG2_ELEMENTS = Collections.unmodifiableMap(DG2_ELEMENTS_map);
		}

		// Data Group 2 field types
		public static final int TEMPLATE = 0;
		public static final int SUB = 1;

		public static final Map<String, Integer> DG2_TYPE;
		static {
			Map<String, Integer> DG2_TYPE_map = new HashMap<String, Integer>();
			DG2_TYPE_map.put(Utils.bytesAsHex(EF_COM), TEMPLATE);
			DG2_TYPE_map.put("7f61", TEMPLATE);
			DG2_TYPE_map.put("02", SUB);
			DG2_TYPE_map.put("7f60", TEMPLATE);
			DG2_TYPE_map.put("a1", TEMPLATE);
			DG2_TYPE_map.put("80", SUB);
			DG2_TYPE_map.put("81", SUB);
			DG2_TYPE_map.put("82", SUB);
			DG2_TYPE_map.put("83", SUB);
			DG2_TYPE_map.put("84", SUB);
			DG2_TYPE_map.put("86", SUB);
			DG2_TYPE_map.put("87", SUB);
			DG2_TYPE_map.put("88", SUB);
			DG2_TYPE_map.put("5f2e", TEMPLATE);
			DG2_TYPE_map.put("7f2e", TEMPLATE);
			DG2_TYPE_map.put("7f60", TEMPLATE);
			DG2_TYPE = Collections.unmodifiableMap(DG2_TYPE_map);
		}

		// ISO 19794_5 (Biometric identifiers)
		public static final Map<String, String> ISO19794_5_GENDER;
		static {
			Map<String, String> ISO19794_5_GENDER_map = new HashMap<String, String>();
			ISO19794_5_GENDER_map.put("00", "Unpecified");
			ISO19794_5_GENDER_map.put("01", "Male");
			ISO19794_5_GENDER_map.put("02", "Female");
			ISO19794_5_GENDER_map.put("03", "Unknown");
			ISO19794_5_GENDER_map.put("ff", "Other");
			ISO19794_5_GENDER = Collections.unmodifiableMap(ISO19794_5_GENDER_map);
		}

		public static final Map<String, String> ISO19794_5_EYECOLOUR;
		static {
			Map<String, String> ISO19794_5_EYECOLOUR_map = new HashMap<String, String>();
			ISO19794_5_EYECOLOUR_map.put("00", "Unspecified");
			ISO19794_5_EYECOLOUR_map.put("01", "Black");
			ISO19794_5_EYECOLOUR_map.put("02", "Blue");
			ISO19794_5_EYECOLOUR_map.put("03", "Brown");
			ISO19794_5_EYECOLOUR_map.put("04", "Grey");
			ISO19794_5_EYECOLOUR_map.put("05", "Green");
			ISO19794_5_EYECOLOUR_map.put("06", "Multi");
			ISO19794_5_EYECOLOUR_map.put("07", "Pink");
			ISO19794_5_EYECOLOUR_map.put("08", "Other");
			ISO19794_5_EYECOLOUR = Collections
					.unmodifiableMap(ISO19794_5_EYECOLOUR_map);
		}

		public static final Map<String, String> ISO19794_5_HAIRCOLOUR;
		static {
			Map<String, String> ISO19794_5_HAIRCOLOUR_map = new HashMap<String, String>();
			ISO19794_5_HAIRCOLOUR_map.put("00", "Unspecified");
			ISO19794_5_HAIRCOLOUR_map.put("01", "Bald");
			ISO19794_5_HAIRCOLOUR_map.put("02", "Black");
			ISO19794_5_HAIRCOLOUR_map.put("03", "Blonde");
			ISO19794_5_HAIRCOLOUR_map.put("04", "Brown");
			ISO19794_5_HAIRCOLOUR_map.put("05", "Grey");
			ISO19794_5_HAIRCOLOUR_map.put("06", "White");
			ISO19794_5_HAIRCOLOUR_map.put("07", "Red");
			ISO19794_5_HAIRCOLOUR_map.put("08", "Green");
			ISO19794_5_HAIRCOLOUR_map.put("09", "Blue");
			ISO19794_5_HAIRCOLOUR_map.put("ff", "Other");
			ISO19794_5_HAIRCOLOUR = Collections
					.unmodifiableMap(ISO19794_5_HAIRCOLOUR_map);
		}
		public static final Map<Integer, String> ISO19794_5_FEATURE;
		static {
			Map<Integer, String> ISO19794_5_FEATURE_map = new HashMap<Integer, String>();
			ISO19794_5_FEATURE_map.put(0x01, "Specified");
			ISO19794_5_FEATURE_map.put(0x02, "Glasses");
			ISO19794_5_FEATURE_map.put(0x04, "Moustache");
			ISO19794_5_FEATURE_map.put(0x08, "Beard");
			ISO19794_5_FEATURE_map.put(0x10, "Teeth Visible");
			ISO19794_5_FEATURE_map.put(0x20, "Blink");
			ISO19794_5_FEATURE_map.put(0x40, "Mouth Open");
			ISO19794_5_FEATURE_map.put(0x80, "Left Eyepatch");
			ISO19794_5_FEATURE_map.put(0x100, "Right Eyepatch");
			ISO19794_5_FEATURE_map.put(0x200, "Dark Glasses");
			ISO19794_5_FEATURE_map.put(0x400, "Distorted");
			ISO19794_5_FEATURE = Collections
					.unmodifiableMap(ISO19794_5_FEATURE_map);
		}

		public static final Map<String, String> ISO19794_5_EXPRESSION;
		static {
			Map<String, String> ISO19794_5_EXPRESSION_map = new HashMap<String, String>();
			ISO19794_5_EXPRESSION_map.put("0000", "Unspecified");
			ISO19794_5_EXPRESSION_map.put("0001", "Neutral");
			ISO19794_5_EXPRESSION_map.put("0002", "Smile Closed");
			ISO19794_5_EXPRESSION_map.put("0003", "Smile Open");
			ISO19794_5_EXPRESSION_map.put("0004", "Raised Eyebrow");
			ISO19794_5_EXPRESSION_map.put("0005", "Looking Away");
			ISO19794_5_EXPRESSION_map.put("0006", "Squinting");
			ISO19794_5_EXPRESSION_map.put("0007", "Frowning");
			ISO19794_5_EXPRESSION = Collections
					.unmodifiableMap(ISO19794_5_EXPRESSION_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_TYPE;
		static {
			Map<String, String> ISO19794_5_IMG_TYPE_map = new HashMap<String, String>();
			ISO19794_5_IMG_TYPE_map.put("00", "Unspecified (Front)");
			ISO19794_5_IMG_TYPE_map.put("01", "Basic");
			ISO19794_5_IMG_TYPE_map.put("02", "Full Front");
			ISO19794_5_IMG_TYPE_map.put("03", "Token Front");
			ISO19794_5_IMG_TYPE_map.put("04", "Other");
			ISO19794_5_IMG_TYPE = Collections
					.unmodifiableMap(ISO19794_5_IMG_TYPE_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_DTYPE;
		static {
			Map<String, String> ISO19794_5_IMG_DTYPE_map = new HashMap<String, String>();
			ISO19794_5_IMG_DTYPE_map.put("00", "JPEG");
			ISO19794_5_IMG_DTYPE_map.put("01", "JPEG 2000");
			ISO19794_5_IMG_DTYPE = Collections
					.unmodifiableMap(ISO19794_5_IMG_DTYPE_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_FTYPE;
		static {
			Map<String, String> ISO19794_5_IMG_FTYPE_map = new HashMap<String, String>();
			ISO19794_5_IMG_FTYPE_map.put("00", "JPG");
			ISO19794_5_IMG_FTYPE_map.put("01", "JP2");
			ISO19794_5_IMG_FTYPE = Collections
					.unmodifiableMap(ISO19794_5_IMG_FTYPE_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_CSPACE;
		static {
			Map<String, String> ISO19794_5_IMG_CSPACE_map = new HashMap<String, String>();
			ISO19794_5_IMG_CSPACE_map.put("00", "Unspecified");
			ISO19794_5_IMG_CSPACE_map.put("01", "RGB24");
			ISO19794_5_IMG_CSPACE_map.put("02", "YUV422");
			ISO19794_5_IMG_CSPACE_map.put("03", "GREY8BIT");
			ISO19794_5_IMG_CSPACE_map.put("04", "Other");
			ISO19794_5_IMG_CSPACE = Collections
					.unmodifiableMap(ISO19794_5_IMG_CSPACE_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_SOURCE;
		static {
			Map<String, String> ISO19794_5_IMG_SOURCE_map = new HashMap<String, String>();
			ISO19794_5_IMG_SOURCE_map.put("00", "Unspecified");
			ISO19794_5_IMG_SOURCE_map.put("01", "Static Unspecified");
			ISO19794_5_IMG_SOURCE_map.put("02", "Static Digital");
			ISO19794_5_IMG_SOURCE_map.put("03", "Static Scan");
			ISO19794_5_IMG_SOURCE_map.put("04", "Video Unknown");
			ISO19794_5_IMG_SOURCE_map.put("05", "Video Analogue");
			ISO19794_5_IMG_SOURCE_map.put("06", "Video Digital");
			ISO19794_5_IMG_SOURCE_map.put("07", "Other");
			ISO19794_5_IMG_SOURCE = Collections
					.unmodifiableMap(ISO19794_5_IMG_SOURCE_map);
		}

		public static final Map<String, String> ISO19794_5_IMG_QUALITY;
		static {
			Map<String, String> ISO19794_5_IMG_QUALITY_map = new HashMap<String, String>();
			ISO19794_5_IMG_QUALITY_map.put("00", "Unspecified");
			ISO19794_5_IMG_QUALITY = Collections
					.unmodifiableMap(ISO19794_5_IMG_QUALITY_map);
		}

		public static final Map<String, String> DG7_ELEMENTS;
		static {
			Map<String, String> DG7_ELEMENTS_map = new HashMap<String, String>();
			DG7_ELEMENTS_map.put(Utils.bytesAsHex(EF_DG7), "EF.DG7");
			DG7_ELEMENTS_map.put("5f43", "Displayed signature or mark");
			DG7_ELEMENTS_map
					.put("02",
							"Integer - Number of instances of this type of displayed image");
			DG7_ELEMENTS = Collections.unmodifiableMap(DG7_ELEMENTS_map);
		}

	}
