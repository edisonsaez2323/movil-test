package com.acepta.eid;

import android.util.Log;

import com.acepta.tlv.BinaryParser;
import com.acepta.tlv.ETLVError;
import com.acepta.tlv.OID;
import com.acepta.tlv.TLVReader;
import com.acepta.tlv.TaggedValue;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static com.acepta.eid.MRTDConst.EF_COM;
import static com.acepta.eid.MRTDConst.EF_DG1;
import static com.acepta.eid.MRTDConst.EF_DG10;
import static com.acepta.eid.MRTDConst.EF_DG11;
import static com.acepta.eid.MRTDConst.EF_DG12;
import static com.acepta.eid.MRTDConst.EF_DG13;
import static com.acepta.eid.MRTDConst.EF_DG14;
import static com.acepta.eid.MRTDConst.EF_DG15;
import static com.acepta.eid.MRTDConst.EF_DG16;
import static com.acepta.eid.MRTDConst.EF_DG2;
import static com.acepta.eid.MRTDConst.EF_DG3;
import static com.acepta.eid.MRTDConst.EF_DG4;
import static com.acepta.eid.MRTDConst.EF_DG5;
import static com.acepta.eid.MRTDConst.EF_DG6;
import static com.acepta.eid.MRTDConst.EF_DG7;
import static com.acepta.eid.MRTDConst.EF_DG8;
import static com.acepta.eid.MRTDConst.EF_DG9;
import static com.acepta.eid.MRTDConst.EF_SOD;
import static com.acepta.eid.MRTDConst.TAG_DG1;
import static com.acepta.eid.MRTDConst.TAG_DG10;
import static com.acepta.eid.MRTDConst.TAG_DG11;
import static com.acepta.eid.MRTDConst.TAG_DG12;
import static com.acepta.eid.MRTDConst.TAG_DG13;
import static com.acepta.eid.MRTDConst.TAG_DG14;
import static com.acepta.eid.MRTDConst.TAG_DG15;
import static com.acepta.eid.MRTDConst.TAG_DG16;
import static com.acepta.eid.MRTDConst.TAG_DG2;
import static com.acepta.eid.MRTDConst.TAG_DG3;
import static com.acepta.eid.MRTDConst.TAG_DG4;
import static com.acepta.eid.MRTDConst.TAG_DG5;
import static com.acepta.eid.MRTDConst.TAG_DG6;
import static com.acepta.eid.MRTDConst.TAG_DG7;
import static com.acepta.eid.MRTDConst.TAG_DG8;
import static com.acepta.eid.MRTDConst.TAG_DG9;
import static com.acepta.eid.MRTDConst.TAG_DGCOM;
import static com.acepta.eid.MRTDConst.TAG_DGSOD;

public class DataGroupReader {

	private static final long PRIVATE_TAG_SOD_HASH_LIST = 0;
    private static final String TAG = "DG_READER";

    final DGInfo[] DG_INFO_TABLE = new DGInfo[] {
			new DGInfo(TAG_DGCOM, EF_COM, "Common"),
			new DGInfo(TAG_DG1, EF_DG1, "DG1"),
			new DGInfo(TAG_DG2, EF_DG2, "DG2"),
			new DGInfo(TAG_DG3, EF_DG3, "DG3"),
			new DGInfo(TAG_DG4, EF_DG4, "DG4"),
			new DGInfo(TAG_DG5, EF_DG5, "DG5"),
			new DGInfo(TAG_DG6, EF_DG6, "DG6"),
			new DGInfo(TAG_DG7, EF_DG7, "DG7"),
			new DGInfo(TAG_DG8, EF_DG8, "DG8"),
			new DGInfo(TAG_DG9, EF_DG9, "DG9"),
			new DGInfo(TAG_DG10, EF_DG10, "DG10"),
			new DGInfo(TAG_DG11, EF_DG11, "DG11"),
			new DGInfo(TAG_DG12, EF_DG12, "DG12"),
			new DGInfo(TAG_DG13, EF_DG13, "DG13"),
			new DGInfo(TAG_DG14, EF_DG14, "DG14"),
			new DGInfo(TAG_DG15, EF_DG15, "DG15"),
			new DGInfo(TAG_DG16, EF_DG16, "DG16"),
			new DGInfo(TAG_DGSOD, EF_SOD, "SecurityData") };

	public static TaggedValue read(ByteBuffer bytes) throws EIDError {
		TLVReader reader = new TLVReader(bytes);
		TaggedValue result;
		try {
			result = reader.readTaggedValue();
		} catch (ETLVError e) {
			throw new EIDError(e);
		}

		if (result.tag == TAG_DG1) {
			parseDG1(result);
		} else if (result.tag == TAG_DG2) {
			parseDG2(result);
		} else if (result.tag == TAG_DG7) {
			parseDG7(result);
		} else if (result.tag == TAG_DG11) {
			parseDG11(result);
		} else if (result.tag == TAG_DGSOD) {
			parseDGSOD(result);
		}
		return result;
	}

	private DGInfo infoForTag(long tag) throws EIDError {
		for (DGInfo info : DG_INFO_TABLE) {
			if (info.tag == tag)
				return info;
		}
		throw new EIDError(String.format("bad tag: %x", tag));
	}

	private static void parseDGSOD(TaggedValue tlv) throws EIDError {
        Log.i(TAG, "DGSOD parsing");
		TaggedValue signedData = tlv.findObject(OID
				.encode("1.2.840.113549.1.7.2"));
		if (signedData == null)
			throw new EIDError("SOD signedData not found");

		// aqui tenemos que validar la firma de la data en el SOD
		// signedData.asBytes es un PKCS7 CSM

		TaggedValue sodHashListObject = tlv.findObject(OID
				.encode("2.23.136.1.1.1"));
		if (sodHashListObject == null)
			throw new EIDError("SOD hash list not found");

		// aqui tenemos que parsear SOD's hash list
		TaggedValue sodHashListOctects = sodHashListObject.find(0xA0).find(0x4);
		if (sodHashListOctects == null)
			throw new EIDError("SOD hash list octets not found");

		TLVReader sodReader = new TLVReader(sodHashListOctects.value);

		TaggedValue parsedHashList = new TaggedValue();
		parsedHashList.tag = PRIVATE_TAG_SOD_HASH_LIST;
		try {
			parsedHashList.tags.add(sodReader.readTaggedValue());
		} catch (ETLVError e) {
			throw new EIDError(e);
		}

		tlv.tags.add(parsedHashList);
        Log.i(TAG, "DGSOD parsed OK");
	}

	private static void parseDG11(TaggedValue tlv) {
        Log.i(TAG, "DG11 parsing");
        Log.i(TAG, "DG11 parsed OK");
	}

	private static void parseDG7(TaggedValue tlv) {
        Log.i(TAG, "DG7 parsing");
        Log.i(TAG, "DG7 parsed OK");
	}

	private static void parseDG2(TaggedValue tlv) throws EIDError {
        Log.i(TAG, "DG2 parsing");
		byte[] value5F2E = tlv.find(0x7F61).find(0x7F60).find(0x5F2E).value;
        int headerSize = 0;
        try {
            headerSize = get5F2EHeaderLength(value5F2E);
        } catch (MRTDError mrtdError) {
            throw new EIDError(mrtdError);
        }
        TaggedValue imageData = new TaggedValue();
		imageData.tag = 0xA1;
		imageData.value = Arrays.copyOfRange(value5F2E, headerSize, value5F2E.length);
		tlv.tags.add(imageData);
        Log.i(TAG, "DG2 parsed OK");
	}

	private static int get5F2EHeaderLength(byte[] value5F2E) throws MRTDError {
        /* DataGroup 2 representa informacion facial de la persona
         * tag 5F2E contiene una cabecera con la informacion del retrato y el retrato
         * esta funcion calcula el tamano de la data de la cara
         * segun especificado en ISO 19794-5 para obtener la foto de la persona
         * sigue a esta cabecera */
		BinaryParser bp = new BinaryParser(ByteBuffer.wrap(value5F2E));
		long magic = bp.readUInt32();
		if (magic != MRTDConst.FACE_IMAGE_DATA_MAGIC) {
			throw new MRTDError("Face Image Data magic not found");
		}
        long tmp4;
        int tmp2;
        short tmp1;
        byte[] tmpb;
        tmp4 = bp.readUInt32(); // version number
        tmp4 = bp.readUInt32(); // length of record
        tmp2 = bp.readUInt16(); // number of facial images
        tmp4 = bp.readUInt32(); // face image block length
        int featurePointCount = bp.readUInt16(); // number of feature points
        tmp1 = bp.readUInt8();  // gender
        tmp1 = bp.readUInt8();  // eye color
        tmp1 = bp.readUInt8();  // hair color
        tmpb = bp.readBytes(3); // features mask
        tmp2 = bp.readUInt16(); // expression
        tmpb = bp.readBytes(3); // pose angle
        tmpb = bp.readBytes(3); // pose angle uncertainty
        for (int n = 0; n < featurePointCount; n++) {
            tmp1 = bp.readUInt8();  // fature type
            tmp1 = bp.readUInt8();  // feature point code
            tmp2 = bp.readUInt16(); // horizontal position
            tmp2 = bp.readUInt16(); // vertical position
            tmp2 = bp.readUInt16(); // reserved
        }
        tmp1 = bp.readUInt8();  // face image type
        tmp1 = bp.readUInt8();  // image data type
        tmp2 = bp.readUInt16(); // image width
        tmp2 = bp.readUInt16(); // image height
        tmp1 = bp.readUInt8();  // image color space
        tmp1 = bp.readUInt8();  // image source type
        tmp2 = bp.readUInt16(); // image device type
        tmp2 = bp.readUInt16(); // image quality
        return bp.getPosition();
	}

	private static void parseDG1(TaggedValue tlv) {
        Log.i(TAG, "DG1 parsing");
		byte[] v = tlv.find(0x5F1F).value;
		switch (v.length) {
		case 0x5A:
			;
			addMrzField(tlv, v, 0x5F03, 0, 2);
			addMrzField(tlv, v, 0x5F28, 2, 5);
			addMrzField(tlv, v, 0x5A, 5, 14);
			addMrzField(tlv, v, 0x5F04, 14, 15);
			addMrzField(tlv, v, 0x53, 15, 30);
			addMrzField(tlv, v, 0x5F57, 30, 36);
			addMrzField(tlv, v, 0x5F05, 36, 37);
			addMrzField(tlv, v, 0x5F35, 37, 38);
			addMrzField(tlv, v, 0x59, 38, 44);
			addMrzField(tlv, v, 0x5F06, 44, 45);
			addMrzField(tlv, v, 0x5F2C, 45, 48);
			addMrzField(tlv, v, 0x53, 48, 59);
			addMrzField(tlv, v, 0x5F07, 59, 60);
			addMrzField(tlv, v, 0x5B, 60);
			break;
		case 0x48:
			;
			addMrzField(tlv, v, 0x5F03, 0, 2);
			addMrzField(tlv, v, 0x5F28, 2, 5);
			addMrzField(tlv, v, 0x5B, 5, 36);
			addMrzField(tlv, v, 0x5A, 36, 45);
			addMrzField(tlv, v, 0x5F04, 45, 46);
			addMrzField(tlv, v, 0x5F2C, 46, 49);
			addMrzField(tlv, v, 0x5F57, 49, 55);
			addMrzField(tlv, v, 0x5F05, 55, 56);
			addMrzField(tlv, v, 0x5F35, 56, 57);
			addMrzField(tlv, v, 0x59, 57, 63);
			addMrzField(tlv, v, 0x5F06, 63, 64);
			addMrzField(tlv, v, 0x53, 64, 71);
			addMrzField(tlv, v, 0x5F07, 71, 72);
			break;
		default:
			;
			addMrzField(tlv, v, 0x5F03, 0, 2);
			addMrzField(tlv, v, 0x5F28, 2, 5);
			addMrzField(tlv, v, 0x5F5B, 5, 44);
			addMrzField(tlv, v, 0x5A, 44, 53);
			addMrzField(tlv, v, 0x5F04, 53, 54);
			addMrzField(tlv, v, 0x5F2C, 54, 57);
			addMrzField(tlv, v, 0x5F57, 57, 63);
			addMrzField(tlv, v, 0x5F05, 63, 64);
			addMrzField(tlv, v, 0x5F35, 64, 65);
			addMrzField(tlv, v, 0x59, 65, 71);
			addMrzField(tlv, v, 0x5F06, 71, 72);
			addMrzField(tlv, v, 0x53, 72, 86);
			addMrzField(tlv, v, 0x5F02, 86, 87);
			addMrzField(tlv, v, 0x5F07, 87, 88);
			break;
		}
        Log.i(TAG, "DG1 parsed OK");
	}

	private static void addMrzField(TaggedValue tlv, byte[] v, int tag,
			int start) {
		addMrzField(tlv, v, tag, start, v.length);
	}

	private static void addMrzField(TaggedValue tlv, byte[] v, int tag,
			int start, int end) {
		TaggedValue newField = new TaggedValue();
		newField.tag = tag;
		newField.value = Arrays.copyOfRange(v, start, end);
		tlv.tags.add(newField);
	}

	private static int asn1fieldlength(String data) {
		int intData = Integer.parseInt(data.substring(0, 2), 16);

		if (intData <= 0x7f)
			return 2;
		if (intData == 0x81)
			return 4;
		if (intData == 0x82)
			return 6;
		else
			return 0;
	}

	private static int asn1datalength(String data) {

		int intData = Integer.parseInt(data.substring(0, 2), 16);

		if (intData <= 0x7f)
			return intData;
		if (intData == 0x81)
			return Integer.parseInt(data.substring(2, 4), 16);
		if (intData == 0x82)
			return Integer.parseInt(data.substring(2, 6), 16);
		else
			return 0;
	}
}
