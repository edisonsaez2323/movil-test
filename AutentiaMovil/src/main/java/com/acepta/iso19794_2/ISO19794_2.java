package com.acepta.iso19794_2;

public class ISO19794_2 {

	/*
    Table 2 - Finger Position Codes
			Finger position
							
							ISO		MOC
	
	Unknown finger			0		
	Right thumb				1		1 + 1 * 4 = 5
	Right index finger		2		1 + 2 * 4 = 9
	Right middle finger		3		1 + 3 * 4 = 13
	Right ring finger		4		1 + 4 * 4 = 17
	Right little finger		5		1 + 5 * 4 = 21
	Left thumb				6		2 + 1 * 4 = 6
	Left index finger		7		2 + 2 * 4 = 10
	Left middle finger		8		2 + 3 * 4 = 14
	Left ring finger		9		2 + 4 * 4 = 18
	Left little finger		10		2 + 5 * 4 = 22
	*/

    public static int decodeMOCFingerPos(int mocFingerPos) {
        switch (mocFingerPos) {
            case 5:
                return 1;
            case 9:
                return 2;
            case 13:
                return 3;
            case 17:
                return 4;
            case 21:
                return 5;
            case 6:
                return 6;
            case 10:
                return 7;
            case 14:
                return 8;
            case 18:
                return 9;
            case 22:
                return 10;
            default:
                return 0;
        }
    }

    /**
     *
     * @param isoFingers
     * @return
     */
    public static boolean[] traslateIsoToBooleanArray(Integer... isoFingers) {
        boolean[] dedos = new boolean[11];

        for (int i = 0; i < 10; i++) {

            for (int dedo : isoFingers) {
                if (i == dedo) {
                    dedos[i] = true;
                }
            }
        }
        return dedos;
    }
}
