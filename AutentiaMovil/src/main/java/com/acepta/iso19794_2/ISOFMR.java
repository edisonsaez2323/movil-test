package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;
import com.acepta.tlv.TLVEncoder;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class ISOFMR {

	private static final int MAXIMUM_CC_MINUTIAE_COUNT = 80;
	private static final ISOCCMinutiae_LowerYFirst ISO_CC_MINUTIAE_LOWER_Y_FIRST = new ISOCCMinutiae_LowerYFirst();
	private static final ISOMinutiae_HigherQFirst ISO_MINUTIAE_HIGHER_Q_FIRST = new ISOMinutiae_HigherQFirst();

	public ISOHeader header;
	public ISOView[] views;
	
	public byte[] ccStraight;
	//public byte[] ccRotated;

	public ISOFMR(byte[] minutiae) throws ISOError {
		this(new BinaryParser(ByteBuffer.wrap(minutiae)));
	}
	
	public ISOFMR(BinaryParser bp) throws ISOError {
		header = new ISOHeader(bp);
		views = new ISOView[header.nr_views];
		for (int n = 0; n < views.length; n++) {
			views[n] = new ISOView(bp);
		}
	}
	
	private static final byte[] TAG_7F2E = {0x7F, 0x2E};
    private static final byte[] TAG_81 = {(byte) 0x81};
	
	public void convertToCC() {

		short minutiae_count = views[0].header.minutiae_count;
		int cc_minutiae_count = Math.min(MAXIMUM_CC_MINUTIAE_COUNT, minutiae_count);

		ByteBuffer hd2 = ByteBuffer.allocate(250);
		hd2.put(TAG_81);
		hd2.put(TLVEncoder.encodeLength(cc_minutiae_count * 3));

		/* sort input ISO minutiae by quality, higher quality first */
		Arrays.sort(views[0].minutiae, ISO_MINUTIAE_HIGHER_Q_FIRST);

		ISOCCMinutiae[] straight_cc = new ISOCCMinutiae[cc_minutiae_count];

		/* process only first cc_minutiae_count minutiaes due to ISOCC format's limitations */
		for (int n = 0; n < cc_minutiae_count; n++) {
	      	straight_cc[n] = new ISOCCMinutiae(views[0].minutiae[n], header);
		}

		Arrays.sort(straight_cc, ISO_CC_MINUTIAE_LOWER_Y_FIRST);
		
		for (ISOCCMinutiae ccm : straight_cc) {
			ccm.put(hd2);
		}

		byte[] hd2Bytes = new byte[hd2.position()];
		hd2.rewind();
		hd2.get(hd2Bytes);

		ByteBuffer hd1 = ByteBuffer.allocate(250);
		hd1.put(TAG_7F2E);
 		hd1.put(TLVEncoder.encodeLength(hd2Bytes.length));
		hd1.put(hd2Bytes);

		ccStraight = hd1.array();
	}
}
