package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;

public class ISOView {

	public ISOViewHeader header;
	public ISOMinutiae[] minutiae;
	public int extended_size;
	public byte[] extended_data;

	public ISOView(BinaryParser bp) {
		header = new ISOViewHeader(bp);
		minutiae = new ISOMinutiae[header.minutiae_count];
		for (int n = 0; n < header.minutiae_count; n++) {
		    minutiae[n] = new ISOMinutiae(bp);
		}
		extended_size = bp.readUInt16();
		if (extended_size > 0)
			extended_data = bp.readBytes(extended_size);
		else
			extended_data = new byte[0];
	}
}
