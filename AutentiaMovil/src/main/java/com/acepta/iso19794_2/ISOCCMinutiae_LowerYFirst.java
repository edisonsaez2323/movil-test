package com.acepta.iso19794_2;

import java.util.Comparator;

public class ISOCCMinutiae_LowerYFirst implements Comparator<ISOCCMinutiae> {

	@Override
	public int compare(ISOCCMinutiae a, ISOCCMinutiae b) {
		if (a.y > b.y) return 1;
		if (a.y < b.y) return -1;
		if (a.x > b.x) return 1;
		if (a.x < b.x) return -1;
		return 0;
	}
}
