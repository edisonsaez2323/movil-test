package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;

public class ISOViewHeader {

	public short position;
	public byte view_nr;
	public byte impresion_type;
	public short quality;
	public short minutiae_count;

	public ISOViewHeader(BinaryParser bp) {
		short tmp8;
		position = bp.readUInt8();
	  	tmp8 = bp.readUInt8();
	  	view_nr = (byte) bp.getBits(tmp8, 4, 4);
	  	impresion_type = (byte) bp.getBits(tmp8, 0, 4);
	  	quality = bp.readUInt8();
	  	minutiae_count = bp.readUInt8();
	}
}
