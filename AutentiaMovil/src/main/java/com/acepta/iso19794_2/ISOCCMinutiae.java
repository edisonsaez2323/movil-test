package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;

import java.nio.ByteBuffer;

public class ISOCCMinutiae {

	final short x;
	final short y;
	final short theta;
	final byte type;

	private ISOCCMinutiae(short x, short y, short theta, byte type) {
		this.x = x;
		this.y = y;
		this.theta = theta;
		this.type = type;
	}

	public ISOCCMinutiae(ISOMinutiae m, ISOHeader h) {
		x = iso_to_isocc_coordinate(m.x, h.x_res);
      	y = iso_to_isocc_coordinate(m.y, h.y_res);
      	theta = iso_to_isocc_angle(m.theta);
      	type = m.type;
	}

	public void put(ByteBuffer buf) {
		buf.put((byte) x);
		buf.put((byte) y);
		byte type_theta;
		type_theta = (byte) BinaryParser.setBits(0, 0, 6, theta);
		type_theta = (byte) BinaryParser.setBits(type_theta, 6, 2, type);
	    buf.put(type_theta);
	}

	private short iso_to_isocc_angle(short angle) {
		return (short) Math.round((angle * 64.0) / 256.0);
	}

	private short iso_to_isocc_coordinate(short v, int v_res) {
		return (short) Math.round((v * 100.0) / v_res);
	}

	public ISOCCMinutiae rotate180() {
		return new ISOCCMinutiae((short)(255 - x), (short)(255 - y), (short)((theta + 32) % 64), type);
	}
}
