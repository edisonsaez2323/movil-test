package com.acepta.iso19794_2;

public class ISOError extends Exception {

	public ISOError(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = 1L;
}
