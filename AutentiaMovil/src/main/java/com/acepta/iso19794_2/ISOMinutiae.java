package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;

public class ISOMinutiae {

	public byte type;
	public short x;
	public short y;
	public short theta;
	public short quality;

	public ISOMinutiae(BinaryParser bp) {
		int tmp16 = bp.readUInt16();
	  	type = (byte) bp.getBits(tmp16, 14, 2);
	  	x = (short) bp.getBits(tmp16, 0, 14);
	  	tmp16 = bp.readUInt16();
	  	y = (short) bp.getBits(tmp16, 0, 14);
	  	theta = bp.readUInt8();
	  	quality = bp.readUInt8();
	}
}
