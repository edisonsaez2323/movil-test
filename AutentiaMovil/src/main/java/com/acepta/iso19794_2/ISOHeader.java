package com.acepta.iso19794_2;

import com.acepta.tlv.BinaryParser;

public class ISOHeader {

	private static final long ISO_FORMAT_MAGIC = 0x464d5200;

	public int nr_views;
	public long format_id;
	public long version;
	public long length;
	public byte CEC;
	public short CDTID;
	public int x_size;
	public int y_size;
	public int x_res;
	public int y_res;
	public short reserved;

	public ISOHeader(BinaryParser bp) throws ISOError {
		int tmp16;
		format_id = bp.readUInt32();
		if (format_id != ISO_FORMAT_MAGIC)
			throw new ISOError("invalid magic");
	  	version = bp.readUInt32();
	  	length = bp.readUInt32();
	  	tmp16 = bp.readUInt16();
	  	CEC = (byte) bp.getBits(tmp16, 12, 4);
	  	CDTID = (short) bp.getBits(tmp16, 0, 12);
	  	x_size = bp.readUInt16();
	  	y_size = bp.readUInt16();
	  	x_res = bp.readUInt16();
	  	y_res = bp.readUInt16();
	  	nr_views = bp.readUInt8();
	  	reserved = bp.readUInt8();
	}
}
