package com.acepta.iso19794_2;

import java.util.Comparator;

public class ISOMinutiae_HigherQFirst implements Comparator<ISOMinutiae> {

	@Override
	public int compare(ISOMinutiae a, ISOMinutiae b) {
	    if (a.quality < b.quality) return 1;
		if (a.quality > b.quality) return -1;
		return 0;
	}
}
