package com.acepta;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;

import jj2000.JJ2000Error;
import jj2000.JJ2000Frontend;

/**
 * Created by marcin on 2/22/17.
 */
public class JJ2000Utils {
    private static final String TAG = "JJ2000_UTILS";

    public static byte[] jp2ToJpeg(byte[] value) throws JJ2000Error, JJ2000UtilsError {
        Log.d(TAG, "JP2: size: " + value.length);
        Utils.dump(TAG, value);

        Bitmap decoded = JJ2000Frontend.decode(value);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (! decoded.compress(Bitmap.CompressFormat.JPEG, 95, baos))
            throw new JJ2000UtilsError("JPEG compression error");

        Log.d(TAG, String.format("image: %d x %d", decoded.getWidth(), decoded.getHeight()));
        Log.d(TAG, "JPEG size: " + baos.size());
        return baos.toByteArray();
    }
}
