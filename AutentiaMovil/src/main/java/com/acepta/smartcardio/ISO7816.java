package com.acepta.smartcardio;

public class ISO7816 {

	private static int maskSW(int value) {
		int masked = value & 0xff00; 
		switch (masked) {
		case 0x6100: case 0x6400: case 0x6600: case 0x6C00:
			return masked;
		}
		return value;
	}
	
	public static String swInfo(int sw) {
		switch (maskSW(sw)) {
			case 0x6100: return "SW2 indicates the number of response bytes still available";
			case 0x6200: return "No information given";
			case 0x6281: return "Part of returned data may be corrupted";
			case 0x6282: return "End of file/record reached before reading Le bytes";
			case 0x6283: return "Selected file invalidated";
			case 0x6284: return "FCI not formatted according to ISO7816-4 section 5.1.5";
			case 0x6300: return "No information given";
			case 0x6381: return "File filled up by the last write";
			case 0x6382: return "Card Key not supported";
			case 0x6383: return "Reader Key not supported";
			case 0x6384: return "Plain transmission not supported";
			case 0x6385: return "Secured Transmission not supported";
			case 0x6386: return "Volatile memory not available";
			case 0x6387: return "Non Volatile memory not available";
			case 0x6388: return "Key number not valid";
			case 0x6389: return "Key length is not correct";
			case 0x630C: return "Counter provided by X (valued from 0 to 15) (exact meaning depending on the command)";
			case 0x6400: return "State of non-volatile memory unchanged (SW2=00, other values are RFU)";
			case 0x6500: return "No information given";
			case 0x6581: return "Memory failure";
			case 0x6600: return "Reserved for security-related issues (not defined in this part of ISO/IEC 7816)";
			case 0x6700: return "Wrong length";
			case 0x6800: return "No information given";
			case 0x6881: return "Logical channel not supported";
			case 0x6882: return "Secure messaging not supported";
			case 0x6900: return "No information given";
			case 0x6981: return "Command incompatible with file structure";
			case 0x6982: return "Security status not satisfied";
			case 0x6983: return "Authentication method blocked";
			case 0x6984: return "Referenced data invalidated";
			case 0x6985: return "Conditions of use not satisfied";
			case 0x6986: return "Command not allowed (no current EF)";
			case 0x6987: return "Expected SM data objects missing";
			case 0x6988: return "SM data objects incorrect";
			case 0x6A00: return "No information given";
			case 0x6A80: return "Incorrect parameters in the data field";
			case 0x6A81: return "Function not supported";
			case 0x6A82: return "File not found";
			case 0x6A83: return "Record not found";
			case 0x6A84: return "Not enough memory space in the file";
			case 0x6A85: return "Lc inconsistent with TLV structure";
			case 0x6A86: return "Incorrect parameters P1-P2";
			case 0x6A87: return "Lc inconsistent with P1-P2";
			case 0x6A88: return "Referenced data not found";
			case 0x6B00: return "Wrong parameter(s) P1-P2";
			case 0x6C00: return "Wrong length Le: SW2 indicates the exact length";
			case 0x6D00: return "Instruction code not supported or invalid";
			case 0x6E00: return "Class not supported";
			case 0x6F00: return "No precise diagnosis";
			case 0x9000: return "Success";
		};
		return "Undecoded";
	}
}
