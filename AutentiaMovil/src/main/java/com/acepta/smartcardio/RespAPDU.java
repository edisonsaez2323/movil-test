package com.acepta.smartcardio;

import java.util.Arrays;

import static com.acepta.Utils.uint16;

public class RespAPDU {

	public int sw;
    public byte[] data;
    public long time;

    public RespAPDU() {
    	sw = 0;
    	data = null;
    	time = 0;
    }
    
    public RespAPDU(byte[] bytes, long time) {
    	sw = uint16(bytes[bytes.length-2], bytes[bytes.length-1]);
    	data = Arrays.copyOf(bytes, bytes.length - 2);
    	this.time = time;
    }
}
