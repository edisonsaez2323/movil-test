package com.acepta.smartcardio;

import android.nfc.tech.IsoDep;

import java.io.IOException;
import java.util.Arrays;

public class NativeSmartCardConnection extends SmartCardConnection {

	public IsoDep m_dep;

	// private int m_cardSlot;

	public NativeSmartCardConnection(IsoDep dep/* , int cardSlot */) {
		m_dep = dep;
		// m_cardSlot = cardSlot;
	}

	@Override
	public byte[] transmit(byte[] bytes, int count) throws SmartCardError {
		try {
			if (! m_dep.isConnected()) {
				m_dep.connect();
				m_dep.setTimeout(5000);
			}
			byte[] result = m_dep.transceive(Arrays.copyOf(bytes, count));
			int bytesReceived = result.length;
			return Arrays.copyOf(result, bytesReceived);
		} catch (IOException e) {
			throw new SmartCardError(e);
		}
	}

	@Override
	public boolean close() {
		try {
			m_dep.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
//            ACRA.getErrorReporter().handleSilentException(e);
			return false;
		}
	}

}
