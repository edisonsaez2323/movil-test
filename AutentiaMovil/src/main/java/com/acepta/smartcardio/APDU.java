package com.acepta.smartcardio;

import java.nio.ByteBuffer;
import java.util.Arrays;


public class APDU {

	public static final int APDU_HAS_LC = 0x01;
	public static final int APDU_HAS_LE = 0x02;
	public static final int APDU_REQ_SEC_CHANNEL = 0x010000; // requires a secure channel to execute
	public static final int APDU_RST_SEC_CHANNEL = 0x020000; // deactivates secure channel upon completion
	  
	byte cla;
	public byte ins;
	public byte p1;
	public byte p2;
	public short le;
	public byte[] data;
    public int flags;

    public APDU(int cla, int ins, int p1, int p2, int flags) {
		this.cla = (byte) cla;
		this.ins = (byte) ins;
		this.p1 = (byte) p1;
		this.p2 = (byte) p2;
		this.le = 0;
		this.flags = (byte) flags;
		this.data = null;
    }
    
    public APDU(int cla, int ins, int p1, int p2, int le, int flags) {
		this.cla = (byte) cla;
		this.ins = (byte) ins;
		this.p1 = (byte) p1;
		this.p2 = (byte) p2;
		this.le = (byte) le;
		this.flags = (byte) flags;
		this.data = null;
	}
    
    public APDU withData(byte[] data) {
    	APDU result = new APDU(cla, ins, p1, p2, le, flags);
    	result.data = data;
    	return result;
    }
    
    public APDU withData(byte[] data, boolean copyData) {
    	APDU result = new APDU(cla, ins, p1, p2, le, flags);
    	result.data = Arrays.copyOf(data, data.length);
    	return result;
    }
    
    public boolean getBytes(ByteBuffer buf) {
    	int startPos = buf.position();
    	buf.put(cla);
    	buf.put(ins);
    	buf.put(p1);
    	buf.put(p2);
    	if ((flags & APDU_HAS_LC) != 0) {
    		if (data == null)
    			return false;
    		if (data.length > 255)
    			return false;
    		buf.put((byte)data.length);
    		buf.put(data);
    	}
    	if ((flags & APDU_HAS_LE) != 0) {
    		buf.put((byte)le);
    	}
    	int constructedBytes = buf.position() - startPos; 
    	if (constructedBytes > 255) {
    		return false;
    	}
    	return true;
    }

	public boolean resetsSecureChannel() {
		return (this.flags & APDU_RST_SEC_CHANNEL) != 0;
	}

	public boolean requiresSecureChannel() {
		return (this.flags & APDU_REQ_SEC_CHANNEL) != 0;
	}

}
