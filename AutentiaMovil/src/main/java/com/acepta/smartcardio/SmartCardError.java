package com.acepta.smartcardio;

import java.io.Serializable;

public class SmartCardError extends Exception implements Serializable{

	public SmartCardError(int sw, String op) {
		super(String.format("%s: %04x: %s", op, sw, ISO7816.swInfo(sw)));
	}

	public SmartCardError(String fmt, Object...args) {
		super(String.format(fmt, args));
	}

	public SmartCardError(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = 1L;
}
