package com.acepta.smartcardio;

import android.util.Log;

import com.acepta.Utils;
import com.acepta.eid.MRTDTools;
import com.acepta.eid.crypto.DESede;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

import cl.autentia.nfc.Tools;

public class SmartCard {

    static final byte ISO_7816_SELECT_BY_NAME = 0x04;
    static final byte ISO_7816_SELECT_BY_EF = 0x02;

    protected static final APDU CMD_SELECT_AID = new APDU(0x00, 0xA4,
            ISO_7816_SELECT_BY_NAME, 0x0C, APDU.APDU_HAS_LC | APDU.APDU_HAS_LE);
    protected static final APDU CMD_SELECT_AID_MOC = new APDU(0x00, 0xA4,
            ISO_7816_SELECT_BY_NAME, 0x0C, APDU.APDU_HAS_LE);
    protected static final APDU CMD_SELECT_FILE = new APDU(0x00, 0xA4,
            ISO_7816_SELECT_BY_EF, 0x0C, APDU.APDU_HAS_LC);
    protected static final APDU CMD_READ_BINARY = new APDU(0x00, 0xB0, 0x00,
            0x00, APDU.APDU_HAS_LE);

    static final APDU CMD_GET_CARD_ID = new APDU(0xFF, 0xCA, 0x00, 0x00,
            APDU.APDU_HAS_LE);
    static final APDU CMD_GET_CHALLENGE = new APDU(0x00, 0x84, 0x00, 0x00,
            APDU.APDU_HAS_LE);
    static final APDU CMD_EXTERNAL_AUTHENTICATE = new APDU(0x00, 0x82, 0x00,
            0x00, APDU.APDU_HAS_LC | APDU.APDU_HAS_LE);
    static final APDU CMD_INTERNAL_AUTHENTICATE = new APDU(0x00, 0x88, 0x00,
            0x00, APDU.APDU_HAS_LC | APDU.APDU_HAS_LE);

    private static final String SCARD = "SCARD";

    private byte[] m_KSenc = null;
    private byte[] m_KSmac = null;
    private BigInteger m_SSC = null;
    private SmartCardConnection m_connection;

    private ByteBuffer m_apduBuffer = ByteBuffer.allocate(1024);
    private DESede m_des3enc;

    public SmartCard(SmartCardConnection connection) {
        m_connection = connection;
    }

//    public boolean isNfcB(){
//        ((NativeSmartCardConnection) m_connection).m_dep.getTag().toString().contains("NfcB");
//    }

    protected RespAPDU sendCommand(APDU cmd) throws SmartCardError {
        if (!hasSecureChannel()) {
            if (cmd.requiresSecureChannel())
                throw new SmartCardError("no secure channel");
            return _sendCommand(cmd);
        } else {
            RespAPDU result = _sendSecuredCommand(cmd);
            if (cmd.resetsSecureChannel())
                resetSecureChannel();
            return result;
        }
    }

    public RespAPDU _sendCommand(APDU cmd) throws SmartCardError {
        long started = System.currentTimeMillis();
        m_apduBuffer.clear();
        cmd.getBytes(m_apduBuffer);
        byte[] received = m_connection.transmit(m_apduBuffer.array(), m_apduBuffer.position());
        long ended = System.currentTimeMillis();
        if (received.length < 2)
            throw new SmartCardError("short response");
        return new RespAPDU(received, ended - started);
    }

    public RespAPDU _sendCommandMoc2(byte[] dedo) throws SmartCardError {
        long started = System.currentTimeMillis();
//        ByteBuffer byteBuffer = ByteBuffer.allocate(cmd.);
//        cmd.getBytes(m_apduBuffer);


        byte[] headers = {(byte)0x00,(byte)0x21,(byte)0x00,(byte)0x91, (byte) dedo.length};

        //byte[] dedomaca = {(byte)0x00,(byte)0x21,(byte)0x00,(byte)0x91,(byte)0x99,(byte)0x7F,(byte)0x2E,(byte)0x96,(byte)0x81,(byte)0x94,(byte)0x3E,(byte)0x0B,(byte)0x5F,(byte)0x62,(byte)0x0E,(byte)0x5D,(byte)0x3E,(byte)0x17,(byte)0x7E,(byte)0x3C,(byte)0x1B,(byte)0x60,(byte)0x6D,(byte)0x1B,(byte)0xBB,(byte)0x6E,(byte)0x23,(byte)0x9C,(byte)0x45,(byte)0x24,(byte)0x7E,(byte)0x50,(byte)0x37,(byte)0x7B,(byte)0x3F,(byte)0x38,(byte)0x61,(byte)0x5A,(byte)0x3C,(byte)0x9C,(byte)0x1D,(byte)0x3E,(byte)0x25,(byte)0x23,(byte)0x41,(byte)0x65,(byte)0x68,(byte)0x46,(byte)0x78,(byte)0x4F,(byte)0x4B,(byte)0x7B,(byte)0x3B,(byte)0x54,(byte)0xA5,(byte)0x46,(byte)0x59,(byte)0xA4,(byte)0x77,(byte)0x5C,(byte)0x57,(byte)0x52,(byte)0x67,(byte)0x9D,(byte)0x6E,(byte)0x67,(byte)0x74,(byte)0x39,(byte)0x6B,(byte)0xAD,(byte)0x25,(byte)0x6,(byte)0xDA,(byte)0xC2,(byte)0xC6,(byte)0xDA,(byte)0xD6,(byte)0x36,(byte)0xD7,(byte)0x21,(byte)0xD6,(byte)0xFA,(byte)0xA4,(byte)0x87,(byte)0x17,(byte)0x64,(byte)0x67,(byte)0x6B,(byte)0x95,(byte)0x37,(byte)0x69,(byte)0x95,(byte)0xD7,(byte)0xAA,(byte)0xD4,(byte)0xF7,(byte)0xB9,(byte)0x61,(byte)0xE8,(byte)0x0A,(byte)0x96,(byte)0x78,(byte)0x0A,(byte)0xE5,(byte)0x28,(byte)0x10,(byte)0xD2,(byte)0xE8,(byte)0x77,(byte)0x73,(byte)0xF8,(byte)0x7B,(byte)0xB2,(byte)0x58,(byte)0x86,(byte)0xB5,(byte)0xC8,(byte)0xA6,(byte)0xB4,(byte)0xA8,(byte)0xB5,(byte)0xB4,(byte)0x88,(byte)0xE1,(byte)0xB2,(byte)0xB8,(byte)0xF7,(byte)0x43,(byte)0x48,(byte)0xF5,(byte)0x92,(byte)0x59,(byte)0x16,(byte)0xA2,(byte)0xD9,(byte)0x24,(byte)0xF5,(byte)0x09,(byte)0x5A,(byte)0x15,(byte)0xC9,(byte)0xA6,(byte)0x42,(byte)0xB9,(byte)0xBA,(byte)0x36,(byte)0x39,(byte)0xC4,(byte)0xB1,(byte)0xA9,(byte)0xD4,(byte)0xA2,(byte)0x0A,(byte)0x24,(byte)0x14,(byte)0x5A,(byte)0x35,(byte)0xF0,0x00};

        byte[] received = m_connection.transmit(Utils.concatArrays(headers, dedo), Utils.concatArrays(headers, dedo).length);
        Log.e("resp moc", new Tools().bytesToString(received));
//        Log.e("APDU VERIF", new Tools().bytesToString(dedomaca));
        long ended = System.currentTimeMillis();
        byte[] sent = Arrays.copyOfRange(m_apduBuffer.array(), 0, m_apduBuffer.position());
        if (received.length < 2)
            throw new SmartCardError("short response");
        return new RespAPDU(received, ended - started);
    }

    public RespAPDU _sendCommandMoc(byte[] cmd) throws SmartCardError {
        long started = System.currentTimeMillis();
        m_apduBuffer.clear();
        byte[] received = m_connection.transmit(cmd, cmd.length);
        long ended = System.currentTimeMillis();
        byte[] sent = Arrays.copyOfRange(m_apduBuffer.array(), 0, m_apduBuffer.position());
        if (received.length < 2)
            throw new SmartCardError("short response");
        return new RespAPDU(received, ended - started);
    }

    private RespAPDU _sendSecuredCommand(APDU cmd) throws SmartCardError {
        long started = System.currentTimeMillis();
        ByteBuffer m_apduSecBuffer = ByteBuffer.allocate(1024);
        cmd.getBytes(m_apduSecBuffer);
        APDU securedCmd = encryptAPDU(cmd);
        RespAPDU securedResp = _sendCommand(securedCmd);
        if (securedResp.sw == 0x9000) {
            securedResp = decryptRAPDU(securedResp);
        }
        long ended = System.currentTimeMillis();
        byte[] sent = Arrays.copyOfRange(m_apduSecBuffer.array(), 0, m_apduSecBuffer.position());
        return securedResp;
    }

    public RespAPDU getCardSerial() throws SmartCardError {
        return sendCommand(CMD_GET_CARD_ID);
    }

    public void resetSecureChannel() {
        m_KSenc = null;
        m_KSmac = null;
        m_SSC = null;
    }

    protected void setSecureChannelParams(byte[] ks_enc, byte[] ks_mac,
                                          BigInteger ssc) {

        m_KSenc = ks_enc;
        m_des3enc = new DESede(m_KSenc);
        m_KSmac = ks_mac;
        m_SSC = ssc;
    }

    protected void checkSuccess(String op, RespAPDU resp) throws SmartCardError {
        if (resp.sw != 0x9000 && resp.sw != 0x6983) {
            Log.e("resp.sw != 0x9000","error: "+ resp.sw+", op: "+op);
            throw new SmartCardError(resp.sw, op);
        }
    }

    protected byte[] iso7816_externalAuthenticate(byte[] data, byte[] kmac)
            throws SmartCardError {
        APDU req = CMD_EXTERNAL_AUTHENTICATE.withData(data);
        req.le = (short) data.length;
        RespAPDU resp = sendCommand(req);
        checkSuccess("iso7816_externalAuthenticate", resp);
        if (!MRTDTools.macVerify(resp.data, kmac)) {
            Log.e("MRTDTools.macVerify","external auth response's mac failed");
            throw new SmartCardError("external auth response's mac failed");
        }
        return resp.data;
    }

    protected byte[] iso7816_internalAuthenticate(byte[] data)
            throws SmartCardError {
        APDU req = CMD_INTERNAL_AUTHENTICATE.withData(data);
        req.le = 0;
        RespAPDU resp = sendCommand(req);
        checkSuccess("iso7816_internalAuthenticate", resp);
        return resp.data;
    }

    protected byte[] iso7816_getChallenge(int size) throws SmartCardError {
        APDU req = CMD_GET_CHALLENGE.withData(null);
        req.le = (short) size;
        RespAPDU resp = sendCommand(req);
        checkSuccess("iso7816_getChallenge", resp);
        return resp.data;
    }

    protected void iso7816_selectFile(byte[] fileId) throws SmartCardError {

        APDU req = CMD_SELECT_FILE.withData(fileId);
        RespAPDU resp = sendCommand(req);
        checkSuccess("iso7816_selectFile", resp);
    }

    protected byte[] iso7816_readBinary(int offset, int amount)
            throws SmartCardError {

        APDU req = CMD_READ_BINARY.withData(null);
        req.p1 = (byte) (offset >> 8);
        req.p2 = (byte) (offset & 0xff);
        req.le = (short) amount;
        RespAPDU resp = sendCommand(req);
        checkSuccess("iso7816_readBinary", resp);
        return resp.data;
    }

    private int readASN1Length(ByteBuffer buf, int[] consumed) {
        int b1 = Utils.uint8(buf.get()), b2;
        if (b1 >= 0 && b1 <= 0x7f) {
            consumed[0] = 1;
            return b1;
        }
        if (b1 == 0x81) {
            consumed[0] = 2;
            return Utils.uint8(buf.get());
        }
        if (b1 == 0x82) {
            consumed[0] = 3;
            b1 = buf.get();
            b2 = buf.get();
            return Utils.uint16(b1, b2);
        }
        Log.e("readASN1Length","error: bad length");
        throw new RuntimeException("bad length");
    }

    static final int DO_TAG_LENGTH = 1;

    private DataObject readDONN(ByteBuffer data, byte tag) {
        DataObject result = new DataObject();
        int length_byte_count[] = new int[1];
        data.mark();
        if (data.get() != tag) {
            data.reset();
            return result;
        }
        result.payloadLen = readASN1Length(data, length_byte_count);
        result.payloadOffset = DO_TAG_LENGTH + length_byte_count[0];
        result.bytes = new byte[result.payloadOffset + result.payloadLen];
        data.reset();
        data.get(result.bytes);
        return result;
    }

    private RespAPDU decryptRAPDU(RespAPDU erapdu) throws SmartCardError {
        DataObject do87, do8E, do99;
        byte[] dataBytes, recv_mac, calc_mac;

        RespAPDU rapdu = new RespAPDU();

        if (((erapdu.data == null) || (erapdu.data.length == 0))
                && (erapdu.sw == 0x9000)) {
            rapdu.sw = erapdu.sw;
            rapdu.time = erapdu.time;
            return rapdu;
        }

        ByteBuffer eData = ByteBuffer.wrap(erapdu.data);
        do87 = readDONN(eData, (byte) 0x87);
        do99 = readDONN(eData, (byte) 0x99);
        do8E = readDONN(eData, (byte) 0x8E);
        incSSC();
        dataBytes = Utils.concatArrays(MRTDTools.uInt64Bytes(m_SSC),
                do87.bytes, do99.bytes);
        calc_mac = MRTDTools.desMac(m_KSmac, MRTDTools.padData(dataBytes));
        recv_mac = do8E.getPayload();
        if (!Arrays.equals(calc_mac, recv_mac)) {
            Log.e("MArrays.equals","encrypted response apdu's mac failed");
            throw new SmartCardError("encrypted response apdu's mac failed");
        }

        rapdu.sw = Utils.uint16(do99.bytes[2], do99.bytes[3]);

        if (!do87.empty()) {
            byte[] decData;
            try {
                decData = m_des3enc.decrypt(do87.getPayload(1));
            } catch (Exception e) {
                Log.e("decrypt(do87.getPayload",e.toString());
                throw new RuntimeException(e);
            }
            rapdu.data = MRTDTools.unPadData(decData);
        }
        return rapdu;
    }

    private void incSSC() {
        m_SSC = m_SSC.add(BigInteger.ONE);
    }

    private APDU encryptAPDU(APDU apdu) throws SmartCardError {

        if (!hasSecureChannel()) {
            Log.e("hasSecureChannel()","no secure channel");
            throw new SmartCardError("no secure channel");
        }
        byte[] cmdHeader;
        byte[] do87 = Const.EMPTY_BYTE_ARRAY;
        byte[] do97 = Const.EMPTY_BYTE_ARRAY;
        byte[] do8E = Const.EMPTY_BYTE_ARRAY;
        byte[] m, n, mac;
        APDU eapdu;

        cmdHeader = maskClassAndPad(apdu);

        if ((apdu.flags & APDU.APDU_HAS_LC) != 0)
            do87 = buildDO87(apdu);

        if ((apdu.flags & APDU.APDU_HAS_LE) != 0)
            do97 = buildDO97(apdu);

        m = Utils.concatArrays(cmdHeader, do87, do97);
        incSSC();
        n = Utils.concatArrays(MRTDTools.uInt64Bytes(m_SSC), m);

        mac = MRTDTools.desMac(m_KSmac, MRTDTools.padData(n));

        do8E = buildDO8E(mac);

        eapdu = new APDU(cmdHeader[0], cmdHeader[1], cmdHeader[2],
                cmdHeader[3], 0, APDU.APDU_HAS_LC | APDU.APDU_HAS_LE);
        eapdu.data = Utils.concatArrays(do87, do97, do8E);
        return eapdu;
    }

    protected boolean hasSecureChannel() {
        return m_KSenc != null;
    }

    private byte[] buildDO8E(byte[] mac) {
        ByteBuffer buf = ByteBuffer.allocate(mac.length + 2);
        buf.put((byte) 0x8E);
        buf.put((byte) mac.length);
        buf.put(mac);
        return buf.array();
    }

    private byte[] buildDO97(APDU apdu) {
        byte[] result = new byte[3];
        result[0] = (byte) 0x97;
        result[1] = 0x01;
        result[2] = (byte) apdu.le;
        return result;
    }

    private byte[] buildDO87(APDU apdu) {
        byte[] encrypted = padAndEncryptData(apdu);
        byte[] encodedLength = MRTDTools.toAsn1Length(encrypted.length + 1);
        ByteBuffer buf = ByteBuffer.allocate(1 + encodedLength.length + 1
                + encrypted.length);
        buf.put((byte) 0x87);
        buf.put(encodedLength);
        buf.put((byte) 0x01);
        buf.put(encrypted);
        return buf.array();
    }

    private byte[] padAndEncryptData(APDU apdu) {
        try {
            return m_des3enc.encrypt(MRTDTools.padData(apdu.data));
        } catch (Exception e) {
            Log.e("padAndEncryptData",e.toString());
            throw new RuntimeException(e);
        }
    }

    private byte[] maskClassAndPad(APDU apdu) {
        byte[] tmp = new byte[4];
        tmp[0] = 0x0C;
        tmp[1] = apdu.ins;
        tmp[2] = apdu.p1;
        tmp[3] = apdu.p2;
        return MRTDTools.padData(tmp);
    }

    public boolean close() {
        return m_connection.close();
    }
}
