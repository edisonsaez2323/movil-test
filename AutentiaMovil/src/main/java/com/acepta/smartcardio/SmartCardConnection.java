package com.acepta.smartcardio;

public abstract class SmartCardConnection {

	public abstract byte[] transmit(byte[] bytes, int count)
			throws SmartCardError;

	public abstract boolean close();
}
