package com.acepta.smartcardio.ccid;

/**
 * Created by marcin on 3/6/17.
 */


import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;

import com.acepta.Utils;
import com.acepta.smartcardio.SmartCardConnection;

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.acepta.Utils.uint8;

/**
 * more info at:
 *
 * http://www.usb.org/developers/docs/devclass_docs/
 *
 * https://github.com/egelke/eIDSuite/blob/master/app/src/main/java/net/egelke/android/eid/usb/CCID.java
 *
 */
public class CCIDReader implements Closeable, CCIDCallback {

    private static final String TAG = "CCIDReader";
    private SmartCardConnection connection = null;

    // CCIDCallback methods start
    @Override
    public synchronized void inserted(SmartCardConnection conn) {
        sequence = 0;
        connection = conn;
    }

    @Override
    public synchronized void removed() {
        connection = null;
    }
    // CCIDCallback methods end

    private enum SlotStatus {
        Active,
        Inactive,
        Missing
    }

    private final static byte CMD_ICC_POWER_ON = 0x62;
    private final static byte CMD_ICC_POWER_OFF = 0x63;
    private final static byte CMD_XFR_BLOCK = 0x6F;

    private static final byte RESP_NOTIFY_SLOT_CHANGE = (byte) 0x50;
    private static final byte RESP_HARDWARE_ERROR = (byte) 0x51;
    private static final byte RESP_DATA_BLOCK = (byte) 0x80;
    private static final byte RESP_SLOT_STATUS = (byte) 0x81;

    private static final byte CCID_STATUS_RESULT_MASK = (byte) 0xC0;
    private static final byte CCID_RESULT_SUCCESS = 0;
    private static final byte CCID_RESULT_ERROR = (1 << 6);
    private static final byte CCID_RESULT_TIMEOUT = (byte) (2 << 6);

    private static final byte CCID_SLOT_STATUS_MASK = 0x03;
    private static final byte CCID_STATUS_ICC_ACTIVE = 0x00;
    private static final byte CCID_STATUS_ICC_PRESENT = 0x01;
    private static final byte CCID_STATUS_ICC_NOT_PRESENT = 0x02;

    private static final byte CCID_SLOT_STATUS_CHANGED_MASK = 0x3;
    private static final byte CCID_SLOT_STATUS_CHANGE_REMOVED = 0x2;
    private static final byte CCID_SLOT_STATUS_CHANGE_PRESENT = 0x3;

    private final static int CCID_ERR_ABORT       = 0xff;
    private final static int CCID_ERR_AUTO_SEQ    = 0xf2;
    private final static int CCID_ERR_BAD_TCK     = 0xf7;
    private final static int CCID_ERR_BAD_TS      = 0xf8;
    private final static int CCID_ERR_BUSY        = 0xe0;
    private final static int CCID_ERR_CLASS       = 0xf5;
    private final static int CCID_ERR_DEACTIVATED = 0xf3;
    private final static int CCID_ERR_HARDWARE    = 0xfb;
    private final static int CCID_ERR_MUTE        = 0xfe;
    private final static int CCID_ERR_OVERRUN     = 0xfc;
    private final static int CCID_ERR_PARITY      = 0xfd;
    private final static int CCID_ERR_PIN_TIMEOUT = 0xf0;
    private final static int CCID_ERR_PROCEDURE   = 0xf4;
    private final static int CCID_ERR_PROTOCOL    = 0xf6;
    private final static int CCID_ERR_USR_MIN     = 0x81;
    private final static int CCID_ERR_USR_MAX     = 0xc0;

    //Input property
    private final UsbDevice usbDevice;
    private final UsbManager usbManager;

    //connection
    private UsbInterface usbInterface;
    private UsbDeviceConnection usbConnection;

    //USB streams
    private UsbEndpoint usbOut;
    private UsbEndpoint usbIn;
    private UsbEndpoint usbInterrupt;

    //State properties
    private int sequence;
    private ScheduledFuture stateRequester;
    private byte[] receiveBuffer = new byte[1024]; /* was 268: 10 header + 258 RAPDU: 256 payload, 2 status word
                                                    * but sometimes we might need more */
    private SlotStatus mSlotStatus = SlotStatus.Missing;

    //callback
    private CCIDCallback callback;

    public synchronized CCIDCallback getCallback() {
        return callback;
    }

    public synchronized boolean isOpen() {
        return usbConnection != null;
    }

    public synchronized boolean isCardPresent() {
        return connection != null;
    }

    public synchronized SmartCardConnection getConnection() {
        return connection;
    }

    public CCIDReader(UsbManager manager, final UsbDevice device, final CCIDCallback callback) {
        usbManager = manager;
        usbDevice = device;
        this.callback = callback;
    }

    public CCIDReader(UsbManager manager, final UsbDevice device) {
        usbManager = manager;
        usbDevice = device;
        callback = this;
    }

    public static boolean isReader(UsbDevice usbDevice) {
        for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
            UsbInterface usbIf = usbDevice.getInterface(i);
            if (usbIf.getInterfaceClass() == UsbConstants.USB_CLASS_CSCID) {
                return true;
            }
        }
        return false;
    }

    public synchronized void open() throws IOException {
        if (usbDevice == null)
            throw new IllegalArgumentException("Device can't be null");

        Log.i(TAG, "opening CCID reader:");

        Log.i(TAG, String.format("> vid=%04x, pid=%04x",
                usbDevice.getVendorId(), usbDevice.getProductId()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.i(TAG, String.format("> manufacturer='%s', product='%s'",
                    usbDevice.getManufacturerName(), usbDevice.getProductName()));
        }

        for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
            UsbInterface usbIf = usbDevice.getInterface(i);
            if (usbIf.getInterfaceClass() == UsbConstants.USB_CLASS_CSCID) {
                usbInterface = usbIf;
            }
        }
        if (usbInterface == null)
            throw new IllegalStateException("The device hasn't a smart card reader");


        usbConnection = usbManager.openDevice(usbDevice);
        if (usbConnection == null)
            throw new IOException("Error opening UsbDevice");

        usbConnection.claimInterface(usbInterface, true);
        sequence = 0;

        //Get the interfaces
        for (int i = 0; i < usbInterface.getEndpointCount(); i++) {
            UsbEndpoint usbEp = usbInterface.getEndpoint(i);
            if (isInterruptInput(usbEp)) {
                usbInterrupt = usbEp;
            } else if (isBulkOutput(usbEp)) {
                usbOut = usbEp;
            } else if (isBulkInput(usbEp)) {
                usbIn = usbEp;
            }
        }

        //Listen for state changes
        if (usbInterrupt != null) {
            ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
            stateRequester = ses.scheduleWithFixedDelay(new StateRequesterTask(), 100, 1000, TimeUnit.MILLISECONDS);
        }

        // power off inicial para resetear al lector
        powerOff();
    }

    private boolean isBulkInput(UsbEndpoint usbEp) {
        return usbEp.getDirection() == UsbConstants.USB_DIR_IN &&
                usbEp.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK;
    }

    private boolean isBulkOutput(UsbEndpoint usbEp) {
        return usbEp.getDirection() == UsbConstants.USB_DIR_OUT &&
                usbEp.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK;
    }

    private boolean isInterruptInput(UsbEndpoint usbEp) {
        return usbEp.getDirection() == UsbConstants.USB_DIR_IN &&
                usbEp.getType() == UsbConstants.USB_ENDPOINT_XFER_INT;
    }

    public synchronized void close() throws IOException {
        connection = null;
        if (stateRequester != null) {
            stateRequester.cancel(false);
            stateRequester = null;
        }
        if (usbInterface != null) {
            usbConnection.releaseInterface(usbInterface);
            usbConnection.close();
            usbConnection = null;
            usbInterface = null;
        }
        Log.i(TAG, "card reader closed");
    }

    private class StateRequesterTask implements Runnable {

        private void checkSlotStatus() {
            byte[] buffer = new byte[10];
            int count = usbConnection.bulkTransfer(usbInterrupt, buffer, buffer.length, 100);

            if (count == -1) {
                // there was no status change info available
                return;
            }

            if (count < 2) {
                Log.v(TAG, "CCID interrupt read returned an invalid length: " + count);
                return;
            }

            if (buffer[0] == RESP_HARDWARE_ERROR) {
                Log.v(TAG, String.format("CCID interrupt read returned an error : %s", Utils.asHex(Arrays.copyOf(buffer, count))));
            }

            if (buffer[0] != RESP_NOTIFY_SLOT_CHANGE) {
                Log.v(TAG, String.format("CCID interrupt read returned an invalid type: %x", uint8(buffer[0])));
                return;
            }

            Log.d(TAG, String.format("CCID interrupt read returned the following status: %x", uint8(buffer[1])));

            int slotStatusChange = buffer[1] & CCID_SLOT_STATUS_CHANGED_MASK;

            if (slotStatusChange == CCID_SLOT_STATUS_CHANGE_REMOVED) {
                try {
                    powerOff();
                } catch (IOException e) {
                    Log.w(TAG, "card power off failed");
                }
                getCallback().removed();
            } else if (slotStatusChange == CCID_SLOT_STATUS_CHANGE_PRESENT) {
                try {
                    processAtr(powerOn());
                    getCallback().inserted(new CCIDConnection(CCIDReader.this));
                } catch (IOException e) {
                    Log.e(TAG, "error while card power up");
                }
            }
        }

        @Override
        public void run() {
            checkSlotStatus();
        }
    }

    private void processAtr(byte[] bytes) {
        Log.v(TAG, "new card arrived: " + Utils.asHex(bytes));
    }

    public byte[] powerOn() throws IOException {
        byte[] rsp = transmit(CMD_ICC_POWER_ON, null, RESP_DATA_BLOCK, false);
        return Arrays.copyOfRange(rsp, 10, rsp.length);
    }

    public void powerOff() throws IOException {
        transmit(CMD_ICC_POWER_OFF, null, RESP_SLOT_STATUS, false);
    }

    public byte[] transmitApdu(byte[] apdu) throws IOException {
        byte[] rsp = transmit(CMD_XFR_BLOCK, apdu, RESP_DATA_BLOCK, true);
        return Arrays.copyOfRange(rsp, 10, rsp.length);
    }

    private synchronized byte[] transmit(byte cmd, byte[] data, byte expectedCmdResp, boolean waitIcc) throws IOException {
        int dataLength = data != null ? data.length : 0;

        if (dataLength > 255) {
            throw new IOException("Data to big (max. 255 bytes)");
        }

        sequence = (sequence + 1) % 0xff;
        byte[] req = new byte[10 + dataLength];
        req[0] = cmd;
        req[1] = (byte) dataLength; // (data) length
        req[2] = 0x00; // length, continued (we don't support long lenghts)
        req[3] = 0x00; // length, continued (we don't support long lenghts)
        req[4] = 0x00; // length, continued (we don't support long lenghts)
        req[5] = (byte) 0; // slot
        req[6] = (byte) sequence;
        req[7] = (byte) (cmd == CMD_XFR_BLOCK ? 0x01 : 0x00); // Not used (Xfr: Block Waiting Timeout)
        req[8] = 0x00; // Not used (Xfr: Param (short APDU))
        req[9] = 0x00; // Not used (Xfr: Param, continued (short APDU))

        if (data != null)
            System.arraycopy(data, 0, req, 10, data.length);

        int count;

        count = usbConnection.bulkTransfer(usbOut, req, req.length, 5000);
        if (count < 0) {
            throw new IOException("Failed to send data to the CCID reader");
        }

        do {
            count = usbConnection.bulkTransfer(usbIn, receiveBuffer, receiveBuffer.length, 10000);
            if (count < 0) {
                throw new IOException("Failed to read data from the CCID reader");
            }
        } while (!checkResponse(receiveBuffer, expectedCmdResp) && waitIcc);

        return Arrays.copyOf(receiveBuffer, count);
    }

    private boolean checkResponse(byte[] rsp, byte expectedResp) throws IOException {
        if (rsp.length < 10) {
            throw new IOException("The response is to short");
        }

        if (rsp[0] != expectedResp) {
            throw new IOException(String.format(
                    "Illegal CCID reader response (wrong type: 0x%02x, expected: 0x%02x)",
                    uint8(rsp[0]), uint8(expectedResp)));
        }

        if (rsp[6] != (byte) sequence) {
            throw new IOException(String.format(
                    "Illegal CCID reader response (wrong sequence: %d vs %d)",
                    uint8(rsp[6]), uint8(sequence)));
        }

        int resultCode = rsp[7] & CCID_STATUS_RESULT_MASK;
        switch (resultCode) {
            case CCID_RESULT_SUCCESS:
                return true;
            case CCID_RESULT_TIMEOUT:
                Log.v(TAG, "timeout extension requested");
                return false;
            case CCID_RESULT_ERROR:
                String errInfo;
                int errCode = uint8(rsp[8]);
                switch (errCode) {
                    case CCID_ERR_ABORT      : errInfo = "ICC Aborted"; break;
                    case CCID_ERR_AUTO_SEQ   : errInfo = "Busy with Auto-Sequencing"; break;
                    case CCID_ERR_BAD_TCK    : errInfo = "Bad ATR TCK"; break;
                    case CCID_ERR_BAD_TS     : errInfo = "Bad ATR TS"; break;
                    case CCID_ERR_BUSY       : errInfo = "Slot Busy"; break;
                    case CCID_ERR_CLASS      : errInfo = "Unsupported CLA"; break;
                    case CCID_ERR_DEACTIVATED: errInfo = "Deactivated Protocol"; break;
                    case CCID_ERR_HARDWARE   : errInfo = "Hardware Error"; break;
                    case CCID_ERR_MUTE       : errInfo = "ICC Timed Out"; break;
                    case CCID_ERR_OVERRUN    : errInfo = "ICC Buffer Overrun"; break;
                    case CCID_ERR_PARITY     : errInfo = "ICC Parity Error"; break;
                    case CCID_ERR_PIN_TIMEOUT: errInfo = "PIN timeout"; break;
                    case CCID_ERR_PROCEDURE  : errInfo = "Procedure Byte Conflict"; break;
                    case CCID_ERR_PROTOCOL   : errInfo = "Unsupported Protocol"; break;
                    default:
                        if (CCID_ERR_USR_MIN <= errCode && errCode <= CCID_ERR_USR_MAX)
                            throw new IOException(String.format("Command: FAILED (0x%02x)", errCode));

                        throw new IOException(String.format("Command: FAILED (RESERVED 0x%02x)", errCode));
                }
                throw new IOException("Command: ERR: " + errInfo);
            default:
                throw new IOException("Error: Unknown result");
        }
    }


    private SlotStatus parseSlotStatus(byte[] rsp) throws IOException {
        int slotStatus = rsp[7] & CCID_SLOT_STATUS_MASK;

        if (slotStatus == CCID_STATUS_ICC_NOT_PRESENT)
            return SlotStatus.Missing;
        else if (slotStatus == CCID_STATUS_ICC_PRESENT)
            return SlotStatus.Inactive;
        else if (slotStatus == CCID_STATUS_ICC_ACTIVE)
            return SlotStatus.Active;
        throw new IOException(String.format("Invalid slot status received from the CCID reader: 0x%02x", slotStatus));
    }
}