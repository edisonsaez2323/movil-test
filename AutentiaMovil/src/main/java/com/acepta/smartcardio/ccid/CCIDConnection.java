package com.acepta.smartcardio.ccid;

import android.util.Log;

import com.acepta.smartcardio.SmartCardConnection;
import com.acepta.smartcardio.SmartCardError;


import java.io.IOException;
import java.util.Arrays;

import cl.autentia.http.HttpUtil;

/**
 * Created by marcin on 3/6/17.
 */

public class CCIDConnection extends SmartCardConnection {

    private static final String TAG = "CCID_CONN";
    private CCIDReader reader;

    public CCIDConnection(CCIDReader reader) {
        this.reader = reader;
    }

    @Override
    public byte[] transmit(byte[] bytes, int count) throws SmartCardError {
        if (reader == null)
            throw new SmartCardError("invalid connection");
        try {
            return reader.transmitApdu(Arrays.copyOf(bytes, count));
        } catch (IOException e) {
            Log.e(TAG, "transmit: " + e.getMessage());
            try {
                reader.powerOff();
            } catch (IOException e1) {
                Log.e(TAG, "transmit powerOff: " + e1.getMessage());
            }
            reader = null;
            throw new SmartCardError(e);
        }
    }

    @Override
    public boolean close() {
        try {
            if (reader != null)
                reader.powerOff();
            return true;
        } catch (IOException e) {
            Log.e(TAG, "close: " + e.getMessage());
//            ACRA.getErrorReporter().handleSilentException(e);
            return false;
        } finally {
            reader = null;
        }
    }
}
