package com.acepta.smartcardio.ccid;

import com.acepta.smartcardio.SmartCardConnection;

/**
 * Created by marcin on 3/6/17.
 */

public interface CCIDCallback {
    void inserted(SmartCardConnection conn);
    void removed();
}
