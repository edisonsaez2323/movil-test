package com.acepta.smartcardio;

import com.acepta.Utils;

public class DataObject {
	public byte tag = 0x00;
	public int payloadOffset = 0;
	public int payloadLen = 0;
	public byte[] bytes = Const.EMPTY_BYTE_ARRAY;

	public byte[] getPayload() {
		return getPayload(0);
	}
	
	public byte[] getPayload(int skip) {
		return Utils.copySlice(bytes, payloadOffset + skip, payloadLen - skip); 
	}
	
	public boolean empty() {
		return bytes.length == 0;
	}
}
