package com.acepta;

import java.util.HashMap;
import java.util.Map;


public class FileHolder {
	public byte[] data;
	public String filename;
	public String mimetype;
	public String summary;
	public Map<String,String> prop;
	
	public FileHolder(byte[] data, String name, String mimetype) {
		this(data, name,null, mimetype);
	}
	public FileHolder(byte[] data, String name, String summary, String mimetype) {
		this.data = data;
		this.filename = name;
		this.mimetype = mimetype;
		this.summary = summary;
		this.prop = new HashMap<String, String>();
	}
	public String getKey(){
		return filename.substring(0, filename.indexOf("."));
	}
}
