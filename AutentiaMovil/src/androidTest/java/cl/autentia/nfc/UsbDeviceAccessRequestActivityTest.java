package cl.autentia.nfc;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import cl.autentia.test.TestUtils;
import cl.autentia.usb.UsbDeviceAccessRequestActivity;

import static org.junit.Assert.*;

/**
 * Created by marcin on 3/15/17.
 */
public class UsbDeviceAccessRequestActivityTest {
    static final String TAG = "USB_DEV_ACC_REQ_TEST";

    Context targetContext;
    Instrumentation instrumentation;

    @Before
    public void setUp() throws Exception {
        Log.v(TAG, "preparing instrumentation");
        targetContext = InstrumentationRegistry.getTargetContext();
        instrumentation = InstrumentationRegistry.getInstrumentation();
    }

    @Test
    public void testActivityCanceled() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, UsbDeviceAccessRequestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, (Parcelable[]) null);
        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        };
        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + TestUtils.resultCodeInfo(resultCode[0]));
        assertNotNull(result);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
    }


    @Test
    public void testActivityOK() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, UsbDeviceAccessRequestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        UsbManager manager = (UsbManager) targetContext.getSystemService(Context.USB_SERVICE);
        for(UsbDevice aDevice : manager.getDeviceList().values()) {
            intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, aDevice);
            Log.v(TAG, "device for request: " + aDevice);
            break;
        }
        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        };
        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + TestUtils.resultCodeInfo(resultCode[0]));
        assertNotNull(result);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
    }
}
