package cl.autentia.activity;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Test;

import cl.autentia.test.TestUtils;

/**
 * Created by marcin on 3/13/17.
 */
public class BarcodeScannerActivityTest {

    static final String TAG = "BARCODE_TEST";

    @Test
    public void testActivity() {
        Log.v(TAG, "preparing instrumentation");
        Context targetContext = InstrumentationRegistry.getTargetContext();
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();

        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, BarcodeScannerActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BarcodeScannerActivity.Extras.In.PARAMETER_TYPE, "qr_type");
        intent.putExtra(BarcodeScannerActivity.Extras.In.REVERSE, false);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);

        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        };

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());

//        testActivityStatus(targetContext, instrumentation, result.getExtras().getString("barcode"));
    }

//    public void testActivityStatus(Context targetContext, Instrumentation instrumentation, String barcode) {
//
//        Intent intent = new Intent();
//        intent.setClass(targetContext, IdentityCardStatus_Activity_.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra(IdentityCardStatusActivity.Extras.In.BARCODE, barcode);
//
//        Log.v(TAG, "starting activity");
//        Activity activity = instrumentation.startActivitySync(intent);
//
//        Log.v(TAG, "waiting for activity termination");
//        while (! activity.isDestroyed()) {
//            SystemClock.sleep(200);
//        };
//
//        Log.v(TAG, "activity terminated");
//        int[] resultCode = new int[1];
//        Intent result = TestUtils.getActivityResult(activity, resultCode);
//        Log.v(TAG, "result/code: " + resultCode[0]);
//        Log.v(TAG, "result/data: " + result.getData());
//        Log.v(TAG, "result/extras: " + result.getExtras());
//
//
//
//    }

}
