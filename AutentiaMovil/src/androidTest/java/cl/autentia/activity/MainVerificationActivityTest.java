package cl.autentia.activity;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import cl.autentia.test.TestUtils;

import static org.junit.Assert.*;

/**
 * Created by marcin on 3/14/17.
 */
public class MainVerificationActivityTest {

    private static final String TAG = "VERIFICA_TEST";

    private Context targetContext;
    private Instrumentation instrumentation;

    int rut = 17573830;
    char dv = '4';

    @Before
    public void setUp() throws Exception {
        Log.v(TAG, "preparing instrumentation");
        targetContext = InstrumentationRegistry.getTargetContext();
        instrumentation = InstrumentationRegistry.getInstrumentation();
    }

//    @Test
//    public void testVerificacionContraServidorOnline() {
//        Log.v(TAG, "preparing intent");
//        Intent intent = new Intent();
//        intent.setClass(targetContext, VerificationActivity_.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra(VerificationActivity_.Extras.In.RUT, 17152172);
//        intent.putExtra(VerificationActivity_.Extras.In.DV, '6');
//
//        Log.v(TAG, "starting activity");
//        Activity activity = instrumentation.startActivitySync(intent);
//        Log.v(TAG, "waiting for activity termination");
//        while (! activity.isDestroyed()) {
//            SystemClock.sleep(200);
//        }
//
//        Log.v(TAG, "activity terminated");
//        int[] resultCode = new int[1];
//        Intent result = TestUtils.getActivityResult(activity, resultCode);
//        Log.v(TAG, "result/code: " + resultCode[0]);
//        Log.v(TAG, "result/data: " + result.getData());
//        Log.v(TAG, "result/extras: " + result.getExtras());
//        TestUtils.dumpExtras(TAG, result);
//    }

    /**
     * realiza la verificacion de identidad contra la base de datos de Autentia en linea
     */
    @Test
    public void testVerificacionContraServidorOnline() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
    }

    @Test
    public void testVerificacionContraServidorOnlineOTI() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);
        intent.putExtra(VerificationActivity_.Extras.In.OTI, true);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
    }

    @Test
    public void testVerificacionContraServidorOnlineTRASPASO() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);
        intent.putExtra(VerificationActivity_.Extras.In.TRASPASO, true);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
    }

    /**
     * verifica la identidad utilizando la cedula nueva, registrando la auditoria en linea
     */
    @Test
    public void testVerificacionContraCedulaNuevaOnline() {
        Log.v(TAG, "preparing instrumentation");
        Context targetContext = InstrumentationRegistry.getTargetContext();
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);
        intent.putExtra(VerificationActivity_.Extras.In.BARCODE,
                "https://portal.sidiv.registrocivil.cl/docstatus?" +
                        "RUN=21149007-1&" +
                        "type=CEDULA_EXT&" +
                        "serial=200143317&" +
                        "mrz=200143317772041841905227");

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
    }

    private String capturarCodigoDeBarra() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, BarcodeScannerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BarcodeScannerActivity.Extras.In.REVERSE, false);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
        Bundle extras = result.getExtras();
        assertNotNull(extras);
        return extras.getString("mBarcode");

    }

    @Test
    private void testCapturaHuellaComoEvidencia() {
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, SelectorEvidenceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SelectorEvidenceActivity.Extras.In.RUT, rut);
        intent.putExtra(SelectorEvidenceActivity.Extras.In.DV, dv);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
        Bundle extras = result.getExtras();
        assertNotNull(extras);
        TestUtils.dumpExtras(TAG, result);

    }

    /**
     * verifica la identidad capturando el codigo PDF417 mediante WS de NEC
     * en linea y registrando la auditoria, tambien en linea
     */
    @Test
    public void testVerificacionContraCedulaAntiguaOnline() {
        Log.v(TAG, "capturando el codigo de barra");
        String mBarcode = capturarCodigoDeBarra();

        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);
        intent.putExtra(VerificationActivity_.Extras.In.BARCODE, mBarcode);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras:");
        TestUtils.dumpExtras(TAG, result);
    }

    @Test
    public void testVerificacionContraCedulaAntiguaOnlineVIGENCIA() {
        Log.v(TAG, "capturando el codigo de barra");
        String mBarcode = capturarCodigoDeBarra();

        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, VerificationActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(VerificationActivity_.Extras.In.RUT, rut);
        intent.putExtra(VerificationActivity_.Extras.In.DV, dv);
        intent.putExtra(VerificationActivity_.Extras.In.VIGENCIA, true);
        intent.putExtra(VerificationActivity_.Extras.In.BARCODE, mBarcode);

        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }

        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras:");
        TestUtils.dumpExtras(TAG, result);
    }
}