package cl.autentia.activity;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.util.Log;
import cl.autentia.test.TestUtils;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

/**
 * Created by marcin on 3/10/17.
 */
public class InfoNFCActivityTest {

    static final String TAG = "NFC_INFO_TEST";


    public void testInfoNfcActivity(String qrCode) {
        Log.v(TAG, "preparing instrumentation");
        Context targetContext = InstrumentationRegistry.getTargetContext();
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Log.v(TAG, "preparing intent");
        Intent intent = new Intent();
        intent.setClass(targetContext, InfoNFCActivity_.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(InfoNFCActivity.Extras.In.CODIGO_BARRAS, qrCode);
        intent.putExtra(InfoNFCActivity.Extras.In.READ_EXTENDED_INFO, true);
        intent.putExtra(InfoNFCActivity.Extras.In.TIMEOUT, 20);
        Log.v(TAG, "starting activity");
        Activity activity = instrumentation.startActivitySync(intent);
        Log.v(TAG, "waiting for activity termination");
        while (! activity.isDestroyed()) {
            SystemClock.sleep(200);
        }
        Log.v(TAG, "activity terminated");
        int[] resultCode = new int[1];
        Intent result = TestUtils.getActivityResult(activity, resultCode);
        Log.v(TAG, "result/code: " + resultCode[0]);
        Log.v(TAG, "result/data: " + result.getData());
        Log.v(TAG, "result/extras: " + result.getExtras());
        TestUtils.dumpExtras(TAG, result);
    }

    @Test
    public void testInfoNfcActivityMarcin() {
        testInfoNfcActivity(
                "https://portal.sidiv.registrocivil.cl/docstatus?" +
                "RUN=21149007-1&" +
                "type=CEDULA_EXT&" +
                "serial=200143317&" +
                "mrz=200143317772041841905227");
    }

    @Test
    public void testInfoNfcActivityItalo() {
        testInfoNfcActivity(
                "https://portal.sidiv.registrocivil.cl/docstatus?" +
                "RUN=13021340-5&" +
                "type=CEDULA&" +
                "serial=110058055&" +
                "mrz=110058055376111312611136");
    }

    @Test
    public void testInfoNfcActivityJorge() {
        testInfoNfcActivity(
                "https://portal.sidiv.registrocivil.cl/docstatus?" +
                "RUN=9039939-K&" +
                "type=CEDULA&" +
                "serial=103152962&" +
                "mrz=103152962763110432311045");
    }
}
