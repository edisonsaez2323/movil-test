package cl.autentia.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by marcin on 3/13/17.
 */

public class TestUtils {

    public static Intent getActivityResult(Activity activity, int[] resultCode) {
        try {
            Field f = Activity.class.getDeclaredField("mResultCode");
            f.setAccessible(true);
            resultCode[0] = (Integer)f.get(activity);
            f = Activity.class.getDeclaredField("mResultData");
            f.setAccessible(true);
            return (Intent)f.get(activity);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Looks like the Android Activity class has changed it's private fields for mResultCode or mResultData. Time to update the reflection code.", e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void dumpExtras(String tag, Intent result) {
        Bundle extras = result.getExtras();
        if (extras != null) {
            for (String key : extras.keySet()) {
                Log.v(tag, String.format("'%s' = '%s'", key, extras.get(key)));
            }
        }
        else {
            Log.v(tag, "extras is null");
        }
    }

    public static String resultCodeInfo(int code) {
        switch (code) {
            case Activity.RESULT_CANCELED: return "CANCELED";
            case Activity.RESULT_OK: return "OK";
            default: return Integer.toString(code);
        }
    }
}
