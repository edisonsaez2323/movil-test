package com.pacific.barcode;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.pacific.mvc.ActivityView;

import java.nio.charset.StandardCharsets;

/**
 * Created by root on 16-5-8.
 */
public class QRView extends ActivityView<QRActivity> implements SurfaceHolder.Callback {

    private QRCodeView qrCodeView;
    private SurfaceView surfaceView;

    public QRView(QRActivity activity) {
        super(activity);
    }

    @Override
    protected void findView() {
        surfaceView = retrieveView(R.id.sv_preview);
        qrCodeView = retrieveView(R.id.qr_view);
    }

    @Override
    protected void setListener() {
        qrCodeView.setPickImageListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setHook(true);
                Intent galleryIntent = new Intent();
                if (Build.VERSION_CODES.KITKAT >= Build.VERSION.SDK_INT) {
                    galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                } else {
                    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                }
                galleryIntent.setType("image/*");
                Intent wrapperIntent = Intent.createChooser(galleryIntent, "选择二维码图片");
                activity.startIntentForResult(wrapperIntent, QRActivity.CODE_PICK_IMAGE, null);
            }
        });
        surfaceView.getHolder().addCallback(this);
    }

    @Override
    protected void setAdapter() {

    }

    @Override
    protected void initialize() {

    }

    @Override
    public void onClick(View v) {

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void resultDialog(QRResult qrResult) {
        if (qrResult == null) {
            new AlertDialog.Builder(activity)
                    .setTitle("No Barcode Result")
                    .setMessage("Can't decode barcode from target picture , \nplease confirm the picture has barcode value.")
                    .setPositiveButton("Ok", null)
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            activity.setHook(false);
                            activity.restartCapture();
                        }
                    })
                    .create()
                    .show();
            return;
        }
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_result, null);
        if (!TextUtils.isEmpty(String.valueOf(qrResult.getResult()))) {
            ((TextView) view.findViewById(R.id.tv_value)).setText(String.valueOf(qrResult.getResult()));
        }
        if (qrResult.getBitmap() != null) {

            Bitmap myBitmap = qrResult.getBitmap();
//            Bitmap bitmap2 = myBitmap.copy(Bitmap.Config.ARGB_8888, true);
//            Canvas tempCanvas = new Canvas(bitmap2);
//            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//            paint.setColor(Color.RED);
            Result result = qrResult.getResult();

            try {

                if (result.getBarcodeFormat().name().equalsIgnoreCase("pdf_417")) {

//                tempCanvas.drawCircle(result.getResultPoints()[0].getX(), result.getResultPoints()[0].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[1].getX(), result.getResultPoints()[1].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[2].getX(), result.getResultPoints()[2].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[3].getX(), result.getResultPoints()[3].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[4].getX(), result.getResultPoints()[4].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[5].getX(), result.getResultPoints()[5].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[6].getX(), result.getResultPoints()[6].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[7].getX(), result.getResultPoints()[7].getY(), 10, paint);

                    int x1 = Math.round(result.getResultPoints()[2].getX());
                    int y1 = Math.round(result.getResultPoints()[2].getY());
                    int x2 = Math.round(result.getResultPoints()[3].getX());
                    int y2 = Math.round(result.getResultPoints()[3].getY());

                    float pdf_up = (float) Math.pow(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2), 0.5);

                    byte[] dataBarcode = qrResult.getResult().getText().getBytes(StandardCharsets.ISO_8859_1);
                    //            writeToFile(byteArrayToHex(bb), "data.txt");
                    //            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    //            paint.setColor(Color.YELLOW);
                    //            tempCanvas.drawCircle(result.getResultPoints()[2].getX() + pdf_up, result.getResultPoints()[2].getY(), 10, paint);
                    //            tempCanvas.drawCircle(result.getResultPoints()[3].getX() + pdf_up, result.getResultPoints()[3].getY(), 10, paint);

                    if (pdf_up > 200) {

                        //Imagen completa
                        //saveImage(qrResult.getBitmap(), "completa");

                        Bitmap imagenHuella = Bitmap.createBitmap(myBitmap, x1, y1, (int) pdf_up, (int) pdf_up);
                        Bitmap fingerGrayScaleARGB = QRUtils.toGrayscale(imagenHuella, Bitmap.Config.ARGB_8888);
                        Bitmap fingerStreched = QRUtils.stretchHistogram(fingerGrayScaleARGB, 0.05f);
//                    saveImage(fingerStreched, "ARGB_STRECHED");

                        Intent intentReturn = new Intent();
                        intentReturn.putExtra("type", "pdf_417");
                        intentReturn.putExtra("barcode", dataBarcode);
                        intentReturn.putExtra("fingerPrint", QRUtils.bitmapToByteArray(fingerStreched));
                        activity.setResult(Activity.RESULT_OK, intentReturn);
                        activity.finish();

                        //Toast.makeText(getController().getApplicationContext(), "PDF 417", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(activity, "Captura de baja resolución, reintente", Toast.LENGTH_LONG).show();
                    }
                } else {

//                tempCanvas.drawCircle(result.getResultPoints()[0].getX(), result.getResultPoints()[0].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[1].getX(), result.getResultPoints()[1].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[2].getX(), result.getResultPoints()[2].getY(), 10, paint);
//                tempCanvas.drawCircle(result.getResultPoints()[3].getX(), result.getResultPoints()[3].getY(), 10, paint);

                    String dataBarcode = qrResult.getResult().getText();

                    Intent intentReturn = new Intent();
                    intentReturn.putExtra("type", "qr");
                    intentReturn.putExtra("barcode", dataBarcode);
                    // intentReturn.putExtra("fingerPrint", fingerStreched);
                    activity.setResult(Activity.RESULT_OK, intentReturn);
                    activity.finish();

                    //Toast.makeText(getController().getApplicationContext(), "QR", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Intent intentReturn = new Intent();
                intentReturn.putExtra("type", "qr");
                intentReturn.putExtra("error", e.getMessage());
                // intentReturn.putExtra("fingerPrint", fingerStreched);
                activity.setResult(Activity.RESULT_CANCELED, intentReturn);
                activity.finish();
            }
        }
    }

    public void setEmptyViewVisible(boolean visible) {
        if (visible) {
            retrieveView(R.id.v_empty).setVisibility(View.VISIBLE);
        } else {
            retrieveView(R.id.v_empty).setVisibility(View.GONE);
        }
    }

    public void setSurfaceViewVisible(boolean visible) {
        if (visible) {
            surfaceView.setVisibility(View.VISIBLE);
        } else {
            surfaceView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        activity.onSurfaceCreated(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        setEmptyViewVisible(true);
        activity.onSurfaceDestroyed();
    }
}