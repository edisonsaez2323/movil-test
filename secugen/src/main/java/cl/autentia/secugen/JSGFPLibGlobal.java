package cl.autentia.secugen;

import android.hardware.usb.UsbManager;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGDeviceInfoParam;

public class JSGFPLibGlobal {

    private JSGFPLib mReader;

    public JSGFPLib getReader(UsbManager usbManager) {

        if (mReader == null)
            mReader = new JSGFPLib(usbManager);
        return mReader;
    }

    private  SecuGen.FDxSDKPro.SGDeviceInfoParam mDeviceInfo;

    public SGDeviceInfoParam getDeviceInfo() {

        if (mDeviceInfo == null)
            mDeviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
        return mDeviceInfo;
    }
}
