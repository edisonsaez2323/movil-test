package cl.autentia.barcode;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * This class is for android targets below android 5.0 and it uses old camera api
 */
public class CameraManager extends BaseCameraManager implements Camera.AutoFocusCallback, Camera.PreviewCallback {

    private static final String TAG = "camera";
    private Camera camera;
    private FocusMode focusMode = FocusMode.AUTO;

    public enum FocusMode {
        AUTO,
        CONTINUOUS,
        INFINITY,
        MACRO
    }

    public CameraManager(Context context) {
        super(context);
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        Log.i(TAG, "onAutoFocus Ready!");
        if (hook || isRelease) return;
        camera.setOneShotPreviewCallback(this);
    }

    @Override
    public void connectCamera(SurfaceHolder surfaceHolder) {
        if (!isRelease) return;
        try {
            camera = Camera.open();
            isRelease = false;
            camera.setPreviewDisplay(surfaceHolder);
            setCameraParameter();
            camera.startPreview();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void releaseCamera() {
        if (isRelease) return;
        isRelease = true;

        try {
            camera.cancelAutoFocus();
            camera.stopPreview();
            camera.setPreviewDisplay(null);
        } catch (IOException e) {
            e.printStackTrace();
//            throw new RuntimeException(e);
        }
        try {
            camera.release();
        } catch (Exception e) {
            e.printStackTrace();
        }

        camera = null;
    }

    @Override
    public void startCapture() {
        if (hook || isRelease || executor.isShutdown()) return;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    camera.autoFocus(CameraManager.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void setCameraParameter() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(0, cameraInfo);

        int degrees = 0;
        switch (rotate) {
            case Surface.ROTATION_0: {
                degrees = 0;
                break;
            }
            case Surface.ROTATION_90: {
                degrees = 90;
                break;
            }
            case Surface.ROTATION_180: {
                degrees = 180;
                break;
            }
            case Surface.ROTATION_270: {
                degrees = 270;
                break;
            }
        }

        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            displayOrientation = (cameraInfo.orientation + degrees) % 360;
            displayOrientation = (360 - displayOrientation) % 360;
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        }

        /** Warning : may throw exception with parameters not supported */
        Camera.Parameters parameters = camera.getParameters();
        List<String> autoFocusModes = parameters.getSupportedFocusModes();
        for (String autoFocusMode: autoFocusModes) {
            Log.e("autofocusmodes",autoFocusMode);
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

//        Camera.Size bestSize = getOptimalPreviewSize(previewSizes,displayMetrics.widthPixels,displayMetrics.heightPixels);//,displayMetrics.widthPixels,displayMetrics.heightPixels);
//        Camera.Size bestSize = getOptimalSize(parameters,displayMetrics.widthPixels,displayMetrics.heightPixels);//,displayMetrics.widthPixels,displayMetrics.heightPixels);
//        Camera.Size bestSize = previewSizes.get(0);
//        for (int i = 1; i < previewSizes.size(); i++) {
//            if (previewSizes.get(i).width * previewSizes.get(i).height > bestSize.width * bestSize.height) {
//                bestSize = previewSizes.get(i);
//            }
//        }
        try {
            Camera.Size bestSize = getOptimalSize(parameters, displayMetrics.widthPixels, displayMetrics.heightPixels);
//            Camera.Size bestSize = getOptimalPreviewSize(previewSizes,displayMetrics.widthPixels,displayMetrics.heightPixels);//,displayMetrics.widthPixels,displayMetrics.heightPixels);
            parameters.setPreviewSize(bestSize.width, bestSize.height);
//            parameters.setPreviewSize(1280,720);
//            parameters.setPictureSize(bestSize.width, bestSize.height);
            parameters.setPictureSize(1280, 720);
            camera.setParameters(parameters);
            camera.setDisplayOrientation(displayOrientation);
        } catch (Exception e) {

            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();

            Camera.Size optimals = getOptimalPreviewSize(previewSizes,displayMetrics.widthPixels,displayMetrics.heightPixels);

            if (optimals.width < 1280){
                parameters.setPreviewSize(1280, 720);
            }else {
                parameters.setPreviewSize(optimals.width, optimals.height);
            }
//            parameters.setPictureSize(bestSize.width, bestSize.height);
            parameters.setPictureSize(1280, 720);
            camera.setParameters(parameters);
            camera.setDisplayOrientation(displayOrientation);
//            Log.e("ERR", e.getMessage());
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            Log.e("size", "width " + size.width + " height " + size.height);
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                targetHeight = Math.min(w, h);
            }
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        Log.e(TAG, "width = " + optimalSize.width + " height = " + optimalSize.height);
        return optimalSize;
    }

    private Camera.Size getOptimalSize(Camera.Parameters params, int w, int h) {

        final double ASPECT_TH = .2; // Threshold between camera supported ratio and screen ratio.
        double minDiff = Double.MAX_VALUE; //  Threshold of difference between screen height and camera supported height.
        double targetRatio = 0;
        double ratio;
        Camera.Size optimalSize = null;

        // check if the orientation is portrait or landscape to change aspect ratio.
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            targetRatio = (double) h / (double) w;
        } else if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            targetRatio = (double) w / (double) h;
        }

        // loop through all supported preview sizes to set best display ratio.
        for (Camera.Size s : params.getSupportedPreviewSizes()) {

            ratio = (double) s.width / (double) s.height;
            if (Math.abs(ratio - targetRatio) <= ASPECT_TH) {

                if (Math.abs(h - s.height) < minDiff) {
                    optimalSize = s;
                    minDiff = Math.abs(h - s.height);
                }
            }
        }
        Log.e("optimalsize", String.format("height -> %s - width -> %s", optimalSize.height, optimalSize.width));
        return optimalSize;
    }

    @Override
    public void onPreviewFrame(final byte[] data, final Camera camera) {
        if (hook || executor.isShutdown()) return;
        try {
            Observable
                    .just(camera.getParameters().getPreviewSize())
                    .subscribeOn(Schedulers.from(executor))
                    .map(new Func1<Camera.Size, QRResult>() {
                        @Override
                        public QRResult call(Camera.Size size) {
                            return getCodeValue(data, new Point(size.width, size.height));
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<QRResult>() {
                        @Override
                        public void call(QRResult qrResult) {
                            if (qrResult == null) {
                                count++;
                                startCapture();
                                return;
                            }
                            QRUtils.vibrate(context);
                            QRUtils.beep(context);
                            if (onResultListener != null) {
                                onResultListener.onResult(qrResult);
                            }
                            count = 0;
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.e("CameraManager", "getCodeValue() failed .");
                        }
                    });
        }catch (Exception e){
            Log.e("CameraManager", "Camera was closed");
        }

    }

    @Override
    public void setCameraFocus(Camera.AutoFocusCallback autoFocus) {
        if (camera != null) {
            try {
                camera.autoFocus(autoFocus);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
