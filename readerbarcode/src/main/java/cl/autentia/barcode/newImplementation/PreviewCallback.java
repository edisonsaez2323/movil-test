package cl.autentia.barcode.newImplementation;

public interface PreviewCallback {
    void onPreview(SourceData sourceData);
    void onPreviewError(Exception e);
}
