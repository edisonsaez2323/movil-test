package cl.autentia.barcode.newImplementation;

import android.hardware.Camera;

public interface CameraParametersCallback {

    /**
     * Changes the settings for Camera.
     *
     * @param parameters {@link Camera.Parameters}.
     * @return {@link Camera.Parameters} with arguments.
     */
    Camera.Parameters changeCameraParameters(Camera.Parameters parameters);
}
