package cl.autentia.common;

/**
 * Created by marcin on 3/13/17.
 */
public interface Status {
    String OK = "OK";
    String ERROR = "ERROR";
    String CANCELADO = "CANCELADO";
    String PENDIENTE = "PENDIENTE";
    String NO_OK = "NO_OK";
}
