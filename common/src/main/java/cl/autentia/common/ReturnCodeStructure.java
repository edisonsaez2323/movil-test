package cl.autentia.common;

public interface ReturnCodeStructure {

    int getCode();

    String getDescription();

}
