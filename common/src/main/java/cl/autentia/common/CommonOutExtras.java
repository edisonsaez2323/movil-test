package cl.autentia.common;

/**
 * Created by marcin on 3/13/17.
 */

public interface CommonOutExtras {

    String CODIGO_RESPUESTA = "CODIGO_RESPUESTA";
    String ESTADO = "ESTADO";
    String DESCRIPCION = "DESCRIPCION";
}
