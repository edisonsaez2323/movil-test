package cl.autentia.common;

public interface CommonInExtras {

    String ICON = "ICON";
    String COLOR_PRIMARY = "COLOR_PRIMARY";
    String COLOR_PRIMARY_DARK = "COLOR_PRIMARY_DARK";
    String COLOR_TITLE = "COLOR_TITLE";
    String COLOR_SUBTITLE = "COLOR_SUBTITLE";
    String TITLE = "TITLE";
    String SUBTITLE = "SUBTITLE";
}
