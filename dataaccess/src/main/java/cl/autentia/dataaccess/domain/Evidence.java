package cl.autentia.dataaccess.domain;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

import cl.autentia.dataaccess.provider.EvidenceContract;

@DatabaseTable
@AdditionalAnnotation.DefaultContentUri(authority = EvidenceContract.AUTHORITY, path = EvidenceContract.CONTENT_URI_PATH_EVIDENCE)
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = EvidenceContract.MIMETYPE_NAME, type = EvidenceContract.MIMETYPE_EVIDENCE_TYPE)
public class Evidence {

    public static final String ID = BaseColumns._ID;
    public static final String TYPE = "type";
    public static final String AUDIT = "audit";
    public static final String JSON_DATA = "jsonData";
    public static final String FINGERPRINT = "fingerprint";
    public static final String ATTACHMENTS = "attachments";
    public static final String CREATE_DATE = "createDate";

    public Evidence() {
    }

    @DatabaseField(columnName = ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    public int idVerification;

    @DatabaseField(columnName = JSON_DATA)
    public String jsonData;

    @DatabaseField(columnName = AUDIT)
    public String audit;

    @DatabaseField(columnName = TYPE)
    public String type;

    @DatabaseField(columnName = FINGERPRINT, dataType = DataType.BYTE_ARRAY)
    public byte[] fingerprint;

    @DatabaseField(columnName = ATTACHMENTS, dataType = DataType.BYTE_ARRAY)
    public byte[] attachments;

    @DatabaseField(columnName = CREATE_DATE)
    public String createDate;
}
