package cl.autentia.dataaccess.domain;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@DatabaseTable
public class Auditoria {

    public static final String ID = BaseColumns._ID;
    public static final String NRO_AUDITORIA = "numero_auditoria";

    @DatabaseField(columnName = ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    public int idAuditoria;

    @DatabaseField(columnName = NRO_AUDITORIA)
    public String numeroAuditoria;
}