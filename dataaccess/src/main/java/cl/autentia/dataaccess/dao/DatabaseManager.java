package cl.autentia.dataaccess.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;

import cl.autentia.dataaccess.domain.Auditoria;
import cl.autentia.dataaccess.domain.Evidence;

public class DatabaseManager extends OrmLiteSqliteOpenHelper {

    public static final String DATABASE_NAME = "AutentiaMovil.db";
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_FOLDER = Environment.getExternalStorageDirectory() + "/AutentiaMovil";

    private static DatabaseManager databaseIntance;

    public static synchronized DatabaseManager getConexion(Context mContext) {

        if (databaseIntance == null)
            databaseIntance = new DatabaseManager(mContext);
        return databaseIntance;
    }

    public DatabaseManager(Context oContext) {
        super(oContext, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {

        try {

            TableUtils.createTableIfNotExists(connectionSource, Auditoria.class);
            TableUtils.createTableIfNotExists(connectionSource, Evidence.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {

            switch (oldVersion) {
                case 1:


                    break;
                case 2:

                    TableUtils.createTableIfNotExists(connectionSource, Evidence.class);
                    TableUtils.dropTable(connectionSource, Auditoria.class, false);
                    TableUtils.createTableIfNotExists(connectionSource, Auditoria.class);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene la ruta de la BD.
     *
     * @return Object
     */
    private static Object getDataBaseFolder() {

        String RutaCarpeta = "";

        try {

            RutaCarpeta = String.format(DATABASE_FOLDER, Environment.getExternalStorageDirectory().getAbsolutePath());

            File dbFolder = new File(RutaCarpeta);

            if (!dbFolder.exists()) {
                dbFolder.mkdirs();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return RutaCarpeta;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=OFF;");
        }
    }
}