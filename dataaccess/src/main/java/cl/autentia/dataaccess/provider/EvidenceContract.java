package cl.autentia.dataaccess.provider;

import android.content.ContentResolver;
import android.net.Uri;


public class EvidenceContract {

    public static final String AUTHORITY = "cl.autentia.evidence";

    public static final String CONTENT_URI_PATH_EVIDENCE = "evidence";

    public static final String MIMETYPE_EVIDENCE_TYPE = "evidence";

    public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

    public static final int CONTENT_URI_PATTERN_MANY_EVIDENCE = 1;
    public static final int CONTENT_URI_PATTERN_ONE_EVIDENCE = 2;

    public static final Uri CONTENT_URI_EVIDENCE =
            new Uri.Builder()
                    .scheme(ContentResolver.SCHEME_CONTENT)
                    .authority(AUTHORITY)
                    .appendPath(CONTENT_URI_PATH_EVIDENCE)
                    .build();
}
