package cl.autentia.dataaccess.provider;

import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MimeTypeVnd.SubType;

import cl.autentia.dataaccess.dao.DatabaseManager;
import cl.autentia.dataaccess.domain.Evidence;

public class EvidenceProvider extends OrmLiteSimpleContentProvider<DatabaseManager> {


    @Override
    protected Class<DatabaseManager> getHelperClass() {
        return DatabaseManager.class;
    }

    @Override
    public boolean onCreate() {

        setMatcherController(new MatcherController()
                .add(Evidence.class, SubType.DIRECTORY, "", EvidenceContract.CONTENT_URI_PATTERN_MANY_EVIDENCE)
                .add(Evidence.class, SubType.ITEM, "#", EvidenceContract.CONTENT_URI_PATTERN_ONE_EVIDENCE));
        return true;
    }
}
